// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;
using System.Collections.Generic;

public class KLEditorTarget : TargetRules
{
	public KLEditorTarget(TargetInfo Target) : base (Target)
	{
		DefaultBuildSettings = BuildSettingsVersion.V2;
		Type = TargetType.Editor;

		// Disabling the unity build can find tricky issues with bulding and packaging
		// It forces to add includes for all used classes and other identifiers
		// bUseUnityBuild = false;
		// bUseAdaptiveUnityBuild = false;

		ExtraModuleNames.Add("KL");
	}
}
