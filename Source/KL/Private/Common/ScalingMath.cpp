// Fill out your copyright notice in the Description page of Project Settings.

#include "Common/ScalingMath.h"

float UScalingMath::GetScaledValue(float BaseValue, int32 Level, float Multiplier) {
	return BaseValue * FMath::Max(1.0f, Level * Multiplier);
}
float UScalingMath::GetDamage(float Attack, float Defense) {
	return Attack * Attack / (Attack + Defense);
}

int32 UScalingMath::GetTier(int32 Value, int32 Initial) {
	int32 N = 2 * Value / Initial; // == Tier * Tier + Tier
	return (int32)(FMath::Sqrt(N + 0.25) - 0.5) + 1;

	/*
	// Fibonacci-based tiers
	int32 Tier = 1;
	int32 Step = Initial;
	int32 Previous = Initial;

	while (Step <= Value) {
		// UE_LOG(KLDebug, Verbose, TEXT("UScalingMath::GetTier: tier == %d (step %d / value %d)"), Tier, Step, Value);
		int32 NewStep = Step + Previous;
		Previous = Step;
		Step = NewStep;

		Tier++;
	}

	return Tier;*/
}

int32 UScalingMath::GetTierValue(int32 Tier, int32 Initial) {
	// Tier == 1 should have zero value
	int32 L = Tier - 1;
	return ((L * L + L) / 2) * Initial;

	/*
	// Fibonacci-based tiers
	if (Tier == 1) {
		return 0;
	}

	int32 CurrentTier = 1;
	int32 Step = 0;
	int32 Previous = Initial;

	while (CurrentTier <= Tier) {
		int32 NewStep = Step + Previous;
		Previous = Step;
		Step = NewStep;

		CurrentTier++;
	}

	return Step;*/
}

int32 UScalingMath::GetValueToNextTier(int32 Value, int32 Initial) {
	int32 CurrentTier = GetTier(Value, Initial);
	int32 NextTierValue = GetTierValue(CurrentTier+1, Initial);

	return NextTierValue - Value;
}

float UScalingMath::GetHealthBuff(EMobType Type) {
	switch (Type) {
	case EMobType::Strong:
		return 200.f;
	case EMobType::Boss:
		return 900.f;
	default:
		return 0.f;
	}
}

float UScalingMath::GetSpeedBuff(EMobType Type) {
	switch (Type) {
	case EMobType::Fast:
		return 100.f;
	default:
		return 0.f;
	}
}

float UScalingMath::GetAttackBuff(EMobType Type) {
	switch (Type) {
	case EMobType::Boss:
		return 200.f;
	default:
		return 0.f;
	}
}

float UScalingMath::GetScoresMultiplier(EMobType Type) {
	switch (Type) {
	case EMobType::Bonus:
		return 1.3f;
	case EMobType::Strong:
		return 2.f;
	case EMobType::Fast:
		return 1.5f;
	case EMobType::Boss:
		return 5.f;
	case EMobType::Regular:
	default:
		return 1.f;
	}
}

EMobType UScalingMath::GetRandomMobType(TMap<EMobType, float> TypesWeights) {
	// The order of the elements should match the order of EMobType enum
	TArray<float> Weights; /*{
		1.f, // Regular
		0.1, // Bonus
		0.01, // Strong,
		0.01, // Fast,
		0.001 // Boss
	};*/
	TypesWeights.GenerateValueArray(Weights);

	float WeightsSum = 0.f;
	for (auto Pair : TypesWeights) {
		WeightsSum += Pair.Value;
	}

	float Random = FMath::FRandRange(0.f, WeightsSum);

	float sum = 0.f;
	for (auto Pair : TypesWeights) {
		float w = Pair.Value;

		if (w <= 0.f) {
			continue;
		}

		sum += w;
		if (sum >= Random) {
			return Pair.Key;
		}
	}

	return EMobType::Regular;
}
