// Fill out your copyright notice in the Description page of Project Settings.

#include "Common/KLGameMode.h"
#include "Player/KLPlayerController.h"
#include "Player/KLCharacter.h"
#include "GameGlobal.h"
#include "Player/PlayerCharacter.h"
#include "Common/HealthComponent.h"
#include "Formations/Formation.h"
#include "Engine/WorldComposition.h"
#include "Common/KLStatics.h"

#include "Subsystems/KLPerksSubsystem.h"
#include "Subsystems/KLScoresSubsystem.h"

#include "Common/KLLoader.h"

#include "Core/ECSEngine.h"

#include "Perks/PerkManager.h"
#include "Perks/PerkTypes.h"

const FName AKLGameMode::GroupPerksID("Group");

AKLGameMode::AKLGameMode() {
	PrimaryActorTick.bCanEverTick = true;

	// use our custom PlayerController class
	PlayerControllerClass = AKLPlayerController::StaticClass();
	DefaultPawnClass = AKLCharacter::StaticClass();
	FormationClass = AFormation::StaticClass();

	PerkManagers = TMap<FName, UPerkManager*>();

	ECSType = UECSEngine::StaticClass();
}

void AKLGameMode::InitGame(const FString& MapName, const FString& Options, FString& ErrorMessage) {
	UGameInstance* Instance = GetGameInstance();
	Global = Cast<UGameGlobal>(Instance);

	Global->GetLoader()->OnCompleted.AddDynamic(this, &AKLGameMode::OnAssetsLoaded);
	Global->SetLoaded(false);

	if (ensure(Global)) {
		UE_LOG(KLLog, Log, TEXT("AKLGameMode::InitGame: Game inited!"));
	} else {
		UE_LOG(KLError, Error, TEXT("AKLGameMode::InitGame: Game Global is missing! Stop everything."));
		return;
	}

	ECSEngine = NewObject<UECSEngine>(this, ECSType);
	check(ECSEngine);
	ECSEngine->Initialize();

	FActorSpawnParameters SpawnParameters;
	SpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

	// Spawn Formation actor
	// TODO: Convert to LocalPlayer subsystem
	// OR get rid of it and implement formations via ECS boid
	Formation = GetWorld()->SpawnActor<AFormation>(FormationClass, FVector::ZeroVector, FRotator::ZeroRotator, SpawnParameters);

	Super::InitGame(MapName, Options, ErrorMessage);
}

void AKLGameMode::InitGameState() {
	Super::InitGameState();
}

void AKLGameMode::StartPlay() {
	bIsPaused = false;

	UE_LOG(KLLog, Log, TEXT("AKLGameMode::StartPlay!"));

	Global->ResetHeroes();
	Global->SetGroupFormation(Formation);

	// Start loading soft-ptr assets
	Global->GetLoader()->Load();

	UE_LOG(KLLog, Log, TEXT("AKLGameMode::StartPlay: current level: %s vs. %s"), *GetWorld()->GetMapName(), *UKLStatics::MakeSafeLevelName(this, "Level_00"));
	bool bIsMainLevel = GetWorld()->GetMapName().Equals(UKLStatics::MakeSafeLevelName(this, "Level_00"));

	if (!bIsMainLevel) {
		StartingLevel = FName();
		OnLevelLoaded();
		return;
	}

	StartingLevel = Global->StartingLevel;

	UE_LOG(KLLog, Log, TEXT("AKLGameMode::StartPlay: starting level: %s"), *StartingLevel.ToString());

	FString SearchPackageName = UKLStatics::MakeSafeLevelName(this, StartingLevel);
	// TODO: find the level's full path withoud hardcoded location
	FString PackageName = FString::Printf(TEXT("/Game/Levels/GameMap/%s"), *SearchPackageName);
	FWorldTileInfo TileInfo = UKLStatics::GetTileInfo(this, *PackageName);

	UE_LOG(KLLog, Log, TEXT("AKLGameMode::StartPlay: level position: %s"), *TileInfo.AbsolutePosition.ToString());

	// Set the start location and update player camera
	Formation->SetMasterLocation(FVector(TileInfo.AbsolutePosition));
	APlayerController* LocalPlayerController = nullptr;
	for (FConstPlayerControllerIterator Iterator = GetWorld()->GetPlayerControllerIterator(); Iterator; ++Iterator) {
		if (APlayerController* PlayerController = Iterator->Get()) {
			PlayerController->UpdateCameraManager(0.f);
		}
	}

	// Wait for level loaded...
	// Super::StartPlay() will be called in KLStartPlay after everything loaded
	// TODO: make sure it's ok to delay the StartPlay call
	// TODO: !!! Check starting at different sub-levels with streaming distance set to a small value

	// Update World Composition levels streaming according to the new camera position
	GEngine->BlockTillLevelStreamingCompleted(GetWorld());

	UE_LOG(KLLog, Log, TEXT("AKLGameMode::StartPlay: waiting for level: %s"), *StartingLevel.ToString());

	ULevelStreaming* Level = UGameplayStatics::GetStreamingLevel(GetWorld(), StartingLevel);
	if (!ensure(Level)) {
		UE_LOG(KLLog, Error, TEXT("AKLGameMode::StartPlay: no starting level found! %s"), *StartingLevel.ToString());
		return;
	}

	if (Level->IsLevelVisible()) {
		OnLevelLoaded();
	} else {
		Level->OnLevelShown.AddDynamic(this, &AKLGameMode::OnLevelLoaded);
	}
}

void AKLGameMode::KLStartPlay() {
	Super::StartPlay();

	bool bIsGameStarted = Global->IsGameStarted();

	// TODO: move reset level stuff into KLLevel
	ResetLevel();
	SpawnHeroes();

	Global->GetSubsystem<UKLPerksSubsystem>()->UpdatePerksList();

	if (bIsGameStarted) {
		Global->SetGameStarted();
	}

	bLevelFound = true;

	Global->SetLoaded();
}

void AKLGameMode::OnAssetsLoaded(float CurrentState) {
	if (bAssetsLoaded) {
		UE_LOG(KLLog, Error, TEXT("AKLGameMode::OnAssetsLoaded: assets are already loaded, should not happen twice!"));
		return;
	}

	UE_LOG(KLLog, Log, TEXT("AKLGameMode::OnAssetsLoaded"));
	bAssetsLoaded = true;

	if (bLevelLoaded) {
		KLStartPlay();
	}
}

void AKLGameMode::OnLevelLoaded() {
	if (bLevelLoaded) {
		UE_LOG(KLLog, Error, TEXT("AKLGameMode::OnLevelLoaded: level is already loaded, should not happen twice!"));
		return;
	}
	UE_LOG(KLLog, Log, TEXT("AKLGameMode::OnLevelLoaded"));

	bLevelLoaded = true;

	if (bAssetsLoaded) {
		KLStartPlay();
	}
}

void AKLGameMode::ActivateLevel(FBox2D Bounds) {
	// ECSEngine->SetActive();
}

void AKLGameMode::DeactivateCurrentLevel() {
	// ECSEngine->SetActive(false);
}

void AKLGameMode::Tick(float DeltaSeconds) {
	Super::Tick(DeltaSeconds);

	if (!bLevelFound) {
		return;
	}

	// TODO: make a separate event for GameOver instead of per-tick check
	if (Global->IsGameOver()) {
		return;
	}

	if (IsGameOver()) {
		UE_LOG(KLLog, Log, TEXT("AKLGameMode::Tick: Game over!"));

		if (Global->IsGameStarted()) {
			// Game mode

			bIsPaused = true;
			Global->GetSubsystem<UKLScoresSubsystem>()->SaveScores(FName(TEXT("")), false);
			Global->SetGameOver();
		} else /* if (Global->IsMainMenu()) */ {
			// Main menu mode

			// Clear the level
			ResetLevel();

			// Remove all characters
			for (TActorIterator<AGameCharacter> ActorItr(GetWorld()); ActorItr; ++ActorItr) {
				AGameCharacter *Char = *ActorItr;
				Char->Destroy();
			}
			// Spawn new Heroes
			SpawnHeroes();
		}

		return;
	}

	UpdateMobsLevel();
}

bool AKLGameMode::IsPaused() const {
	return bIsPaused || Super::IsPaused();
}

void AKLGameMode::ResetLevel() {
	for (auto Pair : PerkManagers) {
		Pair.Value->Deactivate();
	}

	PerkManagers.Empty();

	Global->ResetHeroes();
	Global->Reset();

	DestroyHeroes(true);
}

void AKLGameMode::DestroyHeroes(bool bSkipDestroy) {
	if (IsValid(Formation)) {
		Formation->Reset();
	}

	Global->HeroesList.Empty();

	if (bSkipDestroy) {
		return;
	}

	for (TActorIterator<APlayerCharacter> ActorItr(GetWorld()); ActorItr; ++ActorItr) {
		APlayerCharacter *Hero = *ActorItr;
		Hero->Destroy();
	}
}

void AKLGameMode::SpawnHeroes() {
	TArray<int32> HeroesIDs = Global->Heroes;

	FVector Location;

	TActorIterator<APlayerStart> PlayerStarts(GetWorld());
	APlayerStart* CurrentStart = nullptr;

	for (PlayerStarts; PlayerStarts; ++PlayerStarts) {
		ULevel *Level = PlayerStarts->GetLevel();
		FString LevelName = Level->GetOuter()->GetName();
		if (StartingLevel.IsNone() || FName(*LevelName) == StartingLevel) {
			CurrentStart = *PlayerStarts;
			break;
		}
	}

	if (!CurrentStart) {
		UE_LOG(KLError, Error, TEXT("AKLGameMode::SpawnHeroes: Can't find a player start in starting level (%s)!"), *StartingLevel.ToString());
		return;
	}

	Global->CameraRotationAngle = CurrentStart->GetActorRotation().Yaw;

	// TODO: move into GameGlobal setting
	float HeroPositionZ = 88.f;

	Location = CurrentStart->GetActorLocation();
	Location.Z = 88.f;

	Formation->SetMasterLocation(Location);

	FRotator Rotation = FRotator(0);

	TArray<FVector> LocationAdjustments = TArray<FVector>();
	LocationAdjustments.Add(FVector(+100, -100, 0));
	LocationAdjustments.Add(FVector(+100, +100, 0));
	LocationAdjustments.Add(FVector(-100, -100, 0));
	LocationAdjustments.Add(FVector(-100, +100, 0));

	int32 HeroID = 0;

	Global->HeroesList.Empty();

	UPerkManager* GroupPerkManager = GetPerkManager(GroupPerksID);

	APawn* PlayerPawn = UGameplayStatics::GetPlayerPawn(GetWorld(), 0);
	AGameCharacter* GroupHost = Cast<AGameCharacter>(PlayerPawn);

	GroupPerkManager->Init(GroupHost);

	//FActorSpawnParameters SpawnParameters;
	//SpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;

	for (int32 ClassID : HeroesIDs) {
		TSubclassOf<APlayerCharacter> HeroClass = HeroesClasses[ClassID];
		UPerkManager* PerkManager = GetPerkManagerByClass(HeroClass, HeroID);

		FVector Adjustment = LocationAdjustments[HeroID];

		//APlayerCharacter* Hero = GetWorld()->SpawnActor<APlayerCharacter>(HeroClass, , SpawnParameters);

		FTransform SpawnTransform(Rotation, Location + Adjustment);
		APlayerCharacter* Hero = Cast<APlayerCharacter>(UGameplayStatics::BeginDeferredActorSpawnFromClass(this, HeroClass, SpawnTransform));
		if (Hero != nullptr) {
			Hero->SetHeroID(HeroID);
			UGameplayStatics::FinishSpawningActor(Hero, SpawnTransform);

			PerkManager->Init(Hero);

			Global->HeroesList.Emplace(HeroID, Hero);
			HeroID++;
		}
	}
}

void AKLGameMode::ReplaceHeroClass(int32 HeroID, TSubclassOf<APlayerCharacter> NewHeroClass) {
	APlayerCharacter* OldHero = Global->HeroesList[HeroID];

	if (!IsValid(OldHero)) {
		UE_LOG(KLLog, Error, TEXT("AKLGameMode::ReplaceHeroClass: Hero with ID %d is not valid object!"), HeroID);
		return;
	}

	auto Location = OldHero->GetActorLocation();
	auto Rotation = OldHero->GetActorRotation();
	auto HeroController = OldHero->GetController();
	HeroController->UnPossess();

	//bool bIsMaster = Formation->GetMaster() == OldHero;
	float HealthPercent = OldHero->GetHealthComponent()->GetHealthPercent();

	// Copy all modifiers from old hero attributes
	auto ModifiersHealth = OldHero->AttributeHealth->GetAllModifiers();
	auto ModifiersDefense = OldHero->AttributeDefense->GetAllModifiers();
	auto ModifiersAttack = OldHero->AttributeAttack->GetAllModifiers();
	auto ModifiersWalkSpeed = OldHero->AttributeWalkSpeed->GetAllModifiers();
	auto ModifiersSkillSpeed = OldHero->AttributeSkillSpeed->GetAllModifiers();

	Global->HeroesList.Remove(HeroID);

	FTransform SpawnTransform(Rotation, Location);
	APlayerCharacter* NewHero = Cast<APlayerCharacter>(UGameplayStatics::BeginDeferredActorSpawnFromClass(this, NewHeroClass, SpawnTransform));
	if (NewHero == nullptr) {
		UE_LOG(KLError, Error, TEXT("AKLGameMode::ReplaceHeroClass: Can't spawn a new hero for replacement!"));
		return;
	}

	NewHero->SetHeroID(HeroID);

	UGameplayStatics::FinishSpawningActor(NewHero, SpawnTransform);
	NewHero->SetDefinedHero();

	// Restore all modifiers
	NewHero->AttributeHealth->ReplaceModifiers(ModifiersHealth);
	NewHero->AttributeDefense->ReplaceModifiers(ModifiersDefense);
	NewHero->AttributeAttack->ReplaceModifiers(ModifiersAttack);
	NewHero->AttributeWalkSpeed->ReplaceModifiers(ModifiersWalkSpeed);
	NewHero->AttributeSkillSpeed->ReplaceModifiers(ModifiersSkillSpeed);

	auto HealthComponent = NewHero->GetHealthComponent();
	float NewHealth = HealthComponent->GetMaxHealth() * HealthPercent;
	HealthComponent->SetCurrentHealth(NewHealth);

	Global->HeroesList.Emplace(HeroID, NewHero);
	Formation->ClearSortedHeroes();

	UPerkManager* NewPerkManager = GetPerkManagerByClass(NewHeroClass, HeroID);
	NewPerkManager->Init(NewHero);

	if (OldHero->IsA(HeroesClasses[0])) {
		// if switching from the Undefined class
		UPerkManager* OldPerkManager = GetPerkManagerByClass(HeroesClasses[0], HeroID);

		// TODO:copy all perks to the new class
		TArray<FPerkInfo> OldPerks = OldPerkManager->GetPerksList();
		for (FPerkInfo Info : OldPerks) {
			NewPerkManager->Register(Info.Data, Info.Grade);
		}

		// And clear the undefined-class perks
		OldPerkManager->Clear();
	}

	OldHero->OnReplaced();
	OldHero->Destroy();
	HeroController->Destroy();

	Global->GetSubsystem<UKLPerksSubsystem>()->UpdatePerksList();
	OnHeroReplaced.Broadcast(HeroID);

	HealthComponent->DeathCheck();
}

bool AKLGameMode::IsGameOver() const {
	if (Global->HasResurectors()) {
		return false;
	}

	auto HeroesList = Global->HeroesList;
	for (TPair<int32, APlayerCharacter*> Hero : HeroesList) {
		if (!IsValid(Hero.Value)) {
			continue;
		}

		if (!Hero.Value->IsCharacterDead() || Hero.Value->IsReviving()) {
			return false;
		}
	}

	return true;
}

bool AKLGameMode::IsHeroesDefined() const {
	auto HeroesList = Global->HeroesList;
	for (TPair<int32, APlayerCharacter*> Hero : HeroesList) {
		if (!IsValid(Hero.Value)) {
			continue;
		}

		if (Hero.Value->IsA(HeroesClasses[0])) {
			return false;
		}
	}

	return true;
}

UECSEngine* AKLGameMode::GetECSEngine() const {
	return ECSEngine;
}

void AKLGameMode::UpdateMobsLevel() {
	float Now = GetWorld()->GetTimeSeconds();
	float UpdateTime = LastMobsUpdateTime + MobsUpdateTime;

	if (Now < UpdateTime) {
		// Do nothing
		return;
	}

	// Recalculate mobs' scale factor
	float Alpha = (Now - LastMobsUpdateTime) / MobsLevelUpTime;
	MobsScaleFactor = MobsScaleFactor + FMath::Lerp(0.f, 1.f, Alpha);

	LastMobsUpdateTime = Now;
}

float AKLGameMode::GetMobsScale() const {
	return MobsScaleFactor;
}

UPerkManager* AKLGameMode::GetPerkManagerByClass(TSubclassOf<APlayerCharacter> Type, int32 HeroID) {
	FName PerksID = Type->GetFName();
	if (Type == HeroesClasses[0]) {
		PerksID = FName(*FString::Printf(TEXT("%s%d" ), *PerksID.ToString(), HeroID));
	}

	return GetPerkManager(PerksID);
}

UPerkManager* AKLGameMode::GetPerkManagerForGroup() {
	return GetPerkManager(GroupPerksID);
}

UPerkManager* AKLGameMode::GetPerkManager(FName PerksID) {
	if (PerkManagers.Contains(PerksID)) {
		return PerkManagers.FindRef(PerksID);
	}

	UE_LOG(KLLog, Verbose, TEXT("AKLGameMode::GetPerkManager: Create new PerkManager: %s"), *PerksID.ToString());

	UPerkManager* PerkManager = NewObject<UPerkManager>(this, PerkManagerClass);
	PerkManagers.Emplace(PerksID, PerkManager);

	return PerkManager;
}
