// Fill out your copyright notice in the Description page of Project Settings.

#include "Common/Projectile.h"
#include "Common/CollisionChannels.h"
#include "Common/GameCharacter.h"
#include "Common/KLGameMode.h"
#include "Common/KLStatics.h"
#include "Common/Pickup.h"
#include "ECS/ECSComponents.h"
#include "ECS/ECSMovementComponent.h"
#include "GameGlobal.h"
#include "Mobs/MobCharacter.h"
#include "Player/KLCharacter.h"
#include "Player/PlayerCharacter.h"
#include "Skills/SkillEffect.h"
#include "Summon/SummonedCharacter.h"

#include "Core/ECSEngine.h"

#include "Kismet/KismetSystemLibrary.h"

// Sets default values
AProjectile::AProjectile() {
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootScene"));

	ECSMovementComponent = CreateDefaultSubobject<UECSMovementComponent>(FName("ECSMovementComponent"));
	ECSMovementComponent->bActivateECS = false;
	ECSMovementComponent->EnableSteering(false);
}

bool AProjectile::IsDebug() const {
	return bIsDebug;
}

// Called when the game starts or when spawned
void AProjectile::BeginPlay() {
	RegisterECS();

	if (ECSIndex == INDEX_NONE) {
		UE_LOG(KLError, Error, TEXT("AProjectile::BeginPlay: Can't register ECS: can't get ECSIndex!"));
	}

	Attack = 0;
	Super::BeginPlay();
}

// Called every frame
void AProjectile::Tick(float DeltaTime) {
	Super::Tick(DeltaTime);

	if (!Launched) {
		return;
	}

	CheckCollision();
}

bool AProjectile::CheckCollision() {
	FVector CurrentLocation = GetActorLocation();

	TArray<FHitResult> Hits;
	TArray<AActor*> ActorsToIgnore;

	float ScaledRadius = Radius * GetActorScale3D().GetMax();
	UKismetSystemLibrary::SphereTraceMulti(GetWorld(), PreviousFrameLocation, CurrentLocation, ScaledRadius, UEngineTypes::ConvertToTraceType(ECC_KL_Projectile), false, ActorsToIgnore, EDrawDebugTrace::None, Hits, true);

	for (auto Hit : Hits) {
		AActor* HitActor = Hit.GetActor();

		// TODO: Unify the ignored targets
		// Maybe add a "valid target" interface or something like that?
		// Or only allow mobs & statics?
		if (
			AffectedTargets.Contains(HitActor) ||
			HitActor->IsA(AVolume::StaticClass()) ||
			HitActor->IsA(AKLCharacter::StaticClass()) ||
			HitActor->IsA(ASkillEffect::StaticClass()) ||
			HitActor->IsA(AProjectile::StaticClass()) ||
			HitActor->IsA(bIsPlayerFire ? APlayerCharacter::StaticClass() : AMobCharacter::StaticClass()) ||
			(bIsPlayerFire && HitActor->IsA(ASummonedCharacter::StaticClass()))
		) {
			continue;
		}

		AGameCharacter* HitCharacter = Cast<AGameCharacter>(HitActor);

		if (IsValid(HitCharacter) && HitCharacter->IsCharacterDead()) {
			continue;
		}

		if (IsValid(HitActor)) {
			if (bIsDebug) {
				UE_LOG(KLDebug, Warning, TEXT("AProjectile::CheckCollision: blast on %s"), *HitActor->GetName());
			}

			AffectedTargets.Add(HitActor);
			PerformBlast(HitActor, Hit.ImpactPoint);
			return true;
		}
	}

	float Distance = (CurrentLocation - LaunchPosition).Size2D();
	if (Distance >= Range) {
		if (bIsDebug) {
			UE_LOG(KLDebug, Warning, TEXT("AProjectile::CheckCollision: missed blast"));
		}

		if (DamageType == ProjectileDamageType::Radial) {
			PerformBlast(nullptr, GetActorLocation());
		} else {
			NotifyMissed();
			OnMissed.Broadcast();
			Complete();
		}
	}

	PreviousFrameLocation = CurrentLocation;

	return false;
}

void AProjectile::LaunchProjectile(AGameCharacter* InShooter, EInstigatorType InInstigatorType, float InSpeed, float InRange, float InBaseAttack) {
	if (InShooter == nullptr || !IsValid(InShooter)) {
		UE_LOG(KLLog, Warning, TEXT("AProjectile::LaunchProjectile: No shooter or it's not valid!"));
		return;
	}

	Shooter = InShooter;
	InstigatorType = InInstigatorType;
	if (InstigatorType == EInstigatorType::Default) {
		UE_LOG(KLLog, Warning, TEXT("AProjectile::LaunchProjectile: EInstigatorType::Default should not be used yet"));
	}
	Range = InRange;
	Speed = InSpeed;

	Attack = InBaseAttack;

	bIsPlayerFire = InstigatorType == EInstigatorType::Player;
	if (bIsPlayerFire) {
		APlayerCharacter* ShooterHero = Cast<APlayerCharacter>(Shooter);
		ShooterHeroID = ShooterHero->GetHeroID();
	}

	ECSMovementComponent->SetSpeed(Speed);

	LaunchPosition = GetActorLocation();
	PreviousFrameLocation = LaunchPosition;

	Launched = true;

	NotifyLaunch();

	ECSMovementComponent->Register();
	ECSMovementComponent->MoveTo(GetActorForwardVector(), true);
}

AGameCharacter* AProjectile::GetShooter() const {
	if (IsValid(Shooter)) {
		return Shooter;
	}

	if (ShooterHeroID != INDEX_NONE) {
		auto Global = Cast<UGameGlobal>(GetGameInstance());
		return Global->GetHero(ShooterHeroID);
	}

	UE_LOG(KLError, Error, TEXT("AProjectile::GetShooter: Shooter is not valid and not a Hero!"));

	return Shooter;
}

float AProjectile::GetAttack() const {
	return Attack;
}

void AProjectile::PerformBlast(AActor* TargetActor, const FVector& HitLocation) {
	if (!bCanMakeAttack) {
		return;
	}

	NotifyBeforeHit(TargetActor, HitLocation);

	Handled = false;

	bool bDamageMade = false;
	float DamageMade = 0;
	switch (DamageType) {
	case ProjectileDamageType::Point:
		bDamageMade = BlastActor(TargetActor, DamageMade);
		if (bDamageMade) {
			NotifyHitTarget(TargetActor, HitLocation, DamageMade);
		}

		break;
	case ProjectileDamageType::Radial:
		TArray<AGameCharacter*> DamagedActors;
		bDamageMade = BlastAtLocation(HitLocation, DamageMade, DamagedActors);
		if (bDamageMade) {
			for (auto Target : DamagedActors) {
				if (IsValid(Target)) {
					// TODO: send proper damage for each affected target
					NotifyHitTarget(Target, HitLocation, 1);
				}
			}
		}
		break;
	}

	if (!bDamageMade) {
		return;
	}

	bool bCurrentPenetrate = bCanPenetrate && IsValid(TargetActor) && TargetActor->IsA(AGameCharacter::StaticClass());
	if (!bCurrentPenetrate) {
		Complete();
	}
}

bool AProjectile::BlastActor(AActor* TargetActor, float& DamageMade) {
	if (TargetActor == nullptr || !IsValid(TargetActor)) {
		return false;
	}

	DamageMade = UKLStatics::ApplyDamage(TargetActor, Attack, GetShooter()->GetCharacterInstigatorController(), InstigatorType, this);
	return true;
}

bool AProjectile::BlastAtLocation(const FVector& TargetLocation, float& DamageMade, TArray<AGameCharacter*>& OutDamagedActors) {
	TArray<AGameCharacter*> DamagedActors;
	DamageMade = UKLStatics::ApplyRadialDamageWithFalloff(this, TargetLocation, Attack, GetShooter()->GetCharacterInstigatorController(), InstigatorType, this, FalloffInnerRadius, FalloffOuterRadius, FalloffLevel, DamagedActors);
	OutDamagedActors = DamagedActors;

	return true;
}

void AProjectile::BlastHandled() {
	Handled = true;
}

void AProjectile::Complete() {
	Launched = false;
	Handled = false;
	AffectedTargets.Empty();

	NotifyBlast(true);

	if (!Handled) {
		UE_LOG(KLLog, Warning, TEXT("AProjectile::Complete: NotifyBlast not handled, destroy projectile '%s'"), *GetName());
		bCanMakeAttack = false;

		// Unregister ECS movement
		ECSMovementComponent->Unregister();

		Destroy();
	}
}

EInstigatorType AProjectile::GetInstigatorType() const {
	return InstigatorType;
}

void AProjectile::RegisterECS() {
	if (ECSIndex != INDEX_NONE) {
		return;
	}

	AKLGameMode* KLGameMode = Cast<AKLGameMode>(GetWorld()->GetAuthGameMode());
	if (!KLGameMode) {
		UE_LOG(KLError, Error, TEXT("AProjectile::RegisterECS: Can't register ECS: no KLGameMode found!"));
		return;
	}

	if (!ECS) {
		ECS = KLGameMode->GetECSEngine();

		if (!ECS) {
			UE_LOG(KLError, Error, TEXT("AProjectile::RegisterECS: Can't register ECS: no ECSEngine found!"));
			return;
		}
	}

	ECSIndex = ECS->Register();

	return;
}

void AProjectile::Destroyed() {
	if (IsValid(ECS)) {
		ECS->Unregister(ECSIndex);
	}

	ECSIndex = INDEX_NONE;

	Super::Destroyed();
}

UECSEngine* AProjectile::GetEngineBP_Implementation() const {
	return ECS;
}

int32 AProjectile::GetEntityBP_Implementation() const {
	return ECSIndex;
}
