// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Common/KLGameMode.h"
#include "KLGameModeVFX.generated.h"

/**
 *
 */
UCLASS()
class AKLGameModeVFX : public AKLGameMode {
	GENERATED_BODY()

protected:

	void KLStartPlay() override;
};
