// Fill out your copyright notice in the Description page of Project Settings.

#include "Common/KLLoader.h"
#include "AssetRegistryModule.h"
#include "NiagaraSystem.h"
#include "Skills/SkillData.h"

KL_API UKLLoader* UKLLoader::LoaderInstance = nullptr;

UKLLoader* UKLLoader::GetLoader() {
	return UKLLoader::LoaderInstance;
}

void UKLLoader::Load() {
	UE_LOG(KLLog, Log, TEXT("UKLLoader::Load: Loader is disabled (wip)"));
	OnUpdate.Broadcast(1.f);
	OnCompleted.Broadcast(1.f);
	return;

	if (bIsLoaded) {
		OnCompleted.Broadcast(1.f);
		return;
	}

	StartTime = FDateTime::UtcNow().ToUnixTimestamp();

	UE_LOG(KLLog, Log, TEXT("UKLLoader::Load 1"));
	FAssetRegistryModule& AssetRegistryModule = FModuleManager::LoadModuleChecked<FAssetRegistryModule>("AssetRegistry");

	bIsLoading = true;
	CurrentState = 0;
	Loaded = 0;
	Count = 0;

	UE_LOG(KLLog, Log, TEXT("UKLLoader::Load 2"));

	/*const UClass* NiagaraClass = UNiagaraSystem::StaticClass();
	TArray<FAssetData> NiagaraAssets;
	if (AssetRegistryModule.Get().GetAssetsByClass(NiagaraClass->GetFName(), NiagaraAssets)) {
		Assets.Append(NiagaraAssets);
	} else {
		UE_LOG(KLLog, Log, TEXT("UKLLoader::Load: can't get Niagara assets!"));
	}*/

	const UClass* SkillsClass = USkillData::StaticClass();
	TArray<FAssetData> SkillsAssets;
	if (AssetRegistryModule.Get().GetAssetsByClass(SkillsClass->GetFName(), SkillsAssets)) {
		Assets.Append(SkillsAssets);
	} else {
		UE_LOG(KLLog, Log, TEXT("UKLLoader::Load: can't get Skills assets!"));
	}

	Count = Assets.Num();
	UE_LOG(KLLog, Log, TEXT("UKLLoader::Load: count: %d"), Count);
}

float UKLLoader::GetStatus() const {
	if (bIsLoaded) {
		return 1.f;
	}

	if (!bIsLoading) {
		return 0.f;
	}

	return (float)Loaded / (float)Count;
}

UClass* UKLLoader::GetSkillEffect(const TSoftClassPtr<ASkillEffect>& SoftPtr) {
	if (!UKLLoader::LoaderInstance) {
		return nullptr;
	}

	return UKLLoader::LoaderInstance->LoadedAssets.FindRef(SoftPtr.ToString());
}

void UKLLoader::LoadAsset(FAssetData& Asset) {
	/*UObject* AssetObject = Asset.GetAsset();

	if (!AssetObject) {
		UE_LOG(KLLog, Log, TEXT("UKLLoader::LoadAsset: wrong object"));
		return;
	}

	if (AssetObject->IsA(UNiagaraSystem::StaticClass())) {
		UE_LOG(KLLog, Log, TEXT("UKLLoader::LoadAsset: niagara system: %s"), *AssetObject->GetName());

		UNiagaraSystem* NS = Cast<UNiagaraSystem>(AssetObject);
		if (!NS) {
			UE_LOG(KLLog, Log, TEXT("UKLLoader::LoadAsset: wrong NS"));
			Loaded++;
			return;
		}

#if WITH_EDITORONLY_DATA
		if (!NS->RequestCompile(false)) {
			UE_LOG(KLLog, Log, TEXT("UKLLoader::LoadAsset: already compiled"));
			Loaded++;
			return;
		}

		UE_LOG(KLLog, Log, TEXT("UKLLoader::LoadAsset: compiling..."));
		NS->OnSystemCompiled().AddUObject(this, &UKLLoader::OnSystemLoaded);
#else
		UE_LOG(KLLog, Log, TEXT("UKLLoader::LoadAsset: no editor"));
		Loaded++;
#endif
	} else if (AssetObject->IsA(USkillData::StaticClass())) {
		UE_LOG(KLLog, Log, TEXT("UKLLoader::LoadAsset: USkillData: %s"), *AssetObject->GetName());

		USkillData* SkillData = Cast<USkillData>(AssetObject);
		if (!SkillData) {
			UE_LOG(KLLog, Log, TEXT("UKLLoader::LoadAsset: wrong SkillData"));
			return;
		}

		UClass* SkillEffect = SkillData->SkillEffect.LoadSynchronous();
		LoadedAssets.Add(SkillData->SkillEffect.ToString(), SkillEffect);

		UE_LOG(KLLog, Log, TEXT("UKLLoader::LoadAsset: loaded"));
		Loaded++;
	}*/
}

void UKLLoader::Tick(float DeltaTime) {
	if (!bIsLoading) {
		return;
	}

	if (Loaded == Count) {
		// Everything is loaded

		UE_LOG(KLLog, Log, TEXT("UKLLoader::Tick: loaded in %d sec"), FDateTime::UtcNow().ToUnixTimestamp() - StartTime);

		bIsLoading = false;
		bIsLoaded = true;
		OnUpdate.Broadcast(1.f);
		OnCompleted.Broadcast(1.f);
		return;
	}

	if (CurrentState == Count) {
		// All assets are processed, waiting to load
		return;
	}

	FAssetData Asset = Assets[CurrentState];
	CurrentState++;

	UE_LOG(KLLog, Log, TEXT("UKLLoader::Tick: load next asset (%d/%d)"), CurrentState, Count);

	LoadAsset(Asset);
}

void UKLLoader::OnSystemLoaded(UNiagaraSystem* NS) {
	if (!bIsLoading) {
		UE_LOG(KLLog, Error, TEXT("UKLLoader::OnSystemLoaded: WTF, loader is not working! %s"), *NS->GetName());
		return;
	}

	UE_LOG(KLLog, Log, TEXT("UKLLoader::OnSystemLoaded: ready! %s"), *NS->GetName());
	Loaded++;

	OnUpdate.Broadcast(GetStatus());
}
