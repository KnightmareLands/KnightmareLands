// Fill out your copyright notice in the Description page of Project Settings.

#include "Common/HealthComponent.h"


// Sets default values for this component's properties
UHealthComponent::UHealthComponent() {
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;
}

// Called when the game starts
void UHealthComponent::BeginPlay() {
	Super::BeginPlay();

	if (Inited) {
		CurrentHealth = InitialHealth;
		bIsAlive = CurrentHealth > 0;
	}
}

void UHealthComponent::SetInitialHealth(float Health) {
	Inited = true;
	InitialHealth = Health;
	CurrentHealth = InitialHealth;
}

void UHealthComponent::UpdateInitialHealth(float Health) {
	float Damage = FMath::Max(0.f, InitialHealth - CurrentHealth);
	float Diff = Health - InitialHealth;

	if (Diff < 0) {
		Damage = FMath::Max(0.f, Damage + Diff);
	}

	InitialHealth = Health;
	CurrentHealth = InitialHealth - Damage;

	OnUpdate.Broadcast(Diff, nullptr, false);
}

float UHealthComponent::GetMaxHealth() const {
	return InitialHealth;
}

float UHealthComponent::GetCurrentHealth() const {
	return CurrentHealth;
}

void UHealthComponent::SetCurrentHealth(float InHealth) {
	CurrentHealth = InHealth;
}

float UHealthComponent::GetHealthPercent() const {
	return CurrentHealth / InitialHealth;
}

bool UHealthComponent::IsAlive() const {
	return bIsAlive;
}

float UHealthComponent::TakeDamage(float Amount, AActor* Causer) {
	if (!bIsAlive) {
		return 0;
	}

	auto DamageToTake = FMath::Clamp<float>(Amount, Amount, CurrentHealth);
	CurrentHealth = CurrentHealth - DamageToTake;

	OnDamaged.Broadcast(DamageToTake, Causer, CurrentHealth <= 0);
	DeathCheck();

	return DamageToTake;
}

void UHealthComponent::DeathCheck() {
	if (CurrentHealth <= 0) {
		CurrentHealth = 0;
		bIsAlive = false;
		OnDead.Broadcast(0, nullptr, true);
	}
}

float UHealthComponent::TakeHeal(float Amount, AActor* Causer) {
	if (!bIsAlive) {
		return 0;
	}

	auto HealToTake = FMath::Clamp<float>(Amount, Amount, InitialHealth - CurrentHealth);
	CurrentHealth = CurrentHealth + HealToTake;

	OnHealed.Broadcast(HealToTake, Causer, false);

	return HealToTake;
}

void UHealthComponent::Revive(float Health, AActor* Causer) {
	if (bIsAlive) {
		return;
	}

	CurrentHealth = FMath::Clamp<float>(Health, 1, InitialHealth);
	bIsAlive = true;
	OnRevive.Broadcast(CurrentHealth, Causer, false);
}
