// Fill out your copyright notice in the Description page of Project Settings.

#include "Common/GameCharacter.h"
#include "Common/HealthComponent.h"
#include "Common/KLGameMode.h"
#include "Common/KLStatics.h"
#include "Common/ScalingMath.h"
#include "ECS/ECSCollisionComponent.h"
#include "ECS/ECSComponents.h"
#include "ECS/ECSMovementComponent.h"
#include "ECS/ECSObjectComponent.h"
#include "GameGlobal.h"
#include "Player/KLPlayerController.h"
#include "Player/PlayerCharacter.h"
#include "Skills/SkillLauncher.h"
#include "Skills/SkillsComponent.h"
#include "Skills/StatusManagerComponent.h"

// NorlinECS
#include "Core/ECSEngine.h"

// SpawnWaves
#include "SWDeathDelegate.h"

// UE4
#include "Components/WidgetComponent.h"
#include "Kismet/KismetSystemLibrary.h"

FName AGameCharacter::CapsuleComponentName(TEXT("CollisionCylinder"));
FName AGameCharacter::MeshComponentName(TEXT("CharacterMesh0"));

// Sets default values
AGameCharacter::AGameCharacter() {
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	CapsuleComponent = CreateDefaultSubobject<UCapsuleComponent>(AGameCharacter::CapsuleComponentName);
	CapsuleComponent->InitCapsuleSize(34.0f, 88.0f);
	CapsuleComponent->SetCollisionProfileName(FName("GameCharacter"));

	CapsuleComponent->CanCharacterStepUpOn = ECB_No;
	CapsuleComponent->SetShouldUpdatePhysicsVolume(false);
	CapsuleComponent->SetGenerateOverlapEvents(false);
	CapsuleComponent->SetCanEverAffectNavigation(false);
	CapsuleComponent->bDynamicObstacle = false;
	SetRootComponent(CapsuleComponent);

	Mesh = CreateOptionalDefaultSubobject<USkeletalMeshComponent>(AGameCharacter::MeshComponentName);
	if (Mesh) {
		Mesh->AlwaysLoadOnClient = true;
		Mesh->AlwaysLoadOnServer = true;
		Mesh->bOwnerNoSee = false;
		Mesh->VisibilityBasedAnimTickOption = EVisibilityBasedAnimTickOption::AlwaysTickPose;
		Mesh->bCastDynamicShadow = true;
		Mesh->bAffectDynamicIndirectLighting = true;
		Mesh->PrimaryComponentTick.TickGroup = TG_PrePhysics;
		Mesh->SetupAttachment(CapsuleComponent);
		Mesh->SetCollisionProfileName(UCollisionProfile::NoCollision_ProfileName);
		Mesh->SetGenerateOverlapEvents(false);
		Mesh->SetCanEverAffectNavigation(false);
	}

	HealthComponent = CreateDefaultSubobject<UHealthComponent>(FName("HealthComponent"));

	SkillsComponent = CreateDefaultSubobject<USkillsComponent>(FName("SkillsCimponent"));
	StatusManagerComponent = CreateDefaultSubobject<UStatusManagerComponent>(FName("StatusManagerComponent"));

	ECSMovementComponent = CreateDefaultSubobject<UECSMovementComponent>(FName("ECSMovementComponent"));

	ECSCollisionComponent = CreateDefaultSubobject<UECSCollisionComponent>(FName("ECSCollisionComponent"));
	ECSCollisionComponent->bActivateECS = true;
	ECSCollisionComponent->bGenerateOverlaps = true;

	ECSObjectComponent = CreateDefaultSubobject<UECSObjectComponent>(FName("ECSObjectComponent"));
	ECSObjectComponent->SetupAttachment(RootComponent);

	DeathDelegate = CreateDefaultSubobject<USWDeathDelegate>(FName("DeathDelegate"));

	AttributeHealth = CreateDefaultSubobject<UKLAttribute>(FName("AttributeHealth"));
	AttributeDefense = CreateDefaultSubobject<UKLAttribute>(FName("AttributeDefense"));
	AttributeAttack = CreateDefaultSubobject<UKLAttribute>(FName("AttributeAttack"));
	AttributeWalkSpeed = CreateDefaultSubobject<UKLAttribute>(FName("AttributeWalkSpeed"));
	AttributeSkillSpeed = CreateDefaultSubobject<UKLAttribute>(FName("AttributeSkillSpeed"));

	SummonedCharacters = TArray<AGameCharacter*>();
}

bool AGameCharacter::IsDebug() const {
	return bIsDebug;
}

bool AGameCharacter::IsDebugForbidAttack() const {
	return bDebugForbidAttack;
}

void AGameCharacter::PreInitializeComponents() {
	Super::PreInitializeComponents();
}

void AGameCharacter::PostInitializeComponents() {
	Super::PostInitializeComponents();

	// Avoid possible crash
	if (IsPendingKillPending()) {
		return;
	}

	AttributeHealth->Set(BaseHealth);
	AttributeDefense->Set(BaseDefense);
	AttributeAttack->Set(BaseAttack);
	AttributeWalkSpeed->Set(BaseWalkSpeed);
	AttributeSkillSpeed->Set(BaseSkillSpeed);

	HealthComponent->SetInitialHealth(AttributeHealth->Get());

	AttributeWalkSpeed->OnChanged.AddDynamic(this, &AGameCharacter::OnSpeedUpdate);
	AttributeHealth->OnChanged.AddDynamic(this, &AGameCharacter::OnHealthUpdate);

	//SelectionDecal->SetDecalMaterial(SelectionMaterial);

	InitializeAttributes(true);
}

void AGameCharacter::OnHealthUpdate(float NewBaseHealth) {
	HealthComponent->UpdateInitialHealth(NewBaseHealth);
}

AController* AGameCharacter::GetCharacterController() const {
	return GetController();
}

AController* AGameCharacter::GetCharacterInstigatorController() const {
	AGameCharacter* CurrentInstigator = Cast<AGameCharacter>(GetInstigator());
	if (IsValid(CurrentInstigator)) {
		return CurrentInstigator->GetCharacterController();
	}

	return GetCharacterController();
}

UGameGlobal* AGameCharacter::GetGameGlobal() const {
	UGameInstance* Instance = GetGameInstance();
	return Cast<UGameGlobal>(Instance);
}

void AGameCharacter::InitializeAttributes(bool bInit) {
	// The method is called on init and on each level up
	// Implemented for Mobs scaling
}

void AGameCharacter::Hover() {
	if (IsCharacterDead()) {
		return;
	}

	// SelectionDecal->SetVisibility(true);
	OnHover();
}

void AGameCharacter::Unhover() {
	if (IsCharacterDead()) {
		return;
	}

	// SelectionDecal->SetVisibility(false);
	OnUnhover();
}

void AGameCharacter::RegisterECS() {
	if (ECSIndex != INDEX_NONE) {
		return;
	}

	AKLGameMode* KLGameMode = Cast<AKLGameMode>(GetWorld()->GetAuthGameMode());
	if (!KLGameMode) {
		UE_LOG(KLError, Error, TEXT("AGameCharacter::RegisterECS: Can't register ECS: no KLGameMode found!"));
		return;
	}

	if (!ECS) {
		ECS = KLGameMode->GetECSEngine();

		if (!ECS) {
			UE_LOG(KLError, Error, TEXT("AGameCharacter::RegisterECS: Can't register ECS: no ECSEngine found!"));
			return;
		}
	}

	ECSIndex = ECS->Register();

	return;
}

// Called when the game starts or when spawned
void AGameCharacter::BeginPlay() {
	DetectionDistance = GetGameGlobal()->GetDetectionDistance();

	RegisterECS();

	if (ECSIndex == INDEX_NONE) {
		UE_LOG(KLError, Error, TEXT("AGameCharacter::BeginPlay: Can't register ECS: can't get ECSIndex!"));
	}

	FECSDelegate& DataDelegate = ECS->Get<FECSDelegate>(ECSIndex);
	DataDelegate.Owner = this;

	Super::BeginPlay();

	UpdateSpeed();

	HealthComponent->OnDead.AddDynamic(this, &AGameCharacter::Death);
}

void AGameCharacter::SetLevel(int32 InLevel) {
	if (IsCharacterDead()) {
		return;
	}

	if (InLevel > Level) {
		Level = InLevel;
		InitializeAttributes();

		OnLevelUp(Level);
	}
}

void AGameCharacter::Death(float DamageDealt, AActor* DamageCauser, bool bLethalDamage) {
	StopMovement();

	TimeOfDeath = GetWorld()->GetTimeSeconds();

	OnCharacterDead.Broadcast(this);
	OnCharacterDeadEvent.Broadcast();
	OnDeath(DamageCauser);

	FECSDataObject& DataObject = ECS->Get<FECSDataObject>(ECSIndex);
	DataObject.bIsDead = true;

	ECS->UpdateEntity(ECSIndex);

	DeathDelegate->Die();
}

void AGameCharacter::Destroyed() {
	if (SpeedTimerHandle.IsValid()) {
		GetWorld()->GetTimerManager().ClearTimer(SpeedTimerHandle);
		SpeedTimerHandle.Invalidate();
	}

	AttributeWalkSpeed->OnChanged.RemoveDynamic(this, &AGameCharacter::OnSpeedUpdate);

	if (IsValid(ECS)) {
		ECS->Unregister(ECSIndex);
		ECSIndex = INDEX_NONE;
	}

	// TODO: make this stuff clear - why do I need to manually unpossess?
	auto CharacterController = GetController();
	if (IsValid(CharacterController)) {
		CharacterController->UnPossess();
	}

	Super::Destroyed();
}

void AGameCharacter::BeginRevive() {
	if (!IsCharacterDead() || bIsCharacterReviving) {
		return;
	}

	bIsCharacterReviving = true;
	OnReviveBegin();
}

bool AGameCharacter::IsReviving() const {
	return bIsCharacterReviving;
}

void AGameCharacter::Revive(float Health, AActor* Causer) {
	HealthComponent->Revive(Health, Causer);

	if (IsCharacterDead()) {
		return;
	}

	TimeOfDeath = 0;
	bIsCharacterReviving = false;
	ECSMovementComponent->StartMovement();
	OnCharacterRevive.Broadcast(this);

	FECSDataObject& DataObject = ECS->Get<FECSDataObject>(ECSIndex);
	DataObject.bIsDead = false;

	ECS->UpdateEntity(ECSIndex);

	OnRevive();
}

// Called to bind functionality to input
void AGameCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent) {
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

UHealthComponent* AGameCharacter::GetHealthComponent() const {
	return HealthComponent;
}

bool AGameCharacter::IsCharacterMoving() const {
	return GetVelocity().Size() > 0;
}

bool AGameCharacter::IsCharacterBusy() const {
	return bBusy;
}

float AGameCharacter::GetMaxHealth() const {
	return HealthComponent->GetMaxHealth();
}

float AGameCharacter::GetCurrentHealth() const {
	return HealthComponent->GetCurrentHealth();
}

float AGameCharacter::GetDefense() const {
	return AttributeDefense->Get();
}

float AGameCharacter::GetSpeed() const {
	return AttributeWalkSpeed->Get();
}

float AGameCharacter::GetCurrentSpeed() const {
	return ECSMovementComponent->GetCurrentSpeed();
}

float AGameCharacter::TakeDamage(float Damage, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser) {
	bool bInvulnerable = StatusManagerComponent->IsStatusExist(EStatusType::Invulnerability);

	float TakenDamage;
	if (bInvulnerable) {
		TakenDamage = 0.f;
	} else {
		UDamageType const* const DamageTypeCDO = DamageEvent.DamageTypeClass ? DamageEvent.DamageTypeClass->GetDefaultObject<UDamageType>() : GetDefault<UDamageType>();

		// TODO: get rid of Super::TakeDamage, exact damage should be calculated in KLStatics
		float ActualDamage = Super::TakeDamage(Damage, DamageEvent, EventInstigator, DamageCauser);
		float ProtectedDamage = UScalingMath::GetDamage(ActualDamage, GetDefense());
		TakenDamage = HealthComponent->TakeDamage(ProtectedDamage, DamageCauser);
	}

	auto PC = Cast<AKLPlayerController>(EventInstigator);
	if (IsValid(PC)) {
		PC->OnCauseDamage(TakenDamage, this);
	}

	// TODO: should it still be slowed down if it's invulnerable?
	if (!IsCharacterDead()) {
		auto GameGlobal = GetGameGlobal();
		auto& TimerManager = GetWorld()->GetTimerManager();

		if (SpeedTimerHandle.IsValid()) {
			TimerManager.ClearTimer(SpeedTimerHandle);
		} else {
			SpeedDamageDebuff = AttributeWalkSpeed->Add(EKLModifierType::Percent, GameGlobal->DamagedSpeedFactor, EKLModifierPass::Final);
		}

		FTimerDelegate Delegate = FTimerDelegate::CreateUObject(this, &AGameCharacter::RestoreSpeed);
		TimerManager.SetTimer(SpeedTimerHandle, Delegate, GameGlobal->DamagedSpeedTime, false);
	}

	return TakenDamage;
}

float AGameCharacter::TakeHeal(float Amount, AController* EventInstigator, AActor* Causer) {
	float TakenHeal = HealthComponent->TakeHeal(Amount, Causer);

	if (TakenHeal != 0.f) {
		ReceiveAnyHealing(TakenHeal, EventInstigator, Causer);

		auto PC = Cast<AKLPlayerController>(EventInstigator);
		if (IsValid(PC)) {
			PC->OnCauseHeal(TakenHeal, this);
		}
	}

	return TakenHeal;
}

AGameCharacter* AGameCharacter::GetClosestCharacterOfClass(TSubclassOf<AGameCharacter> CharacterClass, float MaxDistance) const {
	TArray<AActor*> CharactersList;

	TSubclassOf<AGameCharacter> ClassToFind;
	if (CharacterClass == nullptr) {
		ClassToFind = AGameCharacter::StaticClass();
	} else {
		ClassToFind = CharacterClass;
	}

	if (MaxDistance < 0) {
		MaxDistance = DetectionDistance;
	}

	FVector Location = GetActorLocation();
	TArray<TEnumAsByte<EObjectTypeQuery>> ObjectTypes;
	TArray<AActor*> ActorsToIgnore;
	UKismetSystemLibrary::SphereOverlapActors(GetWorld(), Location, MaxDistance, ObjectTypes, ClassToFind, ActorsToIgnore, CharactersList);

	if (CharactersList.Num() == 0) {
		return nullptr;
	}

	float ClosestDistance = DetectionDistance;
	AGameCharacter* ClosestCharacter = nullptr;

	for (AActor* Actor : CharactersList) {
		AGameCharacter* GameCharacter = Cast<AGameCharacter>(Actor);
		if (GameCharacter->IsCharacterDead()) {
			continue;
		}

		float Distance = FVector::Dist(Actor->GetActorLocation(), Location);

		if (Distance < ClosestDistance) {
			ClosestDistance = Distance;
			ClosestCharacter = GameCharacter;
		}
	}

	return ClosestCharacter;
}

bool AGameCharacter::IsCharacterDead() const {
	// Count destroyed AActors as "dead" in-game
	return IsPendingKillPending() || !HealthComponent->IsAlive();
}

int32 AGameCharacter::GetLevel() const {
	return Level;
}

void AGameCharacter::StartBusy() {
	bBusy = true;

	if (ECSIndex == INDEX_NONE) {
		return;
	}

	FECSDataObject& DataObject = ECS->Get<FECSDataObject>(ECSIndex);
	DataObject.bIsBusy = true;
}

void AGameCharacter::EndBusy() {
	FreeRotation();

	if (bStunned) {
		return;
	}

	bBusy = false;

	if (ECSIndex == INDEX_NONE) {
		return;
	}

	FECSDataObject& DataObject = ECS->Get<FECSDataObject>(ECSIndex);
	DataObject.bIsBusy = false;
}

void AGameCharacter::HoldRotation() {
	bHoldRotation = true;
}

void AGameCharacter::FreeRotation() {
	bHoldRotation = false;
}

bool AGameCharacter::IsHoldRotation() const {
	return bHoldRotation;
}

TArray<USkillLauncher*> AGameCharacter::GetSkillsList() const {
	return SkillsComponent->GetSkillsList();
}

USkillLauncher* AGameCharacter::GetValidSkill(int32 SkillIndex) const {
	auto Skills = GetSkillsList();

	if (SkillIndex >= Skills.Num()) {
		return nullptr;
	}

	auto Skill = Skills[SkillIndex];

	if (!ensure(Skill) || !IsValid(Skill)) {
		return nullptr;
	}

	return Skill;
}

int32 AGameCharacter::GetAutoSkillIndex() const {
	return ActiveSkillIndex;
}

void AGameCharacter::SetStun(bool bIsStunned) {
	if (!bIsStunned && (StatusManagerComponent->IsStatusExist(EStatusType::Stun) || StatusManagerComponent->IsStatusExist(EStatusType::Push))) {
		// Don't remove the stun state if a Stun status is active
		return;
	}

	bStunned = bIsStunned;

	//auto MovementComponent = GetCharacterMovement();
	if (bStunned) {
		ECSMovementComponent->PauseMovement();
		StartBusy();
	} else {
		EndBusy();
		ECSMovementComponent->StartMovement();
	}
}

bool AGameCharacter::IsStunned() {
	return bStunned;
}

void AGameCharacter::OnReplaced() {
	ECSMovementComponent->Unregister();
	ECSObjectComponent->Unregister();
	ECS->Unregister(ECSIndex);
	ECSIndex = INDEX_NONE;

	OnCharacterReplaced.Broadcast();
	OnReplacedEvent();
}

void AGameCharacter::StopMovement() {
	ECSMovementComponent->StopMovement();
}

float AGameCharacter::GetDetectDistance() const {
	return DetectDistance;
}

UECSEngine* AGameCharacter::GetEngineBP_Implementation() const {
	return ECS;
}

int32 AGameCharacter::GetEntityBP_Implementation() const {
	return ECSIndex;
}

UECSMovementComponent* AGameCharacter::GetECSMovement() const {
	return ECSMovementComponent;
}

UECSCollisionComponent* AGameCharacter::GetECSCollision() const {
	return ECSCollisionComponent;
}

FTimerHandle SpeedTimerHandle;

void AGameCharacter::RestoreSpeed() {
	if (SpeedTimerHandle.IsValid()) {
		GetWorld()->GetTimerManager().ClearTimer(SpeedTimerHandle);
	}

	if (SpeedDamageDebuff != INDEX_NONE) {
		AttributeWalkSpeed->Remove(SpeedDamageDebuff);
		SpeedDamageDebuff = INDEX_NONE;
	}
}

void AGameCharacter::UpdateSpeed() {
	ECSMovementComponent->SetSpeed(AttributeWalkSpeed->Get());
}

void AGameCharacter::RegisterSummonedCharacter(AGameCharacter* SummonedCharacter) {
	SummonedCharacters.Add(SummonedCharacter);
}

void AGameCharacter::KillSummons() {
	for (auto Summon : SummonedCharacters) {
		if (!IsValid(Summon) || Summon->IsCharacterDead()) {
			continue;
		}

		UKLStatics::ApplyDamage(Summon, 999999.f, nullptr, EInstigatorType::Default, nullptr);
	}

	SummonedCharacters.Empty();
}

void AGameCharacter::SetActorLocationECS(const FVector& InLocation) {
	SetActorLocation(InLocation);
	ECSMovementComponent->UpdatePosition();
}
