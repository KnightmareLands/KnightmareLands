// Fill out your copyright notice in the Description page of Project Settings.

#include "Common/Pickup.h"
#include "Common/KLGameMode.h"
#include "ECS/ECSCollisionComponent.h"
#include "Player/PlayerCharacter.h"

#include "Core/ECSEngine.h"

APickup::APickup() {
	PrimaryActorTick.bCanEverTick = true;

	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootScene"));

	ECSCollisionComponent = CreateDefaultSubobject<UECSCollisionComponent>(FName("ECSCollisionComponent"));
	ECSCollisionComponent->SetRadius(30.f);
	ECSCollisionComponent->bGenerateOverlaps = true;
}

void APickup::BeginPlay() {
	RegisterECS();

	Super::BeginPlay();

	ECSCollisionComponent->OnECSOverlapBegin.AddDynamic(this, &APickup::OnOverlapBegin);
	StartTime = GetWorld()->GetTimeSeconds();
}

void APickup::RegisterECS() {
	if (ECSIndex != INDEX_NONE) {
		return;
	}

	AKLGameMode* KLGameMode = Cast<AKLGameMode>(GetWorld()->GetAuthGameMode());
	if (!KLGameMode) {
		UE_LOG(KLError, Error, TEXT("APickup::RegisterECS: Can't register ECS: no KLGameMode found!"));
		return;
	}

	if (!ECS) {
		ECS = KLGameMode->GetECSEngine();

		if (!ECS) {
			UE_LOG(KLError, Error, TEXT("APickup::RegisterECS: Can't register ECS: no ECSEngine found!"));
			return;
		}
	}

	ECSIndex = ECS->Register();

	return;
}

void APickup::Destroyed() {
	if (IsValid(ECS)) {
		ECS->Unregister(ECSIndex);
	}

	ECSIndex = INDEX_NONE;

	Super::Destroyed();
}

UECSEngine* APickup::GetEngineBP_Implementation() const {
	return ECS;
}

int32 APickup::GetEntityBP_Implementation() const {
	return ECSIndex;
}

void APickup::Tick(float DeltaTime) {
	Super::Tick(DeltaTime);

	if (StartTime == INDEX_NONE) {
		return;
	}

	if (!bIsReady) {
		float Now = GetWorld()->GetTimeSeconds();
		if (Now - StartTime >= SpawnDuration) {
			bIsReady = true;
			StartTime = Now;
		}

		return;
	}

	if (GetTimeoutLeft() <= 0) {
		StartTime = INDEX_NONE;

		bIsHandled = false;
		NotifyDisappear();

		Disappear();
	}
}

float APickup::GetTimeoutLeft() const {
	if (!bIsReady) {
		return 1.f;
	}

	if (Duration <= 0.f) {
		return 1;
	}

	float Now = GetWorld()->GetTimeSeconds();
	return 1 - ((Now - StartTime) / Duration);
}

void APickup::OnOverlapBegin(const TArray<AActor*>& OtherActors) {
	if (bIsTaken || !bIsReady) {
		return;
	}

	APlayerCharacter* PickupHero = nullptr;
	for (auto Actor : OtherActors) {
		auto Character = Cast<APlayerCharacter>(Actor);
		if (IsValid(Character) && CanTakePickup(Character)) {
			PickupHero = Character;
			break;
		}
	}

	if (!IsValid(PickupHero)) {
		return;
	}

	bIsTaken = true;

	bIsHandled = false;
	NotifyPickup(PickupHero);

	Disappear();
}

void APickup::PickupHandled() {
	bIsHandled = true;
}

void APickup::Disappear() {
	// Unregister ECS Collisions
	ECSCollisionComponent->Unregister();

	if (!bIsHandled) {
		Destroy();
	}
}

bool APickup::CanTakePickup_Implementation(const APlayerCharacter* Hero) const {
	return IsValid(Hero);
}
