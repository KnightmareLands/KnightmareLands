// Fill out your copyright notice in the Description page of Project Settings.

#include "Common/KLStatics.h"
#include "Common/GameCharacter.h"
#include "Player/PlayerCharacter.h"
#include "Mobs/MobCharacter.h"
#include "Summon/SummonedCharacter.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Common/CollisionChannels.h"

static bool IsDamageableFrom(UPrimitiveComponent* TargetComponent, FVector const& Origin, AActor const* IgnoredActor, const TArray<AActor*>& IgnoredActors, FHitResult& OutHitResult) {
	UWorld* const World = TargetComponent->GetWorld();

	FVector const TraceEnd = TargetComponent->Bounds.Origin;
	FVector TraceStart = Origin;

	FCollisionQueryParams LineParams(SCENE_QUERY_STAT(ComponentIsVisibleFrom), true, IgnoredActor);
	LineParams.AddIgnoredActors(IgnoredActors);

	bool const bHadBlockingHit = World->LineTraceSingleByChannel(OutHitResult, TraceStart, TraceEnd, ECC_KL_Environment, LineParams);

	if (bHadBlockingHit) {
		if (OutHitResult.Component == TargetComponent) {
			return true;
		} else {

			UE_LOG(KLDebug, Verbose, TEXT("KLStatic @ IsDamageableFrom: radial damage blocking by %s"), *OutHitResult.Component->GetName());

			return false;
		}
	}

	FVector const FakeHitLoc = TargetComponent->GetComponentLocation();
	FVector const FakeHitNorm = (Origin - FakeHitLoc).GetSafeNormal();
	OutHitResult = FHitResult(TargetComponent->GetOwner(), TargetComponent, FakeHitLoc, FakeHitNorm);

	return true;
}

static bool RadialTrace(UObject* WorldContextObject, const FVector& Origin, float Radius, const TArray<AActor*> IgnoredActors, bool bAliveOnly, TMap<AGameCharacter*, TArray<FOverlapResult>>& HitsResults) {
	UWorld* World = GEngine->GetWorldFromContextObject(WorldContextObject, EGetWorldErrorMode::LogAndReturnNull);
	if (!World) {
		return false;
	}

	FCollisionQueryParams SphereParams(SCENE_QUERY_STAT(ApplyRadialEffect), false);
	SphereParams.AddIgnoredActors(IgnoredActors);

	TArray<FOverlapResult> Overlaps;
	World->OverlapMultiByObjectType(
		Overlaps,
		Origin,
		FQuat::Identity,
		FCollisionObjectQueryParams(FCollisionObjectQueryParams::InitType::AllDynamicObjects),
		FCollisionShape::MakeSphere(Radius),
		SphereParams);

	TMap<AGameCharacter*, TArray<FOverlapResult>> OverlapComponentMap;
	for (int32 Idx=0; Idx<Overlaps.Num(); ++Idx) {
		FOverlapResult const& Overlap = Overlaps[Idx];
		AActor* const OverlapActor = Overlap.GetActor();

		AGameCharacter* GameCharacter = Cast<AGameCharacter>(OverlapActor);

		if (OverlapActor &&
			Overlap.Component.IsValid() &&
			IsValid(GameCharacter) &&
			(!bAliveOnly || !GameCharacter->IsCharacterDead()))
		{
			FHitResult Hit;
			TArray<FOverlapResult>& OverlapHist = OverlapComponentMap.FindOrAdd(GameCharacter);
			OverlapHist.Add(Overlap);
		}
	}

	HitsResults = OverlapComponentMap;

	return OverlapComponentMap.Num() > 0;
}

static bool ApplyRadialEffect(UObject* WorldContextObject, const FVector& TargetLocation, float OuterRadius, AController* EventInstigator, EInstigatorType InstigatorType, AActor* EffectCauser, TMap<AGameCharacter*, TArray<FHitResult>>& HitsResults, bool bTargetFriends = false, bool bAffectSelf = false, bool bFriendlyFire = false) {
	UWorld* World = GEngine->GetWorldFromContextObject(WorldContextObject, EGetWorldErrorMode::LogAndReturnNull);
	if (!World) {
		return false;
	}

	auto PlayerClass = APlayerCharacter::StaticClass();
	auto MobClass = AMobCharacter::StaticClass();
	auto SummonClass = ASummonedCharacter::StaticClass();

	AActor* CauserHero = IsValid(EventInstigator) ? EventInstigator->GetPawn() : nullptr;
	bool bIsPlayer = InstigatorType == EInstigatorType::Player;

	TArray<AActor*> IgnoredActors;
	if (!bFriendlyFire) {
		// Find actors to ignore for Radial Effect
		// If friendly fire is allowed – ignore no one

		TArray<UClass*> ClassesToIgnore;
		if (bIsPlayer) {
			// Ignore enemies if targeting friends
			if (bTargetFriends) {
				ClassesToIgnore.Add(MobClass);
			} else {
				ClassesToIgnore.Add(PlayerClass);
				ClassesToIgnore.Add(SummonClass);
			}
		} else {
			if (bTargetFriends) {
				ClassesToIgnore.Add(PlayerClass);
				ClassesToIgnore.Add(SummonClass);
			} else {
				ClassesToIgnore.Add(MobClass);
			}
		}

		TArray<TEnumAsByte<EObjectTypeQuery>> ObjectTypes;
		for (auto FilterClass : ClassesToIgnore) {
			TArray<AActor*> FoundActors;
			UKismetSystemLibrary::SphereOverlapActors(WorldContextObject, TargetLocation, OuterRadius, ObjectTypes, FilterClass, TArray<AActor*>(), FoundActors);
			IgnoredActors.Append(FoundActors);
		}
	}

	if (!bAffectSelf && IsValid(CauserHero)) {
		IgnoredActors.Add(CauserHero);
	}

	TMap<AGameCharacter*, TArray<FOverlapResult>> OverlapsMap;
	// "true" for AlivesOnly
	if (RadialTrace(WorldContextObject, TargetLocation, OuterRadius, IgnoredActors, true, OverlapsMap)) {
		TMap<AGameCharacter*, TArray<FHitResult>> Results;

		// Loop through overlapped actors
		for (TMap<AGameCharacter*, TArray<FOverlapResult>>::TIterator It(OverlapsMap); It; ++It) {
			AGameCharacter* const GameCharacter = It.Key();

			if (GameCharacter->CanBeDamaged() && (bAffectSelf || GameCharacter != CauserHero)) {
				TArray<FOverlapResult> const& Overlaps = It.Value();

				// Loop through actor's components
				for (int32 Idx=0; Idx<Overlaps.Num(); ++Idx) {
					FOverlapResult const& Overlap = Overlaps[Idx];
					FHitResult Hit;
					if (IsDamageableFrom(Overlap.Component.Get(), TargetLocation, CauserHero, IgnoredActors, Hit)) {
						TArray<FHitResult>& HitList = Results.FindOrAdd(GameCharacter);
						HitList.Add(Hit);
					}
				}
			}
		}

		HitsResults = Results;

		return Results.Num() > 0;
	}

	return false;
}

bool UKLStatics::FindRadialTargets(UObject* WorldContextObject, const FVector& Origin, float Radius, const TArray<AActor*> IgnoredActors, bool bAliveOnly, TArray<AGameCharacter*>& OutTargets) {
	TMap<AGameCharacter*, TArray<FOverlapResult>> Overlaps;
	if (RadialTrace(WorldContextObject, Origin, Radius, IgnoredActors, bAliveOnly, Overlaps)) {
		Overlaps.GetKeys(OutTargets);

		return OutTargets.Num() > 0;
	}

	return false;
}

float UKLStatics::ApplyDamage(AActor* TargetActor, float BaseDamage, AController* EventInstigator, EInstigatorType InstigatorType, AActor* DamageCauser) {
	TSubclassOf<UDamageType> const ValidDamageTypeClass = TSubclassOf<UDamageType>(UDamageType::StaticClass());
	auto Character = Cast<AGameCharacter>(TargetActor);
	if (!IsValid(Character) || Character->IsCharacterDead()) {
		return 0.f;
	}

	return UGameplayStatics::ApplyDamage(TargetActor, BaseDamage, EventInstigator, DamageCauser, ValidDamageTypeClass);
}

float UKLStatics::ApplyRadialDamageWithFalloff(UObject* WorldContextObject, const FVector& TargetLocation, float BaseDamage, AController* EventInstigator, EInstigatorType InstigatorType, AActor* DamageCauser, float DamageInnerRadius, float DamageOuterRadius, float DamageFalloff, TArray<AGameCharacter*>& OutDamagedActors) {
	TArray<AGameCharacter*> DamagedActors;

	TMap<AGameCharacter*, TArray<FHitResult>> HitsResults;

	if (ApplyRadialEffect(WorldContextObject, TargetLocation, DamageOuterRadius, EventInstigator, InstigatorType, DamageCauser, HitsResults)) {
		TSubclassOf<UDamageType> const ValidDamageTypeClass = TSubclassOf<UDamageType>(UDamageType::StaticClass());

		FRadialDamageEvent DmgEvent;
		DmgEvent.DamageTypeClass = ValidDamageTypeClass;
		DmgEvent.Origin = TargetLocation;
		DmgEvent.Params = FRadialDamageParams(BaseDamage, 1.f, DamageInnerRadius, DamageOuterRadius, DamageFalloff);

		float DamageApplied = 0;

		// TODO: calculate falloff value for every target
		for (TMap<AGameCharacter*, TArray<FHitResult>>::TIterator It(HitsResults); It; ++It) {
			AGameCharacter* const CurrentTarget = It.Key();
			TArray<FHitResult> const& ComponentHits = It.Value();
			DmgEvent.ComponentHits = ComponentHits;
			DamagedActors.Add(CurrentTarget);

			DamageApplied += CurrentTarget->TakeDamage(BaseDamage, DmgEvent, EventInstigator, DamageCauser);
		}

		OutDamagedActors = DamagedActors;
		return DamageApplied;
	}

	OutDamagedActors = DamagedActors;
	return 0;
}

float UKLStatics::ApplyHeal(AGameCharacter* TargetActor, float BaseAmount, AController* EventInstigator, EInstigatorType InstigatorType, AActor* HealCauser) {
	return TargetActor->TakeHeal(BaseAmount, EventInstigator, HealCauser);
}

bool UKLStatics::ApplyRadialHeal(UObject* WorldContextObject, const FVector& TargetLocation, float BaseAmount, AController* EventInstigator, EInstigatorType InstigatorType, AActor* HealCauser, float InnerRadius, float OuterRadius, float Falloff) {
	bool bHealApplied = false;

	TMap<AGameCharacter*, TArray<FHitResult>> HitsResults;
	if (ApplyRadialEffect(WorldContextObject, TargetLocation, OuterRadius, EventInstigator, InstigatorType, HealCauser, HitsResults, true, true)) {

		// TODO: calculate falloff value for every target
		for (TMap<AGameCharacter*, TArray<FHitResult>>::TIterator It(HitsResults); It; ++It) {
			AGameCharacter* const CurrentTarget = It.Key();
			if (!IsValid(CurrentTarget)) {
				continue;
			}

			CurrentTarget->TakeHeal(BaseAmount, EventInstigator, HealCauser);
			// DrawDebugSphere(CurrentTarget->GetWorld(), CurrentTarget->GetActorLocation(), 50, 32, FColor(60, 255, 60), false, 0.2f);

			bHealApplied = true;
		}
	}

	return bHealApplied;
}

float UKLStatics::GetMontageSectionLength(UAnimMontage* Montage, FName SectionName) {
	if (!IsValid(Montage)) {
		return 0.f;
	}

	int32 SectionIndex = Montage->GetSectionIndex(SectionName);
	if (SectionIndex == INDEX_NONE) {
		return 0.f;
	}

	return Montage->GetSectionLength(SectionIndex);
}

float GetDistanceToClosestActor(FTargetingInfo Target, FVector FromLocation, AGameCharacter*& ClosestActor) {
	auto Actors = Target.Actors;

	if (Actors.Num() > 0) {
		float Dist = INDEX_NONE;
		for (AGameCharacter* Character : Actors) {
			if (!IsValid(Character)) {
				UE_LOG(KLError, Error, TEXT("KLStatics, GetDistanceToClosestActor: not a valid character in targets list!"));
				continue;
			}

			float NewDist = FVector::Dist(FromLocation, Character->GetActorLocation());
			if (Dist == Dist || NewDist < Dist) {
				Dist = NewDist;
				ClosestActor = Character;
			}
		}

		return Dist;
	}

	return -1;
}

float UKLStatics::GetDistanceToTarget(FTargetingInfo Target, FVector FromLocation, AGameCharacter*& ClosestActor) {
	ClosestActor = nullptr;

	switch (Target.Type) {
	case ETargetingType::TargetLocation:
		return FVector::Dist(FromLocation, Target.Location);
	case ETargetingType::TargetActor:
		return GetDistanceToClosestActor(Target, FromLocation, ClosestActor);
	case ETargetingType::NoTarget:
	case ETargetingType::TargetCustom:
		if (Target.Location != FVector::ZeroVector) {
			return FVector::Dist(FromLocation, Target.Location);
		}

		return GetDistanceToClosestActor(Target, FromLocation, ClosestActor);
	}

	return -1;
}

TArray<AGameCharacter*> UKLStatics::SortCharacters(const FSortDelegate SortDelegate, const TArray<AGameCharacter*> Characters) {
	auto ResultList = TArray<AGameCharacter*>();

	for (auto Character : Characters) {
		if (Character == nullptr || !IsValid(Character) || Character->IsCharacterDead()) {
			continue;
		}

		ResultList.Add(Character);
	}

	Algo::Sort(ResultList, [&](const AGameCharacter* Character1, const AGameCharacter* Character2) {
		bool Result = false;
		if (SortDelegate.IsBound()) {
			SortDelegate.Execute(Character1, Character2, Result);
		}

		return Result;
	});

	return ResultList;
}

void UKLStatics::KLErrorLog(FString Message) {
	UE_LOG(KLError, Error, TEXT("BP: %s"), *Message);
}

bool UKLStatics::SphereTraceSingle(UObject* WorldContextObject, const FVector Start, const FVector End, float Radius, const TArray<TEnumAsByte<ECollisionChannel>>& TraceChannels, FHitResult& OutHit, bool bFindInitialOverlaps) {
	FCollisionQueryParams Params;
	Params.bFindInitialOverlaps = bFindInitialOverlaps;

	FCollisionObjectQueryParams ObjectParams;
	for (auto Iter = TraceChannels.CreateConstIterator(); Iter; ++Iter) {
		const ECollisionChannel& Channel = (*Iter);
		if (FCollisionObjectQueryParams::IsValidObjectQuery(Channel)) {
			ObjectParams.AddObjectTypesToQuery(Channel);
		} else {
			UE_LOG(KLLog, Warning, TEXT("%d isn't valid object type"), (int32)Channel);
		}
	}

	if (ObjectParams.IsValid() == false) {
		UE_LOG(KLLog, Warning, TEXT("KLStatics::SphereTraceSingleForObjects Invalid object types"));
		return false;
	}

	bool const bHit = WorldContextObject->GetWorld()->SweepSingleByObjectType(OutHit, Start, End, FQuat::Identity, ObjectParams, FCollisionShape::MakeSphere(Radius), Params);

	return bHit;
}

bool UKLStatics::LineTraceSingle(UObject* WorldContextObject, const FVector Start, const FVector End, ECollisionChannel TraceChannel, FHitResult& OutHit, bool bFindInitialOverlaps) {
	FCollisionQueryParams Params;
	Params.bFindInitialOverlaps = bFindInitialOverlaps;

	bool const bHit = WorldContextObject->GetWorld()->LineTraceSingleByChannel(OutHit, Start, End, TraceChannel, Params);

	return bHit;
}

FString UKLStatics::MakeSafeLevelName(UObject* WorldContextObject, const FName& InLevelName) {
	// Special case for PIE, the PackageName gets mangled.
	UWorld* World = WorldContextObject->GetWorld();

	if (!World->StreamingLevelsPrefix.IsEmpty()) {
		FString PackageName = FPackageName::GetShortName(InLevelName);
		if (!PackageName.StartsWith(World->StreamingLevelsPrefix)) {
			PackageName = World->StreamingLevelsPrefix + PackageName;
		}

		if (!FPackageName::IsShortPackageName(InLevelName)) {
			PackageName = FPackageName::GetLongPackagePath(InLevelName.ToString()) + TEXT("/") + PackageName;
		}

		return PackageName;
	}

	return InLevelName.ToString();
}

static int32 FindTileIndexByName(UObject* WorldContextObject, const FName& InPackageName) {
	auto Tiles = WorldContextObject->GetWorld()->WorldComposition->GetTilesList();

	for (int32 TileIdx = 0; TileIdx < Tiles.Num(); ++TileIdx) {
		const FWorldCompositionTile& Tile = Tiles[TileIdx];

		if (Tile.PackageName == InPackageName) {
			return TileIdx;
		}

		// Check LOD names
		for (const FName& LODPackageName : Tile.LODPackageNames) {
			if (LODPackageName == InPackageName) {
				return TileIdx;
			}
		}
	}

	return INDEX_NONE;
}

static FWorldCompositionTile* FindTileByName(UObject* WorldContextObject, const FName& InPackageName) {
	int32 TileIdx = FindTileIndexByName(WorldContextObject, InPackageName);

	if (TileIdx != INDEX_NONE) {
		auto Tiles = WorldContextObject->GetWorld()->WorldComposition->GetTilesList();
		return const_cast<FWorldCompositionTile*>(&Tiles[TileIdx]);
	} else {
		return nullptr;
	}
}

FWorldTileInfo UKLStatics::GetTileInfo(UObject* WorldContextObject, const FName& InPackageName) {
	FWorldCompositionTile* Tile = FindTileByName(WorldContextObject, InPackageName);
	if (Tile) {
		return Tile->Info;
	}

	return FWorldTileInfo();
}

bool UKLStatics::IsTearingDown(UObject* WorldContextObject) {
	return WorldContextObject->GetWorld()->bIsTearingDown;
}

FString UKLStatics::TimeSecondsToString(float InSeconds) {
	// Get whole minutes
	const int32 NumMinutes = FMath::FloorToInt(InSeconds/60.f);
	// Get seconds not part of whole minutes
	const int32 NumSeconds = FMath::FloorToInt(InSeconds-(NumMinutes*60.f));

	// Create string, including leading zeroes
	return FString::Printf(TEXT("%02d:%02d"), NumMinutes, NumSeconds);
}
