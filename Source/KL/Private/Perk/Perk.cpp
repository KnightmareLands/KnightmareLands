// Fill out your copyright notice in the Description page of Project Settings.

#include "Perks/Perk.h"
#include "Perks/PerkData.h"
#include "Common/GameCharacter.h"
#include "Player/PlayerCharacter.h"
#include "Subsystems/KLStateSubsystem.h"

UClass* UPerkData::GetPerk() const {
	if (Perk.IsNull()) {
		UE_LOG(KLLog, Error, TEXT("UPerkData::GetPerk: can't load perk asset (%s)"), *GetName());
		return nullptr;
	}

	return Perk.LoadSynchronous();
}

void UPerk::Init(const UPerkData* InPerkData, int32 InGrade, AGameCharacter* InHost) {
	ensure(InPerkData);
	ensure(InGrade > 0 && InGrade <= InPerkData->MaxGrade);

	if (Grade > 0) {
		UE_LOG(KLLog, Error, TEXT("UPerk::Setup: Perk is already inited! %s"), *InPerkData->Title[0].ToString());
		return;
	}

	PerkData = InPerkData;
	Grade = InGrade;
	TriggerType = PerkData->Trigger;

	UpdateHost(InHost);
}

void UPerk::UpdateHost(AGameCharacter* InHost) {
	Host = InHost;

	if (TriggerType == EPerkTrigger::Instant) {
		Apply();
	}
}

bool UPerk::CanBeApplied_Implementation(const UKLStateSubsystem* State, const UPerkData* InPerkData, const AGameCharacter* InHost) const {
	// Additional check to override in blueprints depending on the perk's requirements
	return true;
}

void UPerk::Apply_Implementation(AGameCharacter* InTarget) {
	// Overridden in BPs
}

void UPerk::OnPreUpgrade_Implementation() {
	// Overridden in BPs
}

void UPerk::OnPostUpgrade_Implementation() {
	// Overridden in BPs
}

void UPerk::OnRemove_Implementation() {
	// Overridden in BPs
}

// Increase perk's grade
void UPerk::Upgrade() {
	if (Grade >= PerkData->MaxGrade) {
		UE_LOG(KLLog, Error, TEXT("UPerk::Upgrade: Perk is already at the max grade! %s"), *GetTitle().ToString());
		return;
	}

	OnPreUpgrade();
	Grade++;
	OnPostUpgrade();

	if (TriggerType == EPerkTrigger::Instant) {
		Apply();
	}
}

FName UPerk::GetTitle() const {
	int32 TitleIndex = FMath::Min(PerkData->Title.Num() - 1, Grade);
	if (!PerkData->Title.IsValidIndex(TitleIndex)) {
		return FName("");
	}

	return PerkData->Title[TitleIndex];
};
