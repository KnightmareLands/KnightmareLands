// Fill out your copyright notice in the Description page of Project Settings.

#include "Perks/PerkSkill.h"
#include "Perks/PerkData.h"
#include "Common/GameCharacter.h"
#include "Skills/SkillData.h"
#include "Skills/SkillLauncher.h"

void UPerkSkill::Init(const UPerkData* InPerkData, int32 InGrade, AGameCharacter* InHost) {
	ensure(InHost);
	ensure(InPerkData);

	if (InPerkData->Type != EPerkType::Character && InPerkData->Type != EPerkType::Group) {
		UE_LOG(KLLog, Error, TEXT("UPerkSkill::Init: UPerkSkill should be used only for ::Character or ::Group perks! (%s)"), *GetName());
		return;
	}

	if (InPerkData->Trigger != EPerkTrigger::Instant) {
		UE_LOG(KLLog, Warning, TEXT("UPerkSkill::Init: Trigger is not ::Instant, are you sure you want to grant skills with other perk triggers? (%s)"), *GetName());
	}

	Super::Init(InPerkData, InGrade, InHost);
}

USkillData* UPerkSkill::GetSkillData() const {
	auto SkillData = SkillDataGrades[Grade-1];
	ensure(SkillData);
	return SkillData;
}

bool UPerkSkill::SetSkill() {
	ensure(Host);
	ensure(Index >= 0);

	TArray<USkillLauncher*> SkillsList = Host->GetSkillsList();
	if (Index >= SkillsList.Num()) {
		UE_LOG(KLLog, Error, TEXT("UPerkSkill::SetSkill: can't add more skills than was defined in the Hero's SkillComponent (%s, %s)"), *Host->GetName(), *GetName());
		return false;
	}

	bIsSet = true;

	auto SkillData = GetSkillData();
	SkillsList[Index]->SetNewSkill(SkillData);

	return true;
}

bool UPerkSkill::RemoveSkill() {
	ensure(Host);

	auto SkillData = GetSkillData();
	if (Index == INDEX_NONE || SkillData == nullptr || !bIsSet) {
		return false;
	}

	TArray<USkillLauncher*> SkillsList = Host->GetSkillsList();
	if (Index >= SkillsList.Num()) {
		UE_LOG(KLLog, Error, TEXT("UPerkSkill::RemoveSkill: can't remove skill with Index == %d (%s, %s)"), Index, *Host->GetName(), *GetName());
		return false;
	}

	USkillLauncher* Launcher = SkillsList[Index];
	if (!Launcher->HasSkill(SkillData)) {
		UE_LOG(KLLog, Error, TEXT("UPerkSkill::RemoveSkill: Launcher contains wrong skill (%s vs. new %s). %s"), *Launcher->GetTitle().ToString(), *SkillData->GetName(), *GetName());
		return false;
	}

	bIsSet = false;
	Launcher->RemoveSkill();

	return true;
}

void UPerkSkill::Apply_Implementation(AGameCharacter* InTarget) {
	SetSkill();
}

void UPerkSkill::OnRemove_Implementation() {
	RemoveSkill();
}
