// Fill out your copyright notice in the Description page of Project Settings.

#include "Perks/PerkManager.h"
#include "Perks/PerkData.h"
#include "Perks/Perk.h"
#include "Perks/PerkSkill.h"
#include "Common/GameCharacter.h"
#include "Player/PlayerCharacter.h"
#include "Common/KLGameMode.h"
#include "Subsystems/KLTimeFactorSubsystem.h"
#include "Subsystems/KLStateSubsystem.h"

UPerkManager::UPerkManager() {
	Perks = TMap<FName, UPerk*>();
}

void UPerkManager::Init(AGameCharacter* InHost) {
	Host = InHost;

	if (Host == nullptr) {
		Activate();
		return;
	}

	// OnAttackDelegate = Host->OnPerformingAttack.AddUOject() // WTF? probbaly should be handled inside skills
	// OnDamageDelegate = Host->OnTakeAnyDamage.AddUObject(this, &UKLAnimation::OnDeath);

	Host->OnCharacterDeadEvent.AddDynamic(this, &UPerkManager::OnDeath);
	Host->OnCharacterReplaced.AddDynamic(this, &UPerkManager::Deactivate);

	TimeFactor = Host->GetGameInstance()->GetSubsystem<UKLTimeFactorSubsystem>();

	Activate();
}

FName UPerkManager::GetPerkID(const UPerkData* PerkData) const {
	TSubclassOf<UPerk> PerkClass = PerkData->GetPerk();
	return PerkClass->GetFName();
}

void UPerkManager::Activate() {
	for (auto PerkPair : Perks) {
		UPerk* Perk = PerkPair.Value;
		Perk->UpdateHost(Host);
	}

	bActive = true;
}

void UPerkManager::Deactivate() {
	bActive = false;

	if (!IsValid(Host)) {
		Host = nullptr;
		return;
	}

	Host->OnCharacterDeadEvent.RemoveDynamic(this, &UPerkManager::OnDeath);
	Host->OnCharacterReplaced.RemoveDynamic(this, &UPerkManager::Deactivate);
	// Host->OnPerformingAttack.Remove(OnAttackDelegate);
	// Host->OnTakeAnyDamage.Remove(OnDamageDelegate);

	Host = nullptr;
}

void UPerkManager::Tick(float DeltaTime) {
	if (!bActive) {
		return;
	}

	// Don't tick when the game is paused
	if (GetWorld()->GetAuthGameMode()->IsPaused()) {
		return;
	}

	if (Host != nullptr && Host->IsCharacterDead()) {
		return;
	}

	Timeout -= (DeltaTime * TimeFactor->GetPlayerAttackFactor());

	if (Timeout <= 0) {
		ApplyPerks(EPerkTrigger::Tick);
		Timeout = TickInterval;
	}
}

void UPerkManager::ApplyPerks(EPerkTrigger TriggerType, AGameCharacter* TriggerCharacter) {
	for (auto PerkPair : Perks) {
		UPerk* Perk = PerkPair.Value;

		if (Perk->GetTriggerType() == TriggerType) {
			Perk->Apply(TriggerCharacter);
		}
	}
}

void UPerkManager::OnDamage(AGameCharacter* InAttacker) {
	if (!bActive) {
		return;
	}

	if (Host == nullptr || Host->IsCharacterDead()) {
		return;
	}

	ApplyPerks(EPerkTrigger::Damage, InAttacker);
}

void UPerkManager::OnAttack(AGameCharacter* InTarget) {
	if (!bActive) {
		return;
	}

	if (Host == nullptr || Host->IsCharacterDead()) {
		return;
	}

	ApplyPerks(EPerkTrigger::Attack, InTarget);
}

void UPerkManager::OnDeath() {
	if (!bActive) {
		return;
	}

	if (Host == nullptr) {
		return;
	}

	ApplyPerks(EPerkTrigger::Death);
}

bool UPerkManager::RegisterInstant(UObject* WorldContextObject, const UPerkData* PerkData) {
	if (PerkData->Type != EPerkType::Instant || PerkData->Trigger != EPerkTrigger::Instant) {
		UE_LOG(KLLog, Error, TEXT("UPerkManager::RegisterInstant: perk should have Instant type!"));
		return false;
	}

	TSubclassOf<UPerk> PerkClass = PerkData->GetPerk();
	UPerk* Perk = NewObject<UPerk>(WorldContextObject, PerkClass);

	Perk->Init(PerkData, 1, nullptr);

	return true;
}

bool UPerkManager::Register(const UPerkData* PerkData, int32 Grade) {
	if (!bActive) {
		UE_LOG(KLLog, Error, TEXT("UPerkManager::Register: not active"));
		return false;
	}

	bool bIsCharacter = Host != nullptr && PerkData->Type == EPerkType::Character;
	bool bIsGroup = Host != nullptr && PerkData->Type == EPerkType::Group;

	if (!bIsCharacter && !bIsGroup) {
		UE_LOG(KLLog, Error, TEXT("UPerkManager::Register: can't register perk %s (%s), wrong Type or Host"), *PerkData->Title[0].ToString(), *PerkData->GetName());
		return false;
	}

	if (bIsCharacter && !IsAllowedForHost(PerkData, Host)) {
		return false;
	}

	UE_LOG(KLLog, Verbose, TEXT("UPerkManager::Register: %s (%s)"), *PerkData->Title[0].ToString(), *PerkData->GetName());

	FName PerkID = GetPerkID(PerkData);
	UPerk* Perk = Perks.FindRef(PerkID);
	if (Perk != nullptr) {
		if (Perk->GetGrade() >= PerkData->MaxGrade) {
			return false;
		}

		Perk->Upgrade();
		return true;
	}

	Perk = NewObject<UPerk>(this, PerkData->GetPerk());
	if (bIsGroup && Perk->IsA(UPerkSkill::StaticClass())) {
		RemoveGroupSkill();
	}

	Perk->Init(PerkData, Grade, Host);

	// Save all non-instant perks or instant with flag
	if (PerkData->Trigger != EPerkTrigger::Instant || PerkData->bSaved) {
		Perks.Emplace(PerkID, Perk);
	}

	if (Perk->GetTriggerType() == EPerkTrigger::Tick) {
		bShouldTick = true;
	}

	return true;
}

void UPerkManager::RemoveGroupSkill() {
	for(auto PerkPair = Perks.CreateIterator(); PerkPair; ++PerkPair) {
		UPerk* Perk = PerkPair->Value;

		if (Perk->IsA(UPerkSkill::StaticClass())) {
			Perk->OnRemove();
			PerkPair.RemoveCurrent();
		}
	}
}

bool UPerkManager::Remove(const UPerkData* PerkData) {
	FName PerkID = GetPerkID(PerkData);

	if (!Perks.Contains(PerkID)) {
		return false;
	}

	Perks[PerkID]->OnRemove();
	Perks.Remove(PerkID);

	return true;
}

void UPerkManager::Clear() {
	if (!bActive) {
		return;
	}

	bShouldTick = false;

	for (auto PerkPair : Perks) {
		UPerk* Perk = PerkPair.Value;
		Perk->OnRemove();
	}

	Perks.Empty();
}

int32 UPerkManager::GetPerkGrade(const UPerkData* PerkData) const {
	if (!bActive) {
		return 0;
	}

	if (!ensure(PerkData)) {
		UE_LOG(KLLog, Error, TEXT("UPerkManager::GetPerkGrade: perk data not found!"));
		return 0;
	}

	FName PerkID = GetPerkID(PerkData);
	UPerk* Perk = Perks.FindRef(PerkID);
	if (Perk != nullptr) {
		return Perk->GetGrade();
	}

	return 0;
}

TArray<FPerkInfo> UPerkManager::GetPerksList() const {
	TArray<FPerkInfo> Result;

	for (auto PerkPair : Perks) {
		UPerk* Perk = PerkPair.Value;

		FPerkInfo Info;
		Info.Title = Perk->GetTitle();
		Info.Grade = Perk->GetGrade();
		Info.Data = Perk->GetData();

		Result.Add(Info);
	}

	return Result;
}

bool UPerkManager::IsAllowedForHost(const UPerkData* PerkData, const AGameCharacter* Host) {
	ensure(PerkData);
	ensure(Host);

	if (PerkData->Type != EPerkType::Character) {
		return false;
	}

	const APlayerCharacter* HostPlayer = Cast<APlayerCharacter>(Host);
	UWorld* World = Host->GetWorld();

	if (HostPlayer) {
		AKLGameMode* KLGameMode = (AKLGameMode*)World->GetAuthGameMode();
		UPerkManager* PerkManager = KLGameMode->GetPerkManagerByClass(HostPlayer->GetClass(), HostPlayer->GetHeroID());
		if (PerkManager->GetPerkGrade(PerkData) >= PerkData->MaxGrade) {
			return false;
		}
	}

	bool bIsAllowed = false;;

	// TODO: unify all soft ptr loaders
	// Check if they need to be saved to not reload multiple times
	for (TSoftClassPtr<AGameCharacter> HostClassPtr : PerkData->AllowedHosts) {
		if (Host->IsA(HostClassPtr.LoadSynchronous())) {
			bIsAllowed = true;
			continue;
		}
	}

	if (!bIsAllowed) {
		return false;
	}

	UClass* PerkClass = PerkData->GetPerk();
	UPerk* PerkCDO = PerkClass->GetDefaultObject<UPerk>();
	ensure(PerkCDO);

	return PerkCDO->CanBeApplied(Host->GetGameInstance()->GetSubsystem<UKLStateSubsystem>(), PerkData, Host);
}
