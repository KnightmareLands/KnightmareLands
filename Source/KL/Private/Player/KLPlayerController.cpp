// Fill out your copyright notice in the Description page of Project Settings.

#include "Player/KLPlayerController.h"
#include "Runtime/Engine/Classes/Components/DecalComponent.h"
#include "Common/GameCharacter.h"
#include "Player/PlayerCharacter.h"
#include "Mobs/MobCharacter.h"
#include "Player/KLCharacter.h"
#include "GameGlobal.h"
#include "Skills/SkillLauncher.h"
#include "Formations/Formation.h"
#include "Player/HeroController.h"
#include "Common/CollisionChannels.h"
#include "UI/KLMenu.h"
#include "Subsystems/KLScoresSubsystem.h"
#include "Subsystems/KLTimeFactorSubsystem.h"

AKLPlayerController::AKLPlayerController() {
	bShowMouseCursor = true;
	DefaultMouseCursor = EMouseCursor::Crosshairs;

	MovementDirection = FVector::ZeroVector;
}

void AKLPlayerController::BeginPlay() {
	UGameInstance* Instance = GetGameInstance();
	Global = Cast<UGameGlobal>(Instance);

	KLScores = Instance->GetSubsystem<UKLScoresSubsystem>();

	Super::BeginPlay();

	if (!ensure(Global)) {
		UE_LOG(KLError, Error, TEXT("AKLPlayerController::BeginPlay: GameGlobal object not found"));
		return;
	}

	FInputModeGameAndUI InputMode;
	InputMode.SetLockMouseToViewportBehavior(EMouseLockMode::DoNotLock);
	InputMode.SetHideCursorDuringCapture(false);
	SetInputMode(InputMode);

	ZoomLevel = Global->GetZoomLevel();

	MasterSpeed = Global->GetHeroesSpeed();

	Global->OnGameStateChanged.AddDynamic(this, &AKLPlayerController::OnGameStateChanged);
	Global->OnSpeedUpdate.AddDynamic(this, &AKLPlayerController::OnSpeedUpdate);

	// Wait for the streaming level
	Global->OnLevelLoaded.AddDynamic(this, &AKLPlayerController::OnLevelLoaded);
}

void AKLPlayerController::OnLevelLoaded(EKLGameState NewState, bool bLoaded) {
	if (!bLoaded) {
		return;
	}

	if (NewState == EKLGameState::InGame) {
		StartGame();
	} else {
		ShowMenuWidget(MenuWidgetClass, false);
	}
}

void AKLPlayerController::StartGame() {
	Global->GetSubsystem<UKLTimeFactorSubsystem>()->OnFadeComplete.AddDynamic(this, &AKLPlayerController::OnTimeFadeComplete);

	SetHeroesControls(Global->SchemeID);

	KLScores->OnLevelUp.AddDynamic(this, &AKLPlayerController::OnLevelUp);

	GameplayUI = CreateWidget<UUserWidget>(this, UIWidgetClass);
	if (IsValid(GameplayUI)) {
		GameplayUI->AddToViewport(9000);
	}

	ResumeGame();
}

void AKLPlayerController::OnTimeFadeComplete(EKLDilationState State) {
	if (State == EKLDilationState::Pause) {
		ShowCurrentMenu();
		PauseGame();
	}
}

void AKLPlayerController::ShowCurrentMenu() {
	if (CurrentMenuWidget) {
		bMenuFade = false;
		bMenuMode = true;
		CurrentMenuWidget->AddToViewport(9999);
	}
}

void AKLPlayerController::ShowMenuWidget(TSubclassOf<UKLMenu> MenuClass, bool bHasFade) {
	// TODO: implement multiplayer
	if (GetInputIndex() != 0) {
		return;
	}

	if (bMenuFade) {
		return;
	}

	EndCameraMode();

	UKLMenu* MenuWidget = CreateWidget<UKLMenu>(this, MenuClass);
	if (IsValid(MenuWidget)) {
		CurrentMenuWidget = MenuWidget;

		if (bHasFade) {
			bMenuFade = true;
			Global->GetSubsystem<UKLTimeFactorSubsystem>()->FadeToPause();
		} else {
			bMenuMode = true;
			ShowCurrentMenu();
		}
	}
}

void AKLPlayerController::EndMenuMode() {
	bMenuMode = false;
	CurrentMenuWidget = nullptr;
}

void AKLPlayerController::ShowMainMenu() {
	if (bMenuMode) {
		OnCancel();
		return;
	}

	ShowMenuWidget(MenuWidgetClass);
}

void AKLPlayerController::ShowLevelUpMenu() {
	if (bMenuMode) {
		return;
	}

	bHasSkillPoints = KLScores->GetSkillPoints() > 0;

	if (!bHasSkillPoints) {
		return;
	}

	ShowMenuWidget(LevelUpMenuClass);
}

void AKLPlayerController::ShowReplaceMenu() {
	if (bMenuMode) {
		OnCancel();
		return;
	}

	ShowMenuWidget(HeroReplaceMenuClass);
}

TMap<int32, APlayerCharacter*> AKLPlayerController::GetHeroesList() const {
	return Global->HeroesList;
}

APlayerCharacter* AKLPlayerController::GetHero(int32 HeroID) const {
	return Global->GetHero(HeroID);
}

TArray<AActor*> AKLPlayerController::GetHeroesActors() const {
	auto HeroesList = GetHeroesList();

	TArray<AActor*> ActorsList;
	for (TPair<int32, APlayerCharacter*> Hero : HeroesList) {
		if (Hero.Value == nullptr || !IsValid(Hero.Value)) {
			continue;
		}

		AActor* Actor = Cast<AActor>(Hero.Value);
		ActorsList.Add(Actor);
	}

	return ActorsList;
}

void AKLPlayerController::TriggerPause() {
	SetPause(!IsPaused());
}

void AKLPlayerController::PauseGame() {
	SetPause(true);
}

void AKLPlayerController::ResumeGame() {
	EndMenuMode();

	SetPause(false);
	Global->GetSubsystem<UKLTimeFactorSubsystem>()->FadeToGame();
}

void AKLPlayerController::PlayerTick(float DeltaTime) {
	Super::PlayerTick(DeltaTime);

	if (!Global || Global->IsLoading() || Global->IsGameOver()) {
		return;
	}

	if (IsPaused()) {
		if (bMenuMode) {
			UpdateMovementDirection(DeltaTime);
		}

		return;
	}

	// TODO: move the timer to Scores Subsystem
	float StreakDelay = GetWorld()->GetTimeSeconds() - LastKillTime;
	bool bKillStreak = StreakDelay < ComboDelay;
	if (!bKillStreak && MultiplierID != INDEX_NONE) {
		KLScores->Multiplier->Remove(MultiplierID);
		MultiplierID = INDEX_NONE;
		StreakLength = 0;
	}

	bHasSkillPoints = KLScores->GetSkillPoints() > 0;

	if (bCameraMode) {
		UpdateCameraMode();
	}

	// Update movement, rotation and skills usage
	UpdatePlayerInput(DeltaTime);

	UpdateCharacterHover();

	float CurrentSpeed = GetCurrentSpeed();
	if (CurrentSpeed > 0) {
		DistanceSinceLastStep += DeltaTime * CurrentSpeed;
		if (DistanceSinceLastStep >= DistancePerStep) {
			DistanceSinceLastStep = 0;
			OnStep.Broadcast();
		}
	}
}

void AKLPlayerController::SetHeroesControls(int32 SchemeID) {
	for (auto Mapping : ActionMappings) {
		PlayerInput->RemoveActionMapping(Mapping);
	}

	ActionMappings.Empty();

	BindControlsSchemeDefault();
}

void AKLPlayerController::SetupInputComponent() {
	// set up gameplay key bindings
	Super::SetupInputComponent();

	InputComponent->BindAction("ToggleUI", IE_Pressed, this, &AKLPlayerController::OnToggleUI);

	InputComponent->BindAction("ZoomIn", IE_Pressed, this, &AKLPlayerController::OnZoomIn);
	InputComponent->BindAction("ZoomOut", IE_Pressed, this, &AKLPlayerController::OnZoomOut);

	// Deactivate active Hero
	InputComponent->BindAction("Cancel", IE_Released, this, &AKLPlayerController::OnCancel);
	InputComponent->BindAction("Accept", IE_Released, this, &AKLPlayerController::OnAccept);
	InputComponent->BindAction("LevelUp", IE_Pressed, this, &AKLPlayerController::ShowLevelUpMenu);

	InputComponent->BindAction("CameraMode", IE_Pressed, this, &AKLPlayerController::ActivateCameraMode);
	InputComponent->BindAction("CameraMode", IE_Released, this, &AKLPlayerController::EndCameraMode);

	InputComponent->BindAction("Pause", IE_Pressed, this, &AKLPlayerController::TriggerPause);

	InputComponent->BindAxis(MoveForwardBinding);
	InputComponent->BindAxis(MoveRightBinding);

	InputComponent->BindAxis(AimUpBinding);
	InputComponent->BindAxis(AimRightBinding);
}

void AKLPlayerController::AddActionMapping(FInputActionHandlerSignature& OnActivate, FName ActionName, FKey Key, bool bRelease) {
	FInputActionKeyMapping Mapping = FInputActionKeyMapping(ActionName, Key);
	ActionMappings.Add(Mapping);
	PlayerInput->AddActionMapping(Mapping);

	FInputActionBinding ActivationBinding = FInputActionBinding(ActionName, bRelease ? IE_Released : IE_Pressed);
	ActivationBinding.ActionDelegate = OnActivate;
	InputComponent->AddActionBinding(ActivationBinding);
};

void AKLPlayerController::BindControlsSchemeDefault() {
	TMap<int32, FKey> SkillKeys;
	SkillKeys.Add(-1, EKeys::SpaceBar);
	SkillKeys.Add(0, EKeys::Q);
	SkillKeys.Add(1, EKeys::E);
	SkillKeys.Add(2, EKeys::R);
	SkillKeys.Add(3, EKeys::F);

	for (auto Pair : SkillKeys) {
		int32 HeroID = Pair.Key;
		FName ActionName = FName(*("SkillTrigger" + FString::FromInt(HeroID)));

		// bind LaunchSkill action
		FInputActionHandlerSignature OnActivate;
		OnActivate.BindLambda([&](int32 HeroID) {
			LaunchSkill(HeroID, false);
		}, HeroID);

		AddActionMapping(OnActivate, ActionName, Pair.Value, false);
	}
}

const FName AKLPlayerController::MoveForwardBinding("MoveForward");
const FName AKLPlayerController::MoveRightBinding("MoveRight");

void AKLPlayerController::UpdateMovementDirection(float DeltaTime) {
	// Match3
	if (bControlsDisabled) {
		return;
	}

	// Find movement direction
	const float ForwardValue = GetInputAxisValue(MoveForwardBinding);
	const float RightValue = GetInputAxisValue(MoveRightBinding);

	if (bMenuMode) {
		if (ForwardValue == 0.f && RightValue == 0.f) {
			// No gesture
			return;
		}

		EGestureType Gesture;

		if (FMath::Abs(ForwardValue) > FMath::Abs(RightValue)) {
			Gesture = ForwardValue > 0 ? EGestureType::Up : EGestureType::Down;
		} else {
			Gesture = RightValue > 0 ? EGestureType::Right : EGestureType::Left;
		}

		CurrentMenuWidget->Update(Gesture);

		return;
	}

	// Clamp max size so that (X=1, Y=1) doesn't cause faster movement in diagonal directions
	FVector Movement = FVector(ForwardValue, RightValue, 0.f).GetClampedToMaxSize(1.0f);
	MovementDirection = Movement.RotateAngleAxis(Global->CurrentCameraRotationAngle, FVector(0.f, 0.f, 1.f));

	AFormation* Formation = Global->GetGroupFormation();

	float MovementTimeFactor = Global->GetSubsystem<UKLTimeFactorSubsystem>()->GetPlayerMovementFactor();

	Formation->Move(MovementDirection * MasterSpeed * DeltaTime * MovementTimeFactor);
}

const FName AKLPlayerController::AimUpBinding("AimUp");
const FName AKLPlayerController::AimRightBinding("AimRight");

void AKLPlayerController::UpdateCursorLocation() {
	if (IsValid(HoveredCharacter) && !bForcedHover) {
		CursorLocation = HoveredCharacter->GetActorLocation();
		return;
	}

	// Find movement direction
	const float AimUp = GetInputAxisValue(AimUpBinding);
	const float AimRight = GetInputAxisValue(AimRightBinding);

	// TODO: improve gamepad controls detection
	// Add the "last used controls" thing with reset over time
	if (AimUp != 0 || AimRight != 0) {
		FVector AimVector = FVector(AimUp, AimRight, 0.f).GetSafeNormal();
		AimVector = AimVector.RotateAngleAxis(Global->CurrentCameraRotationAngle, FVector(0.f, 0.f, 1.f));

		FVector AimOffset = AimVector * 1000.f;
		FVector GamepadCursor = GetMasterLocation() + AimOffset;

		CursorLocation = FVector(GamepadCursor.X, GamepadCursor.Y, 0.f);

		FVector2D ScreenLocation;
		ProjectWorldLocationToScreen(CursorLocation, ScreenLocation);
		SetMouseLocation(FMath::FloorToInt(ScreenLocation.X), FMath::FloorToInt(ScreenLocation.Y));

		return;
	}

	FVector2D MousePosition;
	if (bCameraMode) {
		// Use the saved cursor position while skills menu is active

		MousePosition = GestureStartPosition;
	} else {
		// Otherwise get the real cursor position

		float PosX;
		float PosY;

		GetMousePosition(PosX, PosY);
		MousePosition = FVector2D(PosX, PosY);
	}

	FHitResult Hit;
	if (!GetHitResultAtScreenPosition(MousePosition, ECC_KL_CursorHit, false, Hit)) {
		return;
	}

	if (Hit.bBlockingHit) {
		// We hit something, move there
		CursorLocation = FVector(Hit.ImpactPoint.X, Hit.ImpactPoint.Y, 0.f);
	}
}

TArray<APlayerCharacter*> AKLPlayerController::GetSortedHeroes() {
	TArray<APlayerCharacter*> HeroesList;
	GetHeroesList().GenerateValueArray(HeroesList);
	return Global->GetGroupFormation()->GetSortedHeroes(HeroesList);
}

void AKLPlayerController::UpdatePlayerInput(float DeltaTime) {
	//UpdateSkillInput();
	UpdateCursorLocation();
	UpdateMovementDirection(DeltaTime);
	UpdateFellowHeroes();
}

void AKLPlayerController::UpdateFellowHeroes() {
	// Match3
	if (bControlsDisabled) {
		return;
	}

	// TODO: get rid of this update – let them update themselves
	// Update movement for all heroes

	// Trigger auto-action for all heroes while LMB is pressed or it's a main menu mode
	// Only after the hold timeout reached
	float Now = GetWorld()->GetTimeSeconds();

	auto PrimaryActionKeys = PlayerInput->GetKeysForAction("PrimaryAction");

	bool bActionPressed = false;
	for (auto Action : PrimaryActionKeys) {
		if (PlayerInput->IsPressed(Action.Key)) {
			bActionPressed = true;
			break;
		}
	}

	auto SortedHeroes = GetSortedHeroes();
	for (int32 i = 0; i < SortedHeroes.Num(); i++) {
		auto Hero = SortedHeroes[i];
		auto Controller = Cast<AHeroController>(Hero->GetController());
		Controller->UpdateMovement(CursorLocation, bIsHovered);
		Controller->SetCanAct(bActionPressed);
	}
}

void AKLPlayerController::UpdateCharacterHover() {
	FHitResult Hit;
	GetHitResultUnderCursor(ECC_KL_TraceControls, false, Hit);

	AActor* HoveredActor = Hit.GetActor();
	if (!IsValid(HoveredActor)) {
		if (!bForcedHover) {
			UnhoverCharacter();
		}
		return;
	}

	HoverCharacter(Cast<AGameCharacter>(HoveredActor));
}

void AKLPlayerController::HoverCharacter(AGameCharacter* InHoveredCharacter) {
	if (!IsValid(InHoveredCharacter)) {
		if (!bForcedHover) {
			UnhoverCharacter();
		}

		return;
	}

	if (InHoveredCharacter != HoveredCharacter) {
		UnhoverCharacter();

		HoveredCharacter = InHoveredCharacter;
		bIsHovered = true;
	}
}

void AKLPlayerController::HoverHero(int32 HeroID) {
	bForcedHover = false;
	HoverCharacter(GetHero(HeroID));
	bForcedHover = true;
}

void AKLPlayerController::UnhoverCharacter() {
	bForcedHover = false;
	if (IsValid(HoveredCharacter)) {
		HoveredCharacter->Unhover();
	}

	bIsHovered = false;
	HoveredCharacter = nullptr;
}

FVector AKLPlayerController::GetCursorPosition() const {
	return CursorLocation;
}

void AKLPlayerController::LaunchSkill(int32 SkillIndex, bool bRepeat) {
	if (bMenuMode) {
		return;
	}

	bool bIsGroupSkill = SkillIndex == -1;

	AGameCharacter* Hero;
	if (bIsGroupSkill) {
		Hero = Cast<AGameCharacter>(GetPawn());
	} else {
		Hero = GetHero(SkillIndex);
	}

	if (!IsValid(Hero) || Hero->IsCharacterDead()) {
		return;
	}

	if (bIsGroupSkill) {
		FTargetingInfo TargetInfo(CursorLocation);

		USkillLauncher* GroupSkillLauncher = Hero->GetValidSkill(0);
		GroupSkillLauncher->Launch(TargetInfo);
		return;
	}

	FTargetingInfo TargetInfo;
	TargetInfo.Location = CursorLocation;
	auto ActiveController = Cast<AHeroController>(Hero->GetController());
	if (!IsValid(ActiveController)) {
		return;
	}

	ActiveController->LaunchSkill(1, TargetInfo);
}

void AKLPlayerController::OnCancel() {
	if (bMenuMode) {
		CurrentMenuWidget->Update(EGestureType::Cancel);

		return;
	}

	ShowMainMenu();
}

void AKLPlayerController::OnAccept() {
	if (bMenuMode) {
		CurrentMenuWidget->Update(EGestureType::Accept);
	}
}

FVector AKLPlayerController::GetPlayerLocation() const {
	auto HeroesList = GetHeroesList();

	FVector Sum = FVector::ZeroVector;
	int32 Num = HeroesList.Num();

	for (TPair<int32, APlayerCharacter*> Hero : HeroesList) {
		if (Hero.Value == nullptr || !IsValid(Hero.Value) || Hero.Value->IsCharacterDead()) {
			Num--;
			continue;
		}

		Sum += Hero.Value->GetActorLocation();
	}

	return Sum / Num;
}

void AKLPlayerController::OnZoomIn() {
	if (ZoomLevel <= 1.f || !Global->IsZoomEnabled()) {
		return;
	}

	ZoomLevel -= 1.f;
}

void AKLPlayerController::OnToggleUI() {

}

void AKLPlayerController::OnZoomOut() {
	if (ZoomLevel >= ZoomMax || !Global->IsZoomEnabled()) {
		return;
	}

	ZoomLevel += 1.f;
}

float AKLPlayerController::GetZoomFactor() const {
	return ZoomLevel / ZoomMax;
}

void AKLPlayerController::SetZoomLevel(float InZoom) {
	ZoomLevel = FMath::Clamp(InZoom, 1.f, ZoomMax);
}

void AKLPlayerController::OnCauseDamage(float Amount, AActor* Target) {
	AMobCharacter* DamagedEnemy = Cast<AMobCharacter>(Target);
	if (!IsValid(DamagedEnemy)) {
		return;
	}

	// On enemy kill
	if (DamagedEnemy->IsCharacterDead()) {
		float Now = GetWorld()->GetTimeSeconds();
		float Delay = Now - LastKillTime;
		LastKillTime = Now;

		bool bKillStreak = Delay < ComboDelay;
		if (bKillStreak) {
			StreakLength++;

			if (MultiplierID != INDEX_NONE) {
				KLScores->Multiplier->Remove(MultiplierID);
			}

			int32 StreakFactor = 1 + StreakLength / 5;
			MultiplierID = KLScores->Multiplier->Add(EKLModifierType::Percent, FMath::Min(StreakFactor * 25.f, 100.f));
		}

		int32 ScoreToAdd = DamagedEnemy->GetScores();

		float Dist = FVector::Dist2D(DamagedEnemy->GetActorLocation(), GetMasterLocation());
		float DistMultiplier = Dist < BonusDistance ? FMath::Lerp(BonusDistanceFactor, LongDistanceFactor, Dist / BonusDistance) : LongDistanceFactor;

		KLScores->AddScore(ScoreToAdd * DistMultiplier);

	}
}

void AKLPlayerController::OnCauseHeal(float Amount, AActor* Target) {
	// TODO: is this method ever needed?
}

void AKLPlayerController::OnLevelUp(int32 NewLevel) {
	auto HeroesList = GetHeroesList();
	for (TPair<int32, APlayerCharacter*> Hero : HeroesList) {
		if (Hero.Value == nullptr || !IsValid(Hero.Value)) {
			continue;
		}

		Hero.Value->SetLevel(NewLevel);
	}
}

bool AKLPlayerController::GetHoveredCharacter(AGameCharacter*& OutHoveredCharacter) const {
	if (bIsHovered) {
		OutHoveredCharacter = HoveredCharacter;
		return true;
	}

	return false;
}

FVector AKLPlayerController::GetMasterLocation() const {
	if (!Global) {
		return FVector::ZeroVector;
	}

	auto Formation = Global->GetGroupFormation();

	if (IsValid(Formation)) {
		return Formation->GetMasterLocation();
	}

	return FVector::ZeroVector;
}

FText AKLPlayerController::GetKeyNameForAction(const FName ActionName, bool bLongName) const {
	auto MappedKeys = PlayerInput->GetKeysForAction(ActionName);
	if (MappedKeys.Num() == 0) {
		return NSLOCTEXT("Common", "NoKeyMapping", "None");;
	}

	return MappedKeys[0].Key.GetDisplayName(bLongName);
}

void AKLPlayerController::OnGameStateChanged(EKLGameState NewState, bool bLoaded) {
	if (NewState == EKLGameState::GameOver) {
		StopMovement();
		ShowMenuWidget(MenuWidgetClass, false);
	}
}

void AKLPlayerController::StopMovement() {
	Super::StopMovement();

	// Full stop

	MovementDirection = FVector::ZeroVector;
	AFormation* Formation = Global->GetGroupFormation();
	Formation->Move(MovementDirection);
	UpdateFellowHeroes();
	bControlsDisabled = true;
}

void AKLPlayerController::UpdateCameraMode() {
	float DeltaX;
	float DeltaY;

	float FactorX = Global->CameraSensitivityX;
	float FactorY = Global->CameraSensitivityY;

	float CameraDeadzone = 3.f;

	GetInputMouseDelta(DeltaX, DeltaY);
	GestureDelta.X = GestureDelta.X+FMath::Abs(DeltaX);
	GestureDelta.Y = GestureDelta.Y+FMath::Abs(DeltaY);

	if (bCameraMode) {
		SetMouseLocation(FMath::FloorToInt(GestureStartPosition.X), FMath::FloorToInt(GestureStartPosition.Y));

		if (GestureDelta.X > CameraDeadzone) {
			Global->CameraRotationAngle = Global->CameraRotationAngle + DeltaX * FactorX;
		}

		if (GestureDelta.Y > CameraDeadzone) {
			ZoomLevel = FMath::Clamp(ZoomLevel - (DeltaY/10) * FactorY, 1.f, ZoomMax);
		}
	}
}

void AKLPlayerController::ActivateCameraMode() {
	float PosX;
	float PosY;

	if (!GetMousePosition(PosX, PosY)) {
		return;
	}

	GestureStartTime = GetWorld()->GetTimeSeconds();

	bCameraMode = true;

	GestureStartPosition = FVector2D(PosX, PosY);
	GestureDelta = FVector2D::ZeroVector;

	bShowMouseCursor = false;
}

void AKLPlayerController::EndCameraMode() {
	if (!bCameraMode) {
		return;
	}

	bCameraMode = false;
	bShowMouseCursor = true;

	SetMouseLocation(FMath::FloorToInt(GestureStartPosition.X), FMath::FloorToInt(GestureStartPosition.Y));
}

bool AKLPlayerController::IsLocationOnScreen(const FVector& Location) const {
	FVector2D ScreenLocation;
	ProjectWorldLocationToScreen(Location, ScreenLocation);

	int32 ScreenWidth = 0;
	int32 ScreenHeight = 0;
	GetViewportSize(ScreenWidth, ScreenHeight);

	int32 ScreenX = (int32)ScreenLocation.X;
	int32 ScreenY = (int32)ScreenLocation.Y;

	return ScreenX >= 0 && ScreenY >= 0 && ScreenX < ScreenWidth && ScreenY < ScreenHeight;
}

void AKLPlayerController::OnSpeedUpdate(float Speed) {
	MasterSpeed = Speed;
}

float AKLPlayerController::GetCurrentSpeed() const {
	if (MovementDirection != FVector::ZeroVector) {
		return MasterSpeed;
	}

	return 0.f;
}

void AKLPlayerController::TeleportGroup(FVector InLocation) {
	FVector MasterLocation = GetMasterLocation();
	FVector NewLocation = FVector(InLocation.X, InLocation.Y, MasterLocation.Z);

	AFormation* Formation = Global->GetGroupFormation();
	Formation->SetMasterLocation(NewLocation);

	TArray<APlayerCharacter*> HeroesAlive = Global->GetHeroes(true);
	for (auto Hero : HeroesAlive) {
		FVector Adjustment = Formation->GetAdjustmentForCharacter(Hero);
		FVector HeroLocation = NewLocation + Adjustment;

		FVector NavLocation;

		auto Controller = Cast<AHeroController>(Hero->GetController());
		if (Controller && Controller->GetRandomReachablePointInRadius(HeroLocation, 300.f, NavLocation)) {
			// Need to keep Z position
			Hero->SetActorLocationECS(FVector(NavLocation.X, NavLocation.Y, HeroLocation.Z));
		} else {
			Hero->SetActorLocationECS(HeroLocation);
		}
	}

	UpdatePlayerInput(GetWorld()->GetDeltaSeconds());
}
