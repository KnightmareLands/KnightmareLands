// Fill out your copyright notice in the Description page of Project Settings.

#include "Player/PlayerCharacter.h"
#include "Player/HeroController.h"
#include "GameGlobal.h"

// Sets default values
APlayerCharacter::APlayerCharacter() {
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
	HeroID = -1;

	AutoPossessAI = EAutoPossessAI::PlacedInWorldOrSpawned;
	AIControllerClass = AHeroController::StaticClass();
}

void APlayerCharacter::PostInitializeComponents() {
	auto Global = GetGameGlobal();
	// In game, use the speed value from the GamGlobal settings
	if (IsValid(Global)) {
		BaseWalkSpeed = GetGameGlobal()->HeroesSpeed;
	}

	Super::PostInitializeComponents();
}

float APlayerCharacter::TakeDamage(float Damage, struct FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser) {
	float Result = Super::TakeDamage(Damage, DamageEvent, EventInstigator, DamageCauser);

	GetGameGlobal()->UpdateHealthState();

	return Result;
}

float APlayerCharacter::TakeHeal(float Amount, AController* EventInstigator, AActor* Causer) {
	float Result = Super::TakeHeal(Amount, EventInstigator, Causer);

	GetGameGlobal()->UpdateHealthState();

	return Result;
}

void APlayerCharacter::SetHeroID(int32 InHeroID) {
	HeroID = InHeroID;
}

int32 APlayerCharacter::GetHeroID() const {
	return HeroID;
}

AController* APlayerCharacter::GetCharacterController() const {
	// TODO: return the current player controller, not just the first player
	return GetWorld()->GetFirstPlayerController();
}

FVector APlayerCharacter::GetHeroLocation() const {
	auto HeroController = Cast<AHeroController>(GetController());
	if (!HeroController) {
		return GetActorLocation();
	}

	return HeroController->GetOriginalLocation();
}

void APlayerCharacter::SetDefinedHero() {
	bDefinedHero = true;
}

bool APlayerCharacter::IsDefinedHero() const {
	return bDefinedHero;
}

void APlayerCharacter::UpdateSpeed() {
	Super::UpdateSpeed();

	auto Global = GetGameGlobal();
	Global->SpeedUpdate();
}
