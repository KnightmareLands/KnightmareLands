// Fill out your copyright notice in the Description page of Project Settings.

#include "Player/HeroController.h"
#include "Common/GameCharacter.h"
#include "Common/KLStatics.h"
#include "ECS/ECSComponents.h"
#include "ECS/ECSMovementComponent.h"
#include "Formations/Formation.h"
#include "GameGlobal.h"
#include "Mobs/MobCharacter.h"
#include "Player/PlayerCharacter.h"
#include "Skills/SkillLauncher.h"

#include "Core/ECSEngine.h"

#include "NavFilters/NavigationQueryFilter.h"
#include "NavigationSystem.h"

AHeroController::AHeroController() {
	PrimaryActorTick.bCanEverTick = true;
}

void AHeroController::BeginPlay() {
	Global = Cast<UGameGlobal>(GetGameInstance());
	check(Global);

	Super::BeginPlay();

	PositionAdjustment = FVector::ZeroVector;
}

bool AHeroController::ECSPostRegister() {
	UECSEngine* ECS = IECSEntity::GetEngine(GetPawn());
	const int32 ECSIndex = IECSEntity::GetEntity(GetPawn());

	FECSDataNavigation& DataNavigation = ECS->Get<FECSDataNavigation>(ECSIndex);
	DataNavigation.bEnabled = true;
	DataNavigation.FilterClass = NavigationFilter;

	return Super::ECSPostRegister();
}

bool AHeroController::ECSPostUnregister() {
	UECSEngine* ECS = IECSEntity::GetEngine(GetPawn());
	const int32 ECSIndex = IECSEntity::GetEntity(GetPawn());

	FECSDataNavigation& DataNavigation = ECS->Get<FECSDataNavigation>(ECSIndex);
	DataNavigation.bEnabled = false;

	return Super::ECSPostUnregister();
}

void AHeroController::Tick(float DeltaSeconds) {
	Super::Tick(DeltaSeconds);

	if (Global->IsLoading()) {
		return;
	}

	auto Hero = GetHero();
	if (!IsValid(Hero) || Hero->IsCharacterDead()) {
		return;
	}

	ProcessController(DeltaSeconds);
}

void AHeroController::ProcessController(float DeltaSeconds) {
	UpdateMaster(DeltaSeconds);
	PerformPlayerAction();
	PerformMovement();
}

AGameCharacter* AHeroController::GetHero() const {
	// TODO: use only on SetPawn

	auto CurrentPawn = GetPawn();
	if (IsValid(CurrentPawn)) {
		return Cast<AGameCharacter>(CurrentPawn);
	}

	return nullptr;
}

void AHeroController::UpdateMaster(float DeltaTime) {
	AFormation* Formation = Global->GetGroupFormation();

	auto Hero = Cast<APlayerCharacter>(GetHero());

	bool bLastHero = Global->GetHeroes(true).Num() == 1;

	if (IsValid(Hero) && !bLastHero) {
		PositionAdjustment = Formation->GetAdjustmentForCharacter(Hero);
	} else {
		PositionAdjustment = FVector::ZeroVector;
	}

	MasterLocation = Formation->GetMasterLocation();
}

void AHeroController::StopMovement() {
	Super::StopMovement();

	AGameCharacter* Hero = GetHero();
	if (!IsValid(Hero)) {
		return;
	}

	Hero->StopMovement();
}

bool AHeroController::CanUpdateMovement(FVector InCursorLocation, bool bIsHovered) {
	auto Hero = GetHero();
	if (!IsValid(Hero)) {
		return false;
	}

	if (Hero->IsCharacterDead()) {
		StopMovement();
		return false;
	}

	if (bIsHovered) {
		FVector HeroLocation = Hero->GetActorLocation();

		if (InCursorLocation == HeroLocation) {
			FVector ForwardVector = Hero->GetActorForwardVector();
			CursorLocation = InCursorLocation + ForwardVector * CursorFocusDistance;
		} else {
			CursorLocation = InCursorLocation;
		}
	} else {
		CursorLocation = MasterLocation + (InCursorLocation - MasterLocation).GetSafeNormal2D() * CursorFocusDistance;
	}

	return true;
}

void AHeroController::UpdateMovement(FVector InCursorLocation, bool bIsHovered) {
	if (!CanUpdateMovement(InCursorLocation, bIsHovered)) {
		return;
	}

	auto Hero = GetHero();

	if (!Hero->IsHoldRotation()) {
		SetFocalPoint(CursorLocation);
	}
}

void AHeroController::SetCanAct(bool InEnabled) {
	bool bNeedUpdate = InEnabled == true && bCanAct == false;
	bCanAct = InEnabled;

	if (bNeedUpdate) {
		PerformPlayerAction();
	}
}

void AHeroController::PerformMovement() {
	auto Hero = GetHero();
	if (!IsValid(Hero)) {
		return;
	}

	if (Global->GetGameState() == EKLGameState::Demo) {
		return;
	}

	FVector NewLocation = MasterLocation + PositionAdjustment;

	if (Hero->IsDebug()) {
		auto DebugColor = Hero->DebugColor;
		DrawDebugSphere(GetWorld(), NewLocation, 20, 32, DebugColor, false, 0.f);
	}

	MoveHeroToLocation(NewLocation);
}

FVector AHeroController::GetOriginalLocation_Implementation() const {
	auto Hero = GetHero();

	if (!IsValid(Hero)) {
		return FVector::ZeroVector;
	}

	return Hero->GetActorLocation();
}

void AHeroController::MoveHeroToLocation(FVector Location, bool bHasTarget) {
	auto Hero = GetHero();

	if (!IsValid(Hero) || Hero->IsCharacterDead()) {
		return;
	}

	// Don't move into the target's exact position, stop nearby
	float AcceptanceRadius = bHasTarget ? 100.f : 10.f;

	Hero->GetECSMovement()->MoveTo(Location, false, AcceptanceRadius);
}

USkillLauncher* AHeroController::GetNextSkill() const {
	auto Hero = GetHero();

	if (!IsValid(Hero)) {
		return nullptr;
	}

	// Returns the queued skill or a skill for auto-usage
	int32 AutoSkillIndex = NextSkill == INDEX_NONE ? Hero->GetAutoSkillIndex() : NextSkill;

	if (AutoSkillIndex == INDEX_NONE) {
		return nullptr;
	}

	auto DefaultSkill = Hero->GetValidSkill(AutoSkillIndex);
	if (!IsValid(DefaultSkill) || !DefaultSkill->IsInited()) {
		return nullptr;
	}

	return DefaultSkill;
}

void AHeroController::LaunchSkill(int32 SkillIndex, FTargetingInfo TargetingInfo) {
	auto Hero = GetHero();
	if (!IsValid(Hero)) {
		return;
	}

	if (Hero->IsCharacterDead()) {
		return;
	}

	auto Skill = Hero->GetValidSkill(SkillIndex);
	if (!IsValid(Skill) || !Skill->IsInited()) {
		return;
	}

	auto DefaultSkill = GetNextSkill();
	if (Hero->IsDebug()) {
		UE_LOG(KLDebug, Warning, TEXT("HeroController::LaunchSkill: new Skill: %s"), *Skill->GetTitle().ToString());
		if (IsValid(DefaultSkill)) {
			UE_LOG(KLDebug, Warning, TEXT("HeroController::LaunchSkill: DefaultSkill: %s"), *DefaultSkill->GetTitle().ToString());
		}
		if (IsValid(ActiveSkill)) {
			UE_LOG(KLDebug, Warning, TEXT("HeroController::LaunchSkill: ActiveSkill: %s"), *ActiveSkill->GetTitle().ToString());
		}
	}

	if (IsValid(ActiveSkill) && ActiveSkill->IsContinuos() && ActiveSkill->IsActive()) {
		ActiveSkill->Deactivate();

		if (Skill == ActiveSkill) {
			ActiveSkill = nullptr;
			bManualTarget = false;
			NextSkill = INDEX_NONE;
			return;
		}

		ActiveSkill = nullptr;
	}

	NextSkill = SkillIndex;
	ManualTarget = TargetingInfo;
	bManualTarget = true;
}

void AHeroController::DeactivateSkill() {
	if (IsValid(ActiveSkill) && ActiveSkill->IsContinuos() && bManualTarget) {
		ActiveSkill->Deactivate();
		ActiveSkill = nullptr;
		bManualTarget = false;
		NextSkill = INDEX_NONE;
	}
}

void AHeroController::PerformPlayerAction() {
	auto Hero = GetHero();
	if (!IsValid(Hero)) {
		return;
	}

	if (Hero->IsCharacterDead() || Hero->IsDebugForbidAttack()) {
		return;
	}

	USkillLauncher* DefaultSkill = GetNextSkill();
	if (!IsValid(DefaultSkill)) {
		return;
	}

	if (Global->GetGameState() == EKLGameState::Demo) {
		CursorLocation = Hero->GetActorLocation() + Hero->GetActorForwardVector() * 1000;
	}

	bool bCanUse = DefaultSkill->CanBeUsed();

	bool bIsBusy = Hero->IsCharacterBusy();
	bool bIsContinuos = IsValid(ActiveSkill) && ActiveSkill->IsContinuos();

	if (bIsBusy && !bIsContinuos) {
		if (Hero->IsDebug()) {
			UE_LOG(KLDebug, Warning, TEXT("HeroController::PerformPlayerAction: character is busy, skip; NextSkill: %d"), NextSkill);
		}
		return;
	}

	if (bCanUse && !IsManualTargetValid()) {
		bManualTarget = false;
	}

	auto TargetType = DefaultSkill->GetTargetingType();
	bool bNoTarget = TargetType == ETargetingType::NoTarget || TargetType == ETargetingType::TargetSelf;
	bool bPositive = DefaultSkill->IsPositive();

	bool bAutoSkill = NextSkill == INDEX_NONE;
	// Can use auto-skill either if it's enabled (e.g. button is pressed) or it's a targeted skill
	bool bCanUseAutoSkill = bAutoActionEnabled || bCanAct; // || !bNoTarget;

	FTargetingInfo Target;
	if ((bAutoSkill && !bCanUseAutoSkill) || !UpdateTargetingInfo(TargetType, bPositive, Target)) {
		SetFocalPoint(CursorLocation);
		if (ActiveSkill == DefaultSkill) {
			ActiveSkill = nullptr;
		}
		DefaultSkill->Deactivate();
		return;
	}

	bNoTarget = Target.Type == ETargetingType::NoTarget || Target.Type == ETargetingType::TargetSelf;

	AGameCharacter* TargetCharacter;
	float Distance = UKLStatics::GetDistanceToTarget(Target, Hero->GetActorLocation(), TargetCharacter);

	bool bLookToCursor = bNoTarget && !CursorLocation.IsZero();
	if (bLookToCursor || TargetCharacter == Hero || (!bCanUse && !bIsContinuos)) {
		bLookToCursor = true;
		SetFocalPoint(CursorLocation);
		if (Hero->IsDebug()) {
			UE_LOG(KLDebug, Warning, TEXT("HeroController::PerformPlayerAction:Look to cursor"));
		}
	}

	// TODO: move this logic to MeleeController
	// Don't trigger an auto skill in case if it's melee and there is no target found
	bool bNoMeleeTarget = bAutoSkill && bNoTarget && DefaultSkill->IsMelee();

	// TODO: fix range check for Mines - they can be placed even if enemy is outside of the range
	float Range = DefaultSkill->GetAttackRange() * 1.5;
	bool bInRange = Range >= Distance && Distance >= 0;
	if (!bNoMeleeTarget && (bLookToCursor || bInRange)) {
		if (bNoTarget) {
			SetFocalPoint(CursorLocation);
		} else if (bCanUse && !bLookToCursor) {
			Hero->HoldRotation();
			if (IsValid(TargetCharacter)) {
				SetFocus(TargetCharacter);
			} else if (!Target.Location.IsZero()) {
				SetFocalPoint(Target.Location);
			} else {
				SetFocalPoint(CursorLocation);
			}
		} else {
			Hero->FreeRotation();
		}

		if (bNoTarget || bIsContinuos) {
			Hero->FreeRotation();
		}

		if (bCanUse) {
			if (Hero->IsDebug()) {
				UE_LOG(KLDebug, Warning, TEXT("HeroController::PerformPlayerAction: Launch skill; NextSkill: %d"), NextSkill);
			}

			// NextSkill = INDEX_NONE;
			if (IsValid(ActiveSkill) && ActiveSkill != DefaultSkill) {
				ActiveSkill->Deactivate();
			}
			ActiveSkill = DefaultSkill;
			DefaultSkill->Launch(Target);
		} else {
			if (Hero->IsDebug() && DefaultSkill->IsActive()) {
				UE_LOG(KLDebug, Warning, TEXT("HeroController::PerformPlayerAction: Can't use skill: inited %d, timeout %f, active %d; NextSkill: %d"), DefaultSkill->IsInited() ? 1 : 0, DefaultSkill->GetRemainingCooldown(), DefaultSkill->IsActive() ? 1 : 0, NextSkill);
			}
		}
	} else {
		if (Hero->IsDebug()) {
			UE_LOG(KLDebug, Warning, TEXT("HeroController::PerformPlayerAction: Out of range, stop/skip; NextSkill: %d"), NextSkill);
		}
		if (IsValid(ActiveSkill)) {
			ActiveSkill->Deactivate();
			ActiveSkill = nullptr;
		}
	}

	// Reset skill in any case, it just will not be applyed in case of cooldown or something
	NextSkill = INDEX_NONE;
	bManualTarget = false;
}

bool AHeroController::FindTarget_Implementation(ETargetingType InType, bool bPositive, FTargetingInfo& OutTarget) {
	bool bTargetFound = false;

	auto Hero = GetHero();

	if (!IsValid(Hero)) {
		return false;
	}

	// UE_LOG(KLDebug, Warning, TEXT("AHeroController::FindTarget: %s"), *Hero->GetName());

	auto TargetType = bPositive ? APlayerCharacter::StaticClass() : AMobCharacter::StaticClass();
	AGameCharacter* ClosestTarget;
	TArray<AGameCharacter*> TargetActors;

	FTargetingInfo NewTarget;

	switch (InType) {
	case ETargetingType::NoTarget:
		// TODO: do we need to get a real target actor/location here?

		NewTarget = FTargetingInfo();
		bTargetFound = true;
		break;
	case ETargetingType::TargetLocation:
		NewTarget = FTargetingInfo(CursorLocation);
		bTargetFound = true;

		break;
	case ETargetingType::TargetActor:
		ClosestTarget = Hero->GetClosestCharacterOfClass(TargetType);
		if (IsValid(ClosestTarget)) {
			TargetActors.Add(ClosestTarget);
		}

		if (TargetActors.Num() > 0) {
			NewTarget = FTargetingInfo(TargetActors);
			bTargetFound = true;
		}

		break;
	case ETargetingType::TargetSelf:
		TargetActors.Add(Hero);

		NewTarget = FTargetingInfo(TargetActors);
		NewTarget.Type = ETargetingType::TargetSelf;

		bTargetFound = true;
		break;
	}

	if (bTargetFound) {
		NewTarget.Type = InType;
		NewTarget.bPositive = bPositive;
		OutTarget = NewTarget;
	}

	return bTargetFound;
}

bool AHeroController::IsManualTargetValid() const {
	if (NextSkill != INDEX_NONE && bManualTarget) {
		return true;
	}

	if (!bManualTarget || ManualTarget.Type != ETargetingType::TargetActor || ManualTarget.Actors.Num() == 0) {
		return false;
	}

	auto Target = ManualTarget.Actors[0];

	if (!IsValid(Target) || Target->IsCharacterDead()) {
		return false;
	}

	return true;
}

bool AHeroController::UpdateTargetingInfo(ETargetingType InType, bool bPositive, FTargetingInfo& OutTargetingInfo) {
	if (bManualTarget && ManualTarget.Type == InType) {
		OutTargetingInfo = ManualTarget;
		return true;
	}

	bManualTarget = false;

	FTargetingInfo Target;
	if (FindTarget(InType, bPositive, Target)) {
		OutTargetingInfo = Target;
		return true;
	}

	return false;
}

bool AHeroController::GetRandomReachablePointInRadius(const FVector& Origin, float Radius, FVector& OutLocation) const {
	auto NavSys = FNavigationSystem::GetCurrent<UNavigationSystemV1>(GetWorld());

	ANavigationData* NavData;
	auto NavDataContext = GetPawn();

	INavAgentInterface* NavAgent = Cast<INavAgentInterface>(NavDataContext);
	if (NavAgent != NULL) {
		const FNavAgentProperties& AgentProps = NavAgent->GetNavAgentPropertiesRef();
		NavData = NavSys->GetNavDataForProps(AgentProps);
	} else {
		NavData = NavSys->GetDefaultNavDataInstance();
	}

	FSharedConstNavQueryFilter QueryFilter = UNavigationQueryFilter::GetQueryFilter(*NavData, NavDataContext, NavigationFilter);

	FNavLocation NavLocation;
	bool Result = NavSys->GetRandomReachablePointInRadius(Origin, Radius, NavLocation, NavData, QueryFilter);
	OutLocation = NavLocation.Location;

	return Result;
}
