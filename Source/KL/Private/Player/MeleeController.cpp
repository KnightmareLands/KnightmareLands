// Fill out your copyright notice in the Description page of Project Settings.

#include "Player/MeleeController.h"
#include "Common/GameCharacter.h"
#include "Mobs/MobCharacter.h"
#include "Skills/SkillLauncher.h"
#include "Common/KLStatics.h"

void AMeleeController::UpdateMovement(FVector InCursorLocation, bool bIsHovered) {
	if (!CanUpdateMovement(InCursorLocation, bIsHovered)) {
		OnMeleeDashEnd();
		return;
	}

	auto Hero = GetHero();

	FVector CursorDirection = (InCursorLocation - MasterLocation).GetSafeNormal2D();

	if (!bAutoActionEnabled && !bCanAct) {
		bMeleeTarget = false;

		// Allow melee to back to group in dash mode before turning it off
		if (FVector::Dist2D(Hero->GetActorLocation(), MasterLocation + PositionAdjustment) < 100.f) {
			OnMeleeDashEnd();
		}

		return Super::UpdateMovement(InCursorLocation, bIsHovered);
	}

	SenseCenter = MasterLocation + (SenseMaxOffset > 0 ? CursorDirection * SenseMaxOffset : PositionAdjustment);
	SenseCenter.Z = 50.f;

	// if (FVector::Dist2D(Hero->GetActorLocation(), SenseCenter) > 100.f) {
	// 	OnMeleeDashStart();
	// } else {
	// 	OnMeleeDashEnd();
	// }

	// DrawDebugCircle(GetWorld(), SenseCenter + FVector(0,0,5), SenseRadius, 32, FColor(255, 0, 0), false, -1.f, 0, 0.f, FVector(0.f,1.f,0.f), FVector(1.f,0.f,0.f), false);

	FTargetingInfo TargetInfo;
	bMeleeTarget = FindTarget(ETargetingType::TargetActor, false, TargetInfo);
	if (!bMeleeTarget) {
		if (FVector::Dist2D(Hero->GetActorLocation(), SenseCenter) < 100.f) {
			OnMeleeDashEnd();
		}

		return Super::UpdateMovement(InCursorLocation, bIsHovered);
	}

	OnMeleeDashStart();
	MeleeTarget = TargetInfo;

	// DrawDebugSphere(GetWorld(), Target->GetActorLocation(), 200, 64, FColor(255, 255, 0));
}

FVector AMeleeController::GetOriginalLocation_Implementation() const {
	return MasterLocation + PositionAdjustment;
}

void AMeleeController::PerformMovement() {
	auto Hero = GetHero();
	if (!IsValid(Hero)) {
		return;
	}

	if (!bAutoActionEnabled && !bCanAct) {
		return Super::PerformMovement();
	}

	FVector Destination;
	FVector SenseLocation;

	AActor* Target = bMeleeTarget && MeleeTarget.Actors.Num() > 0 ? MeleeTarget.Actors[0] : nullptr;
	if (IsValid(Target)) {
		SenseLocation = Target->GetActorLocation();
		Destination = SenseLocation + (Hero->GetActorLocation() - SenseLocation).GetSafeNormal() * 100.f;
	} else {
		Destination = SenseCenter;
		SenseLocation = SenseCenter;
	}

	if (Hero->IsDebug()) {
		DrawDebugSphere(GetWorld(), Destination, 20, 32, FColor(255, 0, 0), false, 0.f);
	}

	SetFocalPoint(SenseLocation);
	MoveHeroToLocation(Destination, bMeleeTarget);
}

bool AMeleeController::FindTarget_Implementation(ETargetingType InType, bool bPositive, FTargetingInfo& OutTarget) {
	auto Hero = GetHero();
	if (!IsValid(Hero)) {
		return false;
	}

	if (InType != ETargetingType::TargetActor) {
		return Super::FindTarget_Implementation(InType, bPositive, OutTarget);
	}

	TArray<AActor*> IgnoredActors;
	IgnoredActors.Add(Hero);

	TArray<AGameCharacter*> Targets;
	if (UKLStatics::FindRadialTargets(this, SenseCenter, SenseRadius, IgnoredActors, true, Targets)) {
		float ClosestDistance = INDEX_NONE;
		AGameCharacter* ClosestTarget = nullptr;

		// TODO: calculate falloff value for every target
		for (auto GameCharacter : Targets) {
			AMobCharacter* Current = Cast<AMobCharacter>(GameCharacter);
			if (!IsValid(Current)) {
				continue;
			}

			float Distance = FVector::Dist(Current->GetActorLocation(), Hero->GetActorLocation());

			if (ClosestDistance == INDEX_NONE || Distance < ClosestDistance) {
				ClosestDistance = Distance;
				ClosestTarget = Current;
			}
		}

		if (IsValid(ClosestTarget) && !ClosestTarget->IsCharacterDead()) {
			TArray<AGameCharacter*> FoundTargets;
			FoundTargets.Add(Cast<AGameCharacter>(ClosestTarget));

			OutTarget = FTargetingInfo(FoundTargets);

			return true;
		}
	}

	return false;
}

bool AMeleeController::IsManualTargetValid() const {
	if (!bMeleeTarget) {
		return false;
	}

	if (!ensure(MeleeTarget.Actors.Num() > 0)) {
		return false;
	}

	// When bMeleeTarget is true there are always should be an actor inside
	auto Target = MeleeTarget.Actors[0];

	if (!IsValid(Target) || Target->IsCharacterDead()) {
		return false;
	}

	return Super::IsManualTargetValid();
}

bool AMeleeController::UpdateTargetingInfo(ETargetingType InType, bool bPositive, FTargetingInfo& OutTargetingInfo) {
	if (InType != ETargetingType::TargetActor) {
		return Super::UpdateTargetingInfo(InType, bPositive, OutTargetingInfo);
	}

	if (bMeleeTarget) {
		OutTargetingInfo = MeleeTarget;
		return true;
	}

	return false;
}
