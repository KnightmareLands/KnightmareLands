// Fill out your copyright notice in the Description page of Project Settings.

#include "Formations/Formation.h"
#include "Player/PlayerCharacter.h"
#include "GameGlobal.h"

AFormation::AFormation() {
	PrimaryActorTick.bCanEverTick = true;

	GroupLocations = TArray<FVector>();
	SortedHeroes = TArray<APlayerCharacter*>();

	GroupDirection = CreateDefaultSubobject<UArrowComponent>(FName("GroupDirection"));
	RootComponent = GroupDirection;
}

void AFormation::BeginPlay() {
	Super::BeginPlay();

	Reset();
}

void AFormation::Tick(float DeltaTime) {
	Super::Tick(DeltaTime);

	Yaw = FMath::FixedTurn(Yaw, TargetYaw, RotationRate * DeltaTime);

	if (!bHasMovement) {
		UpdateGroupCenter();

		float Distance = FVector::Dist2D(MasterLocation, GroupCenter);

		if (Distance > AcceptanceDistance) {
			float SpeedFactor = FMath::Clamp<float>(Distance / MaxDistance, 0.f, 1.f);
			MasterLocation = FMath::VInterpTo(MasterLocation, GroupCenter, DeltaTime, 5.f * SpeedFactor);
		}
	}

	bHasMovement = false;
}

void AFormation::Reset() {
	ClearSortedHeroes();
}

TArray<FVector> AFormation::GetLocalAdjustments() const {
	return GroupLocations;
}

TArray<APlayerCharacter*> AFormation::GetSortedHeroes(const TArray<APlayerCharacter*> HeroesList) {
	if (SortedHeroes.Num() > 0) {
		return SortedHeroes;
	}

	auto ResultList = TArray<APlayerCharacter*>();

	for (auto Hero : HeroesList) {
		if (Hero == nullptr || !IsValid(Hero) || Hero->IsCharacterDead()) {
			continue;
		}

		ResultList.Add(Hero);
	}

	Algo::Sort(ResultList, [&](const APlayerCharacter* Hero1, const APlayerCharacter* Hero2) {
		bool Result;
		SortPredicate(Hero1, Hero2, Result);
		return Result;
	});

	SortedHeroes = ResultList;

	return ResultList;
}

void AFormation::ClearSortedHeroes() {
	SortedHeroes.Empty();
}

void AFormation::SortPredicate_Implementation(const APlayerCharacter* Hero1, const APlayerCharacter* Hero2, bool& Result) const {
	Result = Hero1->GetHeroID() < Hero2->GetHeroID();
}

FVector AFormation::GetAdjustmentForCharacter(APlayerCharacter* Hero) const {
	int32 HeroIndex = SortedHeroes.Find(Hero);

	if (!GroupLocations.IsValidIndex(HeroIndex)) {
		return FVector::ZeroVector;
	}

	return FRotator(0.f, Yaw, 0.f).RotateVector(GroupLocations[HeroIndex]);
}

void AFormation::SetMasterLocation(FVector InLocation) {
	MasterLocation = InLocation;
}

void AFormation::Move(FVector Delta) {
	if (Delta == FVector::ZeroVector) {
		bHasMovement = false;
		return;
	}

	bHasMovement = true;

	TargetYaw = Delta.Rotation().GetNormalized().Yaw;

	UpdateGroupCenter();

	FVector NewLocation = MasterLocation + Delta;
	FVector Diff = NewLocation - GroupCenter;

	float Distance = Diff.Size2D();

	if (Distance > MaxDistance) {
		MasterLocation = GroupCenter + Diff.GetSafeNormal2D() * MaxDistance;
	} else {
		MasterLocation = NewLocation;
	}
}

FVector AFormation::GetMasterLocation() const {
	return MasterLocation;
}

void AFormation::UpdateGroupCenter() {
	UGameInstance* Instance = GetGameInstance();
	UGameGlobal* Global = Cast<UGameGlobal>(Instance);

	FVector NewCenter;
	if (!Global->GetHeroesLocation(NewCenter)) {
		return;
	}

	GroupCenter = NewCenter;
}
