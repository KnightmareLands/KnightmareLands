// Fill out your copyright notice in the Description page of Project Settings.

#include "Summon/SummonMeleeController.h"
#include "Summon/SummonedCharacter.h"
#include "Common/GameCharacter.h"
#include "Mobs/MobCharacter.h"
#include "Common/KLStatics.h"

void ASummonMeleeController::ProcessController(float DeltaSeconds) {
	UpdateMaster(DeltaSeconds);

	UpdateMovement(MasterLocation, false);

	PerformPlayerAction();
	PerformMovement();
}

void ASummonMeleeController::UpdateMaster(float DeltaTime) {
	auto CurrentPawn = GetPawn();

	if (!IsValid(CurrentPawn)) {
		return Super::UpdateMaster(DeltaTime);
	}

	ASummonedCharacter* Summon = Cast<ASummonedCharacter>(CurrentPawn);

	if (!IsValid(Summon)) {
		return Super::UpdateMaster(DeltaTime);
	}

	auto Master = Summon->GetMasterCharacter();
	if (!IsValid(Master)) {
		return Super::UpdateMaster(DeltaTime);
	}

	PositionAdjustment = Master->GetActorRightVector() * PositionOffset;
	MasterLocation = Master->GetActorLocation() + PositionAdjustment;
}

void ASummonMeleeController::UpdateMovement(FVector InCursorLocation, bool bIsHovered) {
	auto Summon = GetHero();
	if (!IsValid(Summon)) {
		return;
	}

	if (Summon->IsCharacterDead()) {
		StopMovement();
		return;
	}

	FVector SummonLocation = Summon->GetActorLocation();
	FVector MasterVector = (MasterLocation - SummonLocation);
	float Distance = MasterVector.Size2D();

	float LeashFactor = FMath::Clamp<float>(Distance / LeashDistance, 0.f, 1.f);

	SenseCenter = SummonLocation + MasterVector * LeashFactor;
	SenseCenter.Z = 50.f;

	// DrawDebugCircle(GetWorld(), SenseCenter, SenseRadius, 32, FColor(255, 0, 0), false, -1.f, 0, 0.f, FVector(0.f,1.f,0.f), FVector(1.f,0.f,0.f), false);

	FTargetingInfo TargetInfo;
	bHaveTarget = FindTarget(ETargetingType::TargetActor, false, TargetInfo);
	if (!bHaveTarget) {
		return; // Super::UpdateMovement(InCursorLocation, bIsHovered);
	}

	CurrentTarget = TargetInfo;

	// DrawDebugSphere(GetWorld(), Target->GetActorLocation(), 200, 64, FColor(255, 255, 0));
}

void ASummonMeleeController::PerformMovement() {
	auto Summon = GetHero();
	if (!IsValid(Summon)) {
		return;
	}

	FVector Destination;
	FVector SenseLocation;

	AActor* Target = bHaveTarget && CurrentTarget.Actors.Num() > 0 ? CurrentTarget.Actors[0] : nullptr;
	if (IsValid(Target)) {
		SenseLocation = Target->GetActorLocation();
		Destination = SenseLocation + (Summon->GetActorLocation() - SenseLocation).GetSafeNormal() * 100.f;
	} else {
		Destination = MasterLocation;
		SenseLocation = MasterLocation;
	}

	if (Summon->IsDebug()) {
		DrawDebugSphere(GetWorld(), Destination, 20, 32, FColor(255, 0, 0), false, 0.f);
	}

	SetFocalPoint(SenseLocation);
	MoveHeroToLocation(Destination, bHaveTarget);
}

bool ASummonMeleeController::FindTarget_Implementation(ETargetingType InType, bool bPositive, FTargetingInfo& OutTarget) {
	auto Summon = GetHero();
	if (!IsValid(Summon)) {
		return false;
	}

	TArray<AActor*> IgnoredActors;
	IgnoredActors.Add(Summon);

	TArray<AGameCharacter*> Targets;
	// TODO: replace this trace with ECS-based lookup? (ECS->GetOverlappedActors)
	if (UKLStatics::FindRadialTargets(this, SenseCenter, SenseRadius, IgnoredActors, true, Targets)) {
		float ClosestDistance = INDEX_NONE;
		AGameCharacter* ClosestTarget = nullptr;

		// TODO: calculate falloff value for every target
		for (auto GameCharacter : Targets) {
			AMobCharacter* Current = Cast<AMobCharacter>(GameCharacter);
			if (!IsValid(Current)) {
				continue;
			}

			float Distance = FVector::Dist(Current->GetActorLocation(), Summon->GetActorLocation());

			if (ClosestDistance == INDEX_NONE || Distance < ClosestDistance) {
				ClosestDistance = Distance;
				ClosestTarget = Current;
			}
		}

		if (IsValid(ClosestTarget) && !ClosestTarget->IsCharacterDead()) {
			TArray<AGameCharacter*> FoundTargets;
			FoundTargets.Add(Cast<AGameCharacter>(ClosestTarget));

			OutTarget = FTargetingInfo(FoundTargets);

			return true;
		}
	}

	return false;
}

bool ASummonMeleeController::IsManualTargetValid() const {
	if (!bHaveTarget) {
		return false;
	}

	if (!ensure(CurrentTarget.Actors.Num() > 0)) {
		return false;
	}

	// When bHaveTarget is true there are always should be an actor inside
	auto Target = CurrentTarget.Actors[0];

	if (!IsValid(Target) || Target->IsCharacterDead()) {
		return false;
	}

	return Super::IsManualTargetValid();
}

bool ASummonMeleeController::UpdateTargetingInfo(ETargetingType InType, bool bPositive, FTargetingInfo& OutTargetingInfo) {
	if (bHaveTarget) {
		OutTargetingInfo = CurrentTarget;
		return true;
	}

	return false;
}
