// Fill out your copyright notice in the Description page of Project Settings.

#include "Summon/SummonedCharacter.h"

void ASummonedCharacter::SetMasterCharacter(AGameCharacter* InMaster) {
	Master = InMaster;

	if (IsValid(Master)) {
		Master->RegisterSummonedCharacter(this);
	}
}

AGameCharacter* ASummonedCharacter::GetMasterCharacter() const {
	return Master;
}

void ASummonedCharacter::Death(float DamageDealt, AActor* DamageCauser, bool bLethalDamage) {
	Super::Death(DamageDealt, DamageCauser, bLethalDamage);

	SetLifeSpan(5.f);
}
