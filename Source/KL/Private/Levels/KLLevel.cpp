// Fill out your copyright notice in the Description page of Project Settings.

#include "Levels/KLLevel.h"
#include "Common/KLGameMode.h"
#include "SWManager.h"
#include "GameGlobal.h"

KL_API const FName PLAYER_TAG(TEXT("Player"));

void AKLLevel::BeginPlay() {
	Super::BeginPlay();

	if (!IsValid(ActivationVolume)) {
		UE_LOG(KLLog, Warning, TEXT("AKLLevel::PostInitializeComponents: ActivationVolume is not found; either you should add it or make a custom activation somehow!"));
		return;
	}

	ActivationVolume->OnActorBeginOverlap.AddDynamic(this, &AKLLevel::OnActivationOverlap);
	ActivationVolume->OnActorEndOverlap.AddDynamic(this, &AKLLevel::OnDeactivationOverlap);

	UGameGlobal* Global = Cast<UGameGlobal>(GetGameInstance());
	Global->OnLevelLoaded.AddDynamic(this, &AKLLevel::OnLevelLoaded);
}

void AKLLevel::OnLevelLoaded(EKLGameState NewState, bool bLoaded) {
	if (!bLoaded) {
		return;
	}

	bool bShouldActivate = false;

	// Check if player is already inside the level on loaded event
	TArray<AActor*> OverlappingActors;
	ActivationVolume->GetOverlappingActors(OverlappingActors);
	for (AActor* Actor : OverlappingActors) {
		if (Actor->ActorHasTag(PLAYER_TAG)) {
			UE_LOG(KLLog, Verbose, TEXT("AKLLevel::OnLevelLoaded: player found: %s"), *Actor->GetName());
			bShouldActivate = true;
			break;
		}
	}

	if (bShouldActivate) {
		ActivateLevel(ActivationVolume);
	}
}

void AKLLevel::OnActivationOverlap(AActor* OverlappedActor, AActor* OtherActor) {
	if (!OtherActor || !OtherActor->ActorHasTag(PLAYER_TAG)) {
		return;
	}

	ActivateLevel(OverlappedActor);
}

void AKLLevel::OnDeactivationOverlap(AActor* OverlappedActor, AActor* OtherActor) {
	if (!OtherActor || !OtherActor->ActorHasTag(PLAYER_TAG)) {
		return;
	}

	if (!bActivated) {
		UE_LOG(KLError, Error, TEXT("AKLLevel::OnDeactivationOverlap: Level %s is already deactivated!"), *GetName());
		return;
	}

	bActivated = false;
	LevelDeactivated();
}

void AKLLevel::ActivateLevel(AActor* LevelBounds) {
	if (bActivated) {
		UE_LOG(KLError, Error, TEXT("AKLLevel::ActivateLevel: Level %s is already activated!"), *GetName());
		return;
	}

	bActivated = true;
	LevelActivated();

	if (!IsValid(LevelBounds)) {
		UE_LOG(KLError, Error, TEXT("AKLLevel::ActivateLevel: LevelBounds actor should be valid!"));
		return;
	}

	// Any gameplay-related logic should not be triggered more than once
	if (bInited) {
		return;
	}

	bInited = true;

	FVector Origin;
	FVector Extent;
	LevelBounds->GetActorBounds(true, Origin, Extent);

	FVector Min = Origin - Extent;
	FVector Max = Origin + Extent;

	AKLGameMode* KLGameMode = (AKLGameMode*)GetWorld()->GetAuthGameMode();
	if (!ensure(KLGameMode)) {
		UE_LOG(KLError, Error, TEXT("AKLLevel::ActivateLevel: KLGameMode should always exist, wtf?!"));
		return;
	}

	FBox2D Bounds2D = FBox2D(FVector2D(Min.X, Min.Y), FVector2D(Max.X, Max.Y));
	KLGameMode->ActivateLevel(Bounds2D);

	if (bDisableSpawn || !IsValid(SpawnManager)) {
		LevelCompleted();
		return;
	}

	// TODO: add an option for SWManager to allow or disallow to re-start it
	SpawnManager->OnSpawnEnded.AddDynamic(this, &AKLLevel::LevelCompleted);

	LevelInited();
}

bool AKLLevel::IsActive() const {
	return bActivated;
}

void AKLLevel::LevelInited_Implementation() {
	SpawnManager->Activate();
}
