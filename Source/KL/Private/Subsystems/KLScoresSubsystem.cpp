// Fill out your copyright notice in the Description page of Project Settings.

#include "Subsystems/KLScoresSubsystem.h"
#include "Common/Attributes.h"
#include "Common/ScalingMath.h"

void UKLScoresSubsystem::Initialize(FSubsystemCollectionBase& Collection) {
	Multiplier = NewObject<UKLAttribute>();
}

void UKLScoresSubsystem::Deinitialize() {
	Reset(1, 0);
}

void UKLScoresSubsystem::Reset(int32 InScoresToFirstLevel, int32 InitialScores) {
	OnScoreAdded.Clear();
	OnSPSpent.Clear();
	OnLevelUp.Clear();

	Multiplier->Reset();
	Multiplier->Set(1.f);
	Score = 0;
	DebugScores = 0;
	ScoresToFirstLevel = InScoresToFirstLevel;
	CurrentLevel = 1;
	SkillPoints = InitialScores;
	SpentPoints = 0;
}

void UKLScoresSubsystem::AddScore(int32 ScoreToAdd, bool bIsDebug) {
	int32 MultipliedScores = ScoreToAdd * Multiplier->Get();
	Score += MultipliedScores;

	if (bIsDebug) {
		DebugScores += MultipliedScores;
	}

	OnScoreAdded.Broadcast(MultipliedScores);

	// Level up each tier (10, 20, 30, 50, 80, 130)
	int32 NewLevel = UScalingMath::GetTier(Score, ScoresToFirstLevel);

	// UE_LOG(KLDebug, Warning, TEXT("UGameGlobal::AddScore: NewLevel %d, Scores: %d"), NewLevel, Score);

	if (NewLevel > CurrentLevel) {
		int32 LevelUpCount = NewLevel - CurrentLevel;

		// In case of jump through multiple levels at once, call LevelUp for each level.
		for (int i = 0; i < LevelUpCount; ++i) {
			LevelUp();
		}

		// Trigger LevelUp event only once
		OnLevelUp.Broadcast(CurrentLevel);
	}
}

void UKLScoresSubsystem::LevelUp() {
	CurrentLevel++;

	// TODO: maybe need to change SP per level?
	SkillPoints++;
}


void UKLScoresSubsystem::SetSkillPoints(int32 InSkillPoints) {
	SkillPoints = InSkillPoints;
}

int32 UKLScoresSubsystem::GetSkillPoints() const {
	if (!ensure(SkillPoints >= 0)) {
		return 0;
	}

	return SkillPoints;
}

bool UKLScoresSubsystem::SpendSkillPoints() {
	if (SkillPoints == 0) {
		return false;
	}

	SkillPoints--;
	SpentPoints++;

	OnSPSpent.Broadcast(-1);

	return true;
}

int32 UKLScoresSubsystem::GetScore() const {
	return Score;
}

int32 UKLScoresSubsystem::GetLevel() const {
	return CurrentLevel;
}

float UKLScoresSubsystem::GetLevelPercent() const {
	float CurrentTierValue = UScalingMath::GetTierValue(CurrentLevel, ScoresToFirstLevel);
	float NextTierValue = UScalingMath::GetTierValue(CurrentLevel+1, ScoresToFirstLevel);

	return ((float)Score - CurrentTierValue) / (NextTierValue - CurrentTierValue);
}

int32 UKLScoresSubsystem::GetScoresToNextLevel() const {
	return UScalingMath::GetValueToNextTier(Score, ScoresToFirstLevel);
}

void SortItems(TArray<FKLScoresItem>& Items) {
	Algo::Sort(Items, [&](const FKLScoresItem& A, const FKLScoresItem& B) {
		return B.bSaved == A.bSaved ?
			(B.Scores == A.Scores ?
				(B.bWinner == A.bWinner ?
					(B.Duration == A.Duration ?
						(B.Level == A.Level ?
							(B.bRecent < A.bRecent) :
							B.Level < A.Level ) :
						B.Duration > A.Duration) :
					B.bWinner < A.bWinner) :
				B.Scores < A.Scores) :
			B.bSaved < A.bSaved;
	});
}

bool UKLScoresSubsystem::GetScoresList(TArray<FKLScoresItem>& OutScoresList) const {
	// TODO: implement online scores board
	UKLScoresList* ScoresList = Cast<UKLScoresList>(UGameplayStatics::CreateSaveGameObject(UKLScoresList::StaticClass()));
	auto LoadedData = UGameplayStatics::LoadGameFromSlot(ScoresList->SaveSlotName, 0);

	if (!LoadedData) {
		OutScoresList.Empty();
		return false;
	}

	ScoresList = Cast<UKLScoresList>(LoadedData);

	OutScoresList = ScoresList->Items;

	SortItems(OutScoresList);
	return true;
}

bool UKLScoresSubsystem::SaveScores(FName InPlayerName, bool bInWinner) {
	UKLScoresList* ScoresList = Cast<UKLScoresList>(UGameplayStatics::CreateSaveGameObject(UKLScoresList::StaticClass()));
	auto LoadedData = UGameplayStatics::LoadGameFromSlot(ScoresList->SaveSlotName, 0);

	if (LoadedData) {
		ScoresList = Cast<UKLScoresList>(LoadedData);
	}

	TArray<FKLScoresItem> Items = ScoresList->Items;
	for (auto& Item : Items) {
		Item.bRecent = false;
	}

	int32 MaxSavedItems = 10;

	if (DebugScores <= 0) {
		FKLScoresItem NewScoresItem;
		NewScoresItem.PlayerName = InPlayerName;
		NewScoresItem.Scores = Score;
		NewScoresItem.bWinner = bInWinner;
		NewScoresItem.PointsSpent = SpentPoints;
		NewScoresItem.Level = CurrentLevel;
		NewScoresItem.Duration = UGameplayStatics::GetRealTimeSeconds(this);
		NewScoresItem.bRecent = true;
		NewScoresItem.bSaved = true;
		Items.Add(NewScoresItem);

		SortItems(Items);

		Items.SetNum(MaxSavedItems, true);
	}

	ScoresList->Items = Items;
	UGameplayStatics::SaveGameToSlot(ScoresList, ScoresList->SaveSlotName, 0);

	return true;
}

bool UKLScoresSubsystem::UpdateRecentUsername(FName InPlayerName) {
	// TODO: implement online scores board
	UKLScoresList* ScoresList = Cast<UKLScoresList>(UGameplayStatics::CreateSaveGameObject(UKLScoresList::StaticClass()));
	auto LoadedData = UGameplayStatics::LoadGameFromSlot(ScoresList->SaveSlotName, 0);

	if (!LoadedData) {
		return false;
	}

	ScoresList = Cast<UKLScoresList>(LoadedData);
	TArray<FKLScoresItem> Items = ScoresList->Items;
	for (auto& Item : Items) {
		if (!Item.bRecent) {
			continue;
		}

		Item.PlayerName = InPlayerName;
		break;
	}

	ScoresList->Items = Items;

	UGameplayStatics::SaveGameToSlot(ScoresList, ScoresList->SaveSlotName, 0);
	return true;
}
