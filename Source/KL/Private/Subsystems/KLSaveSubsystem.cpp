// Fill out your copyright notice in the Description page of Project Settings.

#include "Subsystems/KLSaveSubsystem.h"
#include "Common/KLSave.h"

void UKLSaveSubsystem::Initialize(FSubsystemCollectionBase& Collection) {

}

void UKLSaveSubsystem::Deinitialize() {

}

bool UKLSaveSubsystem::GetSaveSlots(TArray<FKLSaveSlot>& OutSlots) const {
	UKLSavesList* SlotsList = Cast<UKLSavesList>(UGameplayStatics::CreateSaveGameObject(UKLSavesList::StaticClass()));
	auto LoadedData = UGameplayStatics::LoadGameFromSlot(SlotsList->SaveSlotName, UserIndex);

	if (!LoadedData) {
		OutSlots.Empty();
		return false;
	}

	SlotsList = Cast<UKLSavesList>(LoadedData);
	OutSlots = SlotsList->Slots;
	return true;
}

void UKLSaveSubsystem::LoadGame(const FString& SlotName) {
	// TODO: load saved slot

	// TODO: read data from the save

	// TODO: init all teh game state with loaded data
}

bool UKLSaveSubsystem::SaveGame(const FString& SlotName, const FString& SaveName, bool bForced) {
	// if (!bForced /* && is SlotNmae exist */) {
		// require bForced
	//	return false;
	//}

	UKLSavesList* SlotsList = Cast<UKLSavesList>(UGameplayStatics::CreateSaveGameObject(UKLSavesList::StaticClass()));
	auto LoadedData = UGameplayStatics::LoadGameFromSlot(SlotsList->SaveSlotName, UserIndex);

	if (LoadedData) {
		SlotsList = Cast<UKLSavesList>(LoadedData);
	}

	// TODO: save game to the slot

	// TODO: update SlotsList array
	FKLSaveSlot NewSavedSlot;
	NewSavedSlot.SlotName = SlotName;
	NewSavedSlot.SaveName = SaveName;

	SlotsList->Slots.Add(NewSavedSlot);
	UGameplayStatics::SaveGameToSlot(SlotsList, SlotsList->SaveSlotName, UserIndex);

	return true;
}
