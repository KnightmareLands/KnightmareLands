// Fill out your copyright notice in the Description page of Project Settings.

#include "Subsystems/KLPerksSubsystem.h"
#include "GameGlobal.h"
#include "Common/GameCharacter.h"
#include "Player/PlayerCharacter.h"
#include "Perks/Perk.h"
#include "Perks/PerkData.h"
#include "Perks/PerkManager.h"
#include "AssetRegistryModule.h"
#include "Common/KLGameMode.h"
#include "Subsystems/KLStateSubsystem.h"

void UKLPerksSubsystem::Initialize(FSubsystemCollectionBase& Collection) {
	PerksList = TArray<UPerkData*>();
	AvailablePerksList = TArray<UPerkData*>();

	const UClass* PerksClass = UPerkData::StaticClass();
	TArray<FAssetData> PerkAssets;

	FAssetRegistryModule& AssetRegistryModule = FModuleManager::LoadModuleChecked<FAssetRegistryModule>("AssetRegistry");
	if (!AssetRegistryModule.Get().GetAssetsByClass(PerksClass->GetFName(), PerkAssets)) {
		UE_LOG(KLLog, Error, TEXT("UKLPerksSubsystem::Initialize: Can't get perk assets!"));
		return;
	}

	for (FAssetData Asset : PerkAssets) {
		UPerkData* Perk = Cast<UPerkData>(Asset.GetAsset());

		if (IsValid(Perk) && Perk->bActive) {
			PerksList.Add(Perk);
		}
	}

	UE_LOG(KLLog, Log, TEXT("UKLPerksSubsystem::Initialize: Found %d perks!"), PerksList.Num());
}

void UKLPerksSubsystem::Deinitialize() {
	AvailablePerksList.Empty();
	PerksList.Empty();
}

void UKLPerksSubsystem::Reset(int32 InUpgradesAmount) {
	AvailablePerksList.Empty();
	BasicUpgradesAmount = InUpgradesAmount;
	UpgradesAmount = BasicUpgradesAmount;
}

bool UKLPerksSubsystem::GetAvailablePerks(const TArray<AGameCharacter*>& Heroes, TArray<UPerkData*>& OutPerks) {
	AKLGameMode* KLGameMode = (AKLGameMode*)GetWorld()->GetAuthGameMode();
	UPerkManager* GroupPerkManager = KLGameMode->GetPerkManager(AKLGameMode::GroupPerksID);

	// There are always should be a perk manager for the group
	ensure(GroupPerkManager);

	OutPerks.Empty(PerksList.Num());

	for (auto PerkData : PerksList) {

		UClass* PerkClass = PerkData->GetPerk();
		UPerk* PerkCDO = PerkClass->GetDefaultObject<UPerk>();

		bool bIsAvailable = false;

		switch (PerkData->Type) {
		case EPerkType::None:
			break;
		case EPerkType::Instant:
			// Instant perks are only depends on their own requirements
			bIsAvailable = PerkCDO->CanBeApplied(GetGameInstance()->GetSubsystem<UKLStateSubsystem>(), PerkData, nullptr);
			break;
		case EPerkType::Group: {
			// Group perks must be checked for grade since they are saved
			if (GroupPerkManager->GetPerkGrade(PerkData) < PerkData->MaxGrade) {
				bIsAvailable = PerkCDO->CanBeApplied(GetGameInstance()->GetSubsystem<UKLStateSubsystem>(), PerkData, nullptr);
			}

			break;
		}
		case EPerkType::Character:
			for (auto Hero : Heroes) {
				if (UPerkManager::IsAllowedForHost(PerkData, Hero)) {
					bIsAvailable = true;
					break;
				}
			}

			break;
		}

		if (bIsAvailable) {
			OutPerks.Add(PerkData);
		}
	}

	OutPerks.Shrink();
	return OutPerks.Num() > 0;
}

void UKLPerksSubsystem::UpdatePerksList() {
	AvailablePerksList.Empty();

	TArray<UPerkData*> Perks;
	TArray<AGameCharacter*> CurrentHeroes;

	UGameGlobal* GameGlobal = Cast<UGameGlobal>(GetGameInstance());

	auto PlayerCharacters = GameGlobal->GetHeroes();
	for (auto Hero : PlayerCharacters) {
		CurrentHeroes.Add(Hero);
	}

	if (!GetAvailablePerks(CurrentHeroes, Perks)) {
		return;
	}

	int32 LastIndex = Perks.Num() - 1;
	for (int32 i = 0; i <= LastIndex; ++i) {
		int32 Index = FMath::RandRange(i, LastIndex);

		if (i != Index) {
			Perks.SwapMemory(i, Index);
		}
	}

	int32 PerksAmount = FMath::Min(Perks.Num(), UpgradesAmount);
	for (int32 i = 0; i < PerksAmount; i++) {
		AvailablePerksList.Add(Perks[i]);
	}
}
