// Fill out your copyright notice in the Description page of Project Settings.

#include "Subsystems/KLGoalsSubsystem.h"

void UKLGoalsSubsystem::Initialize(FSubsystemCollectionBase& Collection) {}
void UKLGoalsSubsystem::Deinitialize() {}

void UKLGoalsSubsystem::SetGoal(FVector InGoal) {
	Goal = InGoal;
}

FVector UKLGoalsSubsystem::GetGoal() const {
	return Goal;
}
