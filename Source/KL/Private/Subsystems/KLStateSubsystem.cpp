// Fill out your copyright notice in the Description page of Project Settings.

#include "Subsystems/KLStateSubsystem.h"
#include "Common/KLGameMode.h"
#include "GameGlobal.h"
#include "Player/PlayerCharacter.h"

void UKLStateSubsystem::Initialize(FSubsystemCollectionBase& Collection) {}
void UKLStateSubsystem::Deinitialize() {}

bool UKLStateSubsystem::IsGroupDefined() const {
	AKLGameMode* KLGameMode = (AKLGameMode*)GetWorld()->GetAuthGameMode();

	return KLGameMode->IsHeroesDefined();
}

bool UKLStateSubsystem::HasDeadHeroes() const {
	UGameGlobal* Global = (UGameGlobal*)(GetGameInstance());

	TArray<APlayerCharacter*> Heroes = Global->GetHeroes();
	for (auto Hero : Heroes) {
		if (Hero->IsCharacterDead()) {
			return true;
		}
	}

	return false;
}
