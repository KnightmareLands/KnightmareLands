// Fill out your copyright notice in the Description page of Project Settings.

#include "Subsystems/KLSettingsSubsystem.h"
#include "GameFramework/GameUserSettings.h"

void UKLSettingsSubsystem::Initialize(FSubsystemCollectionBase& Collection) {
	auto Settings = UGameUserSettings::GetGameUserSettings();
	Settings->SetVSyncEnabled(true);
	Settings->SetShadowQuality(4);
	Settings->SetTextureQuality(4);
	Settings->SetVisualEffectQuality(4);
	Settings->SetViewDistanceQuality(4);

	FIntPoint Resolution = Settings->GetScreenResolution();
	EWindowMode::Type FullscreenMode = Settings->GetFullscreenMode();

	TArray<FIntPoint> Resolutions;

	bool bResolutionsFound;
	if (FullscreenMode == EWindowMode::Fullscreen) {
		bResolutionsFound = UKismetSystemLibrary::GetSupportedFullscreenResolutions(Resolutions);
	} else {
		bResolutionsFound = UKismetSystemLibrary::GetConvenientWindowedResolutions(Resolutions);
	}

	// DEBUG option
	bool bUseWidescreen = false;
	if (bUseWidescreen) {
		// Debug widescreen resolution
		FIntPoint Widescreen(1280, 548);
		UE_LOG(KLLog, Warning, TEXT("UKLSettingsSubsystem::Init: (debug) Set windowed resolution: %dx%d"), Widescreen.X, Widescreen.Y);

		Settings->SetScreenResolution(Widescreen);
		Settings->SetFullscreenMode(EWindowMode::Windowed);
	} else if (bResolutionsFound && Resolutions.Contains(Resolution)) {
		UE_LOG(KLLog, Log, TEXT("UKLSettingsSubsystem::Init: Set saved confirmed resolution: %dx%d"), Resolution.X, Resolution.Y);

		Settings->SetFullscreenMode(FullscreenMode);
		Settings->SetScreenResolution(Resolution);
	} else if (bResolutionsFound) {
		// Fallback if no settings saved:
		// Detect supported resolutions and take the biggest one not more than FullHD (~1920 by width)

		FIntPoint DefaultResolution = Resolutions[0];

		for (FIntPoint Res : Resolutions) {
			UE_LOG(KLLog, Log, TEXT("UKLSettingsSubsystem::Init: found resolution: %dx%d"), Res.X, Res.Y);

			if (Res.X > DefaultResolution.X && Res.X < 2000) {
				DefaultResolution = Res;
			}
		}

		UE_LOG(KLLog, Log, TEXT("UKLSettingsSubsystem::Init: Set default fullscreen resolution: %dx%d"), DefaultResolution.X, DefaultResolution.Y);

		Settings->SetFullscreenMode(FullscreenMode);
		Settings->SetScreenResolution(DefaultResolution);
	} else {
		// Fallback if nothing worked – do nothing, rely on the engine's defaults
		UE_LOG(KLLog, Warning, TEXT("UKLSettingsSubsystem::Init: Can't get supported fullscreen resolutions!"));

		// TODO: Ask user to choose resolution
	}

	Settings->ApplySettings(false);
}

void UKLSettingsSubsystem::Deinitialize() {

}
