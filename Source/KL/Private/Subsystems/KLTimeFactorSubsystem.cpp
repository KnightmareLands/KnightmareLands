// Fill out your copyright notice in the Description page of Project Settings.

#include "Subsystems/KLTimeFactorSubsystem.h"
#include "Kismet/GameplayStatics.h"

void UKLTimeFactorSubsystem::Initialize(FSubsystemCollectionBase& Collection) {
	Reset();
}

void UKLTimeFactorSubsystem::Deinitialize() {}

void UKLTimeFactorSubsystem::Tick(float DeltaTime) {
	auto World = GetWorld();
	if (!World || World->GetAuthGameMode()->IsPaused()) {
		return;
	}

	if (CurrentDilation == TargetDilation) {
		if (!bIsEnded) {
			bIsEnded = true;
			OnFadeComplete.Broadcast(TargetDilation == 0.f ? EKLDilationState::Pause : EKLDilationState::Game);
		}

		return;
	}

	float WorldDilation = World->GetWorldSettings()->TimeDilation;
	FadeDurationLeft = FMath::Max(0.f, FadeDurationLeft - DeltaTime);
	CurrentDilation = FMath::Lerp(StartDilation, TargetDilation, (FadeDuration - FadeDurationLeft) / FadeDuration);

	UpdateDilation();
}

void UKLTimeFactorSubsystem::SetDilation(float InTarget) {
	StartDilation = CurrentDilation;
	TargetDilation = InTarget;
	FadeDurationLeft = FadeDuration;

	OnFadeBegin.Broadcast(EKLDilationState::Game);

	bIsEnded = false;
}

void UKLTimeFactorSubsystem::UpdateDilation() {
	UGameplayStatics::SetGlobalTimeDilation(GetWorld(), CurrentDilation);
}

void UKLTimeFactorSubsystem::Reset() {
	OnFadeBegin.Clear();
	OnFadeComplete.Clear();

	Mode = EKLTimeMode::Normal;
	TargetDilation = CurrentDilation = StartDilation = NormalFactor;
	FadeDurationLeft = 0;
	SlowDuration = 0;

	UpdateDilation();

	ResetTimer();
}

float UKLTimeFactorSubsystem::GetEnemyMovementFactor() const {
	switch (Mode) {
	case EKLTimeMode::Slow:
		return EnemyMovementFactor;
	}

	return NormalFactor;
}

float UKLTimeFactorSubsystem::GetPlayerMovementFactor() const {
	switch (Mode) {
	case EKLTimeMode::Slow:
		return PlayerMovementFactor;
	}

	return NormalFactor;
}

float UKLTimeFactorSubsystem::GetEnemyAttackFactor() const {
	switch (Mode) {
	case EKLTimeMode::Slow:
		return EnemyAttackFactor;
	}

	return NormalFactor;
}

float UKLTimeFactorSubsystem::GetPlayerAttackFactor() const {
	switch (Mode) {
	case EKLTimeMode::Slow:
		return PlayerAttackFactor;
	}

	return NormalFactor;
}

void UKLTimeFactorSubsystem::SetNormal() {
	Mode = EKLTimeMode::Normal;
	SlowDuration = 0;

	SetDilation(NormalFactor);
}

void UKLTimeFactorSubsystem::AddSlowDown(float InDuration) {
	check(InDuration > 0.f);

	SlowDuration += InDuration * Factor;
	StartTime = GetWorld()->GetTimeSeconds();

	Mode = EKLTimeMode::Slow;

	SetDilation(Factor);

	ResetTimer();

	FTimerDelegate Delegate = FTimerDelegate::CreateUObject(this, &UKLTimeFactorSubsystem::SetNormal);
	GetWorld()->GetTimerManager().SetTimer(TimerHandle, Delegate, SlowDuration, false);
}

void UKLTimeFactorSubsystem::ResetTimer() {
	if (TimerHandle.IsValid()) {
		GetWorld()->GetTimerManager().ClearTimer(TimerHandle);
		TimerHandle.Invalidate();
	}
}

float UKLTimeFactorSubsystem::GetDurationLeft() const {
	float Now = GetWorld()->GetTimeSeconds();
	return FMath::Clamp(1 - ((Now - StartTime) / SlowDuration), 0.f, 1.f);
}

void UKLTimeFactorSubsystem::FadeToPause() {
	SetDilation(0.f);
}

void UKLTimeFactorSubsystem::FadeToGame() {
	SetDilation(Mode == EKLTimeMode::Slow ? Factor : NormalFactor);
}
