// Fill out your copyright notice in the Description page of Project Settings.

#include "Mobs/MobCharacter.h"
#include "Player/PlayerCharacter.h"
#include "GameGlobal.h"
#include "Common/HealthComponent.h"
#include "Common/ScalingMath.h"
#include "Core/ECSEngine.h"
#include "ECS/ECSMovementComponent.h"
#include "Common/KLGameMode.h"

// Sets default values
AMobCharacter::AMobCharacter() {
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
}

void AMobCharacter::PostInitializeComponents() {
	BaseWalkSpeed = BaseWalkSpeed * FMath::FRandRange(0.9f, 1.1f);

	Super::PostInitializeComponents();
}

// Called when the game starts or when spawned
void AMobCharacter::BeginPlay() {
	Super::BeginPlay();
}

void AMobCharacter::InitializeAttributes(bool bInit) {
	// The method is called on init and on each level up

	// Increase mobs speed with time
	float ScaleFactor = 1.f;

	// Fix crash in editor when no game mode instance available
	AKLGameMode* KLGameMode = Cast<AKLGameMode>(GetWorld()->GetAuthGameMode());
	if (IsValid(KLGameMode)) {
		ScaleFactor = KLGameMode->GetMobsScale();
	}

	float HealthBuff = UScalingMath::GetHealthBuff(MobType);
	if (HealthBuff > 0.f) {
		AttributeHealth->Add(EKLModifierType::Percent, HealthBuff);
	}

	// TODO: unify GetScaledValue and Attributes mechanic
	HealthComponent->SetInitialHealth(UScalingMath::GetScaledValue(AttributeHealth->Get(), Level, ScaleFactor));

	float SpeedBuff = UScalingMath::GetSpeedBuff(MobType);
	if (SpeedBuff > 0.f) {
		AttributeWalkSpeed->Add(EKLModifierType::Percent, SpeedBuff);
	}

	if (DefaultSteeringRate == INDEX_NONE) {
		DefaultSteeringRate = ECSMovementComponent->GetSteeringRate();
	}

	float SteeringScaleFactor = ScaleFactor * ScaleFactor;
	float SteeringMultiplier = (SpeedBuff / 100) + 1.f;
	ECSMovementComponent->SetSteeringRate(DefaultSteeringRate * SteeringMultiplier * SteeringScaleFactor);

	float AttackBuff = UScalingMath::GetAttackBuff(MobType);
	// TODO: increase attack power

	// TODO: add attack speed multiplier

	Super::InitializeAttributes(bInit);
}

float AMobCharacter::TakeDamage(float Damage, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser) {
	float TakenDamage = Super::TakeDamage(Damage, DamageEvent, EventInstigator, DamageCauser);

	if (!ensure(EventInstigator)) {
		UE_LOG(KLError, Error, TEXT("AMobCharacter::TakeDamage: EventInstigator must not be null"));
	}

	return TakenDamage;
}

void AMobCharacter::Death(float DamageDealt, AActor* DamageCauser, bool bLethalDamage) {
	Super::Death(DamageDealt, DamageCauser, bLethalDamage);

	SetLifeSpan(5.f);

	auto CurrentController = GetController();
	if (CurrentController) {
		CurrentController->UnPossess();
		CurrentController->Destroy();
	}
}

int32 AMobCharacter::GetScores() const {
	float Multiplier = UScalingMath::GetScoresMultiplier(MobType);

	return FMath::RoundToInt((float)BaseScores * Multiplier);
}
