// Fill out your copyright notice in the Description page of Project Settings.

#include "Mobs/MobController.h"
#include "Common/GameCharacter.h"
#include "ECS/ECSComponents.h"
#include "Mobs/MobCharacter.h"
#include "Player/KLPlayerController.h"
#include "Skills/SkillLauncher.h"

#include "Core/ECSEngine.h"

#include "Kismet/GameplayStatics.h"

AMobController::AMobController() {}

bool AMobController::ECSPostRegister() {
	AActor* CurrentPawn = GetPawn();

	AGameCharacter* CurrentCharacter = Cast<AGameCharacter>(CurrentPawn);
	float VisibilityDistance = CurrentCharacter->GetDetectDistance();

	// Get attack distance from the skill data
	auto Skill = CurrentCharacter->GetValidSkill(0);
	if (IsValid(Skill) && Skill->IsInited()) {
		AttackDistance = Skill->GetAttackRange();
		ActionType = Skill->GetSkillType();
	}

	UECSEngine* ECS = IECSEntity::GetEngine(CurrentPawn);
	const int32 ECSIndex = IECSEntity::GetEntity(CurrentPawn);

	FECSDelegate& DataDelegate = ECS->Get<FECSDelegate>(ECSIndex);
	DataDelegate.AIDelegate = this;

	FECSDataSense& DataSense = ECS->Get<FECSDataSense>(ECSIndex);
	DataSense.bEnabled = true;

	FECSDataAI& DataAI = ECS->Get<FECSDataAI>(ECSIndex);
	DataAI.bEnabled = true;
	DataAI.Group = GroupID;
	DataAI.RoamingDistance = RoamingDistance;
	DataAI.AttackDistance = AttackDistance;
	DataAI.VisibilityDistance = VisibilityDistance;
	DataAI.ActionType = ActionType;
	// MovementType ?

	return Super::ECSPostRegister();
}

bool AMobController::ECSPostUnregister() {
	UECSEngine* ECS = IECSEntity::GetEngine(GetPawn());
	const int32 ECSIndex = IECSEntity::GetEntity(GetPawn());

	FECSDelegate& DataDelegate = ECS->Get<FECSDelegate>(ECSIndex);
	DataDelegate.AIDelegate = nullptr;

	FECSDataAI& DataAI = ECS->Get<FECSDataAI>(ECSIndex);
	DataAI.bEnabled = false;

	FECSDataSense& DataSense = ECS->Get<FECSDataSense>(ECSIndex);
	DataSense.bEnabled = false;
	DataSense.bSenseTarget = false;

	return Super::ECSPostUnregister();
}

bool AMobController::PerformAction(AActor* TargetActor, FVector TargetLocation) {
	auto CurrentPawn = GetPawn();

	if (!IsValid(CurrentPawn)) {
		return false;
	}

	// Prevent attack while mob is spawning
	AMobCharacter* CurrentMob = Cast<AMobCharacter>(CurrentPawn);
	if (IsValid(CurrentMob) && !CurrentMob->bMobReady) {
		return false;
	}

	bool bCanPerform = false;

	if (bCanOnlyPerformOnScreen) {
		auto PC = Cast<AKLPlayerController>(UGameplayStatics::GetPlayerController(GetWorld(), 0));
		bCanPerform = IsValid(PC) && PC->IsLocationOnScreen(CurrentPawn->GetActorLocation());

		// If mob have already attacked then it will not stop even after going off screen
		if (bCanPerform) {
			bCanOnlyPerformOnScreen = false;
		}
	} else {
		bCanPerform = true;
	}

	if (bCanPerform) {
		return Super::PerformAction(TargetActor, TargetLocation);
	}

	return false;
}

bool AMobController::SetMaster(int32 MasterEntity) {
	if (!bIsECSActive) {
		return false;
	}

	UECSEngine* ECS = IECSEntity::GetEngine(GetPawn());
	const int32 ECSIndex = IECSEntity::GetEntity(GetPawn());

	FECSDataAI& DataAI = ECS->Get<FECSDataAI>(ECSIndex);
	DataAI.MasterEntity = MasterEntity;

	FECSDataMovement& DataMovement = ECS->Get<FECSDataMovement>(ECSIndex);
	DataMovement.MasterEntity = MasterEntity;

	FECSDataBoid& DataBoid = ECS->Get<FECSDataBoid>(ECSIndex);
	DataBoid.bEnabled = true;

	return true;
}

bool AMobController::LeaveMaster() {
	if (!bIsECSActive) {
		return false;
	}

	UECSEngine* ECS = IECSEntity::GetEngine(GetPawn());
	const int32 ECSIndex = IECSEntity::GetEntity(GetPawn());

	FECSDataAI& DataAI = ECS->Get<FECSDataAI>(ECSIndex);
	DataAI.MasterEntity = INDEX_NONE;

	FECSDataMovement& DataMovement = ECS->Get<FECSDataMovement>(ECSIndex);
	DataMovement.MasterEntity = INDEX_NONE;

	return true;
}
