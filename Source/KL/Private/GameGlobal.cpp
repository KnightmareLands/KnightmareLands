// Fill out your copyright notice in the Description page of Project Settings.

#include "GameGlobal.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Common/GameCharacter.h"
#include "Player/PlayerCharacter.h"
#include "Skills/SkillLauncher.h"
#include "Formations/Formation.h"
#include "Common/KLLoader.h"
#include "Subsystems/KLScoresSubsystem.h"
#include "Subsystems/KLPerksSubsystem.h"
#include "Subsystems/KLTimeFactorSubsystem.h"

void UGameGlobal::Init() {
	Loader = NewObject<UKLLoader>(this);
	UKLLoader::LoaderInstance = Loader;

	LoadConfig();

	Super::Init();

	HeroesList = TMap<int32, APlayerCharacter*>();
	HeroesList.Reserve(4);
	CharactersMap = TMap<FString, AGameCharacter*>();
	Heroes = TArray<int32>();
}

void UGameGlobal::CallSaveConfig() {
	SaveConfig();
}

void UGameGlobal::OnStart() {
	Super::OnStart();

	Reset();
	ResetHeroes();
}

bool UGameGlobal::IsDebug() const {
	return bIsDebug;
}

void UGameGlobal::ResetHeroes() {
	HeroesList.Empty();
	Heroes.Empty();
	Heroes.Add(0);
	Heroes.Add(0);
	Heroes.Add(0);
	Heroes.Add(0);
}

void UGameGlobal::Reset() {
	CurrentState = EKLGameState::MainMenu;
	CharactersMap.Empty();

	GetSubsystem<UKLScoresSubsystem>()->Reset(ScoresToFirstLevel, InitialSkillPoints);
	GetSubsystem<UKLPerksSubsystem>()->Reset(BasicUpgradesAmount);
	GetSubsystem<UKLTimeFactorSubsystem>()->Reset();

	OnReset();
}

void UGameGlobal::TriggerGameStateChange() {
	OnGameStateChanged.Broadcast(CurrentState, !bIsLoading);
}

EKLGameState UGameGlobal::GetGameState() const {
	return CurrentState;
}

bool UGameGlobal::IsGameOver() const {
	return CurrentState == EKLGameState::GameOver;
}

bool UGameGlobal::IsGameStarted() const {
	return CurrentState == EKLGameState::InGame || CurrentState == EKLGameState::GameOver;
}

bool UGameGlobal::IsInGame() const {
	return CurrentState == EKLGameState::InGame;
}

void UGameGlobal::SetGameOver() {
	CurrentState = EKLGameState::GameOver;
	TriggerGameStateChange();
}

void UGameGlobal::SetGameStarted() {
	CurrentState = EKLGameState::InGame;
	TriggerGameStateChange();
}

void UGameGlobal::SetLoaded(bool bLoaded) {
	bool bNeedTrigger = bIsLoading == bLoaded;
	bIsLoading = !bLoaded;

	if (bNeedTrigger) {
		TriggerGameStateChange();

		if (bLoaded) {
			OnLevelLoaded.Broadcast(CurrentState, bLoaded);
		}
	}
}

void UGameGlobal::SetDemoMode() {
	CurrentState = EKLGameState::Demo;
	SetLoaded(true);
}

bool UGameGlobal::IsLoading() const {
	return bIsLoading;
}

void UGameGlobal::AddCharacter(AGameCharacter* Character) {
	if (Character == NULL || !IsValid(Character)) {
		return;
	}

	FString Name = Character->GetName();
	CharactersMap.Emplace(Name, Character);
}

void UGameGlobal::RemoveCharacter(AGameCharacter* Character) {
	if (Character == NULL || !IsValid(Character)) {
		return;
	}

	FString Name = Character->GetName();
	CharactersMap.Remove(Name);
}

AGameCharacter* UGameGlobal::GetCharacterByName(FString Name) {
	return CharactersMap[Name];
}

bool UGameGlobal::IsNight() const {
	return bIsNight;
}

APlayerCharacter* UGameGlobal::GetHero(int32 HeroID) const {
	if (!HeroesList.Contains(HeroID)) {
		UE_LOG(KLError, Error, TEXT("UGameGlobal::GetHero: Not valid HeroID"));
		return nullptr;
	}

	return HeroesList[HeroID];
}

void UGameGlobal::SetGroupFormation(AFormation* InFormation) {
	GroupFormation = InFormation;
}

AFormation* UGameGlobal::GetGroupFormation() const {
	return GroupFormation;
}

bool UGameGlobal::IsZoomEnabled() const {
	return bZoomEnabled;
}

int32 UGameGlobal::GetZoomLevel() const {
	return ZoomLevel;
}

float UGameGlobal::GetHoldDelay() const {
	return SkillButtonHoldDelay;
}

float UGameGlobal::GetDetectionDistance() const {
	return DetectionDistance;
}

TArray<APlayerCharacter*> UGameGlobal::GetHeroes(bool bAlive) const {
	TArray<APlayerCharacter*> HeroesArray;
	HeroesList.GenerateValueArray(HeroesArray);

	if (bAlive == false) {
		return HeroesArray;
	}

	TArray<APlayerCharacter*> HeroesAlive;
	for (auto Hero : HeroesArray) {
		if (Hero->IsCharacterDead()) {
			continue;
		}

		HeroesAlive.Add(Hero);
	}

	return HeroesAlive;
}

bool UGameGlobal::GetHeroesLocation(FVector& OutLocation) const {
	TArray<APlayerCharacter*> HeroesAlive = GetHeroes(true);
	int32 HeroesAliveCount = HeroesAlive.Num();

	if (HeroesAliveCount == 0) {
		return false;
	}

	FVector GroupCenter = FVector::ZeroVector;
	for (auto Hero : HeroesAlive) {
		GroupCenter = GroupCenter + Hero->GetHeroLocation();
	}

	OutLocation = GroupCenter / HeroesAliveCount;
	return true;
}

TMap<EMobType, float> UGameGlobal::GetTypesWeight() const {
	return TypesWeights;
}

void UGameGlobal::AddResurector() {
	ResurectorCount++;
}

void UGameGlobal::RemoveResurector() {
	check(ResurectorCount > 0);
	ResurectorCount--;
}

bool UGameGlobal::HasResurectors() const {
	return ResurectorCount > 0;
}

float UGameGlobal::GetHeroesSpeed() const {
	TArray<APlayerCharacter*> HeroesArray;
	HeroesList.GenerateValueArray(HeroesArray);

	if (HeroesList.Num() == 0) {
		return 0.f;
	}

	float BasicSpeed = -1.f;

	for (auto Hero : HeroesArray) {
		if (Hero->IsCharacterDead()) {
			continue;
		}

		float CurrentSpeed = Hero->GetSpeed();

		if (BasicSpeed < 0.f) {
			BasicSpeed = CurrentSpeed;
		} else {
			// Looking for the minimal speed, because of the melee characters dash speed buffs
			BasicSpeed = FMath::Min<float>(BasicSpeed, CurrentSpeed);
		}

	}

	return BasicSpeed;
}

void UGameGlobal::SpeedUpdate() {
	float Speed = GetHeroesSpeed();
	OnSpeedUpdate.Broadcast(Speed);
}

UKLLoader* UGameGlobal::GetLoader() const {
	return Loader;
}
