// Fill out your copyright notice in the Description page of Project Settings.

#include "KLAnimation.h"
#include "Common/GameCharacter.h"
#include "Mobs/MobCharacter.h"

void UKLAnimation::NativeBeginPlay() {
	CurrentCharacter = Cast<AGameCharacter>(TryGetPawnOwner());
	if (!IsValid(CurrentCharacter)) {
		return;
	}

	bIsReady = true;
	bIsMob = bIsReady && CurrentCharacter->IsA(AMobCharacter::StaticClass());

	CurrentCharacter->OnCharacterDead.AddUObject(this, &UKLAnimation::OnDeath);
	CurrentCharacter->OnCharacterRevive.AddUObject(this, &UKLAnimation::OnRevive);
}

void UKLAnimation::NativeUpdateAnimation(float DeltaSeconds) {
	if (!bIsReady || bIsDead) {
		return;
	}

	Speed = CurrentCharacter->GetCurrentSpeed();
	bIsMoving = Speed > 0.f;

	if (bIsMob) {
		bSpawnCompleted = Cast<AMobCharacter>(CurrentCharacter)->bMobReady;
	}
}

void UKLAnimation::OnDeath(AGameCharacter* DeadOwner) {
	bIsDead = true;
	Speed = 0.f;
	bIsMoving = false;
}

void UKLAnimation::OnRevive(AGameCharacter* AliveOwner) {
	bIsDead = false;
}
