// Fill out your copyright notice in the Description page of Project Settings.

#include "ECS/ECSSystemCollision.h"
#include "Common/CollisionChannels.h"
#include "Common/KLStatics.h"
#include "ECS/ECSComponents.h"
#include "ECS/ECSObject.h"
#include "ECS/ECSObjectComponent.h"
#include "ECS/ECSTypes.h"

#include "Core/ECSEngine.h"

DEFINE_STAT(STAT_ECSSystemCollision);
DEFINE_STAT(STAT_ECSSystemCollisionTreeBuild);
DEFINE_STAT(STAT_ECSSystemCollisionTreeGet);
#if STATS
TStatId UECSSystemCollision::GetStatId() const {
	return GET_STATID(STAT_ECSSystemCollision);
}
#endif

KL_API const FBox2D UECSSystemCollision::QUAD_TREE_BOUNDS(FVector2D(-1000000, -1000000), FVector2D(1000000, 1000000));
KL_API const float UECSSystemCollision::QUAD_MIN_SIZE = 1.f;

UECSSystemCollision::UECSSystemCollision() :
	QuadTree(UECSSystemCollision::QUAD_TREE_BOUNDS, UECSSystemCollision::QUAD_MIN_SIZE)
{}

void UECSSystemCollision::RequestInit() {
	Init<FECSDataCollision>();
}

void UECSSystemCollision::OnRegistered(int32 Entity) {
	const FECSDataCollision& CollisionData = Get<FECSDataCollision>(Entity);

	float NewRadius = CollisionData.Radius * 2;
	if (NewRadius > MaxRadius) {
		MaxRadius = NewRadius;
	}
}

void UECSSystemCollision::Process(float DeltaTime) {
	{
		SCOPE_CYCLE_COUNTER(STAT_ECSSystemCollisionTreeBuild);

		QuadTree.Empty();

		for (int32 Entity : Entities) {
			FECSDataCollision& CollisionData = Get<FECSDataCollision>(Entity);
			FECSDataPosition& PositionData = Get<FECSDataPosition>(Entity);
			if (!CollisionData.bEnabled || !CollisionData.bGenerateOverlaps) {
				continue;
			}

			float Radius = CollisionData.Radius;
			FVector2D Location = FVector2D(PositionData.Location.X, PositionData.Location.Y);
			FBox2D EntityBox(Location - Radius, Location + Radius);
			QuadTree.Insert(Entity, EntityBox);
		}
	}

	Super::Process(DeltaTime);
}

bool UECSSystemCollision::CanProcess(int32 Entity) const {
	const FECSDataCollision& CollisionData = Get<FECSDataCollision>(Entity);

	return CollisionData.bEnabled && CollisionData.bGenerateOverlaps;
}

void UECSSystemCollision::ProcessEntity(int32 Entity, float DeltaTime) {
	FECSDataCollision& CollisionData = Get<FECSDataCollision>(Entity);
	FECSDataPosition& PositionData = Get<FECSDataPosition>(Entity);

	CollisionData.PendingBeginOverlaps.Reset();
	CollisionData.PendingEndOverlaps.Reset();

	FVector2D Location = FVector2D(PositionData.Location.X, PositionData.Location.Y);
	FBox2D EntityBox(Location - MaxRadius, Location + MaxRadius);

	TArray<int32> IntersectingEntities;
	{
		SCOPE_CYCLE_COUNTER(STAT_ECSSystemCollisionTreeGet);
		QuadTree.GetElements(EntityBox, IntersectingEntities);
	}

	TArray<int32> NewOverlaps;
	NewOverlaps.Reserve(IntersectingEntities.Num());

	for (int32 OtherIndex : IntersectingEntities) {
		if (OtherIndex == Entity) {
			continue;
		}

		FECSDataCollision& OtherCollisionData = Get<FECSDataCollision>(OtherIndex);
		FECSDataPosition& OtherPositionData = Get<FECSDataPosition>(OtherIndex);

		if ((OtherPositionData.Location - PositionData.Location).Size2D() < (OtherCollisionData.Radius + CollisionData.Radius)) {
			NewOverlaps.Add(OtherIndex);

			int32 OldIndex = CollisionData.OverlappedEntities.Find(OtherIndex);
			// If overlap was not exist before – add BeginOverlap event
			if (OldIndex == INDEX_NONE) {
				CollisionData.PendingBeginOverlaps.Add(OtherIndex);
			}
		}
	}

	// Check for previous overlaps if they are still valid
	for (int32 OldOverlapIndex : CollisionData.OverlappedEntities) {
		int32 NewIndex = NewOverlaps.Find(OldOverlapIndex);

		// If the old overlap is not exist anymore – add EndOverlap event
		if (NewIndex == INDEX_NONE) {
			CollisionData.PendingEndOverlaps.Add(OldOverlapIndex);
		}
	}

	CollisionData.OverlappedEntities.Reset();
	CollisionData.OverlappedEntities.Append(NewOverlaps);
}

/*void UECSSystemCollision::ProcessCollision_RadiusCheck3D(float DeltaTime) {
	SCOPE_CYCLE_COUNTER(STAT_ECSCollisionTime);

	int32 Count = Delegates.Num();

	const float MaxRadius = MaxCollisionRadius * 2;

	for (int32 Index = 0; Index < Count; Index++) {
		FECSDataCollision& CollisionData = Get<FECSDataCollision>(Entity);
		FECSDataPosition& PositionData = Get<FECSDataPosition>(Entity);

		if (!CollisionData.bEnabled) {
			continue;
		}

		for (int32 OtherIndex = 0; OtherIndex < Count; OtherIndex++) {
			if (OtherIndex == Index) {
				continue;
			}

			FECSDataCollision& OtherCollisionData = Get<FECSDataCollision>(OtherIndex);
			FECSDataPosition& OtherPositionData = Get<FECSDataPosition>(OtherIndex);

			if ((OtherPositionData.Location - PositionData.Location).Size2D() < (OtherCollisionData.Radius + CollisionData.Radius)) {
				CollisionData.PendingBeginOverlaps.AddUnique(OtherIndex);
			}
		}
	}
}*/

/*void UECSSystemCollision::ProcessCollision_PhysX(float DeltaTime) {
	SCOPE_CYCLE_COUNTER(STAT_ECSCollisionTime);

	auto World = GetWorld();
	auto ClassToFind = AGameCharacter::StaticClass();

	int32 Count = Delegates.Num();
	for (int32 Index = 0; Index < Count; Index++) {
		FECSDataCollision& CollisionData = Get<FECSDataCollision>(Entity);
		FECSDataPosition& PositionData = Get<FECSDataPosition>(Entity);

		if (!CollisionData.bEnabled) {
			continue;
		}

		TArray<AActor*> FoundActors;
		TArray<TEnumAsByte<EObjectTypeQuery>> ObjectTypes;
		TArray<AActor*> ActorsToIgnore;
		if (UKismetSystemLibrary::SphereOverlapActors(World, PositionData.Location, CollisionData.Radius, ObjectTypes, ClassToFind, ActorsToIgnore, FoundActors)) {
			for (auto Actor : FoundActors) {
				AGameCharacter* Char = Cast<AGameCharacter>(Actor);
				if (Char != NULL) {
					CollisionData.PendingBeginOverlaps.AddUnique(Char->GetECSIndex());
				}
			}
		}
	}
}*/
