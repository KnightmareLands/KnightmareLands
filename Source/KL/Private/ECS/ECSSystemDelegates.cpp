// Fill out your copyright notice in the Description page of Project Settings.

#include "ECS/ECSSystemDelegates.h"
#include "ECS/ECSCollisionComponent.h"
#include "ECS/ECSComponents.h"
#include "ECS/ECSController.h"
#include "ECS/ECSMovementComponent.h"
#include "ECS/ECSTypes.h"

#include "Core/ECSEngine.h"

DEFINE_STAT(STAT_ECSSystemDelegates);
DEFINE_STAT(STAT_ECSSystemDelegatesAI);
DEFINE_STAT(STAT_ECSSystemDelegatesMovement);
DEFINE_STAT(STAT_ECSSystemDelegatesCollision);

#if STATS
TStatId UECSSystemDelegates::GetStatId() const {
	return GET_STATID(STAT_ECSSystemDelegates);
}
#endif

void UECSSystemDelegates::RequestInit() {
	Init<FECSDelegate, FECSDataPosition, FECSDataAI, FECSDataSense, FECSDataCollision>();
}

void UECSSystemDelegates::Process(float DeltaTime) {
	if (!ECS) {
		// Should never happens
		return;
	}

	// Copy entities list since it can be changed during the process
	TArray<int32> CachedEntities = Entities;

	// UE4 Delegates must be updated in the main game thread so no ParallelFor
	for (int32 Entity : CachedEntities) {
#if STATS
		FScopeCycleCounter SystemStats(GetStatId());
#endif
		ProcessEntity(Entity, DeltaTime);
	}
}

void UECSSystemDelegates::ProcessEntity(int32 Entity, float DeltaTime) {
	FECSDelegate& Delegate = Get<FECSDelegate>(Entity);

	// Process Position delegate
	if (IsValid(Delegate.PositionDelegate)) {
		SCOPE_CYCLE_COUNTER(STAT_ECSSystemDelegatesMovement);

		FECSDataPosition& PositionData = Get<FECSDataPosition>(Entity);

		if (PositionData.bNeedUpdate) {
			if (PositionData.Orientation == EECSOrientation::None) {
				Delegate.PositionDelegate->UpdateOwnerLocation(PositionData.Location);
			} else {
				Delegate.PositionDelegate->UpdateOwnerLocationAndRotation(PositionData.Location, FRotator(0.f, PositionData.Yaw, 0.f));
			}

			const FECSDataTarget& TargetData = Get<FECSDataTarget>(Entity);
			if (PositionData.bDestinationReached && !PositionData.bDestinationReachedSent && TargetData.bSendReachedEvent) {
				Delegate.PositionDelegate->OnTargetReached();
			}
		}
	}

	// Process AI delegate
	if (IsValid(Delegate.AIDelegate)) {
		SCOPE_CYCLE_COUNTER(STAT_ECSSystemDelegatesAI);

		FECSDataAI& AIData = Get<FECSDataAI>(Entity);
		FECSDataSense& SenseData = Get<FECSDataSense>(Entity);
		switch (AIData.State) {
		case EECSAIState::AttackOnOpportunity:
		case EECSAIState::AttackByAggression:
		case EECSAIState::Attack:
			Delegate.AIDelegate->PerformAction(SenseData.Actor, SenseData.Location);
		}
	}

	if (IsValid(Delegate.CollisionDelegate)) {
		SCOPE_CYCLE_COUNTER(STAT_ECSSystemDelegatesCollision);

		FECSDataCollision& CollisionData = Get<FECSDataCollision>(Entity);

		int32 EndOverlapsNum = CollisionData.PendingEndOverlaps.Num();
		if (EndOverlapsNum > 0) {
			TArray<AActor*> OtherActors;
			OtherActors.Reserve(EndOverlapsNum);
			for (int32 OtherIndex : CollisionData.PendingEndOverlaps) {
				OtherActors.Add(Get<FECSDelegate>(OtherIndex).Owner);
			}

			CollisionData.PendingEndOverlaps.Reset();

			Delegate.CollisionDelegate->OnEndOverlap(OtherActors);
		}

		int32 BeginOverlapsNum = CollisionData.PendingBeginOverlaps.Num();
		if (BeginOverlapsNum > 0) {
			TArray<AActor*> OtherActors;
			OtherActors.Reserve(BeginOverlapsNum);
			for (int32 OtherIndex : CollisionData.PendingBeginOverlaps) {
				OtherActors.Add(Get<FECSDelegate>(OtherIndex).Owner);
			}

			CollisionData.PendingBeginOverlaps.Reset();

			Delegate.CollisionDelegate->OnBeginOverlap(OtherActors);
		}
	}
}
