// Fill out your copyright notice in the Description page of Project Settings.

#include "ECS/ECSSystemMovement.h"
#include "Common/CollisionChannels.h"
#include "Common/KLStatics.h"
#include "ECS/ECSComponents.h"
#include "ECS/ECSObject.h"
#include "ECS/ECSObjectComponent.h"
#include "ECS/ECSTypes.h"
#include "Subsystems/KLTimeFactorSubsystem.h"

#include "API/ECSEntity.h"
#include "Core/ECSEngine.h"

DEFINE_STAT(STAT_ECSSystemMovement);
DEFINE_STAT(STAT_ECSSystemMovementBarriers);
#if STATS
TStatId UECSSystemMovement::GetStatId() const {
	return GET_STATID(STAT_ECSSystemMovement);
}
#endif

UECSSystemMovement::UECSSystemMovement() {
	ObjectTypes.Add(ECC_KL_Objects);
}

void UECSSystemMovement::RequestInit() {
	Init<FECSDataMovement, FECSDataObject, FECSDataPosition, FECSDataTarget, FECSDataNavigation, FECSDataSteering, FECSDataBoid>();
}

void UECSSystemMovement::BeginPlay() {
	TimeFactor = ECS->GetWorld()->GetGameInstance()->GetSubsystem<UKLTimeFactorSubsystem>();

	Super::BeginPlay();
}

bool UECSSystemMovement::CanProcess(int32 Entity) const {
	const FECSDataMovement& MovementData = Get<FECSDataMovement>(Entity);
	const FECSDataObject& ObjectData = Get<FECSDataObject>(Entity);

	// Movement object either should not have Object component or should be alive to move
	bool bCanMove = !ObjectData.bEnabled || !ObjectData.bIsDead;

	return MovementData.bEnabled && !MovementData.bPause && bCanMove;
}

void UECSSystemMovement::OnUnregistered(int32 Entity) {
	FECSDataMovement& MovementData = Get<FECSDataMovement>(Entity);
	FECSDataPosition& PositionData = Get<FECSDataPosition>(Entity);

	PositionData.bNeedUpdate = false;
	MovementData.Velocity = FVector::ZeroVector;
}

void UECSSystemMovement::ProcessEntity(int32 Entity, float DeltaTime) {
	FECSDataMovement& MovementData = Get<FECSDataMovement>(Entity);
	FECSDataPosition& PositionData = Get<FECSDataPosition>(Entity);

	const FECSDataTarget& TargetData = Get<FECSDataTarget>(Entity);
	const FECSDataNavigation& NavigationData = Get<FECSDataNavigation>(Entity);
	const FECSDataObject& ObjectData = Get<FECSDataObject>(Entity);

	if (PositionData.bDestinationReached || MovementData.Speed == 0.f) {
		PositionData.bNeedUpdate = false;
		PositionData.bDestinationReachedSent = true;
		MovementData.Velocity = FVector::ZeroVector;
		return;
	}

	bool bCanUseNavigation = NavigationData.bEnabled;

	// Current step location
	FVector TargetLocation = bCanUseNavigation ? TargetData.Location : TargetData.DesiredLocation;
	FVector Direction = TargetData.bIsDirection ? TargetLocation : (TargetLocation - PositionData.Location);

	// Don't move, just rotate to the tECS->Getarget
	if (MovementData.Type == EECSMovementType::Aim) {
		PositionData.bNeedUpdate = true;

		FRotator CurrentRotation(0.f, PositionData.Yaw, 0.f);
		PositionData.Yaw = FMath::RInterpTo(CurrentRotation, Direction.Rotation(), DeltaTime, 15.f).Yaw;

		MovementData.Velocity = FVector::ZeroVector;

		return;
	}

	// Distance to the goal target
	// TODO: (?) calculate distance along the path?
	float DistanceLeft = TargetData.bIsDirection ? MAX_VISIBILITY_DISTANCE : FVector::Dist2D(TargetData.DesiredLocation, PositionData.Location);
	if (DistanceLeft <= PositionData.AcceptanceRadius || DistanceLeft == 0.f) {
		PositionData.bDestinationReached = true;
		PositionData.bNeedUpdate = true;
		MovementData.Velocity = FVector::ZeroVector;
		return;
	}

	FECSDataSteering& SteeringData = Get<FECSDataSteering>(Entity);
	FECSDataBoid& CurrentBoid = Get<FECSDataBoid>(Entity);

	float CurrentSpeed;
	float CurrentSteeringRate = SteeringData.SteeringRate;

	float MovementTimeFactor = ObjectData.Type == EECSObjectType::Neutral || ObjectData.Type == EECSObjectType::Enemy ? TimeFactor->GetEnemyMovementFactor() : TimeFactor->GetPlayerMovementFactor();

	bool bIsFollower = false;
	float FormationFactor = 1.f;
	FVector DirectionToMaster = FVector::ZeroVector;

	// Calculate movement speed
	switch (MovementData.Type) {
	case EECSMovementType::Formation: {
		const FECSDataMovement& MasterMovement = Get<FECSDataMovement>(MovementData.MasterEntity);
		const FECSDataPosition& MasterPosition = Get<FECSDataPosition>(MovementData.MasterEntity);

		CurrentSpeed = MasterMovement.Speed;
		bIsFollower = true;

		DirectionToMaster = MasterPosition.Location - PositionData.Location;

		float Distance = DirectionToMaster.Size2D();
		FormationFactor = FMath::Min(2.f, Distance / CurrentBoid.SeparationRadiusCurrent);

		break;
	}
	case EECSMovementType::Panic:
		CurrentSpeed = MovementTimeFactor * MovementData.Speed * 2;
		CurrentSteeringRate = MovementTimeFactor * CurrentSteeringRate * 2;
		break;
	default:
		CurrentSpeed = MovementTimeFactor * MovementData.Speed;
	}

	FVector Acceleration;
	if (CurrentBoid.bEnabled && !CurrentBoid.bPause) {
		Acceleration = CurrentBoid.Avoidance + CurrentBoid.Separation + CurrentBoid.Alignment + CurrentBoid.Cohesion + CurrentBoid.Target;
	} else {
		Acceleration = Direction;
	}

	Acceleration.Z = 0.f;
	Acceleration = Acceleration.GetSafeNormal2D();

	if (bIsFollower) {
		// Adjust speed according to movement direction:
		// should move faster if moving towards the Master

		float MasterDot = DirectionToMaster.GetSafeNormal() | Acceleration;
		if (MasterDot < 0) {
			FormationFactor = 1 / FormationFactor;
		}

		CurrentSpeed = CurrentSpeed * FormationFactor;
	}

	if (!SteeringData.bEnabled) {
		CurrentSteeringRate = 1.f;
	}

	Acceleration = Acceleration * CurrentSpeed * CurrentSteeringRate;

	if (!SteeringData.bEnabled) {
		// Instant direction change if no steering
		MovementData.Velocity = Acceleration * MovementData.Velocity.Size2D();
	}

	// TODO: add comment about WTF is this
	float AccelerationDot = FVector::DotProduct(MovementData.Velocity.GetSafeNormal(), Acceleration.GetSafeNormal());
	if (AccelerationDot < -0.8) {
		Acceleration = FRotator(0.f, 90.f * AccelerationDot, 0.f).RotateVector(Acceleration);
	}

	MovementData.Velocity += Acceleration * DeltaTime;
	MovementData.Velocity = MovementData.Velocity.GetClampedToMaxSize(CurrentSpeed);

	PositionData.Yaw = MovementData.Velocity.Rotation().Yaw;

	FVector NewLocation = PositionData.Location + (MovementData.Velocity * DeltaTime).GetClampedToMaxSize(DistanceLeft);

	// TODO: make a common check for all types
	// Slide along some collision surface
	if (ObjectData.Type == EECSObjectType::Enemy) {
		FVector VelocityDirection = MovementData.Velocity.GetSafeNormal();
		FVector SlideVector;
		FVector ImpactPoint;
		if (FindSlideVector(PositionData.Location, NewLocation, ObjectRadius, VelocityDirection, ObjectTypes, ImpactPoint, SlideVector)) {
			MovementData.Velocity = SlideVector * CurrentSpeed;
			DistanceLeft = (PositionData.Location - ImpactPoint).Size2D();
			NewLocation = PositionData.Location + (MovementData.Velocity * DeltaTime).GetClampedToMaxSize(DistanceLeft);
		}
	}

	PositionData.Location = NewLocation;
	PositionData.bNeedUpdate = true;
}

bool UECSSystemMovement::FindSlideVector(const FVector& From, const FVector& To, float Radius, const FVector& VelocityDirection, const TArray<TEnumAsByte<ECollisionChannel>>& Channels, FVector& ImpactPoint, FVector& OutSlideVector, bool bReverse) const {
	FHitResult Hit;
	if (UKLStatics::SphereTraceSingle(GetWorld(), From, To, Radius, Channels, Hit, false)) {
		SCOPE_CYCLE_COUNTER(STAT_ECSSystemMovementBarriers);

		if (Hit.PenetrationDepth > Radius && !bReverse) {
			OutSlideVector = FVector::ZeroVector;
			ImpactPoint = FVector::ZeroVector;
			return false;
		}

		FVector Normal = FVector(Hit.ImpactNormal.X, Hit.ImpactNormal.Y, 0).GetSafeNormal();
		if (bReverse) {
			Normal = -Normal;
		}

		float ImpactDot = Normal | VelocityDirection;
		if (ImpactDot > 0) {
			return false;
		}

		UECSObjectComponent* HitObject = Cast<UECSObjectComponent>(Hit.Component);

		if (IsValid(HitObject)) {
			int32 Index = IECSEntity::GetEntity(HitObject->GetOwner());
			if (Index == INDEX_NONE) {
				return false;
			}

			FECSDataObject ObjectData = Get<FECSDataObject>(Index);
			if (!ObjectData.bEnabled || ObjectData.bIsDead) {
				return false;
			}
		}

		// Clockwise or counter-clockwise
		float SlideDirection = (Normal ^ VelocityDirection).Z < 0 ? -1 : 1;

		// Get vector perpendicular to the impact normal
		OutSlideVector = FVector(0, 0, SlideDirection) ^ Normal;
		ImpactPoint = Hit.ImpactPoint;

		return true;
	}

	OutSlideVector = FVector::ZeroVector;
	ImpactPoint = FVector::ZeroVector;
	return false;
}
