// Fill out your copyright notice in the Description page of Project Settings.

#include "ECS/ECSController.h"
#include "Common/GameCharacter.h"
#include "Common/KLGameMode.h"
#include "GameGlobal.h"
#include "Skills/SkillLauncher.h"

#include "Core/ECSEngine.h"

AECSController::AECSController() {
	PrimaryActorTick.bCanEverTick = false;
}

void AECSController::BeginPlay() {
	Super::BeginPlay();

	bPlayHasBegun = true;

	if (!GetPawn()) {
		return;
	}

	if (bActivateECS && !bIsECSActive) {
		Register();
	}
}

void AECSController::OnPossess(APawn* InPawn) {
	Super::OnPossess(InPawn);

	if (!bPlayHasBegun) {
		return;
	}

	if (bActivateECS && !bIsECSActive) {
		Register();
	}
}

void AECSController::OnUnPossess() {
	Unregister();

	Super::OnUnPossess();
}

void AECSController::StopMovement() {
	Super::StopMovement();

	/*if (bIsECSActive) {
		ECS->StopMovement();
	}*/
}

bool AECSController::Register() {
	if (bIsECSActive) {
		UE_LOG(KLError, Error, TEXT("Can't register ECSController: it's already registered!"));
		return false;
	}

	AGameCharacter* CurrentCharacter = Cast<AGameCharacter>(GetPawn());
	if (!IsValid(CurrentCharacter)) {
		UE_LOG(KLError, Error, TEXT("AECSController::Register: Pawn must be AGameCharacter (for now)"));
		return false;
	}

	CurrentCharacter->RegisterECS();

	const int32 ECSIndex = IECSEntity::GetEntity(GetPawn());
	if (ECSIndex == INDEX_NONE) {
		UE_LOG(KLError, Error, TEXT("Can't register ECSController: can't get ECSIndex!"));
		return false;
	}

	bIsECSActive = true;

	return ECSPostRegister();
}

bool AECSController::ECSPostRegister() {
	return true;
}

bool AECSController::Unregister() {
	if (!bIsECSActive) {
		return false;
	}

	return ECSPostUnregister();
}

bool AECSController::ECSPostUnregister() {
	ECSUnregistered();

	return true;
}

void AECSController::ECSUnregistered() {
	bIsECSActive = false;
}

bool AECSController::PerformAction(AActor* TargetActor, FVector TargetLocation) {
	auto CurrentPawn = Cast<AGameCharacter>(GetPawn());

	if (!IsValid(CurrentPawn) || CurrentPawn->IsDebugForbidAttack() || CurrentPawn->IsCharacterDead()) {
		return false;
	}

	auto Skill = CurrentPawn->GetValidSkill(0);
	if (!IsValid(Skill) || !Skill->IsInited()) {
		return false;
	}

	TArray<AGameCharacter*> TargetCharacters;

	AGameCharacter* TargetCharacter = Cast<AGameCharacter>(TargetActor);
	if (IsValid(TargetCharacter)) {
		TargetCharacters.Add(TargetCharacter);
	}

	FTargetingInfo Target(Skill->GetTargetingType(), TargetLocation, TargetCharacters);
	Skill->Launch(Target);

	return true;
}
