// Fill out your copyright notice in the Description page of Project Settings.

#include "ECS/ECSSystemNavigation.h"
#include "ECS/ECSComponents.h"
#include "ECS/ECSObject.h"
#include "ECS/ECSTypes.h"

#include "Core/ECSEngine.h"

#include "NavFilters/NavigationQueryFilter.h"
#include "NavigationSystem.h"
#include "NavigationPath.h"

DEFINE_STAT(STAT_ECSSystemNavigation);
DEFINE_STAT(STAT_ECSSystemNavigationPath);
#if STATS
TStatId UECSSystemNavigation::GetStatId() const {
	return GET_STATID(STAT_ECSSystemNavigation);
}
#endif

void UECSSystemNavigation::RequestInit() {
	Init<FECSDelegate, FECSDataMovement, FECSDataNavigation, FECSDataObject, FECSDataTarget, FECSDataPosition>();
}

void UECSSystemNavigation::BeginPlay() {
	World = GetWorld();
	NavSys = FNavigationSystem::GetCurrent<UNavigationSystemV1>(World);

	check(IsValid(NavSys));
	check(IsValid(NavSys->GetDefaultNavDataInstance()));

	Super::BeginPlay();
}

void UECSSystemNavigation::OnRegistered(int32 Entity) {
	const FECSDelegate& Delegate = Get<FECSDelegate>(Entity);
	FECSDataNavigation& NavigationData = Get<FECSDataNavigation>(Entity);

	INavAgentInterface* NavAgent = Cast<INavAgentInterface>(Delegate.Owner);
	if (NavAgent != NULL) {
		const FNavAgentProperties& AgentProps = NavAgent->GetNavAgentPropertiesRef();
		NavigationData.NavData = NavSys->GetNavDataForProps(AgentProps);
	} else {
		NavigationData.NavData = NavSys->GetDefaultNavDataInstance();
	}

	NavigationData.QueryFilter = UNavigationQueryFilter::GetQueryFilter(*NavigationData.NavData, Delegate.Owner, NavigationData.FilterClass);
}

void UECSSystemNavigation::OnUnregistered(int32 Entity) {
	FECSDataNavigation& NavigationData = Get<FECSDataNavigation>(Entity);
	NavigationData.NavData = nullptr;
	NavigationData.QueryFilter = nullptr;
}

bool UECSSystemNavigation::CanProcess(int32 Entity) const {
	const FECSDataMovement& MovementData = Get<FECSDataMovement>(Entity);
	const FECSDataNavigation& NavigationData = Get<FECSDataNavigation>(Entity);
	const FECSDataObject& ObjectData = Get<FECSDataObject>(Entity);
	const FECSDataTarget& TargetData = Get<FECSDataTarget>(Entity);

	// Movement object either should not have Object component or should be alive to move
	bool bShouldBeAlive = !ObjectData.bEnabled || !ObjectData.bIsDead;

	return !TargetData.bIsDirection && MovementData.bEnabled && !MovementData.bPause && bShouldBeAlive;
}

void UECSSystemNavigation::ProcessEntity(int32 Entity, float DeltaTime) {
	FECSDataNavigation& NavigationData = Get<FECSDataNavigation>(Entity);
	FECSDataTarget& TargetData = Get<FECSDataTarget>(Entity);

	const FECSDataPosition& PositionData = Get<FECSDataPosition>(Entity);
	const FECSDelegate& Delegate = Get<FECSDelegate>(Entity);

	// TODO: check for acceptance radius for the entity's target
	// TODO: check if the checkpoint reached

	// TODO: (?) check if we need to update the path

	TArray<FVector> PathPoints;

	{
		SCOPE_CYCLE_COUNTER(STAT_ECSSystemNavigationPath);

		auto NavSysData = NavigationData.NavData;

		const FVector Extent(200.f, 200.f, 200.f);
		FNavLocation OutLocation;

		// Ensure that start position is on the nav mesh
		if (!NavSys->ProjectPointToNavigation(PositionData.Location, OutLocation, Extent, NavSysData, NavigationData.QueryFilter)) {
			return;
		}
		NavigationData.PathStart = OutLocation.Location;

		// Ensure that end position is on the nav mesh
		if (!NavSys->ProjectPointToNavigation(TargetData.DesiredLocation, OutLocation, Extent, NavSysData, NavigationData.QueryFilter)) {
			return;
		}
		NavigationData.PathEnd = OutLocation.Location;

		// Query for navigation path
		const FPathFindingQuery Query(Delegate.Owner, *NavSysData, NavigationData.PathStart, NavigationData.PathEnd, NavigationData.QueryFilter);
		const FPathFindingResult Result = NavSysData->FindPath(Query.NavAgentProperties, Query);

		if (Result.IsSuccessful()) {
			// Copy result's path points
			FNavigationPath* NewPath = Result.Path.Get();
			if (NewPath != NULL) {
				auto Points = NewPath->GetPathPoints();
				PathPoints.Reset(Points.Num());

				for (const auto& PathPoint : Points) {
					PathPoints.Add(PathPoint.Location);
				}
			}
		}
	}

	if (PathPoints.Num() < 2) {
		// If no path points found (or only the starting point) then don't update navigation data
		// Assume the entity can use the old path Points if any
		return;
	}

	// Update path points and immediate target for movement
	NavigationData.Points = PathPoints;
	TargetData.Location = NavigationData.Points[1];
}
