// Fill out your copyright notice in the Description page of Project Settings.

#include "ECS/ECSSystemBoid.h"
#include "ECS/ECSComponents.h"
#include "ECS/ECSTypes.h"

#include "Core/ECSEngine.h"

DEFINE_STAT(STAT_ECSSystemBoid);
#if STATS
TStatId UECSSystemBoid::GetStatId() const {
	return GET_STATID(STAT_ECSSystemBoid);
}
#endif

void UECSSystemBoid::RequestInit() {
	Init<FECSDataMovement, FECSDataBoid, FECSDataObject, FECSDataPosition, FECSDataTarget>();
}

bool UECSSystemBoid::CanProcess(int32 Entity) const {
	const FECSDataMovement& CurrentMovement = Get<FECSDataMovement>(Entity);
	const FECSDataBoid& CurrentBoid = Get<FECSDataBoid>(Entity);

	return CurrentMovement.bEnabled && CurrentBoid.bEnabled;
}

void UECSSystemBoid::ProcessEntity(int32 Entity, float DeltaTime) {
	FECSDataBoid& CurrentBoid = Get<FECSDataBoid>(Entity);

	const FECSDataObject& CurrentObject = Get<FECSDataObject>(Entity);
	if (CurrentObject.Type == EECSObjectType::Neutral) {
		// Don't process obstacles
		return;
	}

	const FECSDataMovement& CurrentMovement = Get<FECSDataMovement>(Entity);
	const FECSDataPosition& CurrentPosition = Get<FECSDataPosition>(Entity);
	const FECSDataTarget& CurrentTarget = Get<FECSDataTarget>(Entity);

	int32 BoidIndex = CurrentBoid.BoidIndex;

	FVector Avoidance = FVector::ZeroVector;
	int32 AvoidanceCount = 0;

	FVector Separation = FVector::ZeroVector;
	int32 SeparationCount = 0;

	FVector Alignment = FVector::ZeroVector;
	int32 AlignmentCount = 0;

	FVector Cohesion = FVector::ZeroVector;
	int32 CohesionCount = 0;

	for (int32 Index : Entities) {
		if (Index == Entity) {
			// Skip current entity
			continue;
		}

		const FECSDataBoid& BoidData = Get<FECSDataBoid>(Index);
		const FECSDataMovement& MovementData = Get<FECSDataMovement>(Index);
		const FECSDataPosition& PositionData = Get<FECSDataPosition>(Index);

		// BoidIndex == 0 is only allowed for Followers
		// BoidIndex < 0 is obstacle
		// BoidIndex > 0 is a regular boid, only consider the entities from the same boid
		if (BoidData.BoidIndex > 0 && BoidData.BoidIndex != BoidIndex) {
			continue;
		}

		bool bIsObstacle = BoidData.BoidIndex < 0;

		FVector Diff = PositionData.Location - CurrentPosition.Location;
		Diff.Z = 0.f;

		float Distance = Diff.Size2D();

		if (bIsObstacle) {
			float AvoidanceRadius = FMath::Max(CurrentBoid.SeparationRadiusCurrent, BoidData.SeparationRadiusCurrent);

			if (Distance <= AvoidanceRadius) {
				// Increase avoidance when getting closer
				Avoidance += Diff * (AvoidanceRadius / Distance);
				AvoidanceCount += 1;
			}

			continue;
		}

		bool bIsFollower = MovementData.MasterEntity == Entity;
		if (bIsFollower) {
			// Don't consider own's followers for separation/alignment/cohesion
			continue;
		}

		bool bHasMaster = CurrentMovement.MasterEntity != INDEX_NONE;
		bool bIsMaster = CurrentMovement.MasterEntity == Index;

		// Boid separation
		float SeparationRadius = CurrentBoid.SeparationRadiusCurrent;
		if (Distance <= SeparationRadius) {
			// Increase separation when getting closer
			Separation += Diff * (SeparationRadius / Distance);
			SeparationCount += 1;
		}

		// Boid alignment
		if (bIsMaster || (!bHasMaster && Distance <= CurrentBoid.AlignmentRadiusCurrent)) {
			// Speed alignment with neighbours
			Alignment += MovementData.Velocity;
			AlignmentCount += 1;
		}

		// Boid cohesion
		if (bIsMaster || (!bHasMaster && Distance <= CurrentBoid.CohesionRadiusCurrent)) {
			// Speed alignment with neighbours
			Cohesion += PositionData.Location;
			CohesionCount += 1;
		}
	}

	if (AvoidanceCount > 0) {
		Avoidance = -1.f * Avoidance / AvoidanceCount;
		Avoidance.Normalize();
		CurrentBoid.Avoidance = Avoidance * 5.f;
	} else {
		CurrentBoid.Avoidance = FVector::ZeroVector;
	}

	if (SeparationCount > 0) {
		Separation = -1.f * Separation / SeparationCount;
		Separation.Normalize();
		CurrentBoid.Separation = Separation * CurrentBoid.SeparationWeightCurrent;
	} else {
		CurrentBoid.Separation = FVector::ZeroVector;
	}

	if (AlignmentCount > 0) {
		Alignment = Alignment / AlignmentCount;
		Alignment.Normalize();
		CurrentBoid.Alignment = Alignment * CurrentBoid.AlignmentWeightCurrent;
	} else {
		CurrentBoid.Alignment = FVector::ZeroVector;
	}

	if (CohesionCount > 0) {
		Cohesion = (Cohesion / CohesionCount) - CurrentPosition.Location;
		Cohesion.Normalize();
		CurrentBoid.Cohesion = Cohesion * CurrentBoid.CohesionWeightCurrent;
	} else {
		CurrentBoid.Cohesion = FVector::ZeroVector;
	}

	// Boid following target
	FVector Target = CurrentTarget.Location - CurrentPosition.Location;
	Target.Normalize();
	CurrentBoid.Target = Target * 1.f;
}
