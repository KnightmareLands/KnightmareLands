// Fill out your copyright notice in the Description page of Project Settings.

#include "ECS/ECSSystemAI.h"
#include "Common/CollisionChannels.h"
#include "Common/KLStatics.h"
#include "ECS/ECSComponents.h"
#include "ECS/ECSObject.h"
#include "ECS/ECSObjectComponent.h"
#include "GameGlobal.h"

#include "Core/ECSEngine.h"

DEFINE_STAT(STAT_ECSSystemAI);
DEFINE_STAT(STAT_ECSSystemAIBan);
#if STATS
TStatId UECSSystemAI::GetStatId() const {
	return GET_STATID(STAT_ECSSystemAI);
}
#endif

void UECSSystemAI::RequestInit() {
	Init<FECSDataAI, FECSDataObject, FECSDataPosition, FECSDataTarget, FECSDataSense>();
}

void UECSSystemAI::BeginPlay() {
	World = GetWorld();

	AttackGroups = TArray<FECSAIAttackGroup>();

	UGameInstance* Instance = World->GetGameInstance();
	UGameGlobal* Global = Cast<UGameGlobal>(Instance);

	MinTargetedCount = Global->MinTargetedCount;
	MaxTargetedPercent = Global->MaxTargetedPercent;

	SpawnedMobsCounterIndex = ECS->RegisterCounter(UECSObjectComponent::SpawnedMobsCounter);

	Super::BeginPlay();
}

void UECSSystemAI::OnRegistered(int32 Entity) {
	const FECSDataAI& AIData = Get<FECSDataAI>(Entity);

	if (AttackGroups.Num() <= AIData.Group) {
		int32 GroupsCount = AIData.Group+1;
		AttackGroups.Init(FECSAIAttackGroup(), GroupsCount);
	}
}

void UECSSystemAI::OnUnregistered(int32 Entity) {
	FECSDataAI& AIData = Get<FECSDataAI>(Entity);
	AIData.State = EECSAIState::Idle;
	AIData.Direction = EECSMovementDirection::None;
}

bool UECSSystemAI::CanProcess(int32 Entity) const {
	const FECSDataObject& ObjectData = Get<FECSDataObject>(Entity);
	const FECSDataAI& AIData = Get<FECSDataAI>(Entity);

	bool bValidObject = ObjectData.Type == EECSObjectType::Enemy;

	return bValidObject && ObjectData.bEnabled && !ObjectData.bIsDead && AIData.bEnabled;
}

void UECSSystemAI::Process(float DeltaTime) {
	const int32 SpawnedMobsCount = ECS->GetCounter(SpawnedMobsCounterIndex);

	MaxTargetedCount = FMath::Max<float>(SpawnedMobsCount * MaxTargetedPercent, MinTargetedCount);

	// Clear Attacking Groups
	for (auto& Group : AttackGroups) {
		Group.Empty(Count);
	}

	Super::Process(DeltaTime);

	ProcessAttackGroups();
}

void UECSSystemAI::ProcessAttackGroups() {
	SCOPE_CYCLE_COUNTER(STAT_ECSSystemAIBan);

	for (auto& Group : AttackGroups) {
		TArray<int32> Bans;
		Group.GetAttackBans(MaxTargetedCount, Bans);

		// Attacks are banned by distance
		for (auto Entity : Bans) {
			// Reset chase/attack state for those who's are on top of limit
			FECSDataAI& AIData = Get<FECSDataAI>(Entity);
			AIData.bAttackBanned = true;

			switch (AIData.PreviousState) {
			case EECSAIState::Attack:
				if (AIData.ActionType == ESkillType::Melee) {
					AIData.State = EECSAIState::AttackOnOpportunity;
					break;
				}

				// fallthrough
			case EECSAIState::Chase:
				AIData.State = EECSAIState::InitRoaming;
				break;
			default:
				AIData.State = AIData.PreviousState;
			}

			if (AIData.PreviousState != EECSAIState::Attack && AIData.Alienation == 0.f) {
				AIData.Alienation = 1000.f * FMath::RandRange(1, 3);
			}
		}
	}
}

void UECSSystemAI::ProcessEntity(int32 Entity, float DeltaTime) {
	FECSDataObject& ObjectData = Get<FECSDataObject>(Entity);
	FECSDataAI& AIData = Get<FECSDataAI>(Entity);
	FECSDataPosition& PositionData = Get<FECSDataPosition>(Entity);
	FECSDataTarget& TargetData = Get<FECSDataTarget>(Entity);

	if (TargetData.bSendReachedEvent && PositionData.bDestinationReachedSent) {
		AIData.bPause = false;
		TargetData.bSendReachedEvent = false;
	}

	if (AIData.bPause) {
		return;
	}

	AIData.PreviousState = AIData.State;

	FECSDataSense& SenseData = Get<FECSDataSense>(Entity);
	if (!SenseData.bSenseTarget) {
		// No target – stop and do nothing
		// Should not happens while at least 1 Hero alive

		// TODO: the Summoned entity should still follow the master
		AIData.State = EECSAIState::Idle;
		AIData.Direction = EECSMovementDirection::None;

		return;
	}

	bool bIsAfraid = SenseData.Aggression < 0.f;
	if (bIsAfraid) {
		AIData.State = EECSAIState::InitPanic;
		return;
	}

	if (AIData.State == EECSAIState::Flee) {
		// Keep panic
		return;
	}

	float Distance = FVector::Dist2D(PositionData.Location, SenseData.Location);

	bool bFollowingMaster = AIData.MasterEntity != INDEX_NONE;
	bool bIsAggression = SenseData.Aggression > 0.f && !bFollowingMaster;
	bool bCanSeeTarget = (!bFollowingMaster && AttackGroups[AIData.Group].Num() < MaxTargetedCount) || Distance <= AIData.VisibilityDistance || AIData.VisibilityDistance == -1.f;
	bool bIsTargetInRange = SenseData.bSenseTarget && (bIsAggression || bCanSeeTarget);

	if (bIsTargetInRange) {
		// Target found, in range and entity can attack
		AIData.bAttackBanned = false;

		// TODO: Calculate the real attack distance based on speed, skill casting time and other parameters
		PositionData.bDestinationReached = Distance <= AIData.AttackDistance * 0.9;
		PositionData.bDestinationReachedSent = PositionData.bDestinationReachedSent && PositionData.bDestinationReached;

		// If target is in reach – start attacking; otherwise chasing
		bool bShouldAttack = PositionData.bDestinationReached;

		// Check if the target is reachable
		FHitResult OutHit;

		if (!bShouldAttack && UKLStatics::LineTraceSingle(World, PositionData.Location, SenseData.Location, ECC_KL_Objects, OutHit)) {
			if (AIData.State != EECSAIState::Roaming) {
				AIData.State = EECSAIState::InitRoaming;
				AIData.Alienation = FMath::FRandRange(1000.f, 10000.f);
			}

			return;
		}

		// TODO: separate Attack state and Chase state

		if (bIsAggression || bFollowingMaster) {
			AIData.State = bShouldAttack ? EECSAIState::AttackByAggression : EECSAIState::ChaseByAggression;
		} else {
			AIData.State = bShouldAttack ? EECSAIState::Attack : EECSAIState::Chase;

			AttackGroups[AIData.Group].Add(Entity, Distance);
		}

		AIData.Direction = EECSMovementDirection::None;
		return;
	}

	if (AIData.ActionType == ESkillType::Melee && Distance < AIData.AttackDistance) {
		// Perform an immediate attack if target is in close reach of melee attack

		AIData.State = EECSAIState::AttackOnOpportunity;
		return;
	}

	bool bIsRoaming = false;
	switch (AIData.State) {
	case EECSAIState::InitRoaming:
	case EECSAIState::Roaming:
		bIsRoaming = true;
	}

	if (!bIsRoaming) {
		// Target found, not in range: Hanging around
		// Start Roam state

		AIData.State = EECSAIState::InitRoaming;
		AIData.Direction = EECSMovementDirection::None;

		AIData.Alienation = FMath::FRandRange(1000.f, 10000.f);
	}
}
