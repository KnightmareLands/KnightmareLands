// Fill out your copyright notice in the Description page of Project Settings.

#include "ECS/ECSMovementComponent.h"
#include "Common/GameCharacter.h"
#include "Common/KLGameMode.h"
#include "ECS/ECSComponents.h"

#include "API/ECSEntity.h"
#include "Core/ECSEngine.h"

// Sets default values for this component's properties
UECSMovementComponent::UECSMovementComponent() {
	PrimaryComponentTick.bCanEverTick = false;
}

// Called when the game starts
void UECSMovementComponent::BeginPlay(){
	Super::BeginPlay();

	bIsECSActive = false;

	if (bActivateECS) {
		Register();
	}
}

bool UECSMovementComponent::Register() {
	if (bIsECSActive) {
		UE_LOG(KLError, Error, TEXT("ECSMovementComponent is already registered!"));
		return false;
	}

	const int32 ECSIndex = IECSEntity::GetEntity(GetOwner());
	if (ECSIndex == INDEX_NONE) {
		UE_LOG(KLError, Error, TEXT("Can't register ECSMovementComponent: can't get ECSIndex!"));
		return false;
	}

	UECSEngine* ECS = IECSEntity::GetEngine(GetOwner());
	ECS->UpdateEntity(ECSIndex);

	FECSDelegate& DataDelegate = ECS->Get<FECSDelegate>(ECSIndex);
	DataDelegate.PositionDelegate = this;

	FECSDataPosition& DataPosition = ECS->Get<FECSDataPosition>(ECSIndex);
	FECSDataMovement& DataMovement = ECS->Get<FECSDataMovement>(ECSIndex);
	FECSDataSteering& DataSteering = ECS->Get<FECSDataSteering>(ECSIndex);
	FECSDataBoid& DataBoid = ECS->Get<FECSDataBoid>(ECSIndex);

	DataPosition.Location = GetOwnerLocation();
	DataPosition.Yaw = GetOwnerRotation().Yaw;
	DataPosition.AcceptanceRadius = AcceptanceRadius;
	DataPosition.Orientation = bOrientToMovement ? EECSOrientation::Movement : EECSOrientation::None;

	DataMovement.bEnabled = true;
	DataMovement.Speed = Speed;

	DataBoid.bEnabled = BoidIndex != 0;
	DataBoid.BoidIndex = BoidIndex;

	DataBoid.SeparationRadius = SeparationRadius;
	DataBoid.SeparationRadiusCurrent = SeparationRadius;
	DataBoid.SeparationWeight = SeparationWeight;
	DataBoid.SeparationWeightCurrent = SeparationWeight;

	DataBoid.AlignmentRadius = AlignmentRadius;
	DataBoid.AlignmentRadiusCurrent = AlignmentRadius;
	DataBoid.AlignmentWeight = AlignmentWeight;
	DataBoid.AlignmentWeightCurrent = AlignmentWeight;

	DataBoid.CohesionWeight = CohesionWeight;
	DataBoid.CohesionWeightCurrent = CohesionWeight;

	bIsECSActive = true;

	UpdateSteering();

	return true;
}

void UECSMovementComponent::UpdateSteering() {
	if (!bIsECSActive) {
		return;
	}

	UECSEngine* ECS = IECSEntity::GetEngine(GetOwner());
	const int32 ECSIndex = IECSEntity::GetEntity(GetOwner());

	FECSDataSteering& DataSteering = ECS->Get<FECSDataSteering>(ECSIndex);
	DataSteering.bEnabled = bSteeringEnabled;

	if (SteeringRate != -1.f) {
		DataSteering.SteeringRate = SteeringRate;
	}
}

bool UECSMovementComponent::Unregister() {
	if (!bIsECSActive) {
		return false;
	}

	UECSEngine* ECS = IECSEntity::GetEngine(GetOwner());
	const int32 ECSIndex = IECSEntity::GetEntity(GetOwner());

	FECSDataMovement& DataMovement = ECS->Get<FECSDataMovement>(ECSIndex);
	DataMovement.bEnabled = false;

	FECSDataSteering& DataSteering = ECS->Get<FECSDataSteering>(ECSIndex);
	DataSteering.bEnabled = false;

	FECSDataBoid& DataBoid = ECS->Get<FECSDataBoid>(ECSIndex);
	DataBoid.bEnabled = false;
	DataBoid.BoidIndex = 0;

	FECSDelegate& DataDelegate = ECS->Get<FECSDelegate>(ECSIndex);
	DataDelegate.PositionDelegate = nullptr;

	ECSUnregistered();

	return true;
}

void UECSMovementComponent::ECSUnregistered() {
	bIsECSActive = false;
}

EMovementRequestResults UECSMovementComponent::MoveTo(FVector Target, bool bIsDirection, float InAcceptanceRadius) {
	if (!bIsECSActive) {
		return EMovementRequestResults::Failure;
	}

	UECSEngine* ECS = IECSEntity::GetEngine(GetOwner());
	const int32 ECSIndex = IECSEntity::GetEntity(GetOwner());

	FECSDataTarget& DataTarget = ECS->Get<FECSDataTarget>(ECSIndex);
	bool bTargetChanged = DataTarget.DesiredLocation != Target;
	DataTarget.DesiredLocation = Target;
	DataTarget.bIsDirection = bIsDirection;
	DataTarget.bSendReachedEvent = !bIsDirection;

	FECSDataPosition& DataPosition = ECS->Get<FECSDataPosition>(ECSIndex);
	DataPosition.bDestinationReachedSent = false;

	FECSDataMovement& DataMovement = ECS->Get<FECSDataMovement>(ECSIndex);
	DataMovement.bEnabled = true;

	FECSDataAI& AIData = ECS->Get<FECSDataAI>(ECSIndex);
	AIData.bPause = true;

	const float CurrentAcceptanceRadius = InAcceptanceRadius == -1.f ? AcceptanceRadius : InAcceptanceRadius;
	if (CurrentAcceptanceRadius >= 0.f) {
		DataPosition.AcceptanceRadius = CurrentAcceptanceRadius;
	}

	const float DistanceLeft = bIsDirection ? MAX_VISIBILITY_DISTANCE : (DataTarget.DesiredLocation - DataPosition.Location).Size2D();
	if (DistanceLeft <= DataPosition.AcceptanceRadius || DistanceLeft == 0.f) {
		DataPosition.bDestinationReached = true;
		return EMovementRequestResults::Reached;
	}

	DataPosition.bDestinationReached = false;

	return EMovementRequestResults::Success;
}

bool UECSMovementComponent::StopMovement() {
	if (!bIsECSActive) {
		return false;
	}

	UECSEngine* ECS = IECSEntity::GetEngine(GetOwner());
	const int32 ECSIndex = IECSEntity::GetEntity(GetOwner());

	FECSDataMovement& DataMovement = ECS->Get<FECSDataMovement>(ECSIndex);
	DataMovement.bEnabled = false;

	return true;
}

bool UECSMovementComponent::PauseMovement() {
	if (!bIsECSActive) {
		return false;
	}

	UECSEngine* ECS = IECSEntity::GetEngine(GetOwner());
	const int32 ECSIndex = IECSEntity::GetEntity(GetOwner());

	FECSDataMovement& DataMovement = ECS->Get<FECSDataMovement>(ECSIndex);
	DataMovement.bPause = true;

	return true;
}

bool UECSMovementComponent::UpdatePosition() {
	if (!bIsECSActive) {
		return false;
	}

	UECSEngine* ECS = IECSEntity::GetEngine(GetOwner());
	const int32 ECSIndex = IECSEntity::GetEntity(GetOwner());

	FECSDataPosition& DataPosition = ECS->Get<FECSDataPosition>(ECSIndex);
	DataPosition.Location = GetOwnerLocation();
	DataPosition.Yaw = GetOwnerRotation().Yaw;
	DataPosition.bDestinationReachedSent = false;

	return true;
}

bool UECSMovementComponent::StartMovement() {
	if (!bIsECSActive) {
		return false;
	}

	UpdatePosition();

	UECSEngine* ECS = IECSEntity::GetEngine(GetOwner());
	const int32 ECSIndex = IECSEntity::GetEntity(GetOwner());

	FECSDataMovement& MovementData = ECS->Get<FECSDataMovement>(ECSIndex);
	MovementData.bEnabled = true;
	MovementData.bPause = false;

	return true;
}

void UECSMovementComponent::SetSpeed(float InSpeed) {
	Speed = InSpeed;

	if (!bIsECSActive) {
		return;
	}

	UECSEngine* ECS = IECSEntity::GetEngine(GetOwner());
	const int32 ECSIndex = IECSEntity::GetEntity(GetOwner());

	FECSDataMovement& DataMovement = ECS->Get<FECSDataMovement>(ECSIndex);
	DataMovement.Speed = Speed;
}

void UECSMovementComponent::EnableSteering(bool bEnabled) {
	bSteeringEnabled = bEnabled;

	UpdateSteering();
}

void UECSMovementComponent::SetSteeringRate(float InSteeringRate) {
	SteeringRate = InSteeringRate;

	UpdateSteering();
}

void UECSMovementComponent::SetAcceptanceRadius(float InAcceptanceRadius) {
	AcceptanceRadius = FMath::Max(0.f, InAcceptanceRadius);
}

float UECSMovementComponent::GetMaxSpeed() const {
	return Speed;
}

float UECSMovementComponent::GetCurrentSpeed() const {
	if (!bIsECSActive) {
		return 0.f;
	}

	UECSEngine* ECS = IECSEntity::GetEngine(GetOwner());
	const int32 ECSIndex = IECSEntity::GetEntity(GetOwner());

	const FECSDataPosition& DataPosition = ECS->Get<FECSDataPosition>(ECSIndex);
	const FECSDataMovement& DataMovement = ECS->Get<FECSDataMovement>(ECSIndex);

	if (DataPosition.bDestinationReached || !DataMovement.bEnabled || DataMovement.bPause || DataMovement.Type == EECSMovementType::Aim) {
		return 0.f;
	}

	return DataMovement.Speed;
}

bool UECSMovementComponent::UpdateOwnerLocationAndRotation(FVector NewLocation, FRotator NewRotation) {
	return GetOwner()->SetActorLocationAndRotation(NewLocation, NewRotation);
}

bool UECSMovementComponent::UpdateOwnerLocation(FVector NewLocation) {
	return GetOwner()->SetActorLocation(NewLocation);
}

bool UECSMovementComponent::UpdateOwnerRotation(FRotator NewRotation) {
	return GetOwner()->SetActorRotation(NewRotation);
}

void UECSMovementComponent::OnTargetReached() {
	OnECSTargetReached.Broadcast();
}

FVector UECSMovementComponent::GetOwnerLocation() const {
	return GetOwner()->GetActorLocation();
}

FRotator UECSMovementComponent::GetOwnerRotation() const {
	return GetOwner()->GetActorRotation();
}

float UECSMovementComponent::GetSteeringRate() const {
	return SteeringRate;
}
