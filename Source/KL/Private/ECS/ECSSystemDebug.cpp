// Fill out your copyright notice in the Description page of Project Settings.

#include "ECS/ECSSystemDebug.h"
#include "ECS/ECSComponents.h"

#include "Core/ECSEngine.h"

DEFINE_STAT(STAT_ECSSystemDebug);
#if STATS
TStatId UECSSystemDebug::GetStatId() const {
	return GET_STATID(STAT_ECSSystemDebug);
}
#endif

void UECSSystemDebug::Process(float DeltaTime) {
	check(ECS);

#if STATS
	TStatId SystemStatId = GetStatId();
#endif

	// DrawDebug* methods are not working with ParallelFor so using a single-thread loop
	for (int32 Entity : Entities) {
#if STATS
		FScopeCycleCounter SystemStats(SystemStatId);
#endif
		ProcessEntity(Entity, DeltaTime);
	}
}

void UECSSystemDebug::RequestInit() {
	Init<FECSDataPosition, FECSDataAI, FECSDataSense, FECSDataCollision, FECSDataMovement, FECSDataNavigation, FECSDataObject, FECSDataTarget, FECSDataBoid>();
}

bool UECSSystemDebug::CanProcess(int32 Entity) const {
	// Debug is disabled for all entities
	return false;

	const FECSDataMovement& MovementData = Get<FECSDataMovement>(Entity);
	const FECSDataNavigation& NavigationData = Get<FECSDataNavigation>(Entity);
	const FECSDataObject& ObjectData = Get<FECSDataObject>(Entity);
	const FECSDataAI& AIData = Get<FECSDataAI>(Entity);
	const FECSDataTarget& TargetData = Get<FECSDataTarget>(Entity);

	// Movement object either should not have Object component or should be alive to move
	bool bShouldBeAlive = !ObjectData.bEnabled || !ObjectData.bIsDead;

	return (bShouldBeAlive && ObjectData.Type == EECSObjectType::Enemy); // || ObjectData.Type == EECSObjectType::Neutral;

	return AIData.bEnabled && !TargetData.bIsDirection && MovementData.bEnabled && !MovementData.bPause && bShouldBeAlive && NavigationData.bEnabled && ObjectData.Type != EECSObjectType::Player;

	// Enable for all enemies
	//const FECSDataObject& ObjectData = Get<FECSDataObject>(Entity);
	//return ObjectData.Type == EECSObjectType::Enemy;

	// Enable for navigating entities
	//const FECSDataNavigation& NavigationData = Get<FECSDataNavigation>(Entity);
	return NavigationData.bEnabled && Super::CanProcess(Entity);

	// Enable for all active entities
	return Super::CanProcess(Entity);
}

void UECSSystemDebug::ProcessEntity(int32 Entity, float DeltaTime) {
	FECSDataObject& ObjectData = Get<FECSDataObject>(Entity);

	//if (!ObjectData.bDebug) {
	//	continue;
	//}

	FECSDelegate& Delegate = Get<FECSDelegate>(Entity);
	FECSDataAI& AIData = Get<FECSDataAI>(Entity);
	FECSDataSense& SenseData = Get<FECSDataSense>(Entity);
	FECSDataPosition& PositionData = Get<FECSDataPosition>(Entity);
	// FECSDataMovement& MovementData = Get<FECSDataMovement>(Entity);
	FECSDataTarget& TargetData = Get<FECSDataTarget>(Entity);
	FECSDataBoid& BoidData = Get<FECSDataBoid>(Entity);
	FECSDataNavigation& NavigationData = Get<FECSDataNavigation>(Entity);

	if (ObjectData.Type == EECSObjectType::Enemy) {

		//DrawDebugPoint(GetWorld(), TargetData.Location, 5.f, FColor(255, 0, 0), false, 0.f, 1);
		// DrawDebugSphere(GetWorld(), TargetData.DesiredLocation, 50, 32, FColor(0, 255, 0), false, 0.f);
		// DrawDebugSphere(GetWorld(), TargetData.Location, 50, 32, FColor(0, 150, 150), false, 0.f);
		// DrawDebugSphere(GetWorld(), NavigationData.PathEnd, 100, 32, FColor(0, 0, 255), false, 0.f);

		// DrawDebugCircle(GetWorld(), PositionData.Location, BoidData.SeparationRadiusCurrent, 32, FColor(255, 0, 0), false, 0.f, 1, 8.f, FVector(1, 0, 0), FVector(0, 1, 0), false);
		// DrawDebugCircle(GetWorld(), PositionData.Location, BoidData.AlignmentRadiusCurrent, 32, FColor(0, 255, 255), false, 0.f, 1, 8.f, FVector(1, 0, 0), FVector(0, 1, 0), false);

		// DrawDebugDirectionalArrow(GetWorld(), PositionData.Location, PositionData.Location + BoidData.Separation * 200.f, 20.f, FColor(255, 0, 0), false, 0.f, 1, 10.f);
		// DrawDebugDirectionalArrow(GetWorld(), PositionData.Location, PositionData.Location + BoidData.Alignment * 200.f, 20.f, FColor(0, 255, 255), false, 0.f, 1, 10.f);
		// DrawDebugDirectionalArrow(GetWorld(), PositionData.Location, PositionData.Location + BoidData.Cohesion * 200.f, 20.f, FColor(0, 0, 255), false, 0.f, 1, 10.f);

		// DrawDebugDirectionalArrow(GetWorld(), PositionData.Location, PositionData.Location + BoidData.Avoidance * 200.f, 20.f, FColor(255, 255, 255), false, 0.f, 1, 10.f);

		UE_LOG(KLDebug, Warning, TEXT("UECSEngine::ProcessDebug: Old state: %s, AI state: %s, Movement: %s, a: %f"), *GET_ENUM_STRING("EECSAIState", AIData.PreviousState), *GET_ENUM_STRING("EECSAIState", AIData.State), *GET_ENUM_STRING("EECSMovementType", AIData.Type), AIData.Alienation);

		FVector TargetVector = SenseData.Location - PositionData.Location;
		float Distance = TargetVector.Size2D();
		float AlignDot = TargetVector | SenseData.Velocity;
		DrawDebugDirectionalArrow(GetWorld(), PositionData.Location, PositionData.Location + FVector(0,0,FMath::Abs(AlignDot))*400.f, 20.f, AlignDot >=0 ? FColor(0, 0, 255) : FColor(255, 0, 0), false, 0.f, 1, 10.f);

	} else if (ObjectData.Type == EECSObjectType::Neutral) {
		if (BoidData.bEnabled) {
			DrawDebugSphere(GetWorld(), PositionData.Location, BoidData.SeparationRadiusCurrent, 32, FColor(255, 0, 0), false, 0.f);
		} else {
			DrawDebugPoint(GetWorld(), PositionData.Location, 10.f, FColor(255, 0, 0), false, 0.f, 1);
		}
	}

	int32 LastPointIndex = NavigationData.Points.Num() - 1;
	for (int32 Index = 0; Index < LastPointIndex; Index++) {
		DrawDebugLine(GetWorld(), NavigationData.Points[Index], NavigationData.Points[Index+1], FColor(0, 0, 255));
	}

	// UE_LOG(KLDebug, Warning, TEXT("UECSEngine::ProcessDebug: entity: %d, actor: %s, type: %s"), Entity, *Delegate.Owner->GetName(), *GET_ENUM_STRING("EECSObjectType", ObjectData.Type));
	// UE_LOG(KLDebug, Warning, TEXT("UECSEngine::ProcessDebug: AI enabled: %d, AI paused: %d, IsDead: %d"), AIData.bEnabled, AIData.bPause, ObjectData.bIsDead);
	// UE_LOG(KLDebug, Warning, TEXT("UECSEngine::ProcessDebug: AI state: %s, Movement: %s"), *GET_ENUM_STRING("EECSAIState", AIData.State), *GET_ENUM_STRING("EECSMovementType", AIData.Type));
	// UE_LOG(KLDebug, Warning, TEXT("UECSEngine::ProcessDebug: orientation: %s"), *GET_ENUM_STRING("EECSOrientation", PositionData.Orientation));
	// UE_LOG(KLDebug, Warning, TEXT("UECSEngine::ProcessDebug: sense target: %s"), *(SenseData.bSenseTarget ? SenseData.Actor->GetName() : "none"));
	//UE_LOG(KLDebug, Warning, TEXT("UECSEngine::ProcessDebug: Movement enabled: %d"), MovementData.bEnabled);
	//UE_LOG(KLDebug, Warning, TEXT("UECSEngine::ProcessDebug: Movement enabled: %d"), MovementData.bEnabled);

	// DebugDraw should not be used from inside ParallelFor
	// DrawDebugLine(GetWorld(), PositionData.Location, TargetData.Location, FColor(0, 255, 0));
	// DrawDebugLine(GetWorld(), PositionData.Location, TargetData.DesiredLocation, FColor(255, 0, 0));


	// UE_LOG(KLDebug, Warning, TEXT("UECSEngine::ProcessDebug: orient to target"));

	/*if (PositionData.Orientation == EECSOrientation::Target) {

		DrawDebugSphere(GetWorld(), TargetData.Location, 20, 32, FColor(255, 0, 0), false, 0.1f);

		FRotator Direction(0.f, PositionData.Yaw, 0.f);
		DrawDebugLine(GetWorld(), PositionData.Location, PositionData.Location + Direction.Vector() * 1000.f, FColor(255.f, 0, 0));
	}*/
}
