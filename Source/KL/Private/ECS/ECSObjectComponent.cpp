// Fill out your copyright notice in the Description page of Project Settings.

#include "ECS/ECSObjectComponent.h"
#include "Common/GameCharacter.h"
#include "ECS/ECSComponents.h"

#include "API/ECSEntity.h"
#include "Core/ECSEngine.h"

/* static */ FName UECSObjectComponent::SpawnedMobsCounter = FName("SpawnedMobs");

// Sets default values for this component's properties
UECSObjectComponent::UECSObjectComponent() {
	PrimaryComponentTick.bCanEverTick = false;
}

// Called when the game starts
void UECSObjectComponent::BeginPlay(){
	Super::BeginPlay();

	bIsECSActive = false;

	if (bActivateECS) {
		Register();
	}
}

bool UECSObjectComponent::Register() {
	if (bIsECSActive) {
		UE_LOG(KLError, Error, TEXT("ECSObjectComponent is already registered!"));
		return false;
	}

	const int32 ECSIndex = IECSEntity::GetEntity(GetOwner());
	if (ECSIndex == INDEX_NONE) {
		UE_LOG(KLError, Error, TEXT("Can't register ECSObjectComponent: can't get ECSIndex!"));
		return false;
	}

	UECSEngine* ECS = IECSEntity::GetEngine(GetOwner());
	ECS->UpdateEntity(ECSIndex);

	FECSDataObject& DataObject = ECS->Get<FECSDataObject>(ECSIndex);
	DataObject.bEnabled = true;
	DataObject.bIsDead = false;
	DataObject.Type = Type;
	DataObject.Aggr = 0.f;

	FECSDelegate& DataDelegate = ECS->Get<FECSDelegate>(ECSIndex);
	DataDelegate.ObjectDelegate = this;

	// // TODO: Implement ECS 'global' counters / components!
	// UE_LOG(KLDebug, Warning, TEXT("UECSObjectComponent::Register: TODO: Implement ECS 'global' counters / components!"));
	if (Type == EECSObjectType::Enemy) {
		const int32 counterIndex = ECS->RegisterCounter(UECSObjectComponent::SpawnedMobsCounter);
		ECS->IncrementCounter(counterIndex);
	}

	bIsECSActive = true;

	// Unify the death logic for non-GameCharacter actors
	AGameCharacter* Character = Cast<AGameCharacter>(GetOwner());
	if (Character) {
		Character->OnCharacterDead.AddUObject(this, &UECSObjectComponent::OnDeath);
		Character->OnCharacterRevive.AddUObject(this, &UECSObjectComponent::OnRevive);
	}

	return true;
}

bool UECSObjectComponent::Unregister() {
	if (!bIsECSActive) {
		return false;
	}

	AGameCharacter* CurrentCharacter = Cast<AGameCharacter>(GetOwner());
	if (IsValid(CurrentCharacter)) {
		CurrentCharacter->OnCharacterDead.RemoveAll(this);
		CurrentCharacter->OnCharacterRevive.RemoveAll(this);
	}

	const int32 ECSIndex = IECSEntity::GetEntity(GetOwner());
	UECSEngine* ECS = IECSEntity::GetEngine(GetOwner());

	FECSDelegate& DataDelegate = ECS->Get<FECSDelegate>(ECSIndex);
	DataDelegate.ObjectDelegate = nullptr;

	FECSDataObject& DataObject = ECS->Get<FECSDataObject>(ECSIndex);
	DataObject.bIsDead = true;
	DataObject.bEnabled = false;

	ECS->UpdateEntity(ECSIndex);

	// UE_LOG(KLDebug, Warning, TEXT("UECSObjectComponent::Unregister: TODO: Implement ECS 'global' counters / components!"));
	if (DataObject.Type == EECSObjectType::Enemy) {
		const int32 counterIndex = ECS->RegisterCounter(UECSObjectComponent::SpawnedMobsCounter);
		ECS->DecrementCounter(counterIndex);
	}

	ECSUnregistered();

	return true;
}

void UECSObjectComponent::ECSUnregistered() {
	bIsECSActive = false;
}

bool UECSObjectComponent::SetAggr(float Aggr) {
	if (!bIsECSActive) {
		return false;
	}

	const int32 ECSIndex = IECSEntity::GetEntity(GetOwner());
	UECSEngine* ECS = IECSEntity::GetEngine(GetOwner());

	FECSDataObject& DataObject = ECS->Get<FECSDataObject>(ECSIndex);
	DataObject.Aggr = Aggr;

	return true;
}

bool UECSObjectComponent::SetEntityDead(bool bIsDead) {
	if (!bIsECSActive) {
		return false;
	}

	const int32 ECSIndex = IECSEntity::GetEntity(GetOwner());
	UECSEngine* ECS = IECSEntity::GetEngine(GetOwner());

	FECSDataObject& DataObject = ECS->Get<FECSDataObject>(ECSIndex);
	DataObject.bIsDead = bIsDead;

	ECS->UpdateEntity(ECSIndex);

	return true;
}

void UECSObjectComponent::OnDeath(AGameCharacter* DeadOwner) {
	SetEntityDead(true);
}

void UECSObjectComponent::OnRevive(AGameCharacter* DeadOwner) {
	SetEntityDead(false);
}
