// Fill out your copyright notice in the Description page of Project Settings.

#include "ECS/ECSCollisionComponent.h"
#include "Common/KLGameMode.h"
#include "ECS/ECSComponents.h"

#include "API/ECSEntity.h"
#include "Core/ECSEngine.h"

// Sets default values for this component's properties
UECSCollisionComponent::UECSCollisionComponent() {
	PrimaryComponentTick.bCanEverTick = false;
}

// Called when the game starts
void UECSCollisionComponent::BeginPlay(){
	Super::BeginPlay();

	bIsECSActive = false;

	if (bActivateECS) {
		Register();
	}
}

bool UECSCollisionComponent::Register() {
	if (bIsECSActive) {
		UE_LOG(KLError, Error, TEXT("UECSCollisionComponent::Register: ECSCollisionComponent is already registered!"));
		return false;
	}

	const int32 ECSIndex = IECSEntity::GetEntity(GetOwner());
	if (ECSIndex == INDEX_NONE) {
		UE_LOG(KLError, Error, TEXT("UECSCollisionComponent::Register: Can't get ECSIndex!"));
		return false;
	}

	UECSEngine* ECS = IECSEntity::GetEngine(GetOwner());
	ECS->UpdateEntity(ECSIndex);

	FECSDelegate& DataDelegate = ECS->Get<FECSDelegate>(ECSIndex);
	DataDelegate.CollisionDelegate = this;

	FECSDataCollision& DataCollision = ECS->Get<FECSDataCollision>(ECSIndex);
	DataCollision.bEnabled = true;
	DataCollision.Radius = Radius;
	DataCollision.bGenerateOverlaps = bGenerateOverlaps;

	bIsECSActive = true;

	return true;
}
bool UECSCollisionComponent::Unregister() {
	if (!bIsECSActive) {
		return false;
	}

	UECSEngine* ECS = IECSEntity::GetEngine(GetOwner());
	const int32 ECSIndex = IECSEntity::GetEntity(GetOwner());

	FECSDataCollision& DataCollision = ECS->Get<FECSDataCollision>(ECSIndex);
	DataCollision.bEnabled = false;

	FECSDelegate& DataDelegate = ECS->Get<FECSDelegate>(ECSIndex);
	DataDelegate.CollisionDelegate = nullptr;

	ECSUnregistered();

	return true;
}

void UECSCollisionComponent::ECSUnregistered() {
	bIsECSActive = false;
}

void UECSCollisionComponent::SetRadius(float InRadius) {
	Radius = InRadius;

	if (!bIsECSActive) {
		return;
	}

	UECSEngine* ECS = IECSEntity::GetEngine(GetOwner());
	const int32 ECSIndex = IECSEntity::GetEntity(GetOwner());

	FECSDataCollision& DataCollision = ECS->Get<FECSDataCollision>(ECSIndex);
	DataCollision.Radius = Radius;
}

void UECSCollisionComponent::OnBeginOverlap(TArray<AActor*> InOverlappingActors) {
	OverlappingActors = InOverlappingActors;
	OnECSOverlapBegin.Broadcast(InOverlappingActors);
}

void UECSCollisionComponent::OnEndOverlap(TArray<AActor*> InOverlappingActors) {
	OverlappingActors = InOverlappingActors;
	OnECSOverlapEnd.Broadcast(InOverlappingActors);
}
