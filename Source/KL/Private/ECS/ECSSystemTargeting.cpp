// Fill out your copyright notice in the Description page of Project Settings.

#include "ECS/ECSSystemTargeting.h"
#include "ECS/ECSComponents.h"
#include "ECS/ECSObject.h"

#include "Core/ECSEngine.h"

DEFINE_STAT(STAT_ECSSystemTargeting);
#if STATS
TStatId UECSSystemTargeting::GetStatId() const {
	return GET_STATID(STAT_ECSSystemTargeting);
}
#endif

void UECSSystemTargeting::RequestInit() {
	Init<FECSDataAI, FECSDataObject, FECSDataMovement, FECSDataSense, FECSDataPosition, FECSDataTarget, FECSDataBoid>();
}

bool UECSSystemTargeting::CanProcess(int32 Entity) const {
	const FECSDataAI& AIData = Get<FECSDataAI>(Entity);
	const FECSDataObject& ObjectData = Get<FECSDataObject>(Entity);
	const FECSDataMovement& MovementData = Get<FECSDataMovement>(Entity);

	return AIData.bEnabled && MovementData.bEnabled && ObjectData.bEnabled && !ObjectData.bIsDead && ObjectData.Type != EECSObjectType::Player;
}

void UECSSystemTargeting::ProcessEntity(int32 Entity, float DeltaTime) {
	FECSDataAI& AIData = Get<FECSDataAI>(Entity);
	if (AIData.bPause) {
		return;
	}

	// TODO: think about how to reduce the required components amount?
	FECSDataObject& ObjectData = Get<FECSDataObject>(Entity);
	FECSDataMovement& MovementData = Get<FECSDataMovement>(Entity);
	FECSDataSense& SenseData = Get<FECSDataSense>(Entity);
	FECSDataPosition& PositionData = Get<FECSDataPosition>(Entity);
	FECSDataTarget& TargetData = Get<FECSDataTarget>(Entity);
	FECSDataBoid& BoidData = Get<FECSDataBoid>(Entity);
	BoidData.bPause = false;

	if (PositionData.Orientation != EECSOrientation::None) {
		PositionData.Orientation = EECSOrientation::Movement;
	}

	FVector TargetVector = SenseData.Location - PositionData.Location;
	float Distance = TargetVector.Size2D();
	float AlignDot = TargetVector | SenseData.Velocity;

	// Previous state & current state can be equal if Targeting system inited after the AI system
	bool bNewState = (AIData.PreviousState != AIData.State) || (AIData.PreviousState == EECSAIState::Idle) || (AIData.Type == EECSMovementType::Idle);
	// TODO: сделать отдельный обработчик на начало стейта / смену стейта
	// в этом обработчике применять рандомные выборы, чтобы не менять их на каждый тик
	// //
	// Сделать несколько радиусов нахождения мобов вокруг игрока
	//
	AIData.Alienation = 1;

	switch (AIData.State) {
	case EECSAIState::InitRoaming:
		AIData.UpdateTargetTimeout = 0.f;
		AIData.State = EECSAIState::Roaming;

		if (AIData.MasterEntity == INDEX_NONE) {
			// AIData.Type = EECSMovementType::Random;
			AIData.Type = AIData.bAttackBanned && AlignDot >= 0 ? EECSMovementType::Align : EECSMovementType::AxisFlocking;
		} else {
			AIData.Type = EECSMovementType::Formation;
		}

		// Check in case if MasterEntity was reset in AI processing
		if (MovementData.MasterEntity != AIData.MasterEntity) {
			MovementData.MasterEntity = AIData.MasterEntity;
		}

		if (PositionData.Orientation == EECSOrientation::Target) {
			PositionData.Orientation = EECSOrientation::Movement;
		}

		//fallthrough;
	case EECSAIState::Roaming:
		if (AIData.RoamingDistance <= 0.f) {
			AIData.Type = EECSMovementType::Direct;
			break;
		} else if (AIData.Type == EECSMovementType::Direct) {
			// TODO: remove duplication & improve switch
			if (AIData.MasterEntity == INDEX_NONE) {
				// AIData.Type = EECSMovementType::Random;
				AIData.Type = EECSMovementType::AxisFlocking; //FMath::RandBool() ? EECSMovementType::AxisFlocking : EECSMovementType::Flocking;
			} else {
				AIData.Type = EECSMovementType::Formation;
			}
		}

		break;
	case EECSAIState::Idle:
		// Do nothing
		AIData.Type = EECSMovementType::Idle;
		break;
	case EECSAIState::Chase:
	case EECSAIState::ChaseByAggression:
		if (bNewState) {
			AIData.Type = FMath::RandBool() ? EECSMovementType::Align : EECSMovementType::Flocking;
		}

		AIData.Alienation = 2;
		break;
	case EECSAIState::Attack:
	case EECSAIState::AttackByAggression:
		AIData.Alienation = 3;
		// Don't consider Orientation here because all the targeting logic is for non-player entities
		if (ObjectData.bIsBusy && AIData.ActionType != ESkillType::Melee) {
			AIData.Type = EECSMovementType::Aim;
		} else if (AIData.MasterEntity == INDEX_NONE) {
			AIData.Type = EECSMovementType::Advance;
		} else {
			AIData.Type = EECSMovementType::Direct;
		}

		BoidData.bPause = true;
		break;
	case EECSAIState::InitPanic:
		AIData.UpdateTargetTimeout = 0.f;
		AIData.PanicTimeout = 2.f;
		AIData.State = EECSAIState::Flee;

		// fallthrough
	case EECSAIState::Flee:
		if (AIData.PanicTimeout > 0) {
			AIData.Type = EECSMovementType::Panic;
		} else {
			AIData.UpdateTargetTimeout = 0.f;
			AIData.State = EECSAIState::Idle;
			AIData.Type = EECSMovementType::Idle;
		}

		break;
	}

	if (AIData.Type != EECSMovementType::AxisFlocking) {
		AIData.Direction = EECSMovementDirection::None;
	}

	bool bTargetUpdated = false;

	switch (AIData.Type) {
	case EECSMovementType::Aim: {
		// TODO: improve targeting, consider distance and projectile speed

		// TODO: Calculate average velocity direction to avoid flickering
		// Should be calculated in Sense system (AI system?)
		FVector Advance = SenseData.Velocity * 0.4;
		TargetData.DesiredLocation = SenseData.Location + SenseData.Velocity;

		PositionData.Orientation = EECSOrientation::Target;

		bTargetUpdated = true;
		break;
	}
	case EECSMovementType::Direct:
		TargetData.DesiredLocation = SenseData.Location;

		bTargetUpdated = true;
		break;
	case EECSMovementType::Advance: {
		FVector Advance = SenseData.Velocity * 2;
		if (Distance > Advance.Size2D()) {
			TargetData.DesiredLocation = SenseData.Location + Advance;
		} else {
			TargetData.DesiredLocation = SenseData.Location;
		}

		bTargetUpdated = true;
		break;
	}
	case EECSMovementType::AxisFlocking: {
		if (AIData.UpdateTargetTimeout > 0.f) {
			// Wait for the timeout before updating target location
			// UE_LOG(KLDebug, Warning, TEXT("Wait timeout (#%d): %f"), Index, AIData.UpdateTargetTimeout);
			AIData.UpdateTargetTimeout -= DeltaTime;
			break;
		}

		EECSMovementDirection Direction = UECSSystemTargeting::GetDirectionToTarget(PositionData.Location, SenseData.Location);
		EECSMovementDirection ShortDirection = UECSSystemTargeting::GetDirectionToTarget(PositionData.Location, SenseData.Location, true);

		// UE_LOG(KLDebug, Warning, TEXT("UECSSystemTargeting::ProcessTargeting: update axis target: %s"), *GET_ENUM_STRING("EECSMovementDirection", Direction));

		if (AIData.Direction == EECSMovementDirection::None) {
			// If it's a new AxisFlocking state
			AIData.Direction = Direction;
		} else if ((UECSSystemTargeting::IsOppositeDirection(AIData.Direction, Direction) || UECSSystemTargeting::IsOppositeDirection(AIData.Direction, ShortDirection)) || UECSSystemTargeting::IsReachedTargetAxis(PositionData.Location, SenseData.Location)) {
			// If direction should be changed
			AIData.Direction = Direction;
		}

		// if (AIData.Alienation - Distance > 0) {
		// 	AIData.Direction = GetOppositeDirection(Direction);
		// }

		TargetData.DesiredLocation = PositionData.Location + UECSSystemTargeting::GetRoamingDirection(AIData.Direction, AIData.RoamingVarianceAngle) * AIData.RoamingDistance;

		AIData.UpdateTargetTimeout = FMath::Min(3.f, AIData.RoamingDistance / MovementData.Speed);

		// if (FMath::Abs(AIData.Alienation - Distance) < 500.f) {
		// 	AIData.Type = EECSMovementType::Roundelay;
		// }

		bTargetUpdated = true;
		break;
	}
	case EECSMovementType::Flocking:
		if (AIData.UpdateTargetTimeout > 0.f) {
			// Wait for the timeout before updating target location
			// UE_LOG(KLDebug, Warning, TEXT("Wait timeout (#%d): %f"), Index, AIData.UpdateTargetTimeout);
			AIData.UpdateTargetTimeout -= DeltaTime;
			break;
		}

		TargetData.DesiredLocation = SenseData.Location;

		AIData.UpdateTargetTimeout = FMath::Min(1.f, AIData.RoamingDistance / MovementData.Speed);

		bTargetUpdated = true;

		break;
	case EECSMovementType::Roundelay: {
		if (AIData.UpdateTargetTimeout > 0.f) {
			// Wait for the timeout before updating target location
			AIData.UpdateTargetTimeout -= DeltaTime;
			break;
		}

		FVector AnchorVector;

		if (FMath::RandBool()) {
			EECSMovementDirection Direction = UECSSystemTargeting::GetDirectionToTarget(PositionData.Location, SenseData.Location);
			AnchorVector = UECSSystemTargeting::GetRoamingDirection(Direction, AIData.RoamingVarianceAngle);
		} else {
			AnchorVector = (SenseData.Location - PositionData.Location).GetSafeNormal2D();
		}

		FVector DirectionVector = AnchorVector ^ FVector(0,0, Entity % 2 == 0 ? 1 : -1);
		TargetData.DesiredLocation = PositionData.Location + DirectionVector * AIData.RoamingDistance;

		AIData.UpdateTargetTimeout = MovementData.Speed > 0.f ? AIData.RoamingDistance / MovementData.Speed : 3.f;

		// if (FMath::Abs(AIData.Alienation - Distance) < 500.f) {
		// 	AIData.Type = EECSMovementType::AxisFlocking;
		// }

		bTargetUpdated = true;
		break;
	}
	case EECSMovementType::Align: {
		if (AIData.UpdateTargetTimeout > 0.f) {
			// Wait for the timeout before updating target location
			AIData.UpdateTargetTimeout -= DeltaTime;
			break;
		}

		if (AlignDot < 0 || SenseData.Velocity.IsZero()) {
			AIData.Type = EECSMovementType::AxisFlocking;
			break;
		}

		TargetData.DesiredLocation = PositionData.Location + SenseData.Velocity.GetSafeNormal2D() * AIData.RoamingDistance;
		AIData.UpdateTargetTimeout = FMath::Min(1.f, MovementData.Speed > 0.f ? AIData.RoamingDistance / MovementData.Speed : 1.f);
	}
	case EECSMovementType::Random: {
		BoidData.bPause = true;

		if (AIData.UpdateTargetTimeout > 0.f) {
			// Wait for the timeout before updating target location
			// UE_LOG(KLDebug, Warning, TEXT("Wait timeout (#%d): %f"), Index, AIData.UpdateTargetTimeout);
			AIData.UpdateTargetTimeout -= DeltaTime;
			break;
		}

		FVector Direction = SenseData.Location - PositionData.Location;
		float Bias = 1.2f;

		// Should limit only one axis, otherwise entities will move directly to the target
		bool bLimitX = FMath::RandBool();
		float BiasX = bLimitX ? Bias : 1.f;
		float BiasY = bLimitX ? 1.f : Bias;

		float TargetX = Direction.X > 0 ? FMath::FRandRange(-1, BiasX) : FMath::FRandRange(-BiasX, 1);
		float TargetY = Direction.Y > 0 ? FMath::FRandRange(-1, BiasY) : FMath::FRandRange(-BiasY, 1);

		// TODO: update random targeting to make targets only inside the current level
		TargetData.DesiredLocation = PositionData.Location + FVector(TargetX, TargetY, 0.f).GetSafeNormal2D() * 1000.f;
		float DistanceLeft = FVector::Dist(PositionData.Location, TargetData.DesiredLocation);

		AIData.UpdateTargetTimeout = MovementData.Speed > 0.f ? DistanceLeft / MovementData.Speed : 5.f;
		// UE_LOG(KLDebug, Warning, TEXT("Set timeout (#%d): %f"), Index, AIData.UpdateTargetTimeout);

		bTargetUpdated = true;
		break;
	}
	case EECSMovementType::Formation: {
		const FECSDataMovement& MasterMovement = Get<FECSDataMovement>(AIData.MasterEntity);
		const FECSDataPosition& MasterPosition = Get<FECSDataPosition>(AIData.MasterEntity);
		const FECSDataTarget& MasterTarget = Get<FECSDataTarget>(AIData.MasterEntity);

		TargetData.DesiredLocation = MasterTarget.DesiredLocation;

		bTargetUpdated = true;
		break;
	}
	case EECSMovementType::Panic: {
		AIData.PanicTimeout -= DeltaTime;

		// Update targeting if reached the previous target, not just by timeout
		if (AIData.UpdateTargetTimeout > 0.f) {
			AIData.UpdateTargetTimeout -= DeltaTime;
			break;
		}

		FVector Direction = (TargetData.Location - SenseData.Location).GetSafeNormal2D();
		// Calculate distance based on the Entity speed instead of fixed value
		TargetData.DesiredLocation = PositionData.Location + Direction * 2000.f;

		AIData.UpdateTargetTimeout = 1.f;

		bTargetUpdated = true;
		break;
	}
	case EECSMovementType::Idle:
		// do nothing
		break;
	}

	if (bTargetUpdated) {
		PositionData.bDestinationReached = false;
		PositionData.bDestinationReachedSent = false;
	}

	MovementData.Type = AIData.Type;
}

EECSMovementDirection UECSSystemTargeting::GetOppositeDirection(EECSMovementDirection Direction) {
	switch (Direction) {
	case EECSMovementDirection::Up:
		return EECSMovementDirection::Down;
	case EECSMovementDirection::Down:
		return EECSMovementDirection::Up;
	case EECSMovementDirection::Left:
		return EECSMovementDirection::Right;
	case EECSMovementDirection::Right:
		return EECSMovementDirection::Left;
	default:
		return EECSMovementDirection::None;
	}
}

bool UECSSystemTargeting::IsVerticalDirection(EECSMovementDirection Direction) {
	return Direction == EECSMovementDirection::Down || Direction == EECSMovementDirection::Up;
}

bool UECSSystemTargeting::IsOppositeDirection(EECSMovementDirection D1, EECSMovementDirection D2) {
	return (UECSSystemTargeting::IsVerticalDirection(D1) == UECSSystemTargeting::IsVerticalDirection(D2)) && (D1 != D2);
}

EECSMovementDirection UECSSystemTargeting::GetDirectionToTarget(FVector Location, FVector Target, bool bNeedShort) {
	// Consider the current target rotation instead of 90-degrees from world directions

	FVector Distance = Target - Location;

	bool bIsX = FMath::Abs(Distance.X) >= FMath::Abs(Distance.Y);

	if (bIsX != bNeedShort) {
		if (Distance.X < 0) {
			return EECSMovementDirection::Down;
		}

		return EECSMovementDirection::Up;
	}

	if (Distance.Y < 0) {
		return EECSMovementDirection::Left;
	}

	return EECSMovementDirection::Right;
}

FVector UECSSystemTargeting::GetRoamingDirection(EECSMovementDirection Direction, float MaxVariance) {
	FVector DirectionVector;

	switch (Direction) {
	case EECSMovementDirection::Up:
		DirectionVector = FVector(1.f, 0.f, 0.f);
		break;
	case EECSMovementDirection::Down:
		DirectionVector = FVector(-1.f, 0.f, 0.f);
		break;
	case EECSMovementDirection::Left:
		DirectionVector = FVector(0.f, -1.f, 0.f);
		break;
	case EECSMovementDirection::Right:
		DirectionVector = FVector(0.f, 1.f, 0.f);
		break;
	default:
		DirectionVector = FVector::ZeroVector;
	}

	if (MaxVariance > 0.f) {
		float Variance = FMath::FRandRange(-MaxVariance, MaxVariance);
		FRotator VarianceRotator = FRotator(0.f, Variance, 0.f);
		return VarianceRotator.RotateVector(DirectionVector);
	}

	return DirectionVector;
}

bool UECSSystemTargeting::IsReachedTargetAxis(FVector Location, FVector Target) {
	float Accuracy = 300.f;

	FVector Diff = Location - Target;

	float diffX = FMath::Abs(Diff.X);
	float diffY = FMath::Abs(Diff.Y);

	// UE_LOG(KLDebug, Warning, TEXT("UECSSystemTargeting:IsReachedTargetAxis: X: %f, Y: %f"), diffX, diffY);

	return (diffX < Accuracy) || (diffY < Accuracy);
}
