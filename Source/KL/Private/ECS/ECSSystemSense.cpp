// Fill out your copyright notice in the Description page of Project Settings.

#include "ECS/ECSSystemSense.h"
#include "ECS/ECSComponents.h"
#include "ECS/ECSObject.h"

#include "Core/ECSEngine.h"

DEFINE_STAT(STAT_ECSSystemSense);
#if STATS
TStatId UECSSystemSense::GetStatId() const {
	return GET_STATID(STAT_ECSSystemSense);
}
#endif

void UECSSystemSense::RequestInit() {
	Init<FECSDataAI, FECSDataObject, FECSDataSense, FECSDataPosition, FECSDataMovement>();
}

bool UECSSystemSense::CanProcess(int32 Entity) const {
	const FECSDataObject& ObjectData = Get<FECSDataObject>(Entity);
	const FECSDataAI& AIData = Get<FECSDataAI>(Entity);

	bool bValidObject = ObjectData.Type == EECSObjectType::Enemy;

	return bValidObject && ObjectData.bEnabled && !ObjectData.bIsDead && AIData.bEnabled;
}

void UECSSystemSense::ProcessEntity(int32 Entity, float DeltaTime) {
	FECSDataAI& AIData = Get<FECSDataAI>(Entity);
	if (AIData.bPause) {
		return;
	}

	FECSDataSense& SenseData = Get<FECSDataSense>(Entity);

	bool bFollowingMaster = AIData.MasterEntity != INDEX_NONE;
	if (bFollowingMaster) {
		// If moving in a formation, then copy Master's sense
		// TODO: the Summoned entity should process own sense

		const FECSDataObject& MasterObject = Get<FECSDataObject>(AIData.MasterEntity);
		const FECSDataSense& MasterSense = Get<FECSDataSense>(AIData.MasterEntity);

		// If Master is dead or not enabled then stop following and act by it's own
		if (MasterObject.bIsDead || !MasterSense.bEnabled) {
			AIData.MasterEntity = INDEX_NONE;
			AIData.State = EECSAIState::InitRoaming;

			return;
		}

		SenseData.bSenseTarget = MasterSense.bSenseTarget;
		SenseData.Location = MasterSense.Location;
		SenseData.Actor = MasterSense.Actor;
		SenseData.Velocity = MasterSense.Velocity;

		return;
	}

	// Find actor target

	FECSDataObject& ObjectData = Get<FECSDataObject>(Entity);
	FECSDataPosition& PositionData = Get<FECSDataPosition>(Entity);
	FVector CurrentLocation = PositionData.Location;

	float ClosestDistance = MAX_VISIBILITY_DISTANCE;
	float MaxAggression = 0.f;

	FVector TargetLocation;
	AActor* TargetActor = nullptr;
	FVector TargetVelocity = FVector::ZeroVector;
	float TargetAggression = 0;

	// Loop through all entities to find relevant targets
	// Should process all entities including those who's not registered for the AI system
	const TArray<int32>& AllEntities = ECS->GetEntities();
	for (int32 TargetIndex : AllEntities) {
		// Don't target to itself
		if (TargetIndex == Entity) {
			continue;
		}

		FECSDataObject& TargetObject = Get<FECSDataObject>(TargetIndex);
		if (TargetObject.bIsDead) {
			continue;
		}

		bool bValidTarget;

		switch (ObjectData.Type) {
		case EECSObjectType::Enemy:
			bValidTarget = TargetObject.Type == EECSObjectType::Player || TargetObject.Type == EECSObjectType::Summoned;
			break;
		case EECSObjectType::Player:
		case EECSObjectType::Summoned:
			bValidTarget = TargetObject.Type == EECSObjectType::Enemy;
			break;
		default:
			bValidTarget = false;
		}

		if (!bValidTarget) {
			continue;
		}

		FECSDataPosition& TargetPosition = Get<FECSDataPosition>(TargetIndex);

		float Distance = (TargetPosition.Location - CurrentLocation).Size2D();
		TargetAggression = TargetObject.Aggr;
		TargetAggression = Distance <= FMath::Abs(TargetAggression) ? TargetAggression : 0.f;

		if (FMath::Abs(TargetAggression) > FMath::Abs(MaxAggression) || (TargetAggression == MaxAggression && Distance <= ClosestDistance)) {
			FECSDelegate& TargetDelegate = Get<FECSDelegate>(TargetIndex);
			FECSDataMovement& TargetMovement = Get<FECSDataMovement>(TargetIndex);

			MaxAggression = TargetAggression;
			ClosestDistance = Distance;
			TargetLocation = TargetPosition.Location;
			TargetActor = TargetDelegate.Owner;
			TargetVelocity = TargetMovement.Velocity;
		}
	}

	SenseData.bSenseTarget = TargetActor != nullptr;
	if (SenseData.bSenseTarget) {
		// TODO: improve average velocity detection:
		// instead of just last 20 frames, record by timing (for the last N seconds)
		if (SenseData.RecentVelocity.Num() > 20) {
			SenseData.RecentVelocity.RemoveAt(0, 1, false);
		}

		SenseData.RecentVelocity.Add(TargetVelocity);

		SenseData.Location = TargetLocation;
		SenseData.Actor = TargetActor;
		SenseData.Aggression = TargetAggression;

		FVector AverageVelocity = FVector::ZeroVector;
		for (FVector Velocity : SenseData.RecentVelocity) {
			AverageVelocity += Velocity;
		}

		SenseData.Velocity = AverageVelocity / SenseData.RecentVelocity.Num();
	}
}
