// Fill out your copyright notice in the Description page of Project Settings.

#include "Skills/StatusManagerComponent.h"
#include "Skills/SkillEffect.h"

UStatusManagerComponent::UStatusManagerComponent() {
	PrimaryComponentTick.bCanEverTick = true;

	ActiveStatuses = TMap<EStatusType, ASkillEffect*>();
	EventHandlers = TMap<EStatusType, FDelegateHandle>();
}

void UStatusManagerComponent::BeginPlay() {
	Super::BeginPlay();

}

void UStatusManagerComponent::TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) {
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (!ShouldTick()) {
		return;
	}

	for (auto StatusPair : ActiveStatuses) {
		ASkillEffect* Status = StatusPair.Value;

		// Status->TickStatus();
	}
}

bool UStatusManagerComponent::ShouldTick() const {
	return false;
	// return ActiveStatuses.Num() > 0;
}

bool UStatusManagerComponent::IsStatusExist(EStatusType Type) const {
	return ActiveStatuses.Contains(Type);
}

void UStatusManagerComponent::RegisterStatus(EStatusType Type, ASkillEffect* StatusEffect) {
	bool bNeedRenew = IsStatusExist(Type);

	if (bNeedRenew) {
		// TODO: decide if need to cancel or not
		// Don't cancel if the new status if weaker than existing one

		// The "renew" method should silently cancel the existing status effect
		ASkillEffect* ExistingStatus = ActiveStatuses[Type];
		ExistingStatus->OnEffectEnd.Remove(EventHandlers[Type]);
		ExistingStatus->RenewStatus();
	}

	// TODO: implement status time management here instead of the Basic_Status BP

	StatusEffect->StatusType = Type;
	ActiveStatuses.Emplace(Type, StatusEffect);

	FDelegateHandle EventHandler = StatusEffect->OnEffectEnd.AddUObject(this, &UStatusManagerComponent::UnregisterStatus);
	EventHandlers.Emplace(Type, EventHandler);

	if (bNeedRenew) {
		OnStatusRenewed.Broadcast(Type);
	} else {
		OnStatusAdded.Broadcast(Type);
	}
}

void UStatusManagerComponent::UnregisterStatus(EStatusType Type) {
	ActiveStatuses.Remove(Type);

	OnStatusRemoved.Broadcast(Type);
}
