// Fill out your copyright notice in the Description page of Project Settings.

#include "Skills/SkillsComponent.h"
#include "Skills/SkillLauncher.h"
#include "Skills/SkillData.h"
#include "Common/GameCharacter.h"

USkillsComponent::USkillsComponent() {
	PrimaryComponentTick.bCanEverTick = false;
	bWantsInitializeComponent = true;

	SkillsList = TArray<USkillLauncher*>();
}

void USkillsComponent::InitializeComponent() {
	SkillsList.Reset();

	for (auto SkillData : AvailableSkills) {
		USkillLauncher* Launcher = NewObject<USkillLauncher>(this);
		Launcher->SetupSkill(Cast<AGameCharacter>(this->GetOwner()));

		if (IsValid(SkillData)) {
			Launcher->SetNewSkill(SkillData);
		}

		SkillsList.Add(Launcher);
	}

	Super::InitializeComponent();
}

void USkillsComponent::BeginPlay() {
	Super::BeginPlay();
}

TArray<USkillLauncher*> USkillsComponent::GetSkillsList() const {
	return SkillsList;
}
