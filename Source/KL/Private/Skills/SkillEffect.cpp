// Fill out your copyright notice in the Description page of Project Settings.

#include "Skills/SkillEffect.h"
#include "Skills/SkillData.h"
#include "Common/GameCharacter.h"

// Sets default values
ASkillEffect::ASkillEffect(){
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	TargetActors = TArray<AGameCharacter*>();
}

// Called when the game starts or when spawned
void ASkillEffect::BeginPlay(){
	Super::BeginPlay();

}

// Called every frame
void ASkillEffect::Tick(float DeltaTime){
	Super::Tick(DeltaTime);

}

void ASkillEffect::Setup(AGameCharacter* InOwner, USkillData* InSkillData, const FVector InTargetVector, const TArray<AGameCharacter*>& InTargetActors, const EInstigatorType InInstigatorType, AGameCharacter* const InBoundTo) {
	SkillOwner = InOwner;
	InstigatorType = InInstigatorType;
	if (InstigatorType == EInstigatorType::Default) {
		UE_LOG(KLLog, Warning, TEXT("ASkillEffect::Setup: EInstigatorType::Default should not be used yet"));
	}

	SkillData = InSkillData;

	TargetVector = InTargetVector;
	TargetActors = InTargetActors;

	if (IsValid(InBoundTo)) {
		BoundTo = InBoundTo;
	} else {
		BoundTo = SkillOwner;
	}

	Launch();
}

void ASkillEffect::EffectStart() {
	OnEffectStart.Broadcast(StatusType);
}

void ASkillEffect::EffectLaunch() {
	OnEffectLaunch.Broadcast(StatusType);
}

void ASkillEffect::EffectEnd() {
	OnEffectEnd.Broadcast(StatusType);
}

void ASkillEffect::Launch_Implementation() {
	// TODO: handle SkillEffect object cleanup/destroy
	// currently it's done manually in FireBolt_Skill BP
	EffectStart();
	if (!SkillData->bContinuos) {
		EffectLaunch();
		EffectEnd();
	}
}

void ASkillEffect::DeactivateTrigger() {
	bDeactivated = true;
	Deactivate();
}

void ASkillEffect::Deactivate_Implementation() {
	// Implement in BP for continuos skills
	if (SkillData->bContinuos) {
		EffectLaunch();
		EffectEnd();
	}
}

void ASkillEffect::RenewStatus_Implementation() {
	// Implement for Basic_Status skills
}

EInstigatorType ASkillEffect::GetInstigatorType() const {
	return InstigatorType;
}
