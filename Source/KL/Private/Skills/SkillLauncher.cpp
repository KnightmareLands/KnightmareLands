// Fill out your copyright notice in the Description page of Project Settings.

#include "Skills/SkillLauncher.h"
#include "Common/GameCharacter.h"
#include "Player/PlayerCharacter.h"
#include "Mobs/MobCharacter.h"
#include "Skills/SkillEffect.h"
#include "Skills/SkillData.h"
#include "GameGlobal.h"
#include "Subsystems/KLTimeFactorSubsystem.h"

static const FString ContextString(TEXT("SkillLauncher"));

// Sets default values
USkillLauncher::USkillLauncher() {
	ActiveSkill = nullptr;
}

void USkillLauncher::SetupSkill(AGameCharacter* InOwner){
	if (IsValid(Owner)) {
		UE_LOG(KLError, Error, TEXT("USkillLauncher::SetupSkill: Skill is already inited with Owner!"));
		return;
	}

	Owner = InOwner;
	Owner->OnCharacterDead.AddUObject(this, &USkillLauncher::OnOwnerDeath);
	Owner->OnCharacterReplaced.AddDynamic(this, &USkillLauncher::OnOwnerReplaced);

	auto GameInstance = Owner->GetGameInstance();
	if (GameInstance) {
		TimeFactor = GameInstance->GetSubsystem<UKLTimeFactorSubsystem>();
	}
}

void USkillLauncher::SetNewSkill(USkillData* InSkillData) {
	if (!ensure(InSkillData)) {
		UE_LOG(KLLog, Error, TEXT("USkillLauncher::SetNewSkill: Owner: %s"), *Owner->GetName());
		return;
	}

	Deactivate();
	ResetCooldown();

	SkillData = InSkillData;

	OnUpdate.Broadcast();
}

void USkillLauncher::RemoveSkill() {
	Deactivate();
	ResetCooldown();
	SkillData = nullptr;

	OnUpdate.Broadcast();
}

bool USkillLauncher::HasSkill(const USkillData* InSkillData) const {
	if (InSkillData == nullptr || SkillData == nullptr) {
		return false;
	}

	return InSkillData->GetFName() == SkillData->GetFName();
}

FName USkillLauncher::GetTitle() const {
	if (!IsValid(SkillData)) {
		return FName("");
	}

	return SkillData->Title;
}

UTexture2D* USkillLauncher::GetIcon() const {
	if (!IsValid(SkillData)) {
		return nullptr;
	}

	return SkillData->Icon;
}

float USkillLauncher::GetAttackRange() const {
	if (!IsValid(SkillData)) {
		return 0.f;
	}

	return SkillData->Range;
}

float USkillLauncher::GetCooldown() const {
	if (!IsValid(SkillData)) {
		return 0.f;
	}

	return SkillData->Cooldown / Owner->AttributeSkillSpeed->Get();
}

float USkillLauncher::GetCastingTime() const {
	if (!IsValid(SkillData)) {
		return 0.f;
	}

	return SkillData->CastingTime / Owner->AttributeSkillSpeed->Get();
}

bool USkillLauncher::IsMelee() const {
	if (!IsValid(SkillData)) {
		return false;
	}

	return SkillData->SkillType == ESkillType::Melee;
}

ESkillType USkillLauncher::GetSkillType() const {
	if (!IsValid(SkillData)) {
		return ESkillType::Cast;
	}

	return SkillData->SkillType;
}

void USkillLauncher::Tick(float DeltaTime) {
	// Don't tick when the game is paused
	if (GetWorld()->GetAuthGameMode()->IsPaused()) {
		return;
	}

	float CooldownRate = 1 / GetCooldown();
	CooldownLeft -= DeltaTime * CooldownRate * TimeFactor->GetPlayerAttackFactor();
	CooldownLeft = FMath::Clamp(CooldownLeft, 0.f, 1.f);

	if (CooldownLeft == 0.f) {
		bShouldTick = false;
	}
}

// Return 0.0..1.0
float USkillLauncher::GetRemainingCooldown() const {
	return CooldownLeft;
}

bool USkillLauncher::IsCooldownReady() const {
	return GetRemainingCooldown() == 0.f;
}

void USkillLauncher::ResetCooldown() {
	CooldownLeft = 0.f;
	bShouldTick = false;
}

bool USkillLauncher::IsInited() const {
	return IsValid(SkillData);
}

bool USkillLauncher::CanBeUsed() const {
	bool bActive = ActiveSkill != nullptr || IsValid(ActiveSkill);
	return IsInited() && IsCooldownReady() && !bActive;
}

bool USkillLauncher::IsActive() const {
	return ActiveSkill != nullptr && IsValid(ActiveSkill);
}

ETargetingType USkillLauncher::GetTargetingType() const {
	if (!IsInited()) {
		return ETargetingType::NoTarget;
	}

	return SkillData->TargetingType;
}

bool USkillLauncher::Launch(const FTargetingInfo InTargetingInfo) {
	if (!CanBeUsed()) {
		return false;
	}

	FVector InTargetVector;
	TArray<AGameCharacter*> InTargetActors;

	if (SkillData->TargetingType != ETargetingType::NoTarget) {
		InTargetVector = InTargetingInfo.Location;
		InTargetActors = InTargetingInfo.Actors;
	}

	Owner->StartBusy();

	auto Location = Owner->GetActorLocation();
	auto Rotation = Owner->GetActorRotation();

	FActorSpawnParameters SpawnParameters;
	SpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

	ActiveSkill = GetWorld()->SpawnActor<ASkillEffect>(SkillData->SkillEffect, Location, Rotation, SpawnParameters);
	ActiveSkill->OnEffectStart.AddUObject(this, &USkillLauncher::LaunchStart);
	ActiveSkill->OnEffectLaunch.AddUObject(this, &USkillLauncher::LaunchDone);
	ActiveSkill->OnEffectEnd.AddUObject(this, &USkillLauncher::LaunchEnd);

	EInstigatorType InstigatorType = Owner->IsA(APlayerCharacter::StaticClass()) ? EInstigatorType::Player : EInstigatorType::Enemy;
	ActiveSkill->Setup(Owner, SkillData, InTargetVector, InTargetActors, InstigatorType);

	return true;
}

void USkillLauncher::OnOwnerDeath(AGameCharacter* DeadOwner) {
	Deactivate();
}

void USkillLauncher::OnOwnerReplaced() {
	Deactivate();
}

void USkillLauncher::Deactivate() {
	if (ActiveSkill == nullptr || !IsValid(ActiveSkill)) {
		return;
	}

	ActiveSkill->DeactivateTrigger();
	if (ActiveSkill != nullptr) {
		Cleanup();
	}
}

void USkillLauncher::LaunchDone(EStatusType Type) {
	CooldownLeft = 1.f;
	bShouldTick = true;

	Cleanup();
}

void USkillLauncher::LaunchStart(EStatusType Type) {

}

void USkillLauncher::LaunchEnd(EStatusType Type) {

}

void USkillLauncher::Cleanup() {
	ActiveSkill = nullptr;
	Owner->EndBusy();
}

bool USkillLauncher::IsContinuos() const {
	return SkillData->bContinuos;
}

bool USkillLauncher::IsPositive() const {
	return SkillData->bPositive;
}
