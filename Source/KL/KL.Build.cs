// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class KL : ModuleRules {
	public KL(ReadOnlyTargetRules Target) : base (Target) {
		PrivatePCHHeaderFile = "KL.h";

		// UE4 modules
		PublicDependencyModuleNames.AddRange(new string[] {
			"Core",
			"CoreUObject",
			"Engine",
			"InputCore",
			"Niagara",
			"UMG",
			"Slate",
			"SlateCore",
			"NavigationSystem",
		});

		// Custom modules
		PublicDependencyModuleNames.AddRange(new string[] {
			"NorlinECS",
			"SpawnWaves",
		});
	}
}
