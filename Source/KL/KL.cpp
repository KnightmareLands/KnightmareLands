// Fill out your copyright notice in the Description page of Project Settings.

#include "KL.h"


IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, KL, "KL" );

DEFINE_LOG_CATEGORY(KLLog)
DEFINE_LOG_CATEGORY(KLError)
DEFINE_LOG_CATEGORY(KLDebug)
