// Fill out your copyright notice in the Description page of Project Settings.

#ifndef __KL_H__
#define __KL_H__

#include "EngineMinimal.h"
#include "Runtime/UMG/Public/UMG.h"
#include "Runtime/UMG/Public/UMGStyle.h"
#include "Runtime/UMG/Public/Slate/SObjectWidget.h"
#include "Runtime/UMG/Public/IUMGModule.h"
#include "Runtime/UMG/Public/Blueprint/UserWidget.h"

DECLARE_LOG_CATEGORY_EXTERN(KLLog, Verbose, All);
DECLARE_LOG_CATEGORY_EXTERN(KLError, Error, All);
DECLARE_LOG_CATEGORY_EXTERN(KLDebug, Verbose, All);

#define GET_ENUM_STRING(etype, evalue) ( (FindObject<UEnum>(ANY_PACKAGE, TEXT(etype), true) != nullptr) ? FindObject<UEnum>(ANY_PACKAGE, TEXT(etype), true)->GetNameStringByIndex((uint8)evalue) : FString("Invalid - are you sure enum uses UENUM() macro?") )

#endif
