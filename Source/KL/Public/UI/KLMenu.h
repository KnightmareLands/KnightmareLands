// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Common/GestureType.h"
#include "KLMenu.generated.h"

/**
 * The base class for the fullscreen menus to handle player input
 */
UCLASS()
class KL_API UKLMenu : public UUserWidget {
	GENERATED_BODY()

public:

	UFUNCTION(BlueprintImplementableEvent, Category = "!KL: Menu")
	void Show(FVector2D Location = FVector2D::ZeroVector);

	UFUNCTION(BlueprintImplementableEvent, Category = "!KL: Menu")
	void Hide(EGestureType GestureType = EGestureType::None);

	UFUNCTION(BlueprintImplementableEvent, Category = "!KL: Menu")
	void Update(EGestureType GestureType);

};
