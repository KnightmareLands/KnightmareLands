// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Animation/AnimInstance.h"
#include "KLAnimation.generated.h"

class AGameCharacter;

/**
 *
 */
UCLASS()
class KL_API UKLAnimation : public UAnimInstance
{
	GENERATED_BODY()

public:
	void NativeBeginPlay() override;
	void NativeUpdateAnimation(float DeltaSeconds) override;

protected:
	UPROPERTY(BlueprintReadOnly, Category = "!KL: Animation")
	float Speed = 0.f;

	UPROPERTY(BlueprintReadOnly, Category = "!KL: Animation")
	bool bIsMoving = false;

	UPROPERTY(BlueprintReadOnly, Category = "!KL: Animation")
	bool bSpawnCompleted = false;

	UPROPERTY(BlueprintReadOnly, Category = "!KL: Animation")
	bool bIsDead = false;

	UPROPERTY(BlueprintReadOnly, Category = "!KL: Animation")
	AGameCharacter* CurrentCharacter = nullptr;

private:
	bool bIsReady = false;
	bool bIsMob = false;

	void OnDeath(AGameCharacter* DeadOwner);
	void OnRevive(AGameCharacter* AliveOwner);
};
