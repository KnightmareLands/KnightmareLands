// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "ECS/ECSController.h"
#include "MobController.generated.h"

class UECSEngine;

/**
 * A mob controller
 */
UCLASS()
class KL_API AMobController : public AECSController {
	GENERATED_BODY()

public:
	AMobController();

	bool PerformAction(AActor* TargetActor = nullptr, FVector TargetLocation = FVector::ZeroVector) override;

	UFUNCTION(BlueprintCallable, Category = "ECS: Formation")
	bool SetMaster(int32 MasterEntity);

	UFUNCTION(BlueprintCallable, Category = "ECS: Formation")
	bool LeaveMaster();

	bool ECSPostRegister() override;
	bool ECSPostUnregister() override;

protected:
	// Maximum distance at which the entity can see a target
	UPROPERTY(EditDefaultsOnly, Category = "ECS: AI")
	float DetectDistance = 3000.f;

	// Roaming step distance
	UPROPERTY(EditDefaultsOnly, Category = "ECS: AI")
	float RoamingDistance = 1000.f;

	// Attack distance
	UPROPERTY(EditDefaultsOnly, Category = "ECS: AI")
	float AttackDistance = 100.f;

	// Action type
	UPROPERTY(EditDefaultsOnly, Category = "ECS: AI")
	ESkillType ActionType = ESkillType::Melee;

	// AI Movement type
	UPROPERTY(EditDefaultsOnly, Category = "ECS: AI")
	EECSMovementType MovementType = EECSMovementType::Flocking;

	// AI group id
	UPROPERTY(EditDefaultsOnly, Category = "ECS: AI")
	int32 GroupID = 0;

	// Allows to use any actions only when the mob is visible on screen
	// To avoid unexpected fireballs from outside of the screen
	UPROPERTY(EditDefaultsOnly, Category = "!KL: Controller")
	bool bCanOnlyPerformOnScreen = false;

};
