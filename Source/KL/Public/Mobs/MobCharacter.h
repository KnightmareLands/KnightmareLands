// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Common/GameCharacter.h"
#include "Common/MobTypes.h"
#include "MobCharacter.generated.h"

UCLASS()
class KL_API AMobCharacter : public AGameCharacter {
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AMobCharacter();

	void PostInitializeComponents() override;

	float TakeDamage(float Damage, struct FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser) override;

	void InitializeAttributes(bool bInit = false) override;

	// Should be set to true after spawn animation acompleted
	UPROPERTY(BlueprintReadWrite, Category = "!KL: Animation")
	bool bMobReady = false;

	// Return the scores amount for killing this mob
	int32 GetScores() const;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "!KL: Mob")
	EMobType MobType = EMobType::Regular;

	// Base scores for a regular type of this mob
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "!KL: Mob")
	int32 BaseScores = 40;

protected:
	// Called when the game starts or when spawned
	void BeginPlay() override;

	void Death(float DamageDealt, AActor* DamageCauser, bool bLethalDamage) override;

private:

	float DefaultSteeringRate = INDEX_NONE;

};
