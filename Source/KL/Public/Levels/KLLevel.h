// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/LevelScriptActor.h"
#include "KLLevel.generated.h"

class ATriggerVolume;
class ASWManager;

/**
 *
 */
UCLASS()
class KL_API AKLLevel : public ALevelScriptActor {
	GENERATED_BODY()

public:

protected:
	virtual void BeginPlay() override;

	void ActivateLevel(AActor* LevelBounds);

	UFUNCTION(BlueprintNativeEvent, Category = "!KL: Level")
	void LevelInited();

	UFUNCTION(BlueprintImplementableEvent, Category = "!KL: Level")
	void LevelActivated();

	UFUNCTION(BlueprintImplementableEvent, Category = "!KL: Level")
	void LevelDeactivated();

	UFUNCTION(BlueprintImplementableEvent, Category = "!KL: Level")
	void LevelCompleted();

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "!KL: Level")
	bool IsActive() const;

	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = "!KL: Level")
	ATriggerVolume* ActivationVolume;

	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = "!KL: Level")
	ASWManager* SpawnManager;

	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = "!KL: Debug")
	bool bDisableSpawn = false;

private:
	UFUNCTION()
	void OnLevelLoaded(EKLGameState NewState, bool bLoaded);

	UFUNCTION()
	void OnActivationOverlap(AActor* OverlappedActor, AActor* OtherActor);

	UFUNCTION()
	void OnDeactivationOverlap(AActor* OverlappedActor, AActor* OtherActor);

	bool bInited = false;
	bool bActivated = false;
};
