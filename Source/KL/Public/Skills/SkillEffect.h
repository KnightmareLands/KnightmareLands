// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Common/Enums.h"
#include "Skills/SkillTypes.h"
#include "Skills/StatusTypes.h"
#include "SkillEffect.generated.h"

class USkillData;
class AGameCharacter;

DECLARE_EVENT_OneParam(ASkillEffect, FEffectCallback, EStatusType);

UCLASS()
class KL_API ASkillEffect : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ASkillEffect();

	// Called every frame
	void Tick(float DeltaTime) override;

	FEffectCallback OnEffectStart;
	FEffectCallback OnEffectLaunch;
	FEffectCallback OnEffectEnd;

	UFUNCTION(BlueprintCallable, Category = "!KL: Skill")
	void Setup(AGameCharacter* InOwner, USkillData* InSkillData, const FVector InTargetVector, const TArray<AGameCharacter*>& InTargetActors, const EInstigatorType InInstigatorType, AGameCharacter* const InBoundTo = nullptr);
	void DeactivateTrigger();

	/* >> BP HANDLERS */

	UPROPERTY(BlueprintReadOnly, Category = "!KL: Skill")
	AGameCharacter* SkillOwner;

	UPROPERTY(BlueprintReadOnly, Category = "!KL: Skill")
	AGameCharacter* BoundTo;

	UPROPERTY(BlueprintReadOnly, Category = "!KL: Skill")
	USkillData* SkillData;

	UFUNCTION(BlueprintNativeEvent, Category = "!KL: Skill")
	void RenewStatus();

	UFUNCTION(BlueprintNativeEvent, Category = "!KL: Skill")
	void Launch();

	UFUNCTION(BlueprintNativeEvent, Category = "!KL: Skill")
	void Deactivate();

	UFUNCTION(BlueprintCallable, Category = "!KL: Skill")
	void EffectStart();
	UFUNCTION(BlueprintCallable, Category = "!KL: Skill")
	void EffectLaunch();
	UFUNCTION(BlueprintCallable, Category = "!KL: Skill")
	void EffectEnd();

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "!KL: Skill")
	EInstigatorType GetInstigatorType() const;

	/* << BP HANDLERS */

	EStatusType StatusType = EStatusType::NotStatus;

protected:
	// Called when the game starts or when spawned
	void BeginPlay() override;

	UPROPERTY(BlueprintReadOnly, Category = "!KL: Skill")
	FVector TargetVector;

	UPROPERTY(BlueprintReadOnly, Category = "!KL: Skill")
	TArray<AGameCharacter*> TargetActors;

	UPROPERTY(BlueprintReadOnly, Category = "!KL: Skill")
	bool bDeactivated = false;

	UPROPERTY()
	EInstigatorType InstigatorType = EInstigatorType::Default;

private:

};
