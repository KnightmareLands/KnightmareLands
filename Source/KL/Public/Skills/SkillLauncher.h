// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Common/Enums.h"
#include "Skills/SkillData.h"
#include "Skills/SkillTypes.h"
#include "SkillLauncher.generated.h"

class AGameCharacter;
class ASkillEffect;
class UKLTimeFactorSubsystem;

DECLARE_DYNAMIC_MULTICAST_DELEGATE( FSkillDelegate );

UCLASS(Blueprintable, BlueprintType, EditInlineNew)
class KL_API USkillLauncher : public UObject, public FTickableGameObject {
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	USkillLauncher();

	UWorld* GetWorld() const override {
		if (HasAllFlags(RF_ClassDefaultObject)) {
			// If we are a CDO, we must return nullptr instead of calling Outer->GetWorld() to fool UObject::ImplementsGetWorld.
			return nullptr;
		}

		return GetOuter()->GetWorld();
	}

	//~ Begin FTickableObject Interface
	virtual void Tick(float DeltaTime) override;
	virtual bool IsTickable() const override { return bShouldTick; }
	virtual TStatId GetStatId() const override { RETURN_QUICK_DECLARE_CYCLE_STAT(USkillLauncher, STATGROUP_Tickables); }
	//~ End FTickableObject Interface

	/* >> Skill methods */

	// Set specific skill type for this launcher from SkillTypes_DT table
	void SetupSkill(AGameCharacter* InOwner);

	// Set new skill & reset the launcher
	UFUNCTION(BlueprintCallable, Category = "!KL: Skill")
	void SetNewSkill(USkillData* InSkillData);

	// Remove skill from the launcher; deactivate;
	UFUNCTION(BlueprintCallable, Category = "!KL: Skill")
	void RemoveSkill();

	UPROPERTY(BlueprintReadOnly, Category = "!KL: Skill")
	AGameCharacter* Owner = nullptr;

	UFUNCTION(BlueprintCallable, Category = "!KL: Skill")
	bool IsInited() const;

	UFUNCTION(BlueprintCallable, Category = "!KL: Skill")
	virtual bool CanBeUsed() const;

	UFUNCTION(BlueprintCallable, Category = "!KL: Skill")
	bool IsActive() const;

	UFUNCTION(BlueprintCallable, Category = "!KL: Skill")
	ETargetingType GetTargetingType() const;

	/* << Skill methods */

	/* >> BP HANDLERS */

	UFUNCTION(BlueprintCallable, Category = "!KL: Skill")
	bool HasSkill(const USkillData* InSkillData) const;

	UFUNCTION(BlueprintCallable, Category = "!KL: Skill")
	float GetCooldown() const;

	UFUNCTION(BlueprintCallable, Category = "!KL: Skill")
	float GetCastingTime() const;

	UFUNCTION(BlueprintCallable, Category = "!KL: Skill")
	bool IsMelee() const;

	UFUNCTION(BlueprintCallable, Category = "!KL: Skill")
	ESkillType GetSkillType() const;

	UFUNCTION(BlueprintCallable, Category = "!KL: Skill")
	float GetRemainingCooldown() const;

	UFUNCTION(BlueprintCallable, Category = "!KL: Skill")
	FName GetTitle() const;

	UFUNCTION(BlueprintCallable, Category = "!KL: Skill")
	UTexture2D* GetIcon() const;

	UFUNCTION(BlueprintCallable, Category = "!KL: Skill")
	float GetAttackRange() const;

	UFUNCTION(BlueprintCallable, Category = "!KL: Skill")
	bool Launch(const FTargetingInfo InTargetingInfo);

	UFUNCTION(BlueprintCallable, Category = "!KL: Skill")
	void Deactivate();

	UFUNCTION(BlueprintCallable, Category = "!KL: Skill")
	bool IsContinuos() const;

	UFUNCTION(BlueprintCallable, Category = "!KL: Skill")
	bool IsPositive() const;

	UFUNCTION(BlueprintCallable, Category = "!KL: Skill")
	void ResetCooldown();

	/* << BP HANDLERS */

	/* >> BP EVENTS */

	UPROPERTY(BlueprintAssignable, Category = "!KL: Skill")
	FSkillDelegate OnUpdate;

	/* << BP EVENTS */

protected:

	void Cleanup();

	/* >> BP HANDLERS */

	UFUNCTION(BlueprintCallable, Category = "!KL: Skill")
	bool IsCooldownReady() const;

	/* << BP HANDLERS */

	UFUNCTION()
	void OnOwnerDeath(AGameCharacter* DeadOwner);

	UFUNCTION()
	void OnOwnerReplaced();

	void LaunchStart(EStatusType Type);
	void LaunchDone(EStatusType Type);
	void LaunchEnd(EStatusType Type);

	bool SpendSkillPoints();

	// True when need to calculate cooldown
	bool bShouldTick = false;
	float CooldownLeft = 0.f;

	UPROPERTY()
	ASkillEffect* ActiveSkill = nullptr;

	UPROPERTY()
	USkillData* SkillData;

	UPROPERTY()
	UKLTimeFactorSubsystem* TimeFactor;

};
