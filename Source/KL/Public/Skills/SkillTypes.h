// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "SkillTypes.generated.h"

UENUM(BlueprintType)
enum class ESkillType: uint8 {
	Melee UMETA(DisplayName="Melee skill"),
	Ranged UMETA(DisplayName="Ranged skill"),
	Cast UMETA(DisplayName="Magic skill")
};
