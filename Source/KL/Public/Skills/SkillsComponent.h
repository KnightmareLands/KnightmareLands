// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Skills/SkillData.h"
#include "SkillsComponent.generated.h"

class USkillLauncher;
class ASkillEffect;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class KL_API USkillsComponent : public UActorComponent {
	GENERATED_BODY()

public:
	// Sets default values for this component's properties
	USkillsComponent();

	void InitializeComponent() override;
	void BeginPlay() override;

	UFUNCTION(BlueprintCallable, Category = "!KL: Skill")
	TArray<USkillLauncher*> GetSkillsList() const;

protected:
	// Select the SkillTypes_DT
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "!KL: Skill")
	TArray<USkillData*> AvailableSkills;

	UPROPERTY()
	TArray<USkillLauncher*> SkillsList;

private:
};
