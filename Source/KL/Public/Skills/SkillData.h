// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "Skills/SkillEffect.h"
#include "Common/Enums.h"
#include "SkillData.generated.h"

UCLASS(BlueprintType)
class KL_API USkillData : public UDataAsset
{
	GENERATED_BODY()

public:
	/** Skill title */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="!KL: Skill")
	FName Title;

	/** Attack power */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="!KL: Skill")
	float Power;

	/** Attack range */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="!KL: Skill")
	float Range;

	/** Cooldown, seconds */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="!KL: Skill")
	float Cooldown;

	/** Casting time, seconds */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="!KL: Skill")
	float CastingTime;

	/** Effect duration time, seconds */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="!KL: Skill")
	float DurationTime;

	/** True for continuos skills (e.g. flamethrower) */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="!KL: Skill")
	bool bContinuos;

	/** True for good skills (targeted to a player characters) */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="!KL: Skill")
	bool bPositive;

	/** The time between effect ticks */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="!KL: Skill")
	float TickTime;

	/** Icon of the skill */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="!KL: Skill")
	UTexture2D* Icon;

	/** Skill effect reference */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, AssetRegistrySearchable, Category="!KL: Skill")
	TSubclassOf<ASkillEffect> SkillEffect;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "!KL: Skill")
	ESkillType SkillType = ESkillType::Cast;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "!KL: Skill")
	ETargetingType TargetingType = ETargetingType::TargetLocation;
};
