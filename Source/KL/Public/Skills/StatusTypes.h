// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "StatusTypes.generated.h"

UENUM(BlueprintType)
enum class EStatusType: uint8 {
	NotStatus,
	Speed,
	Attack,
	Defense,
	Health,
	SkillSpeed,
	Stun,
	Push,
	Regeneration,
	Burning,
	Electrification,
	Bleeding,
	Revive,
	Invulnerability,
};
