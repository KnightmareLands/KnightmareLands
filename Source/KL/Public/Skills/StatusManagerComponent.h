// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Skills/StatusTypes.h"
#include "StatusManagerComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FKLStatusEvent, EStatusType, StatusType);

class ASkillEffect;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class KL_API UStatusManagerComponent : public UActorComponent {
	GENERATED_BODY()

public:
	// Sets default values for this component's properties
	UStatusManagerComponent();

	// Called when the game starts
	void BeginPlay() override;
	void TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "!KL: Skill")
	bool IsStatusExist(EStatusType Type) const;

	UFUNCTION(BlueprintCallable, Category = "!KL: Skill")
	void RegisterStatus(EStatusType Type, ASkillEffect* StatusEffect);

	void UnregisterStatus(EStatusType Type);

	// The status has been added
	UPROPERTY(BlueprintAssignable, Category = "!KL: Skill: Status")
	FKLStatusEvent OnStatusAdded;

	// The status has been removed
	UPROPERTY(BlueprintAssignable, Category = "!KL: Skill: Status")
	FKLStatusEvent OnStatusRemoved;

	// The status has been renewed
	UPROPERTY(BlueprintAssignable, Category = "!KL: Skill: Status")
	FKLStatusEvent OnStatusRenewed;

protected:

	bool ShouldTick() const;

	UPROPERTY()
	TMap<EStatusType, ASkillEffect*> ActiveStatuses;
	TMap<EStatusType, FDelegateHandle> EventHandlers;

private:

};
