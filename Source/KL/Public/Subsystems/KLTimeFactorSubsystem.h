// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Subsystems/GameInstanceSubsystem.h"
#include "Common/TimeEnums.h"
#include "KLTimeFactorSubsystem.generated.h"

/**
 *
 */
UCLASS(DisplayName="Time Subsystem")
class KL_API UKLTimeFactorSubsystem : public UGameInstanceSubsystem, public FTickableGameObject {
	GENERATED_BODY()

public:
	// Begin USubsystem
	void Initialize(FSubsystemCollectionBase& Collection) override;
	void Deinitialize() override;
	// End USubsystem

	//~ Begin FTickableObject Interface
	void Tick(float DeltaTime) override;
	bool IsTickable() const override { return true; }
	TStatId GetStatId() const override { RETURN_QUICK_DECLARE_CYCLE_STAT(UKLTimeFactorSubsystem, STATGROUP_Tickables); }
	//~ End FTickableObject Interface

	UFUNCTION(BlueprintCallable, Category = "!KL: Time Factor")
	void FadeToPause();

	UFUNCTION(BlueprintCallable, Category = "!KL: Time Factor")
	void FadeToGame();

	// Percent, 0..1
	UFUNCTION(BlueprintCallable, Category = "!KL: Time Factor")
	float GetDurationLeft() const;

	UFUNCTION(BlueprintCallable, Category = "!KL: Time Factor")
	float GetEnemyMovementFactor() const;

	UFUNCTION(BlueprintCallable, Category = "!KL: Time Factor")
	float GetPlayerMovementFactor() const;

	UFUNCTION(BlueprintCallable, Category = "!KL: Time Factor")
	float GetEnemyAttackFactor() const;

	UFUNCTION(BlueprintCallable, Category = "!KL: Time Factor")
	float GetPlayerAttackFactor() const;

	UFUNCTION(BlueprintCallable, Category = "!KL: Time Factor")
	void SetNormal();

	UFUNCTION(BlueprintCallable, Category = "!KL: Time Factor")
	void AddSlowDown(float InDuration);

	UFUNCTION(BlueprintCallable, Category = "!KL: Time Factor")
	void Reset();

	DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FKLTime, EKLDilationState, State);

	UPROPERTY(BlueprintAssignable, Category = "!KL: Scores")
	FKLTime OnFadeBegin;

	UPROPERTY(BlueprintAssignable, Category = "!KL: Scores")
	FKLTime OnFadeComplete;

protected:
	FORCEINLINE void UpdateDilation();

	void SetDilation(float InTarget);
	bool bIsEnded = false;

	float TargetDilation;
	float StartDilation;
	float FadeDurationLeft;
	float CurrentDilation;

	void ResetTimer();

	EKLTimeMode Mode = EKLTimeMode::Normal;

	float FadeDuration = 0.5f;

	float Factor = 0.6f;
	float NormalFactor = 1.f;

	float EnemyMovementFactor = 1.f;
	float PlayerMovementFactor = 1.2f;

	float EnemyAttackFactor = 1.f;
	float PlayerAttackFactor = 1.2f;

	FTimerHandle TimerHandle;
	float StartTime = 0.f;
	float SlowDuration = 0.f;

};
