// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Subsystems/GameInstanceSubsystem.h"
#include "Common/KLSavesList.h"
#include "KLSaveSubsystem.generated.h"

/**
 *
 */
UCLASS(DisplayName="Saves Subsystem")
class KL_API UKLSaveSubsystem : public UGameInstanceSubsystem {
	GENERATED_BODY()

public:
	// Begin USubsystem
	void Initialize(FSubsystemCollectionBase& Collection) override;
	void Deinitialize() override;
	// End USubsystem

	UFUNCTION(BlueprintCallable, Category = "!KL: Save")
	bool GetSaveSlots(TArray<FKLSaveSlot>& OutSlots) const;

	UFUNCTION(BlueprintCallable, Category = "!KL: Save")
	void LoadGame(const FString& SlotName);

	UFUNCTION(BlueprintCallable, Category = "!KL: Save")
	bool SaveGame(const FString& SlotName, const FString& SaveName, bool bForced = false);

	// TODO: implement user profiles
	UPROPERTY(BlueprintReadOnly, VisibleAnywhere, Category = "!KL: Save")
	uint8 UserIndex = 0;

protected:

};
