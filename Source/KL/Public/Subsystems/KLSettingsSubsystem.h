// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Subsystems/GameInstanceSubsystem.h"
#include "KLSettingsSubsystem.generated.h"

/**
 *
 */
UCLASS(DisplayName="Settings Subsystem")
class KL_API UKLSettingsSubsystem : public UGameInstanceSubsystem {
	GENERATED_BODY()

public:
	// Begin USubsystem
	void Initialize(FSubsystemCollectionBase& Collection) override;
	void Deinitialize() override;
	// End USubsystem

protected:

};
