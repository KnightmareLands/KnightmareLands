// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Subsystems/GameInstanceSubsystem.h"
#include "Common/KLScoresList.h"
#include "KLScoresSubsystem.generated.h"

class UKLAttribute;

/**
 *
 */
UCLASS(DisplayName="Scores Subsystem")
class KL_API UKLScoresSubsystem : public UGameInstanceSubsystem {
	GENERATED_BODY()

public:
	// Begin USubsystem
	void Initialize(FSubsystemCollectionBase& Collection) override;
	void Deinitialize() override;
	// End USubsystem

	void Reset(int32 InScoresToFirstLevel, int32 InitialScores);

	UFUNCTION(BlueprintCallable, Category = "!KL: Scores")
	int32 GetScore() const;

	UFUNCTION(BlueprintCallable, Category = "!KL: Scores")
	void AddScore(int32 ScoreToAdd, bool bIsDebug = false);

	UFUNCTION(BlueprintCallable, Category = "!KL: Scores")
	void SetSkillPoints(int32 InSkillPoints);

	UFUNCTION(BlueprintCallable, Category = "!KL: Scores")
	int32 GetSkillPoints() const;

	UFUNCTION(BlueprintCallable, Category = "!KL: Scores")
	bool SpendSkillPoints();

	UFUNCTION(BlueprintCallable, Category = "!KL: Scores")
	int32 GetLevel() const;

	UFUNCTION(BlueprintCallable, Category = "!KL: Scores")
	float GetLevelPercent() const;

	UFUNCTION(BlueprintCallable, Category = "!KL: Scores")
	int32 GetScoresToNextLevel() const;

	UFUNCTION(BlueprintCallable, Category = "!KL: Scores")
	bool GetScoresList(TArray<FKLScoresItem>& OutScoresList) const;

	UFUNCTION(BlueprintCallable, Category = "!KL: Scores")
	bool SaveScores(FName InPlayerName, bool bInWinner);

	UFUNCTION(BlueprintCallable, Category = "!KL: Scores")
	bool UpdateRecentUsername(FName InPlayerName);

	UFUNCTION(BlueprintCallable, Category = "Debug")
	void LevelUp();

	DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FScoresChanged, int32, AddedScores);
	DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FLevelUp, int32, CurrentLevel);

	UPROPERTY(BlueprintAssignable, Category = "!KL: Scores")
	FScoresChanged OnScoreAdded;

	UPROPERTY(BlueprintAssignable, Category = "!KL: Scores")
	FScoresChanged OnSPSpent;

	UPROPERTY(BlueprintAssignable, Category = "!KL: Scores")
	FLevelUp OnLevelUp;

	UPROPERTY(BlueprintReadOnly, Category = "!KL: Scores")
	UKLAttribute* Multiplier;
protected:

	int32 ScoresToFirstLevel = 0;
	int32 Score = 0;

	int32 DebugScores = 0;

	int32 CurrentLevel = 1;

	int32 SkillPoints = 0;

	int32 SpentPoints = 0;
};
