// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Subsystems/GameInstanceSubsystem.h"
#include "KLStateSubsystem.generated.h"

/**
 * Subsystem for the group and heroes states getters
 */
UCLASS(DisplayName="State Subsystem")
class KL_API UKLStateSubsystem : public UGameInstanceSubsystem {
	GENERATED_BODY()

public:
	// Begin USubsystem
	void Initialize(FSubsystemCollectionBase& Collection) override;
	void Deinitialize() override;
	// End USubsystem

	UFUNCTION(BlueprintCallable, Category = "!KL: State")
	bool IsGroupDefined() const;

	UFUNCTION(BlueprintCallable, Category = "!KL: State")
	bool HasDeadHeroes() const;
};
