// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Subsystems/GameInstanceSubsystem.h"
#include "KLGoalsSubsystem.generated.h"

/**
 *
 */
UCLASS(DisplayName="Goals Subsystem")
class KL_API UKLGoalsSubsystem : public UGameInstanceSubsystem {
	GENERATED_BODY()

public:
	// Begin USubsystem
	void Initialize(FSubsystemCollectionBase& Collection) override;
	void Deinitialize() override;
	// End USubsystem

	UFUNCTION(BlueprintCallable, Category = "!KL: Goals")
	void SetGoal(FVector InGoal);

	UFUNCTION(BlueprintCallable, Category = "!KL: Goals")
	FVector GetGoal() const;

protected:
	FVector Goal;

};
