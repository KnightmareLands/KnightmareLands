// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Subsystems/GameInstanceSubsystem.h"
#include "KLPerksSubsystem.generated.h"

class UPerkData;

/**
 *
 */
UCLASS(DisplayName="Perks Subsystem")
class KL_API UKLPerksSubsystem : public UGameInstanceSubsystem {
	GENERATED_BODY()

public:
	// Begin USubsystem
	void Initialize(FSubsystemCollectionBase& Collection) override;
	void Deinitialize() override;
	// End USubsystem

	void Reset(int32 InUpgradesAmount);

	UFUNCTION(BlueprintCallable, Category = "!KL: Perks")
	void UpdatePerksList();

	UFUNCTION(BlueprintCallable, Category = "!KL: Perk")
	bool GetAvailablePerks(const TArray<AGameCharacter*>& Heroes, TArray<UPerkData*>& OutPerks);

protected:
	// List of all possible perks in game
	UPROPERTY(BlueprintReadOnly, Category = "!KL: Perks")
	TArray<UPerkData*> PerksList;

	// List of perks available for current upgrade
	UPROPERTY(BlueprintReadOnly, Category = "!KL: Perks")
	TArray<UPerkData*> AvailablePerksList;

	UPROPERTY(BlueprintReadOnly, Category = "!KL: Perks")
	int32 BasicUpgradesAmount = 0;

	UPROPERTY(BlueprintReadWrite, Category = "!KL: Perks")
	int32 UpgradesAmount;
};
