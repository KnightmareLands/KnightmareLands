// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Formation.generated.h"

class APlayerCharacter;

UCLASS()
class KL_API AFormation : public AActor {
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AFormation();

	void BeginPlay() override;
	void Tick(float DeltaTime) override;

	void Reset();

	TArray<FVector> GetLocalAdjustments() const;

	TArray<APlayerCharacter*> GetSortedHeroes(const TArray<APlayerCharacter*> HeroesList);
	void ClearSortedHeroes();

	UFUNCTION(BlueprintNativeEvent, Category = "!KL: Formation")
	void SortPredicate(const APlayerCharacter* Hero1, const APlayerCharacter* Hero2, bool& Result) const;

	void SetMasterLocation(FVector InLocation);
	void Move(FVector Delta);
	FVector GetMasterLocation() const;

	FVector GetAdjustmentForCharacter(APlayerCharacter* Hero) const;

protected:
	void UpdateGroupCenter();

	UPROPERTY()
	UArrowComponent* GroupDirection;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "!KL: Formation")
	TArray<FVector> GroupLocations;

	UPROPERTY()
	TArray<APlayerCharacter*> SortedHeroes;

	FVector MasterLocation = FVector::ZeroVector;
	FVector GroupCenter = FVector::ZeroVector;

	float Yaw = 0.f;
	float TargetYaw = 0.f;

	UPROPERTY(EditDefaultsOnly, Category = "!KL: Formation")
	float RotationRate = 360.f;

	UPROPERTY(EditDefaultsOnly, Category = "!KL: Formation")
	float MaxDistance = 500.f;

	UPROPERTY(EditDefaultsOnly, Category = "!KL: Formation")
	float AcceptanceDistance = 100.f;

	bool bHasMovement = false;
};
