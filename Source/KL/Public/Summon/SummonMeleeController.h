// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Player/HeroController.h"
#include "SummonMeleeController.generated.h"

/**
 *
 */
UCLASS()
class KL_API ASummonMeleeController : public AHeroController {
	GENERATED_BODY()

public:
	void UpdateMovement(FVector InCursorLocation, bool bIsHovered) override;

protected:
	void ProcessController(float DeltaSeconds) override;
	void UpdateMaster(float DeltaTime) override;

	bool UpdateTargetingInfo(ETargetingType InType, bool bPositive, FTargetingInfo& OutTargetingInfo) override;
	void PerformMovement() override;
	bool FindTarget_Implementation(ETargetingType InType, bool bPositive, FTargetingInfo& OutTarget) override;
	bool IsManualTargetValid() const override;

	UPROPERTY(EditDefaultsOnly, Category = "!KL: Summon Controller")
	float PositionOffset = 100.f;

	UPROPERTY(EditDefaultsOnly, Category = "!KL: Summon Controller")
	float SenseRadius = 500.f;

	UPROPERTY(EditDefaultsOnly, Category = "!KL: Summon Controller")
	float LeashDistance = 2000.f;

	UPROPERTY()
	FTargetingInfo CurrentTarget;
	bool bHaveTarget = false;

	FVector SenseCenter;
};
