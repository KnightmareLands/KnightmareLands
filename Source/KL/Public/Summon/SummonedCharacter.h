// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Common/GameCharacter.h"
#include "SummonedCharacter.generated.h"

/**
 * A character which can be summoned by player for help
 */
UCLASS()
class KL_API ASummonedCharacter : public AGameCharacter {
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintCallable, Category="!KL: Summon")
	void SetMasterCharacter(AGameCharacter* InMaster);

	UFUNCTION(BlueprintCallable, BlueprintPure, Category="!KL: Summon")
	AGameCharacter* GetMasterCharacter() const;

protected:
	UPROPERTY(BlueprintReadOnly, Category="!KL: Summon")
	AGameCharacter* Master = nullptr;

	void Death(float DamageDealt, AActor* DamageCauser, bool bLethalDamage) override;
};
