// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Core/ECSSystem.h"
#include "ECS/ECSStats.h"

#include "GenericQuadTree.h"

#include "ECSSystemCollision.generated.h"

DECLARE_CYCLE_STAT_EXTERN(TEXT("ECSSystem: Collision"),                 STAT_ECSSystemCollision,          STATGROUP_KLECS, KL_API);
DECLARE_CYCLE_STAT_EXTERN(TEXT("ECSSystem: Collision: QuadTree build"), STAT_ECSSystemCollisionTreeBuild, STATGROUP_KLECS, KL_API);
DECLARE_CYCLE_STAT_EXTERN(TEXT("ECSSystem: Collision: QuadTree get"),   STAT_ECSSystemCollisionTreeGet,   STATGROUP_KLECS, KL_API);

UCLASS()
class KL_API UECSSystemCollision : public UECSSystem {
	GENERATED_BODY()

static const FBox2D QUAD_TREE_BOUNDS;
static const float QUAD_MIN_SIZE;

public:
	UECSSystemCollision();

public: // UECSSystem

	void RequestInit() override;
	void Process(float DeltaTime) override;

protected:
	bool CanProcess(int32 Index) const override;
	void ProcessEntity(int32 Entity, float DeltaTime) override;

	void OnRegistered(int32 Entity) override;

#if STATS
	TStatId GetStatId() const override;
#endif

	TQuadTree<int32, 20> QuadTree;
	float MaxRadius = 0;

};
