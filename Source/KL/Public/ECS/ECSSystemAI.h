// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Core/ECSSystem.h"
#include "ECS/ECSAIAttackGroup.h"
#include "ECS/ECSComponents.h"
#include "ECS/ECSStats.h"
#include "ECS/ECSTypes.h"
#include "ECSSystemAI.generated.h"

DECLARE_CYCLE_STAT_EXTERN(TEXT("ECSSystem: AI"),      STAT_ECSSystemAI,    STATGROUP_KLECS, KL_API);
DECLARE_CYCLE_STAT_EXTERN(TEXT("ECSSystem: AI: ban"), STAT_ECSSystemAIBan, STATGROUP_KLECS, KL_API);

UCLASS()
class KL_API UECSSystemAI : public UECSSystem {
	GENERATED_BODY()

public: // UECSSystem

	void RequestInit() override;
	void BeginPlay() override;

protected:
	bool CanProcess(int32 Index) const override;
	void Process(float DeltaTime) override;
	void ProcessAttackGroups();

	void OnRegistered(int32 Entity) override;
	void OnUnregistered(int32 Entity) override;

	void ProcessEntity(int32 Entity, float DeltaTime) override;

#if STATS
	TStatId GetStatId() const override;
#endif

	UPROPERTY()
	UWorld* World = nullptr;

	int32 SpawnedMobsCounterIndex = INDEX_NONE;

	int32 MinTargetedCount = 1;
	float MaxTargetedPercent = 0.1f;

	int32 TargetedCount = 0;
	int32 MaxTargetedCount = 0;

	TArray<FECSAIAttackGroup> AttackGroups;

};
