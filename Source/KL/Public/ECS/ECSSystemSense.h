// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Core/ECSSystem.h"
#include "ECS/ECSComponents.h"
#include "ECS/ECSStats.h"
#include "ECS/ECSTypes.h"
#include "ECSSystemSense.generated.h"

DECLARE_CYCLE_STAT_EXTERN(TEXT("ECSSystem: Sense"), STAT_ECSSystemSense, STATGROUP_KLECS, KL_API);

UCLASS()
class KL_API UECSSystemSense : public UECSSystem {
	GENERATED_BODY()

public: // UECSSystem

	void RequestInit() override;

protected:
	bool CanProcess(int32 Index) const override;
	void ProcessEntity(int32 Entity, float DeltaTime) override;

#if STATS
	TStatId GetStatId() const override;
#endif

};
