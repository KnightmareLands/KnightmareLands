// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"

#include "API/ECSEntity.h"

#include "ECS/ECSTypes.h"
#include "Skills/SkillTypes.h"
#include "ECSController.generated.h"

class UECSEngine;
class UGameGlobal;

/**
 * Basic ECS Controller class for ECS AI stuff
 */
UCLASS()
class KL_API AECSController : public AAIController {
	GENERATED_BODY()

public: // AAIController

	AECSController();
	void BeginPlay() override;
	void StopMovement() override;

protected: // AAIController

	void OnPossess(APawn* InPawn) override;
	void OnUnPossess() override;

public: // Methods

	UFUNCTION(BlueprintCallable, Category = "ECS: AI")
	bool Register();

	virtual bool ECSPostRegister();

	UFUNCTION(BlueprintCallable, Category = "ECS: AI")
	bool Unregister();

	virtual bool ECSPostUnregister();
	virtual void ECSUnregistered();

	virtual bool PerformAction(AActor* TargetActor = nullptr, FVector TargetLocation = FVector::ZeroVector);

protected:

	UPROPERTY(EditDefaultsOnly, Category = "ECS: AI")
	bool bActivateECS = true;

	UPROPERTY(BlueprintReadOnly, Category = "ECS: AI")
	bool bIsECSActive = false;

private:

	bool bPlayHasBegun = false;

};
