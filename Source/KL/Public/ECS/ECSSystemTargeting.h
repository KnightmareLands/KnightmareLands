// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Core/ECSSystem.h"
#include "ECS/ECSStats.h"
#include "ECS/ECSTypes.h"
#include "ECSSystemTargeting.generated.h"

DECLARE_CYCLE_STAT_EXTERN(TEXT("ECSSystem: Targeting"), STAT_ECSSystemTargeting, STATGROUP_KLECS, KL_API);

UCLASS()
class KL_API UECSSystemTargeting : public UECSSystem {
	GENERATED_BODY()

/* Helper methods */

static EECSMovementDirection GetOppositeDirection(EECSMovementDirection Direction);
static bool IsVerticalDirection(EECSMovementDirection Direction);
static bool IsOppositeDirection(EECSMovementDirection D1, EECSMovementDirection D2);
static EECSMovementDirection GetDirectionToTarget(FVector Location, FVector Target, bool bNeedShort = false);
static FVector GetRoamingDirection(EECSMovementDirection Direction, float MaxVariance = 0);
static bool IsReachedTargetAxis(FVector Location, FVector Target);

public: // UECSSystem

	void RequestInit() override;

protected:
	bool CanProcess(int32 Index) const override;
	void ProcessEntity(int32 Entity, float DeltaTime) override;

#if STATS
	TStatId GetStatId() const override;
#endif
};
