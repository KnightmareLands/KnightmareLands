// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/MovementComponent.h"
#include "ECS/ECSTypes.h"
#include "ECSMovementComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FECSMovementEvent);

class UECSEngine;

UCLASS(ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class KL_API UECSMovementComponent : public UMovementComponent {
	GENERATED_BODY()

public:

	// Sets default values for this component's properties
	UECSMovementComponent();

	UFUNCTION(BlueprintCallable, Category = "ECS: Movement")
	bool Register();

	UFUNCTION(BlueprintCallable, Category = "ECS: Movement")
	bool Unregister();

	UFUNCTION(BlueprintCallable, Category = "ECS: Movement")
	EMovementRequestResults MoveTo(FVector Target, bool bIsDirection = false, float InAcceptanceRadius = -1.f);

	UFUNCTION(BlueprintCallable, Category = "ECS: Movement")
	bool StopMovement();

	UFUNCTION(BlueprintCallable, Category = "ECS: Movement")
	bool PauseMovement();

	UFUNCTION(BlueprintCallable, Category = "ECS: Position")
	bool UpdatePosition();

	UFUNCTION(BlueprintCallable, Category = "ECS: Movement")
	bool StartMovement();

	UFUNCTION(BlueprintCallable, Category = "ECS: Movement")
	void SetSpeed(float InSpeed);

	UFUNCTION(BlueprintCallable, Category = "ECS: Movement")
	void SetAcceptanceRadius(float InAcceptanceRadius);

	UFUNCTION(BlueprintCallable, Category = "ECS: Movement")
	void EnableSteering(bool bEnabled);

	UFUNCTION(BlueprintCallable, Category = "ECS: Movement")
	void SetSteeringRate(float InSteeringRate);

	float GetMaxSpeed() const override;

	UFUNCTION(BlueprintCallable, Category = "ECS: Movement")
	float GetCurrentSpeed() const;

	UFUNCTION(BlueprintCallable, Category = "ECS: Movement")
	float GetSteeringRate() const;

	/* ECS Delegate methods */

	// Update location and rotation from ECS movement
	virtual bool UpdateOwnerLocationAndRotation(FVector NewLocation, FRotator NewRotation);
	// Update location from ECS movement
	virtual bool UpdateOwnerLocation(FVector NewLocation);
	// Update rotation from ECS movement
	virtual bool UpdateOwnerRotation(FRotator NewRotation);

	// Trigger the target reached callback for Blueprints
	virtual void OnTargetReached();

	// The Target reached callback for blueprints
	UPROPERTY(BlueprintAssignable, Category = "ECS: Movement")
	FECSMovementEvent OnECSTargetReached;

	virtual FVector GetOwnerLocation() const;
	virtual FRotator GetOwnerRotation() const;

	virtual void ECSUnregistered();

	UPROPERTY(EditDefaultsOnly, Category = "ECS: Movement")
	bool bActivateECS = true;

protected:

	// Called when the game starts
	void BeginPlay() override;

	void UpdateSteering();

	UPROPERTY(BlueprintReadOnly, Category = "ECS: Movement")
	bool bIsECSActive = false;

	UPROPERTY(EditDefaultsOnly, Category = "ECS: Movement")
	float Speed = 0.f;

	UPROPERTY(EditDefaultsOnly, Category = "ECS: Movement")
	float AcceptanceRadius = 10.f;

	// Rate of steering turn
	UPROPERTY(EditDefaultsOnly, Category = "ECS: Movement")
	float SteeringRate = 2.5f;

	// If true then steering is enabled;
	// If false then agents are turning instantaniously
	UPROPERTY(EditDefaultsOnly, Category = "ECS: Movement")
	bool bSteeringEnabled = true;

	// If true then actors are rotated towards the movement direction
	UPROPERTY(EditDefaultsOnly, Category = "ECS: Movement")
	bool bOrientToMovement = true;

	// Index of the boid group
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "ECS: Boid")
	int32 BoidIndex = 0;

	// Boid separation radius
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "ECS: Boid")
	float SeparationRadius = 400.f;

	// Boid separation weight
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "ECS: Boid")
	float SeparationWeight = 1.2f;

	// Boid alignment radius
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "ECS: Boid")
	float AlignmentRadius = 1000.f;

	// Boid Alignment weight
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "ECS: Boid")
	float AlignmentWeight = 1.2f;

	// Boid separation weight
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "ECS: Boid")
	float CohesionWeight = 1.f;

};
