// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Core/ECSSystem.h"
#include "ECS/ECSStats.h"
#include "ECSSystemNavigation.generated.h"

DECLARE_CYCLE_STAT_EXTERN(TEXT("ECSSystem: Navigation"),       STAT_ECSSystemNavigation,     STATGROUP_KLECS, KL_API);
DECLARE_CYCLE_STAT_EXTERN(TEXT("ECSSystem: Navigation: Path"), STAT_ECSSystemNavigationPath, STATGROUP_KLECS, KL_API);

class UNavigationSystemV1;

UCLASS()
class KL_API UECSSystemNavigation : public UECSSystem {
	GENERATED_BODY()

public: // UECSSystem

	void RequestInit() override;
	void BeginPlay() override;

protected:
	bool CanProcess(int32 Index) const override;
	void ProcessEntity(int32 Entity, float DeltaTime) override;

	void OnRegistered(int32 Entity) override;
	void OnUnregistered(int32 Entity) override;

#if STATS
	TStatId GetStatId() const override;
#endif

	UPROPERTY()
	UWorld* World = nullptr;

	UPROPERTY()
	UNavigationSystemV1* NavSys = nullptr;
};
