// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "ECSTypes.generated.h"

const int32 ENTITIES_MAX_COUNT = 5000;
const float MAX_VISIBILITY_DISTANCE = 100000000.f;

UENUM(BlueprintType)
enum class EMovementRequestResults: uint8 {
	Success UMETA(Tooltip="Success"),
	Reached UMETA(Tooltip="Destination is reached"),
	Failure UMETA(Tooltip="Can't perform the movement"),
};

UENUM(BlueprintType)
enum class EECSAIState: uint8 {
	Idle UMETA(Tooltip="Doing nothing"),
	InitRoaming UMETA(Tooltip="Starting to hang around"),
	Roaming UMETA(Tooltip="Hanging around"),
	Chase UMETA(Tooltip="Chasing target"),
	ChaseByAggression UMETA(Tooltip="Chasing target with high aggression"),
	Attack UMETA(Tooltip="Attacking target"),
	AttackOnOpportunity UMETA(Tooltip="Attack target when it's in immediate reach"),
	AttackByAggression UMETA(Tooltip="Attack target with high aggression"),
	InitPanic UMETA(Tooltip="Panic is started! reset timers, fle away"),
	Flee UMETA(Tooltip="Flee in fear away from the target")
};

UENUM(BlueprintType)
enum class EECSMovementType: uint8 {
	Idle UMETA(Tooltip="Not moving yet"),
	Direct UMETA(Tooltip="Directly chasing the target"),
	Advance UMETA(Tooltip="Chasing the target with prediction"),
	Flocking UMETA(Tooltip="Default flocking"),
	AxisFlocking UMETA(Tooltip="Flocking with axis-based directions"),
	Roundelay UMETA(Tooltip="Moving approximately around the target"),
	Align UMETA(Tooltip="Align velocity with the target's velocity"),
	Random UMETA(Tooltip="Random waypoints"),
	Formation UMETA(Tooltip="Movement in a formation"),
	Panic UMETA(Tooltip="Is in panic, flee away"),
	Aim UMETA(Tooltip="Don't move, just rotate to the target")
};

UENUM(BlueprintType)
enum class EECSMovementDirection: uint8 {
	None,
	Up,
	Down,
	Left,
	Right
};

UENUM(BlueprintType)
enum class EECSFocusType: uint8 {
	None UMETA(Tooltip="No focus set"),
	FocalPoint UMETA(Tooltip="Focus to a fixed location"),
	FocusTarget UMETA(Tooltip="Focus to a moving target"),
};

UENUM(BlueprintType)
enum class EECSOrientation: uint8 {
	None UMETA(Tooltip="No specific orientation / orientation handled outside of ECS"),
	Movement UMETA(Tooltip="Orient to movement direction"),
	Target UMETA(Tooltip="Orient to detected target"),
};
