// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "ECSObject.generated.h"

UENUM(BlueprintType)
enum class EECSObjectType: uint8 {
	Neutral UMETA(Tooltip="Entity is a part of environment"),
	Player UMETA(Tooltip="Player entity"),
	Enemy UMETA(Tooltip="Enemy entity"),
	Summoned UMETA(Tooltip="Summoned creature for help player"),
};
