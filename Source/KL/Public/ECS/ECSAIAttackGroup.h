// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "ECSAIAttackGroup.generated.h"

USTRUCT()
struct FECSAIAttackGroup {
	GENERATED_BODY()

public:
	void Add(int32 Entity, float Distance) {
		Distances.Add(TPairInitializer<int32, float>(Entity, Distance));
	}

	void Empty(int32 Count) {
		Distances.Empty();
		Distances.Reserve(Count);
	}

	void GetAttackBans(int32 Limit, TArray<int32>& Bans) {
		int32 Count = Distances.Num();
		if (Count <= Limit) {
			return;
		}

		Sort();

		// Skip first Limit number of closest entities
		for (int32 Index = Limit; Index < Count; Index++) {
			int32 Entity = Distances[Index].Key;
			Bans.Add(Entity);
		}
	}

	FECSAIAttackGroup() {
		Distances = TArray<TPair<int32, float>>();
	}

	int32 Num() {
		return Distances.Num();
	}

protected:
	TArray<TPair<int32, float>> Distances;

	void Sort() {
		// Ascending sort
		Distances.Sort([](const TPair<int32, float> One, const TPair<int32, float> Two) {
			return One.Value < Two.Value;
		});
	}

};
