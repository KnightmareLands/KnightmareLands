// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Core/ECSSystem.h"
#include "ECS/ECSStats.h"
#include "ECSSystemDelegates.generated.h"

DECLARE_CYCLE_STAT_EXTERN(TEXT("ECSSystem: Delegates"),           STAT_ECSSystemDelegates,          STATGROUP_KLECS, KL_API);
DECLARE_CYCLE_STAT_EXTERN(TEXT("ECSSystem: Delegates AI"),        STAT_ECSSystemDelegatesAI,        STATGROUP_KLECS, KL_API);
DECLARE_CYCLE_STAT_EXTERN(TEXT("ECSSystem: Delegates Movement"),  STAT_ECSSystemDelegatesMovement,  STATGROUP_KLECS, KL_API);
DECLARE_CYCLE_STAT_EXTERN(TEXT("ECSSystem: Delegates Collision"), STAT_ECSSystemDelegatesCollision, STATGROUP_KLECS, KL_API);

UCLASS()
class KL_API UECSSystemDelegates : public UECSSystem {
	GENERATED_BODY()

public: // UECSSystem

	void RequestInit() override;
	void Process(float DeltaTime) override;

protected:
	void ProcessEntity(int32 Entity, float DeltaTime) override;

#if STATS
	TStatId GetStatId() const override;
#endif
};
