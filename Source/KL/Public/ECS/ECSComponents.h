// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AI/Navigation/NavQueryFilter.h"

// NorlinECS
#include "Core/ECSComponent.h"

// KL
#include "ECS/ECSObject.h"
#include "ECS/ECSTypes.h"
#include "Skills/SkillTypes.h"
#include "ECSComponents.generated.h"

class ANavigationData;
class UNavigationQueryFilter;

class AECSController;
class UECSCollisionComponent;
class UECSMovementComponent;
class UECSObjectComponent;

USTRUCT(BlueprintType)
struct FECSDelegate : public FECSComponent {
	GENERATED_BODY()

	UPROPERTY(BlueprintReadWrite)
	AActor* Owner = nullptr;

	UPROPERTY(BlueprintReadWrite)
	UECSMovementComponent* PositionDelegate = nullptr;

	UPROPERTY(BlueprintReadWrite)
	AECSController* AIDelegate = nullptr;

	UPROPERTY(BlueprintReadWrite)
	UECSObjectComponent* ObjectDelegate = nullptr;

	UPROPERTY(BlueprintReadWrite)
	UECSCollisionComponent* CollisionDelegate = nullptr;

	FECSDelegate() {}
};

USTRUCT(BlueprintType)
struct FECSDataPosition: public FECSComponent {
	GENERATED_BODY()

	// Current location of the actor
	UPROPERTY(BlueprintReadWrite)
	FVector Location = FVector::ZeroVector;

	// Current rotation yaw
	UPROPERTY(BlueprintReadWrite)
	float Yaw = 0.f;

	// Should orient actor to movement direction
	UPROPERTY(BlueprintReadWrite)
	EECSOrientation Orientation = EECSOrientation::Movement;

	// If true need to update the Actor's location for the UE4
	UPROPERTY(BlueprintReadWrite)
	bool bNeedUpdate = false;

	// Acceptance radius to the target
	UPROPERTY(BlueprintReadWrite)
	float AcceptanceRadius = 10.f;

	// Destination is reached
	UPROPERTY(BlueprintReadWrite)
	bool bDestinationReached = true;

	UPROPERTY(BlueprintReadWrite)
	bool bDestinationReachedSent = true;

	FECSDataPosition() {}
};

USTRUCT(BlueprintType)
struct FECSDataTarget: public FECSComponent {
	GENERATED_BODY()

	// Target destination
	UPROPERTY(BlueprintReadWrite)
	FVector Location = FVector::ZeroVector;

	// Location where the entity wants to be
	// In comparison with the Location field, which is the factual movement target
	UPROPERTY(BlueprintReadWrite)
	FVector DesiredLocation = FVector::ZeroVector;

	// If it's need to send the reached event
	UPROPERTY(BlueprintReadWrite)
	bool bSendReachedEvent = false;

	// Read Location vector as a direction unit-vector for a constant movement
	UPROPERTY(BlueprintReadWrite)
	bool bIsDirection = false;

	FECSDataTarget() {}
};

USTRUCT(BlueprintType)
struct FECSDataSense: public FECSComponent {
	GENERATED_BODY()

	// Is a target found
	bool bSenseTarget = false;

	// Known target location
	FVector Location = FVector::ZeroVector;

	// Target's aggression
	float Aggression = 0;

	// Sensed target actor
	UPROPERTY()
	AActor* Actor = nullptr;

	// Target velocity
	FVector Velocity = FVector::ZeroVector;

	// Multiple recent values to calculate average
	TArray<FVector> RecentVelocity;

	FECSDataSense() {}
};

USTRUCT(BlueprintType)
struct FECSDataMovement: public FECSComponent {
	GENERATED_BODY()

	/* Basic Movement options */

	// Max speed
	UPROPERTY(BlueprintReadWrite)
	float Speed = 0.f;

	// If actor can move (holding until cancelled)
	UPROPERTY(BlueprintReadWrite)
	bool bPause = false;

	// Current velocity
	UPROPERTY(BlueprintReadWrite)
	FVector Velocity = FVector::ZeroVector;

	// Entity ID of the formation's Master
	UPROPERTY(BlueprintReadWrite)
	int32 MasterEntity = INDEX_NONE;

	// Current movement type
	UPROPERTY(BlueprintReadWrite)
	EECSMovementType Type = EECSMovementType::Idle;

	FECSDataMovement() {}
};

USTRUCT(BlueprintType)
struct FECSDataBoid: public FECSComponent {
	GENERATED_BODY()

	/* Boid algorithm data */

	// If flocking is paused for this entity
	bool bPause = false;

	// Boid groupping index
	int32 BoidIndex = 1;

	// Avoidance vector
	FVector Avoidance = FVector::ZeroVector;

	// Separation vector
	FVector Separation = FVector::ZeroVector;

	// Alignment vector
	FVector Alignment = FVector::ZeroVector;

	// Cohesion vector
	FVector Cohesion = FVector::ZeroVector;

	// Target vector
	FVector Target = FVector::ZeroVector;

	// Separation weight
	float SeparationWeight = 1.2f;
	float SeparationWeightCurrent = 1.2f;

	// Separation radius
	float SeparationRadius = 400.f;
	float SeparationRadiusCurrent = 400.f;

	// Alignment weight
	float AlignmentWeight = 1.2f;
	float AlignmentWeightCurrent = 1.2f;

	// Alignment radius
	float AlignmentRadius = 400.f;
	float AlignmentRadiusCurrent = 400.f;

	// Cohesion radius
	float CohesionRadius = 500.f;
	float CohesionRadiusCurrent = 500.f;

	// Cohesion weight
	float CohesionWeight = 1.f;
	float CohesionWeightCurrent = 1.f;

	FECSDataBoid() {}
};

USTRUCT(BlueprintType)
struct FECSDataSteering: public FECSComponent {
	GENERATED_BODY()

	/* Steering logic */

	// Steering weight
	float SteeringRate = 2.5f;

	FECSDataSteering() {}
};

USTRUCT(BlueprintType)
struct FECSDataAI: public FECSComponent {
	GENERATED_BODY()

	/* AI logic */

	// If AI is temporary suspended (e.g. when moving via SetTargetFor)
	bool bPause = false;

	// If need to update another systems or delegate
	bool bNeedUpdate = false;

	// Max distance of target detection; -1.0 means no distance limit
	float VisibilityDistance = -1.f;

	// Distance of one movement "step"
	float RoamingDistance = 1000.f;

	// Max angle of variance for roaming movement
	float RoamingVarianceAngle = 15.f;

	// Entity's attack distance
	float AttackDistance = 100.f;

	// Action type
	ESkillType ActionType = ESkillType::Melee;

	// Current AI state
	EECSAIState State = EECSAIState::Idle;

	// Previous AI State
	EECSAIState PreviousState = EECSAIState::Idle;

	// AI Movement type
	EECSMovementType Type = EECSMovementType::Idle;

	// Current AI movement direction
	EECSMovementDirection Direction = EECSMovementDirection::None;

	// If entity was denied to attack
	bool bAttackBanned = false;

	// Distance from target to movement trajectory
	float Alienation = 0.f;

	// Seconds left till update the target location
	float UpdateTargetTimeout = 0.f;

	// Seconds left for panic effect
	float PanicTimeout = 0.f;

	// AI group id, to split mobs to different layers
	int32 Group = 0;

	// Entity ID of the formation's Master
	int32 MasterEntity = INDEX_NONE;

	FECSDataAI() {}
};

USTRUCT(BlueprintType)
struct FECSDataCollision: public FECSComponent {
	GENERATED_BODY()

	/* Overlaps logic */

	// If this entity can generate overlaps by tick
	// If == false then overlaps are possible only via manual requests
	bool bGenerateOverlaps = false;

	// Collider radius
	float Radius = 0.f;

	// List of currently overlapped entities
	TArray<int32> OverlappedEntities;

	// List of the entities with overlap in current tick
	TArray<int32> PendingBeginOverlaps;
	TArray<int32> PendingEndOverlaps;

	FECSDataCollision() {
		OverlappedEntities = TArray<int32>();
		PendingBeginOverlaps = TArray<int32>();
		PendingEndOverlaps = TArray<int32>();
	}
};

USTRUCT(BlueprintType)
struct FECSDataObject: public FECSComponent {
	GENERATED_BODY()

	// True if the entity is already dead
	UPROPERTY(BlueprintReadWrite)
	bool bIsDead = false;

	// True if the entity is busy at the moment
	UPROPERTY(BlueprintReadWrite)
	bool bIsBusy = false;

	// Type of the object entity
	UPROPERTY(BlueprintReadWrite)
	EECSObjectType Type = EECSObjectType::Neutral;

	// Aggression level (mobs will attack the one with higher aggression)
	UPROPERTY(BlueprintReadWrite)
	float Aggr = 0.f;

	// Process debug output
	UPROPERTY(BlueprintReadWrite)
	bool bDebug = false;

	FECSDataObject() {}
};

USTRUCT(BlueprintType)
struct FECSDataNavigation: public FECSComponent {
	GENERATED_BODY()

	// If path needs to be updated
	bool bNeedUpdate = false;

	// Navigation path start & end locations
	FVector PathStart;
	FVector PathEnd;

	// Path points
	TArray<FVector> Points;

	// Acceptance radius for the points
	float AcceptanceRadius = 0.f;

	// Timeout before path updating
	float UpdateTimeout = 0.f;

	UPROPERTY()
	const ANavigationData* NavData = nullptr;

	UPROPERTY()
	TSubclassOf<UNavigationQueryFilter> FilterClass;

	// Navigation filter for current entity
	FSharedConstNavQueryFilter QueryFilter;
};
