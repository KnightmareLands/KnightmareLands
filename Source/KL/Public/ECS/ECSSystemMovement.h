// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Core/ECSSystem.h"
#include "ECS/ECSStats.h"
#include "ECSSystemMovement.generated.h"

DECLARE_CYCLE_STAT_EXTERN(TEXT("ECSSystem: Movement"),           STAT_ECSSystemMovement,         STATGROUP_KLECS, KL_API);
DECLARE_CYCLE_STAT_EXTERN(TEXT("ECSSystem: Movement: Barriers"), STAT_ECSSystemMovementBarriers, STATGROUP_KLECS, KL_API);

class UKLTimeFactorSubsystem;

UCLASS()
class KL_API UECSSystemMovement : public UECSSystem {
	GENERATED_BODY()

public:
	UECSSystemMovement();

public: // UECSSystem

	void RequestInit() override;
	void BeginPlay() override;

protected:
	bool CanProcess(int32 Index) const override;
	void ProcessEntity(int32 Entity, float DeltaTime) override;

	void OnUnregistered(int32 Entity) override;

#if STATS
	TStatId GetStatId() const override;
#endif

	float ObjectRadius = 60.f;
	TArray<TEnumAsByte<ECollisionChannel>> ObjectTypes;

	bool FindSlideVector(const FVector& From, const FVector& To, float Radius, const FVector& VelocityDirection, const TArray<TEnumAsByte<ECollisionChannel>>& Channels, FVector& ImpactPoint, FVector& OutSlideVector, bool bReverse = false) const;

	UPROPERTY()
	UKLTimeFactorSubsystem* TimeFactor;
};
