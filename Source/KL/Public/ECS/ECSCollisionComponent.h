// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "ECS/ECSTypes.h"
#include "ECSCollisionComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FECSCollisionEvent, const TArray<AActor*>&, OverlappingActors);

UCLASS(ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class KL_API UECSCollisionComponent : public UActorComponent {
	GENERATED_BODY()

public:
	// Sets default values for this component's properties
	UECSCollisionComponent();

	UFUNCTION(BlueprintCallable, Category = "ECS: Collision")
	bool Register();

	UFUNCTION(BlueprintCallable, Category = "ECS: Collision")
	bool Unregister();

	UFUNCTION(BlueprintCallable, Category = "ECS: Collision")
	void SetRadius(float InRadius);

	/* ECS Delegate methods */

	// When overlap detected
	virtual void OnBeginOverlap(TArray<AActor*> InOverlappingActors);
	virtual void OnEndOverlap(TArray<AActor*> InOverlappingActors);

	// Overlap callback for blueprints
	UPROPERTY(BlueprintAssignable, Category = "ECS: Collision")
	FECSCollisionEvent OnECSOverlapBegin;

	UPROPERTY(BlueprintAssignable, Category = "ECS: Collision")
	FECSCollisionEvent OnECSOverlapEnd;

	virtual void ECSUnregistered();

	UPROPERTY(EditDefaultsOnly, Category = "ECS: Collision")
	bool bActivateECS = true;

	UPROPERTY(EditDefaultsOnly, Category = "ECS: Collision")
	bool bGenerateOverlaps = false;

protected:

	// Called when the game starts
	void BeginPlay() override;

	UPROPERTY(BlueprintReadOnly, Category = "ECS: Collision")
	bool bIsECSActive = false;

	UPROPERTY(EditDefaultsOnly, Category = "ECS: Collision")
	float Radius = 60.f;

	UPROPERTY(BlueprintReadOnly, Category = "ECS: Collision")
	TArray<AActor*> OverlappingActors;

private:


};
