// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Core/ECSSystem.h"
#include "ECS/ECSStats.h"
#include "ECSSystemDebug.generated.h"

DECLARE_CYCLE_STAT_EXTERN(TEXT("ECSSystem: Debug"), STAT_ECSSystemDebug, STATGROUP_KLECS, KL_API);

UCLASS()
class KL_API UECSSystemDebug : public UECSSystem {
	GENERATED_BODY()

public: // UECSSystem

	void RequestInit() override;
	void Process(float DeltaTime) override;

protected:
	bool CanProcess(int32 Index) const override;
	void ProcessEntity(int32 Entity, float DeltaTime) override;

#if STATS
	TStatId GetStatId() const override;
#endif
};
