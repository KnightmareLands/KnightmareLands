// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/SphereComponent.h"
#include "ECS/ECSObject.h"
#include "ECSObjectComponent.generated.h"

class AGameCharacter;

UCLASS(ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class KL_API UECSObjectComponent : public USphereComponent {
	GENERATED_BODY()

public: // Statics

	static FName SpawnedMobsCounter;

public:
	// Sets default values for this component's properties
	UECSObjectComponent();

	void ECSUnregistered();

	UFUNCTION(BlueprintCallable, Category = "ECS: AI: Object")
	bool Register();

	UFUNCTION(BlueprintCallable, Category = "ECS: AI: Object")
	bool Unregister();

	UFUNCTION(BlueprintCallable, Category = "ECS: AI: Object")
	bool SetAggr(float Aggr);

	UFUNCTION(BlueprintCallable, Category = "ECS: AI")
	bool SetEntityDead(bool bIsDead);

protected:
	// Called when the game starts
	void BeginPlay() override;

	UPROPERTY(EditDefaultsOnly, Category = "ECS: AI: Object")
	bool bActivateECS = true;

	UPROPERTY(BlueprintReadOnly, Category = "ECS: AI: Object")
	bool bIsECSActive = false;

	// Define if this Object can be targeted by another entities
	UPROPERTY(EditDefaultsOnly, Category = "ECS: AI: Object")
	EECSObjectType Type = EECSObjectType::Neutral;

	UFUNCTION()
	void OnDeath(AGameCharacter* DeadOwner);

	UFUNCTION()
	void OnRevive(AGameCharacter* DeadOwner);

};
