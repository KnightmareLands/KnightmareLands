// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Common/GameCharacter.h"
#include "PlayerCharacter.generated.h"

UCLASS(Blueprintable)
class KL_API APlayerCharacter : public AGameCharacter {
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	APlayerCharacter();

	void PostInitializeComponents() override;

	AController* GetCharacterController() const override;

	UFUNCTION(BlueprintCallable, Category = "!KL: Player")
	FVector GetHeroLocation() const;

	float TakeDamage(float Damage, struct FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser) override;
	float TakeHeal(float Amount, AController* EventInstigator, AActor* Causer) override;

	void SetHeroID(int32 InHeroID);

	UFUNCTION(BlueprintCallable, Category = "!KL: Player")
	int32 GetHeroID() const;

	void SetDefinedHero();

	UFUNCTION(BlueprintCallable, Category = "!KL: Player")
	bool IsDefinedHero() const;

protected:
	void UpdateSpeed() override;

private:
	int32 HeroID;

	bool bDefinedHero = false;
};
