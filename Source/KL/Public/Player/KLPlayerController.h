// Fill out your copyright notice in the Description page of Project Settings.
#pragma once
#include "GameFramework/PlayerController.h"
#include "Common/Enums.h"
#include "Common/GestureType.h"
#include "Common/TimeEnums.h"
#include "KLPlayerController.generated.h"

class APlayerCharacter;
class AGameCharacter;
class UGameGlobal;
class USkillLauncher;
class AHeroController;
class UKLMenu;
class UKLScoresSubsystem;

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FKLPlayerStep);
DECLARE_EVENT_OneParam(AKLPlayerController, FTargetEvent, bool);

UCLASS()
class AKLPlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	AKLPlayerController();

	/* >> BP HANDLERS*/

	// Get Hero character by id
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "!KL: Player Controller")
	APlayerCharacter* GetHero(int32 HeroID) const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "!KL: Player Controller")
	TArray<AActor*> GetHeroesActors() const;

	UFUNCTION(BlueprintCallable, Category = "!KL: Player Controller")
	void TriggerPause();

	UFUNCTION(BlueprintCallable, Category = "!KL: Player Controller")
	void PauseGame();

	UFUNCTION(BlueprintCallable, Category = "!KL: Player Controller")
	void ResumeGame();

	UFUNCTION(BlueprintCallable, Category = "!KL: Player Controller")
	void StartGame();

	UFUNCTION(BlueprintCallable, Category = "!KL: Player Controller")
	void ShowMainMenu();

	UFUNCTION(BlueprintCallable, Category = "!KL: Player Controller")
	void ShowLevelUpMenu();

	UFUNCTION(BlueprintCallable, Category = "!KL: Player Controller")
	void ShowReplaceMenu();

	// Get current camera zoom level
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "!KL: Player Controller")
	float GetZoomFactor() const;

	UFUNCTION(BlueprintCallable, Category = "!KL: Player Controller")
	void SetZoomLevel(float InZoom);

	UFUNCTION(BlueprintCallable, Category = "!KL: Player Controller")
	TArray<APlayerCharacter*> GetSortedHeroes();

	UFUNCTION(BlueprintCallable, Category = "!KL: Player Controller")
	void HoverHero(int32 HeroID);

	UFUNCTION(BlueprintCallable, Category = "!KL: Player Controller")
	void UnhoverCharacter();

	UFUNCTION(BlueprintCallable, Category = "!KL: Player Controller")
	bool GetHoveredCharacter(AGameCharacter*& OutHoveredCharacter) const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "!KL: Player Controller")
	FVector GetCursorPosition() const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "!KL: Player Controller")
	FVector GetMasterLocation() const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "!KL: Player Controller")
	FText GetKeyNameForAction(const FName ActionName, bool bLongName = false) const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "!KL: Player Controller")
	bool IsLocationOnScreen(const FVector& Location) const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "!KL: Player Controller")
	float GetCurrentSpeed() const;

	UFUNCTION(BlueprintCallable, Category = "!KL: Player Controller")
	void EndMenuMode();

	void StopMovement() override;

	UFUNCTION(BlueprintCallable, Category = "!KL: Player Controller")
	void TeleportGroup(FVector InLocation);

	/* << BP HANDLERS*/

	void OnCauseDamage(float Amount, AActor* Target);
	void OnCauseHeal(float Amount, AActor* Target);

protected:
	void BeginPlay() override;

	// Begin PlayerController interface
	void PlayerTick(float DeltaTime) override;
	void SetHeroesControls(int32 SchemeID = 0);
	void SetupInputComponent() override;
	// End PlayerController interface

	/** Process all player input */
	void UpdatePlayerInput(float DeltaTime);
	/** Process skill buttons and mouse clicks */
	void UpdateSkillInput();
	/** Update movement info */
	void UpdateMovementDirection(float DeltaTime);
	/** Update rotation info */
	void UpdateCursorLocation();
	/** Process Hero controllers with movement and skills usage data */
	void UpdateFellowHeroes();
	/** Update hovered state for characters */
	void UpdateCharacterHover();

	/** Trigger Hover event for character */
	void HoverCharacter(AGameCharacter* InHoveredCharacter);

	void OnToggleUI();

	void OnZoomIn();
	void OnZoomOut();

	void OnAccept();
	void OnCancel();

	void LaunchSkill(int32 SkillIndex, bool bRepeat);

	void OnLevelUp(int32 NewLevel);

	UFUNCTION()
	void OnGameStateChanged(EKLGameState NewState, bool bLoaded);

	UFUNCTION()
	void OnLevelLoaded(EKLGameState NewState, bool bLoaded);

	UFUNCTION()
	void OnSpeedUpdate(float Speed);

	void UpdateCameraMode();

	UFUNCTION()
	void ActivateCameraMode();

	UFUNCTION()
	void EndCameraMode();

	UPROPERTY(EditDefaultsOnly, Category = "!KL: Controller")
	bool bControlsDisabled = false;

	UPROPERTY(BlueprintAssignable, Category = "!KL: Player Controller")
	FKLPlayerStep OnStep;

	UPROPERTY(EditDefaultsOnly, Category = "!KL: Controller")
	float DistancePerStep = 10.f;

	UPROPERTY(BlueprintReadOnly, Category = "!KL: Player Controller")
	bool bMenuMode = false;

	UPROPERTY(BlueprintReadOnly, Category = "!KL: Player Controller")
	bool bMenuFade = false;

	UPROPERTY()
	UGameGlobal* Global;

	UPROPERTY()
	UKLScoresSubsystem* KLScores;

	UPROPERTY()
	TArray<FInputActionKeyMapping> ActionMappings;
	void AddActionMapping(FInputActionHandlerSignature& OnActivate, FName ActionName, FKey Key, bool bRelease = false);

	void BindControlsSchemeDefault();

	static const FName MoveForwardBinding;
	static const FName MoveRightBinding;

	static const FName AimUpBinding;
	static const FName AimRightBinding;

	UPROPERTY(BlueprintReadOnly, Category = "!KL: Controller")
	FVector MovementDirection;
	FVector CursorLocation;

	UPROPERTY()
	AGameCharacter* HoveredCharacter = nullptr;
	bool bIsHovered = false;
	bool bForcedHover = false;

	UPROPERTY(EditDefaultsOnly, Category = "!KL: Controller")
	TSubclassOf<UUserWidget> UIWidgetClass;

	UPROPERTY()
	UUserWidget* GameplayUI = nullptr;

	UPROPERTY(EditDefaultsOnly, Category = "!KL: Controller")
	TSubclassOf<UKLMenu> MenuWidgetClass;

	UPROPERTY(EditDefaultsOnly, Category = "!KL: Controller")
	TSubclassOf<UKLMenu> LevelUpMenuClass;

	UPROPERTY(EditDefaultsOnly, Category = "!KL: Controller")
	TSubclassOf<UKLMenu> HeroReplaceMenuClass;

	UPROPERTY()
	UKLMenu* CurrentMenuWidget;

	TMap<int32, APlayerCharacter*> GetHeroesList() const;

	FVector GetPlayerLocation() const;

	float ZoomMax = 10.f;
	float ZoomLevel = 5.f;

	bool bHasSkillPoints = false;

	void ShowMenuWidget(TSubclassOf<UKLMenu> MenuClass, bool bHasFade = true);
	void ShowCurrentMenu();

	float GestureThreshold = 3.f;
	FVector2D GestureStartPosition = FVector2D::ZeroVector;
	FVector2D GestureDelta = FVector2D::ZeroVector;
	bool bCameraMode = false;

	int32 GestureLastSkill = INDEX_NONE;

	float GestureStartTime = 0.f;

	float MasterSpeed = 0;

	float DistanceSinceLastStep = 0.f;

	void UpdateKillStreak();

	float LastKillTime = 0;
	int32 MultiplierID = INDEX_NONE;
	int32 StreakLength = 0;

	UPROPERTY(EditDefaultsOnly, Category = "!KL: Controller")
	float ComboDelay = 2.f;

	UPROPERTY(EditDefaultsOnly, Category = "!KL: Controller")
	float BonusDistance = 500.f;

	UPROPERTY(EditDefaultsOnly, Category = "!KL: Controller")
	float BonusDistanceFactor = 2.f;

	UPROPERTY(EditDefaultsOnly, Category = "!KL: Controller")
	float LongDistanceFactor = 0.8f;

	UFUNCTION()
	void OnTimeFadeComplete(EKLDilationState State);
};


