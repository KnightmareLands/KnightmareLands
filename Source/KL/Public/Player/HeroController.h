// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "ECS/ECSController.h"
#include "Common/TargetingInfoInterface.h"
#include "HeroController.generated.h"

class AFormation;
class AGameCharacter;
class UGameGlobal;

/**
 * An AI Controller to handle Heroes which are not directly controlled by players
 */
UCLASS()
class KL_API AHeroController : public AECSController, public ITargetingInfoInterface {
	GENERATED_BODY()

public:
	AHeroController();

	void BeginPlay() override;
	void Tick(float DeltaSeconds) override;
	void StopMovement() override;

	bool CanUpdateMovement(FVector InCursorLocation, bool bIsHovered);
	virtual void UpdateMovement(FVector InCursorLocation, bool bIsHovered);
	void SetCanAct(bool InEnabled);

	USkillLauncher* GetNextSkill() const;

	void LaunchSkill(int32 SkillIndex, FTargetingInfo TargetingInfo);
	void DeactivateSkill();

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "!KL: Hero Controller")
	FVector GetOriginalLocation() const;

	//UFUNCTION(BlueprintNativeEvent, Category = "!KL: Hero Controller")
	void MoveHeroToLocation(FVector Location, bool bHasTarget = false);

	bool ECSPostRegister() override;
	bool ECSPostUnregister() override;

	UFUNCTION(BlueprintCallable, Category = "!KL: Hero Controller")
	bool GetRandomReachablePointInRadius(const FVector& Origin, float Radius, FVector& OutLocation) const;

protected:
	void PerformPlayerAction();

	virtual void ProcessController(float DeltaSeconds);
	virtual void UpdateMaster(float DeltaTime);
	virtual void PerformMovement();
	virtual bool UpdateTargetingInfo(ETargetingType InType, bool bPositive, FTargetingInfo& OutTargetingInfo) override;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "!KL: Hero Controller")
	bool FindTarget(ETargetingType InType, bool bPositive, FTargetingInfo& OutTarget);

	virtual bool IsManualTargetValid() const;

	UFUNCTION(BlueprintCallable, Category = "!KL: Hero Controller")
	AGameCharacter* GetHero() const;

	FVector CursorLocation;

	UPROPERTY()
	FTargetingInfo ManualTarget;

	bool bManualTarget = false;

	int32 NextSkill = INDEX_NONE;
	USkillLauncher* ActiveSkill = nullptr;

	// Allow automatic use for the default skill
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "!KL: Hero Controller")
	bool bAutoActionEnabled = false;

	// If controller can trigger skill (e.g. while attack button is pressed)
	bool bCanAct = false;

	UPROPERTY(BlueprintReadOnly, Category = "!KL: Hero Controller")
	FVector MasterLocation = FVector::ZeroVector;

	UPROPERTY(BlueprintReadOnly, Category = "!KL: Hero Controller")
	FVector PositionAdjustment;

	UPROPERTY(EditDefaultsOnly, Category = "!KL: Settings")
	float CursorFocusDistance = 2000.f;

	UPROPERTY(EditDefaultsOnly, Category = "!KL: Settings")
	TSubclassOf<UNavigationQueryFilter> NavigationFilter = NULL;

protected: // Cache

	UPROPERTY()
	UGameGlobal* Global = nullptr;

};
