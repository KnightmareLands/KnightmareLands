// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Common/GameCharacter.h"
#include "KLCharacter.generated.h"

UCLASS(Blueprintable)
class AKLCharacter : public AGameCharacter {
	GENERATED_BODY()

public:
	AKLCharacter();

	float TakeDamage(float Damage, struct FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser) override;
};
