// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Player/HeroController.h"
#include "MeleeController.generated.h"

/**
 *
 */
UCLASS()
class KL_API AMeleeController : public AHeroController {
	GENERATED_BODY()

public:
	void UpdateMovement(FVector InCursorLocation, bool bIsHovered) override;

	FVector GetOriginalLocation_Implementation() const override;

protected:
	bool UpdateTargetingInfo(ETargetingType InType, bool bPositive, FTargetingInfo& OutTargetingInfo) override;

	void PerformMovement() override;
	bool FindTarget_Implementation(ETargetingType InType, bool bPositive, FTargetingInfo& OutTarget) override;
	bool IsManualTargetValid() const override;

	UPROPERTY()
	FTargetingInfo MeleeTarget;
	bool bMeleeTarget = false;

	UFUNCTION(BlueprintImplementableEvent, Category = "!KL: Melee Controller")
	void OnMeleeDashStart();

	UFUNCTION(BlueprintImplementableEvent, Category = "!KL: Melee Controller")
	void OnMeleeDashEnd();

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "!KL: Melee Controller")
	float SenseMaxOffset = 350.f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "!KL: Melee Controller")
	float SenseRadius = 500.f;

	UPROPERTY(BlueprintReadOnly, Category = "!KL: Melee Controller")
	FVector SenseCenter;
};
