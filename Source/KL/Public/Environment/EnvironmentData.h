#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "EnvironmentData.generated.h"

UCLASS(BlueprintType)
class KL_API UEnvironmentData : public UDataAsset {
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UMaterialParameterCollection* Colors;
};
