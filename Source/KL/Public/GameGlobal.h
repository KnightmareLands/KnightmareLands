// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "Common/Enums.h"
#include "Common/MobTypes.h"
#include "GenericPlatform/GenericWindow.h"
#include "GameGlobal.generated.h"

class AGameCharacter;
class APlayerCharacter;
class AFormation;
class UKLLoader;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FGameStateChanged, EKLGameState, NewGameState, bool, bLoaded);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FSpeedUpdate, float, NewSpeed);

/**
 *
 */
UCLASS(Config = Game)
class KL_API UGameGlobal : public UGameInstance {
	GENERATED_BODY()

public:
	void Init() override;

	UFUNCTION(BlueprintCallable, Category = "Debug")
	bool IsDebug() const;

	UFUNCTION(BlueprintCallable, Category = "!KL: Instance")
	void ResetHeroes();

	UFUNCTION(BlueprintCallable, Category = "!KL: Instance")
	void Reset();

	UFUNCTION(BlueprintImplementableEvent, Category = "!KL: Instance")
	void OnReset();

	UPROPERTY(BlueprintReadWrite, Category = "!KL: Instance")
	TArray<int32> Heroes;

	UPROPERTY(BlueprintReadWrite, Category = "!KL: Settings")
	int32 SchemeID = 0;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "!KL: Settings")
	float CameraRotationAngle = -45.f;

	UPROPERTY(BlueprintReadWrite, Category = "!KL: Settings")
	float CurrentCameraRotationAngle = 0.f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "!KL: Settings")
	float SpawnRate = 1.0;

	void AddCharacter(AGameCharacter* Character);
	void RemoveCharacter(AGameCharacter* Character);

	UFUNCTION(BlueprintCallable, Category = "!KL: Instance")
	AGameCharacter* GetCharacterByName(FString Name);

	UFUNCTION(BlueprintCallable, Category = "!KL: Settings")
	bool IsNight() const;

	UFUNCTION(BlueprintCallable, Category = "!KL: Instance")
	EKLGameState GetGameState() const;

	UFUNCTION(BlueprintCallable, Category = "!KL: Instance")
	bool IsGameOver() const;

	UFUNCTION(BlueprintCallable, Category = "!KL: Instance")
	bool IsGameStarted() const;

	UFUNCTION(BlueprintCallable, Category = "!KL: Instance")
	bool IsInGame() const;

	UFUNCTION(BlueprintCallable, Category = "!KL: Instance")
	void SetGameOver();

	UFUNCTION(BlueprintCallable, Category = "!KL: Instance")
	void SetGameStarted();

	APlayerCharacter* GetHero(int32 HeroID) const;

	UPROPERTY(BlueprintReadOnly, VisibleInstanceOnly, Category = "!KL: Instance")
	TMap<int32, APlayerCharacter*> HeroesList;

	void SetGroupFormation(AFormation* InFormation);
	AFormation* GetGroupFormation() const;

	UFUNCTION(BlueprintCallable, Category = "!KL: Settings")
	bool IsZoomEnabled() const;

	UFUNCTION(BlueprintCallable, Category = "!KL: Settings")
	int32 GetZoomLevel() const;

	UFUNCTION(BlueprintCallable, Category = "!KL: Settings")
	float GetHoldDelay() const;

	UFUNCTION(BlueprintCallable, Category = "!KL: Settings")
	float GetDetectionDistance() const;

	UFUNCTION(BlueprintCallable, BlueprintPure = false, Category = "!KL: Settings")
	TArray<APlayerCharacter*> GetHeroes(bool bAlive = false) const;

	UFUNCTION(BlueprintCallable, Category = "!KL: Instance")
	bool GetHeroesLocation(FVector& OutLocation) const;

	UPROPERTY(EditDefaultsOnly, Category = "!KL: Settings")
	float HeroesSpeed = 600.f;

	UFUNCTION(BlueprintCallable, Category = "!KL: Settings")
	TMap<EMobType, float> GetTypesWeight() const;

	// -90% from the speed attribute
	UPROPERTY(EditDefaultsOnly, Category = "!KL: Settings")
	float DamagedSpeedFactor = -90.f;

	UPROPERTY(EditDefaultsOnly, Category = "!KL: Settings")
	float DamagedSpeedTime = 0.1f;

	// Minimum possible targeted mobs
	UPROPERTY(EditDefaultsOnly, Category = "!KL: Settings")
	int32 MinTargetedCount = 4;

	// Percent of mobs who can be targeted to players from overall spawned mobs
	UPROPERTY(EditDefaultsOnly, Category = "!KL: Settings")
	float MaxTargetedPercent = 0.1;

	UPROPERTY(BlueprintAssignable, Category = "!KL: Instance")
	FGameStateChanged OnGameStateChanged;

	UPROPERTY(BlueprintAssignable, Category = "!KL: Instance")
	FGameStateChanged OnLevelLoaded;

	// Camera rotation sensitivity
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "!KL: Settings", meta = (ClampMin = "0.1", ClampMax = "10.0", UIMin = "0.1", UIMax = "10.0"))
	float CameraSensitivityX = 1.f;

	// Camera zoom sensitivity
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "!KL: Settings", meta = (ClampMin = "0.1", ClampMax = "10.0", UIMin = "0.1", UIMax = "10.0"))
	float CameraSensitivityY = 1.f;

	UFUNCTION(BlueprintCallable, Category = "!KL: Instance")
	void AddResurector();

	UFUNCTION(BlueprintCallable, Category = "!KL: Instance")
	void RemoveResurector();

	bool HasResurectors() const;

	UFUNCTION(BlueprintCallable, Category = "!KL: Instance")
	float GetHeroesSpeed() const;

	void SpeedUpdate();

	UPROPERTY(BlueprintAssignable, Category = "!KL: Instance")
	FSpeedUpdate OnSpeedUpdate;

	UFUNCTION(BlueprintImplementableEvent, Category = "!KL: Instance")
	void UpdateHealthState();

	UFUNCTION(BlueprintCallable, Category = "!KL: Instance")
	void SetLoaded(bool bLoaded = true);

	UFUNCTION(BlueprintCallable, Category = "!KL: Instance")
	void SetDemoMode();

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "!KL: Instance")
	bool IsLoading() const;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Debug")
	FName StartingLevel;

	UKLLoader* GetLoader() const;

	UFUNCTION(BlueprintCallable, Category = "!KL: Settings")
	void CallSaveConfig();

protected:
	void OnStart() override;

	UPROPERTY(EditDefaultsOnly, Category = "Debug")
	bool bIsDebug = false;

	UPROPERTY(BlueprintReadOnly, Category = "!KL: Instance")
	EKLGameState CurrentState = EKLGameState::MainMenu;

	UPROPERTY(EditDefaultsOnly, Category = "Debug")
	int32 InitialSkillPoints = 0;

	UPROPERTY()
	TMap<FString, AGameCharacter*> CharactersMap;

	UPROPERTY(EditDefaultsOnly, Category = "!KL: Settings")
	bool bIsNight = false;

	UPROPERTY()
	AFormation* GroupFormation;

	UPROPERTY(EditDefaultsOnly, Category = "!KL: Settings")
	int32 ScoresToFirstLevel = 500;

	// Set if zooming is enabled or not
	UPROPERTY(EditDefaultsOnly, Category = "!KL: Settings")
	bool bZoomEnabled = false;

	// Default zoom level for the camera
	UPROPERTY(EditDefaultsOnly, Category = "!KL: Settings")
	int32 ZoomLevel = 5;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "!KL: Settings")
	int32 BasicUpgradesAmount = 4;

	// Seconds before hold event triggered
	UPROPERTY(EditDefaultsOnly, Category = "!KL: Settings")
	float SkillButtonHoldDelay = 0.2f;

	// Max distance for the GetClosestCharacter method
	UPROPERTY(EditDefaultsOnly, Category = "!KL: Settings")
	float DetectionDistance = 5000.f;

	UPROPERTY(EditDefaultsOnly, Category = "!KL: Settings")
	TMap<EMobType, float> TypesWeights;

	void TriggerGameStateChange();

	int32 ResurectorCount = 0;

	bool bIsLoading = true;

	UPROPERTY()
	UKLLoader* Loader;

private:

};
