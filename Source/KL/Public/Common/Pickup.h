// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"

#include "API/ECSEntity.h"
#include "Pickup.generated.h"

class APlayerCharacter;
class UECSCollisionComponent;

UCLASS()
class KL_API APickup : public AActor, public IECSEntity {
	GENERATED_BODY()

public: // AActor

	APickup();

	void Tick(float DeltaTime) override;
	void Destroyed() override;

protected: // AActor

	void BeginPlay() override;

public: // IECSEntity

	UECSEngine* GetEngineBP_Implementation() const override;
	int32 GetEntityBP_Implementation() const override;

protected: // ECS Entity data

	UPROPERTY()
	UECSEngine* ECS = nullptr;
	int32 ECSIndex = INDEX_NONE;

public: // Methods

	UFUNCTION(BlueprintCallable, Category = "!KL: Pickup")
	float GetTimeoutLeft() const;

	// Should be called from blueprint in case of manual destroy required (e.g. wait for particles effect or something else)
	UFUNCTION(BlueprintCallable, Category = "!KL: Pickup")
	void PickupHandled();

	UFUNCTION(BlueprintImplementableEvent, Category = "!KL: Pickup")
	void NotifyPickup(APlayerCharacter* Hero);

	UFUNCTION(BlueprintImplementableEvent, Category = "!KL: Pickup")
	void NotifyDisappear();

	UFUNCTION(BlueprintNativeEvent, Category = "!KL: Pickup")
	bool CanTakePickup(const APlayerCharacter* Hero) const;

protected: // Internal methods

	void RegisterECS();

	UFUNCTION()
	void OnOverlapBegin(const TArray<AActor*>& OtherActors);

	void Disappear();

protected: // Settings & variables

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "!KL: Setup")
	float Duration = 10.f;
	
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "!KL: Setup")
	float SpawnDuration = 1.f;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "!KL: Character")
	UECSCollisionComponent* ECSCollisionComponent = nullptr;

	UPROPERTY(BlueprintReadWrite, Category = "!KL: Setup")
	bool bIsReady = false;
	
	UPROPERTY(BlueprintReadOnly, Category = "!KL: Setup")
	bool bIsHandled = true;

	UPROPERTY(BlueprintReadOnly, Category = "!KL: Setup")
	bool bIsTaken = false;

	UPROPERTY(BlueprintReadOnly, Category = "!KL: Setup")
	float StartTime = INDEX_NONE;

};
