// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "Common/MobTypes.h"
#include "ScalingMath.generated.h"

/**
 * Static methods for calculating the game's math and scaling
 */
UCLASS()
class KL_API UScalingMath : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:

	UFUNCTION(BlueprintPure, Category = "!KL: Scaling")
	static float GetScaledValue(float BaseValue, int32 Level, float Multiplier = 1.0f);

	UFUNCTION(BlueprintPure, Category = "!KL: Scaling")
	static float GetDamage(float Attack, float Defense);

	UFUNCTION(BlueprintPure, Category = "!KL: Scaling")
	static int32 GetTier(int32 Value, int32 Initial = 1);

	UFUNCTION(BlueprintPure, Category = "!KL: Scaling")
	static int32 GetTierValue(int32 Tier, int32 Initial = 1);

	UFUNCTION(BlueprintPure, Category = "!KL: Scaling")
	static int32 GetValueToNextTier(int32 Value, int32 Initial = 1);

	UFUNCTION(BlueprintPure, Category = "!KL: Scaling")
	static float GetHealthBuff(EMobType Type);

	UFUNCTION(BlueprintPure, Category = "!KL: Scaling")
	static float GetSpeedBuff(EMobType Type);

	UFUNCTION(BlueprintPure, Category = "!KL: Scaling")
	static float GetAttackBuff(EMobType Type);

	UFUNCTION(BlueprintPure, Category = "!KL: Scaling")
	static float GetScoresMultiplier(EMobType Type);

	UFUNCTION(BlueprintPure, Category = "!KL: Scaling")
	static EMobType GetRandomMobType(TMap<EMobType, float> TypesWeights);
};
