// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"

// NorlinECS
#include "API/ECSEntity.h"

#include "Common/Enums.h"
#include "Common/Attributes.h"
#include "GameCharacter.generated.h"

// UE4
class USkeletalMeshComponent;
class UWidgetComponent;

// NorlinECS
class UECSEngine;

// SpawnWaves
class USWDeathDelegate;

// KL
class UECSCollisionComponent;
class UECSMovementComponent;
class UECSObjectComponent;
class UGameGlobal;
class UHealthComponent;
class USkillLauncher;
class USkillsComponent;
class UStatusManagerComponent;

UENUM(BlueprintType)
enum class ESpeedMode: uint8 {
	None UMETA(Tooltip="Keep going what's was before"),
	Normal UMETA(Tooltip="Normal movement"),
	Damaged UMETA(Tooltip="Right after take damage"),
	Catch UMETA(Tooltip="Catching in-group position"),
	Stun UMETA(Tooltip="Character is stunned, usually can't move")
};

DECLARE_EVENT_OneParam(AGameCharacter, FCharacterDeath, AGameCharacter*);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FKLCharacterEvent);

UCLASS()
class KL_API AGameCharacter : public APawn, public IECSEntity {
	GENERATED_BODY()

public: // APawn

	AGameCharacter();
	void Destroyed() override;

protected: // APawn

	void BeginPlay() override;

public: // IECSEntity

	UECSEngine* GetEngineBP_Implementation() const override;
	int32 GetEntityBP_Implementation() const override;

protected: // ECS Entity data

	UPROPERTY()
	UECSEngine* ECS = nullptr;
	int32 ECSIndex = INDEX_NONE;

public: // Methods

	void RegisterECS();


	static FName MeshComponentName;
	static FName CapsuleComponentName;

	void PreInitializeComponents() override;
	void PostInitializeComponents() override;

	// Called to bind functionality to input
	void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	float TakeDamage(float Damage, struct FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser) override;
	virtual float TakeHeal(float Amount, AController* EventInstigator, AActor* Causer);

	void Hover();
	void Unhover();

	virtual void InitializeAttributes(bool bInit = false);

	void SetLevel(int32 InLevel);

	/* >> BP EVENTS */

	/** Event when this actor takes ANY healing */
	UFUNCTION(BlueprintImplementableEvent, BlueprintAuthorityOnly, meta=(DisplayName = "AnyHealing"), Category="!KL: Character")
	void ReceiveAnyHealing(float Amount, class AController* InstigatedBy, AActor* Causer);

	/* << BP EVENTS */

	/* >> Attributes */

	UPROPERTY(BlueprintReadOnly, Category = "!KL: Character")
	UKLAttribute* AttributeHealth;

	UPROPERTY(BlueprintReadOnly, Category = "!KL: Character")
	UKLAttribute* AttributeDefense;

	UPROPERTY(BlueprintReadOnly, Category = "!KL: Character")
	UKLAttribute* AttributeAttack;

	UPROPERTY(BlueprintReadOnly, Category = "!KL: Character")
	UKLAttribute* AttributeWalkSpeed;

	UPROPERTY(BlueprintReadOnly, Category = "!KL: Character")
	UKLAttribute* AttributeSkillSpeed;

	/* << Attributes */

	/* >> BP HANDLERS */

	// Return true if the CHaracter is in debug mode
	UFUNCTION(BlueprintCallable, Category = "Debug")
	bool IsDebug() const;

	// Return true if Character shoul not attack
	UFUNCTION(BlueprintCallable, Category = "Debug")
	bool IsDebugForbidAttack() const;

	UPROPERTY(EditDefaultsOnly, Category = "!KL: Debug")
	FColor DebugColor;

	UFUNCTION(BlueprintCallable, Category = "!KL: Character")
	UHealthComponent* GetHealthComponent() const;

	UFUNCTION(BlueprintCallable, Category = "!KL: Character")
	bool IsCharacterMoving() const;

	UFUNCTION(BlueprintCallable, Category = "!KL: Character")
	bool IsCharacterBusy() const;

	UFUNCTION(BlueprintCallable, Category = "!KL: Character")
	float GetMaxHealth() const;

	UFUNCTION(BlueprintCallable, Category = "!KL: Character")
	float GetCurrentHealth() const;

	UFUNCTION(BlueprintCallable, Category = "!KL: Character")
	float GetDefense() const;

	UFUNCTION(BlueprintCallable, Category = "!KL: Character")
	float GetSpeed() const;

	UFUNCTION(BlueprintCallable, Category = "ECS: Movement")
	float GetCurrentSpeed() const;

	UFUNCTION(BlueprintCallable, Category = "!KL: Character")
	virtual AController* GetCharacterController() const;

	UFUNCTION(BlueprintCallable, Category = "!KL: Character")
	AController* GetCharacterInstigatorController() const;

	UFUNCTION(BlueprintCallable, Category = "!KL: Character")
	UGameGlobal* GetGameGlobal() const;

	UFUNCTION(BlueprintCallable, Category = "!KL: Character")
	AGameCharacter* GetClosestCharacterOfClass(TSubclassOf<AGameCharacter> CharacterClass, float MaxDistance = -1.f) const;

	UFUNCTION(BlueprintCallable, Category = "!KL: Character")
	bool IsCharacterDead() const;

	UFUNCTION(BlueprintCallable, Category = "!KL: Character")
	int32 GetLevel() const;

	UFUNCTION(BlueprintCallable, Category = "!KL: Character")
	virtual void StartBusy();

	UFUNCTION(BlueprintCallable, Category = "!KL: Character")
	virtual void EndBusy();

	UFUNCTION(BlueprintCallable, Category = "!KL: Character")
	TArray<USkillLauncher*> GetSkillsList() const;

	UFUNCTION(BlueprintCallable, Category = "!KL: Character")
	void SetStun(bool bIsStunned);

	UFUNCTION(BlueprintCallable, Category = "!KL: Character")
	bool IsStunned();

	// Used for the ECSController to set the ECS AI visibility distance
	UFUNCTION(BlueprintCallable, Category = "!KL: Character")
	float GetDetectDistance() const;

	UFUNCTION(BlueprintCallable, Category = "!KL: Character")
	UECSMovementComponent* GetECSMovement() const;

	UFUNCTION(BlueprintCallable, Category = "!KL: Character")
	UECSCollisionComponent* GetECSCollision() const;

	UFUNCTION(BlueprintCallable, Category = "!KL: Character")
	bool IsReviving() const;

	UFUNCTION(BlueprintCallable, Category = "!KL: Character")
	virtual void BeginRevive();

	UFUNCTION(BlueprintCallable, Category = "!KL: Character")
	virtual void Revive(float Health, AActor* Causer);

	/* << BP HANDLERS */

	// Check if a skill for id exist and return it
	USkillLauncher* GetValidSkill(int32 SkillIndex) const;

	int32 GetAutoSkillIndex() const;

	// Prevent character rotation till EndBusy called
	void HoldRotation();
	void FreeRotation();
	bool IsHoldRotation() const;

	void OnReplaced();

	FCharacterDeath OnCharacterDead;
	FCharacterDeath OnCharacterRevive;

	UPROPERTY(BlueprintAssignable, Category = "!KL: Character")
	FKLCharacterEvent OnCharacterDeadEvent;

	UPROPERTY(BlueprintAssignable, Category = "!KL: Character")
	FKLCharacterEvent OnCharacterReviveEvent;

	UPROPERTY(BlueprintAssignable, Category = "!KL: Character")
	FKLCharacterEvent OnCharacterReplaced;

	void StopMovement();

	UFUNCTION(BlueprintCallable, Category = "!KL Summon")
	void RegisterSummonedCharacter(AGameCharacter* SummonedCharacter);

	UFUNCTION(BlueprintCallable, Category = "!KL Summon")
	void KillSummons();

	UFUNCTION(BlueprintCallable, Category = "!KL Summon")
	void SetActorLocationECS(const FVector& InLocation);

protected:

	/* >> BP EVENTS */

	UFUNCTION(BlueprintImplementableEvent, Category = "!KL: Character")
	void OnHover();

	UFUNCTION(BlueprintImplementableEvent, Category = "!KL: Character")
	void OnUnhover();

	UFUNCTION(BlueprintImplementableEvent, Category = "!KL: Character")
	void OnDeath(AActor* DamageCauser);

	UFUNCTION(BlueprintImplementableEvent, Category = "!KL: Character")
	void OnReviveBegin();

	UFUNCTION(BlueprintImplementableEvent, Category = "!KL: Character")
	void OnRevive();

	UFUNCTION(BlueprintImplementableEvent, Category = "!KL: Character", meta = (DisplayName="OnReplaced"))
	void OnReplacedEvent();

	UFUNCTION(BlueprintImplementableEvent, Category = "!KL: Character")
	void OnLevelUp(int32 NewLevel);

	/* << BP EVENTS */

	bool bIsCharacterReviving = false;
	float TimeOfDeath = 0;

	UPROPERTY(EditInstanceOnly, Category = "!KL: Character")
	int32 Level = 1;

	/* >> Character Options */

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "!KL: Character")
	float BaseHealth = 100;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "!KL: Character")
	float BaseDefense = 1;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "!KL: Character")
	float BaseAttack = 10;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "!KL: Character")
	float BaseSkillSpeed = 1.f;

	// Only for mobs;
	// For PlayerCharacter it depends on the GameGlobal->HeroesSpeed setting
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "!KL: Character")
	float BaseWalkSpeed = 200;

	UPROPERTY(EditDefaultsOnly, Category = "!KL: Character")
	float DetectDistance = 1000.f;

	UPROPERTY(EditDefaultsOnly, Category = "!KL: Character")
	UMaterialInterface* SelectionMaterial;

	/* << Character Options */

	/* >> Components */

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "!KL: Character")
	UHealthComponent* HealthComponent = nullptr;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "!KL: Character")
	UWidgetComponent* HealthWidget = nullptr;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "!KL: Character")
	USkillsComponent* SkillsComponent = nullptr;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "!KL: Character")
	UStatusManagerComponent* StatusManagerComponent = nullptr;

	/* << Components */

	UFUNCTION()
	virtual void Death(float DamageDealt, AActor* DamageCauser, bool bLethalDamage);

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "!KL: Character")
	UECSMovementComponent* ECSMovementComponent = nullptr;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "!KL: Character")
	UECSCollisionComponent* ECSCollisionComponent = nullptr;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "!KL: Character")
	UECSObjectComponent* ECSObjectComponent = nullptr;

	UFUNCTION()
	void OnHealthUpdate(float NewBaseHealth);

	UFUNCTION()
	void OnSpeedUpdate(float NewSpeed) {
		UpdateSpeed();
	}

	virtual void UpdateSpeed();

	UFUNCTION()
	void RestoreSpeed();

	UPROPERTY()
	USWDeathDelegate* DeathDelegate = nullptr;

	UPROPERTY()
	TArray<AGameCharacter*> SummonedCharacters;

private:
	/** The main skeletal mesh associated with this Character (optional sub-object). */
	UPROPERTY(Category=Character, VisibleAnywhere, BlueprintReadOnly, meta=(AllowPrivateAccess = "true"))
	USkeletalMeshComponent* Mesh;

	/** The CapsuleComponent being used for movement collision (by CharacterMovement). Always treated as being vertically aligned in simple collision check functions. */
	UPROPERTY(Category=Character, VisibleAnywhere, BlueprintReadOnly, meta=(AllowPrivateAccess = "true"))
	UCapsuleComponent* CapsuleComponent;

	UPROPERTY(EditAnywhere, Category = "Debug")
	bool bIsDebug = false;

	UPROPERTY(EditAnywhere, Category = "Debug")
	bool bDebugForbidAttack = false;

	bool bBusy = false;
	bool bHoldRotation = false;
	int32 ActiveSkillIndex = 0;

	bool bStunned = false;

	float DetectionDistance = 5000.f;

	FTimerHandle SpeedTimerHandle;
	int32 SpeedDamageDebuff = INDEX_NONE;

};
