// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "MobTypes.generated.h"

UENUM(BlueprintType)
enum class EMobType: uint8 {
	// A regular mob
	Regular UMETA(DisplayName="Regular", ShortToolTip="A regular mob"),
	// Regular mob with increased scores
	Bonus UMETA(DisplayName="Bonus", ShortToolTip="Regular mob with increased scores"),
	// A mob with increased health and damage
	Strong UMETA(DisplayName="Strong", ShortToolTip="A mob with increased health and damage"),
	// A mob with increased speed
	Fast UMETA(DisplayName="Fast", ShortToolTip="A mob with increased speed"),
	// A mob with highly increased health and damage (and scores
	Boss UMETA(DisplayName="Boss", ShortToolTip="A mob with highly increased health and damage (and scores)")
};
