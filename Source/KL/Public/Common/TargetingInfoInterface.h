// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Common/Enums.h"
#include "TargetingInfoInterface.generated.h"

/**
 * Interface for target updating
 */
UINTERFACE(Blueprintable, meta = (CannotImplementInterfaceInBlueprint))
class UTargetingInfoInterface : public UInterface {
	GENERATED_BODY()
};

class ITargetingInfoInterface {
	GENERATED_BODY()

protected:
	// Targeting info response, must be overridden in children classes
	UFUNCTION(BlueprintCallable, Category = "!KL: Targeting")
	virtual bool UpdateTargetingInfo(ETargetingType InType, bool bPositive, FTargetingInfo& OutTargetingInfo) {
		return false;
	}
};
