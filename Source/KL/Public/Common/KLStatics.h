// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "Animation/AnimMontage.h"
#include "Common/Enums.h"
#include "Engine/WorldComposition.h"
#include "KLStatics.generated.h"

class AGameCharacter;

DECLARE_DYNAMIC_DELEGATE_ThreeParams(FSortDelegate, const AGameCharacter*, Character1, const AGameCharacter*, Character2, bool&, Result);

/**
 * Common functions
 */
UCLASS()
class KL_API UKLStatics : public UBlueprintFunctionLibrary {
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintCallable, Category="!KL: Common", meta=(WorldContext="WorldContextObject"))
	static bool FindRadialTargets(UObject* WorldContextObject, const FVector& Origin, float Radius, const TArray<AActor*> IgnoredActors, bool bAliveOnly, TArray<AGameCharacter*>& OutTargets);

	UFUNCTION(BlueprintCallable, BlueprintAuthorityOnly, Category="!KL: Damage")
	static float ApplyDamage(AActor* TargetActor, float BaseDamage, AController* EventInstigator, EInstigatorType InstigatorType, AActor* DamageCauser);

	UFUNCTION(BlueprintCallable, BlueprintAuthorityOnly, Category="!KL: Damage", meta=(WorldContext="WorldContextObject"))
	static float ApplyRadialDamageWithFalloff(UObject* WorldContextObject, const FVector& TargetLocation, float BaseDamage, AController* EventInstigator, EInstigatorType InstigatorType, AActor* DamageCauser, float DamageInnerRadius, float DamageOuterRadius, float DamageFalloff, TArray<AGameCharacter*>& OutDamagedActors);

	UFUNCTION(BlueprintCallable, BlueprintAuthorityOnly, Category="!KL: Healing")
	static float ApplyHeal(AGameCharacter* TargetActor, float BaseAmount, AController* EventInstigator, EInstigatorType InstigatorType, AActor* HealCauser);

	UFUNCTION(BlueprintCallable, BlueprintAuthorityOnly, Category="!KL: Healing", meta=(WorldContext="WorldContextObject"))
	static bool ApplyRadialHeal(UObject* WorldContextObject, const FVector& TargetLocation, float BaseAmount, AController* EventInstigator, EInstigatorType InstigatorType, AActor* HealCauser, float InnerRadius, float OuterRadius, float Falloff);


	UFUNCTION(BlueprintCallable, BlueprintPure, Category="!KL: Common")
	static float GetMontageSectionLength(UAnimMontage* Montage, FName SectionName);

	UFUNCTION(BlueprintCallable, BlueprintPure, Category="!KL: Common")
	static float GetDistanceToTarget(FTargetingInfo Target, FVector FromLocation, AGameCharacter*& ClosestActor);

	UFUNCTION(BlueprintCallable, BlueprintPure, Category="!KL: Common")
	static TArray<AGameCharacter*> SortCharacters(const FSortDelegate SortDelegate, const TArray<AGameCharacter*> Characters);

	UFUNCTION(BlueprintCallable, BlueprintPure, Category="!KL: Common")
	static FString TimeSecondsToString(float InSeconds);


	UFUNCTION(BlueprintCallable, BlueprintPure, Category="!KL: System")
	static bool IsTearingDown(UObject* WorldContextObject);

	UFUNCTION(BlueprintCallable, Category="!KL: Log")
	static void KLErrorLog(FString Message);


	static bool SphereTraceSingle(UObject* WorldContextObject, const FVector Start, const FVector End, float Radius, const TArray<TEnumAsByte<ECollisionChannel>>& TraceChannels, FHitResult& OutHit, bool bFindInitialOverlaps = true);
	static bool LineTraceSingle(UObject* WorldContextObject, const FVector Start, const FVector End, ECollisionChannel TraceChannel, FHitResult& OutHit, bool bFindInitialOverlaps = true);

	static FString MakeSafeLevelName(UObject* WorldContextObject, const FName& InLevelName);

	static FWorldTileInfo GetTileInfo(UObject* WorldContextObject, const FName& InPackageName);
};
