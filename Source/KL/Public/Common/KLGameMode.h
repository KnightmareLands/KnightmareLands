// Fill out your copyright notice in the Description page of Project Settings.
#pragma once
#include "GameFramework/GameModeBase.h"
#include "KLGameMode.generated.h"

class APlayerCharacter;
class UGameGlobal;
class AFormation;
class UECSEngine;
class UKLLoader;
class UPerkManager;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FHeroReplaced, int32, HeroID);

UCLASS(minimalapi)
class AKLGameMode : public AGameModeBase {
	GENERATED_BODY()

public:
	AKLGameMode();

	static const FName GroupPerksID;

	void InitGame(const FString& MapName, const FString& Options, FString& ErrorMessage) override;
	void InitGameState() override;
	void StartPlay() override;
	void Tick(float DeltaSeconds) override;

	bool IsPaused() const override;

	UFUNCTION(BlueprintCallable, Category = "!KL: Game")
	bool IsGameOver() const;

	UFUNCTION(BlueprintCallable, Category = "!KL: Game")
	void ReplaceHeroClass(int32 HeroID, TSubclassOf<APlayerCharacter> NewHeroClass);

	UFUNCTION(BlueprintCallable, Category = "!KL: Game")
	bool IsHeroesDefined() const;

	UFUNCTION(BlueprintCallable, Category = "ECS")
	UECSEngine* GetECSEngine() const;

	UPROPERTY(BlueprintAssignable, Category = "!KL: Instance")
	FHeroReplaced OnHeroReplaced;

	UFUNCTION(BlueprintCallable, Category = "!KL: Instance")
	float GetMobsScale() const;

	UFUNCTION(BlueprintCallable, Category = "!KL: Game")
	void ActivateLevel(FBox2D Bounds);

	UFUNCTION(BlueprintCallable, Category = "!KL: Game")
	void DeactivateCurrentLevel();

	UFUNCTION(BlueprintCallable, Category = "!KL: Perk")
	UPerkManager* GetPerkManagerByClass(TSubclassOf<APlayerCharacter> Type, int32 HeroID);

	UFUNCTION(BlueprintCallable, Category = "!KL: Perk")
	UPerkManager* GetPerkManager(FName PerksID);

	UFUNCTION(BlueprintCallable, Category = "!KL: Perk")
	UPerkManager* GetPerkManagerForGroup();

protected:
	bool IsLevelFound();

	UFUNCTION()
	void OnLevelLoaded();
	bool bLevelLoaded = false;

	UFUNCTION()
	void OnAssetsLoaded(float CurrentState);
	bool bAssetsLoaded = false;

	virtual void KLStartPlay();

	UPROPERTY(EditDefaultsOnly, Category = "!KL: Settings")
	TMap<int32, TSubclassOf<APlayerCharacter>> HeroesClasses;

	UPROPERTY(EditDefaultsOnly, Category = "!KL: Settings")
	TSubclassOf<AFormation> FormationClass;

	void UpdateMobsLevel();

	float MobsScaleFactor = 1.f;

	// Interval to recalculate scale factor
	UPROPERTY(EditDefaultsOnly, Category = "!KL: Settings")
	float MobsUpdateTime = 60.f; // 1 minute

	// Time to increase the scale factor (e.g. from 1.f to 2.f)
	UPROPERTY(EditDefaultsOnly, Category = "!KL: Settings")
	float MobsLevelUpTime = 60.f * 5.f; // 5 minutes by default

	float LastMobsUpdateTime = 0.f;

	UPROPERTY()
	UGameGlobal* Global;

	bool bIsPaused = false;

	void ResetLevel();
	void DestroyHeroes(bool bSkipDestroy = false);
	void SpawnHeroes();

	UPROPERTY()
	AFormation* Formation;

	UPROPERTY(EditDefaultsOnly, Category = "!KL: ECS")
	TSubclassOf<UECSEngine> ECSType;

	UPROPERTY()
	UECSEngine* ECSEngine = nullptr;

	bool bLevelFound = false;

	FName StartingLevel;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "!KL: Settings")
	TSubclassOf<UPerkManager> PerkManagerClass;

	UPROPERTY()
	TMap<FName, UPerkManager*> PerkManagers;

};
