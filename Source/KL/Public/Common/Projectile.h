// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"

#include "API/ECSEntity.h"

#include "Common/Enums.h"
#include "Skills/SkillLauncher.h"
#include "Projectile.generated.h"

class AGameCharacter;
class UECSMovementComponent;

UENUM()
enum class ProjectileDamageType : uint8 {
	Point,
	Radial
};

UCLASS()
class KL_API AProjectile : public AActor, public IECSEntity {
	GENERATED_BODY()

public: // AActor

	AProjectile();

	void Tick(float DeltaTime) override;
	void Destroyed() override;

protected: // AActor

	void BeginPlay() override;

public: // IECSEntity

	UECSEngine* GetEngineBP_Implementation() const override;
	int32 GetEntityBP_Implementation() const override;

protected: // ECS Entity data

	UPROPERTY()
	UECSEngine* ECS = nullptr;
	int32 ECSIndex = INDEX_NONE;

public: // Methods

	UFUNCTION(BlueprintCallable, Category = "!KL: Projectile")
	void LaunchProjectile(AGameCharacter* InShooter, EInstigatorType InInstigatorType, float InSpeed, float InRange, float InBaseAttack);

	/* >> BP HANDLERS */

	// Return true if the CHaracter is in debug mode
	UFUNCTION(BlueprintCallable, Category = "Debug")
	bool IsDebug() const;

	UFUNCTION(BlueprintCallable, Category = "!KL: Projectile")
	AGameCharacter* GetShooter() const;

	UFUNCTION(BlueprintCallable, Category = "!KL: Projectile")
	float GetAttack() const;

	UFUNCTION(BlueprintCallable, Category = "!KL: Projectile")
	void PerformBlast(AActor* TargetActor, const FVector& HitLocation);

	UFUNCTION(BlueprintCallable, Category = "!KL: Projectile")
	void BlastHandled();

	/* << BP HANDLERS */

	/* >> BP EVENTS */

	UFUNCTION(BlueprintImplementableEvent, Category = "!KL: Projectile")
	void NotifyLaunch();

	UFUNCTION(BlueprintImplementableEvent, Category = "!KL: Projectile")
	void NotifyBeforeHit(AActor* Target, const FVector& HitLocation);

	UFUNCTION(BlueprintImplementableEvent, Category = "!KL: Projectile")
	void NotifyHitTarget(AActor* Target, const FVector& HitLocation, float DamageMade);

	UFUNCTION(BlueprintImplementableEvent, Category = "!KL: Projectile")
	void NotifyBlast(bool bFinalBlast);

	UFUNCTION(BlueprintImplementableEvent, Category = "!KL: Projectile")
	void NotifyMissed();

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "!KL: Skill")
	EInstigatorType GetInstigatorType() const;
	/* << BP EVENTS */

protected: // Internal stuff

	void RegisterECS();

	UPROPERTY(BlueprintAssignable, Category = "!KL: Projectile")
	FSkillDelegate OnHitTarget;

	UPROPERTY(BlueprintAssignable, Category = "!KL: Projectile")
	FSkillDelegate OnMissed;

	// Cleanup after everything is completed
	void Complete();

	// For example, set to false after blast
	UPROPERTY(BlueprintReadWrite, Category = "!KL: Projectile")
	bool bCanMakeAttack = true;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	UECSMovementComponent* ECSMovementComponent = nullptr;

	UPROPERTY(EditDefaultsOnly, Category = "!KL: Projectile")
	ProjectileDamageType DamageType = ProjectileDamageType::Point;

	// Radius of the projectile's collision
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "!KL: Projectile")
	float Radius = 30.f;

	// Only for DamageType == Radial
	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = "!KL: Projectile", Meta = (ExposeOnSpawn = true))
	float FalloffInnerRadius = 100.f;

	// Only for DamageType == Radial
	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = "!KL: Projectile", Meta = (ExposeOnSpawn = true))
	float FalloffOuterRadius = 500.f;

	// Only for DamageType == Radial
	UPROPERTY(EditDefaultsOnly, Category = "!KL: Projectile")
	float FalloffLevel = 1.f;

	// If true – projectile will not be destroyed on blast
	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "!KL: Projectile")
	bool bCanPenetrate = false;

	UPROPERTY(BlueprintReadOnly, Category = "!KL: Projectile")
	float Attack = 0;

	UPROPERTY(BlueprintReadOnly, Category = "!KL: Projectile")
	float Speed = 0;

	// If == 0 will blast immediately on fire
	UPROPERTY(BlueprintReadOnly, Category = "!KL: Projectile")
	float Range = 0;

	UPROPERTY(EditDefaultsOnly, Category = "!KL: Projectile")
	bool bCanHitSelf = false;

	bool CheckCollision();
	bool Handled = true;

	UPROPERTY(EditAnywhere, Category = "Debug")
	bool bIsDebug = false;

	bool BlastActor(AActor* TargetActor, float& DamageMade);
	bool BlastAtLocation(const FVector& TargetLocation, float& DamageMade, TArray<AGameCharacter*>& OutDamagedActors);

	bool Launched = false;
	FVector LaunchPosition;

	bool bIsPlayerFire = true;

	UPROPERTY()
	AGameCharacter* Shooter = nullptr;

	int32 ShooterHeroID = INDEX_NONE;

	UPROPERTY()
	EInstigatorType InstigatorType = EInstigatorType::Default;

	FVector PreviousFrameLocation = FVector::ZeroVector;

	UPROPERTY()
	TArray<AActor*> AffectedTargets;
};
