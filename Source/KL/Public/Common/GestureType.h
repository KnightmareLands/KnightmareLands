// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GestureType.generated.h"

UENUM(BlueprintType)
enum class EGestureType: uint8 {
	None UMETA(Tooltip="No direction"),
	Up UMETA(Tooltip="To the up"),
	Right UMETA(Tooltip="To the right"),
	Down UMETA(Tooltip="To the down"),
	Left UMETA(Tooltip="To the left"),
	Accept UMETA(Tooltip="Accept"),
	Cancel UMETA(Tooltip="Cancel")
};
