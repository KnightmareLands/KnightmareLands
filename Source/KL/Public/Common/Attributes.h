// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Attributes.generated.h"

UENUM(BlueprintType)
enum class EKLModifierType: uint8 {
	None UMETA(Tooltip="Empty modifier, do nothing"),
	Percent UMETA(Tooltip="Multiply the value"),
	Value UMETA(Tooltip="Add to the value")
};

UENUM(BlueprintType)
enum class EKLModifierPass: uint8 {
	Base UMETA(Tooltip="Applied to the base value"),
	Final UMETA(Tooltip="Applied to the final value (after base pass)"),
};

USTRUCT(BlueprintType)
struct FKLAttributeModifier {
	GENERATED_BODY()

	UPROPERTY(BlueprintReadWrite, Category = "!KL: Attributes")
	EKLModifierType Type = EKLModifierType::None;

	UPROPERTY(BlueprintReadWrite, Category = "!KL: Attributes")
	float Value = 0;

	UPROPERTY(BlueprintReadWrite, Category = "!KL: Attributes")
	EKLModifierPass Pass = EKLModifierPass::Base;

	FKLAttributeModifier() {}

	FKLAttributeModifier(EKLModifierType InType, float InValue, EKLModifierPass InPass = EKLModifierPass::Base) :
		Type(InType),
		Value(InValue),
		Pass(InPass)
	{}
};

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FKLAttributeChanged, float, CurrentValue);

UCLASS(BlueprintType)
class KL_API UKLAttribute : public UObject {
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintAssignable, Category = "!KL: Attributes")
	FKLAttributeChanged OnChanged;

	UKLAttribute() {
		Modifiers = TMap<int32, const FKLAttributeModifier>();
	}

	UFUNCTION(BlueprintCallable, Category = "!KL: Attributes")
	void Set(float InValue) {
		OriginalValue = InValue;
		Update();
	}

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "!KL: Attributes")
	float Get() const {
		return CurrentValue;
	}

	// Add value to the attribute (not affecting the base value)
	UFUNCTION(BlueprintCallable, Category = "!KL: Attributes")
	int32 Add(EKLModifierType Type, float Value, EKLModifierPass Pass = EKLModifierPass::Base) {
		Index++;

		Modifiers.Add(Index, FKLAttributeModifier(Type, Value, Pass));
		Update();

		return Index;
	}

	UFUNCTION(BlueprintCallable, Category = "!KL: Attributes")
	void Remove(int32 ModifierIndex) {
		if (!Modifiers.Contains(ModifierIndex)) {
			return;
		}

		Modifiers.Remove(ModifierIndex);
		if (ModifierIndex == Index) {
			Index--;
		}

		Update();
	}

	UFUNCTION(BlueprintCallable, Category = "!KL: Attributes")
	void Reset() {
		Modifiers.Empty();
		Index = -1;

		Update();
	}

	// Used only for Hero replacement from the KLGameMode
	// to copy all the modifiers from old hero
	TMap<int32, const FKLAttributeModifier> GetAllModifiers() const {
		return Modifiers;
	}

	// Used only for Hero replacement from the KLGameMode
	// to copy all the modifiers from old hero
	void ReplaceModifiers(TMap<int32, const FKLAttributeModifier> NewModifiers) {
		Modifiers = NewModifiers;
		Index = -1;

		for (auto Pair : Modifiers) {
			if (Pair.Key > Index) {
				Index = Pair.Key;
			}
		}

		Update();
	}

protected:
	float OriginalValue = 0.f;
	float CurrentValue = 0;

	int32 Index = -1;
	TMap<int32, const FKLAttributeModifier> Modifiers;

	void ApplyModifiers() {
		float AddedBase = 0.f;
		float PercentFinal = 0.f;
		float AddedFinal = 0.f;

		for (auto Pair : Modifiers) {
			auto Modifier = Pair.Value;

			switch (Modifier.Type) {
			case EKLModifierType::Value:
				if (Modifier.Pass == EKLModifierPass::Base) {
					AddedBase += Modifier.Value;
				} else {
					AddedFinal += Modifier.Value;
				}
				break;
			case EKLModifierType::Percent:
				if (Modifier.Pass == EKLModifierPass::Base) {
					AddedBase += ((Modifier.Value / 100) * OriginalValue);
				} else {
					PercentFinal += Modifier.Value;
				}
				break;
			}
		}

		float NewValue = FMath::Max(0.f, OriginalValue + AddedBase);
		AddedFinal += ((PercentFinal / 100) * NewValue);

		// Can't be below zero for now
		CurrentValue = FMath::Max(0.f, NewValue + AddedFinal);
	}

	FORCEINLINE void Update() {
		ApplyModifiers();
		OnChanged.Broadcast(CurrentValue);
	}
};
