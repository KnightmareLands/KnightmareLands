// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/SaveGame.h"
#include "KLSavesList.generated.h"

USTRUCT(BlueprintType)
struct FKLSaveSlot {
	GENERATED_BODY()

	UPROPERTY(BlueprintReadWrite, Category = "!KL: Save")
	FString SlotName;

	UPROPERTY(BlueprintReadWrite, Category = "!KL: Save")
	FString SaveName;
};

/**
 *
 */
UCLASS()
class KL_API UKLSavesList : public USaveGame {
	GENERATED_BODY()

public:
	UKLSavesList();

	UPROPERTY(VisibleAnywhere, Category = "!KL: Save")
	FString SaveSlotName;

	UPROPERTY(VisibleAnywhere, Category = "!KL: Save")
	TArray<FKLSaveSlot> Slots;
};
