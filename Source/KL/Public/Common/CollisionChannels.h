// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

/* Custom collision channels defined in Config/DefaultEngine.ini */

// TODO: Why do I need a custom landscape channel?
#define ECC_KL_Landscape ECC_GameTraceChannel2

// Custom environment channel to separate from the default WorldStatic to avoid SWVolume spawn collisions
#define ECC_KL_Environment ECollisionChannel::ECC_GameTraceChannel7

// TODO: why so much cursor/controls channels??
#define ECC_KL_TraceControls ECC_GameTraceChannel1
#define ECC_KL_CursorHit ECC_GameTraceChannel3
#define ECC_KL_CursorHitPlane ECC_GameTraceChannel6

#define ECC_KL_PlayerCharacter ECC_GameTraceChannel4
#define ECC_KL_MobCharacter ECC_GameTraceChannel5
#define ECC_KL_Objects ECC_GameTraceChannel8

#define ECC_KL_Projectile ECC_GameTraceChannel11
