// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "KLLoader.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FKLLoadingState, float, CurrentState);

class UNiagaraSystem;

/**
 * Assets loader
 */
UCLASS()
class KL_API UKLLoader : public UObject, public FTickableGameObject {
	GENERATED_BODY()

public:
	static UKLLoader* LoaderInstance;

	UFUNCTION(BlueprintCallable, Category = "!KL: Loader")
	static UKLLoader* GetLoader();

	void Load();
	float GetStatus() const;

	UFUNCTION(BlueprintCallable, Category = "!KL: Loader")
	static UClass* GetSkillEffect(const TSoftClassPtr<ASkillEffect>& SoftPtr);

	FKLLoadingState OnUpdate;
	FKLLoadingState OnCompleted;

	//~ Begin FTickableObject Interface
	virtual void Tick(float DeltaTime) override;
	virtual bool IsTickable() const override { return true; }
	virtual TStatId GetStatId() const override { RETURN_QUICK_DECLARE_CYCLE_STAT(UKLLoader, STATGROUP_Tickables); }
	//~ End FTickableObject Interface

protected:
	int32 Count = 0;
	int32 CurrentState = INDEX_NONE;

	int32 Loaded = 0;

	bool bIsLoading = false;
	bool bIsLoaded = false;

	TArray<FAssetData> Assets;

	void LoadAsset(FAssetData& Asset);
	void OnSystemLoaded(UNiagaraSystem* NS);

	int64 StartTime = 0;

	TMap<FString, UClass*> LoadedAssets;
};
