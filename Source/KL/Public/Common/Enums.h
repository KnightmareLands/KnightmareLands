// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Enums.generated.h"

class AGameCharacter;
class APlayerCharacter;
class USkillLauncher;

UENUM(BlueprintType)
enum class ETargetingType: uint8 {
	NoTarget UMETA(Tooltip="No target"),
	TargetLocation UMETA(Tooltip="Target to a location"),
	TargetActor UMETA(Tooltip="Target to a character"),
	TargetCustom UMETA(Tooltip="Custom targeting"),
	TargetSelf UMETA(Tooltip="Target to self")
};

UENUM(BlueprintType)
enum class EInstigatorType: uint8 {
	Default UMETA(Tooltip="No specific instigator"),
	Player UMETA(Tooltip="Is triggered by player"),
	Enemy UMETA(Tooltip="Is triggered by an enemy")
};

USTRUCT(BlueprintType)
struct FTargetingInfo {
	GENERATED_BODY()

	UPROPERTY(BlueprintReadWrite, Category = "!KL: Character")
	ETargetingType Type;

	UPROPERTY(BlueprintReadWrite, Category = "!KL: Character")
	FVector Location;

	UPROPERTY(BlueprintReadWrite, Category = "!KL: Character")
	TArray<AGameCharacter*> Actors;

	UPROPERTY(BlueprintReadWrite, Category = "!KL: Character")
	bool bPositive;

	FTargetingInfo() :
		Type(ETargetingType::NoTarget),
		Location(FVector::ZeroVector),
		Actors(TArray<AGameCharacter*>()),
		bPositive(false)
	{}

	FTargetingInfo(FVector InTargetLocation) :
		Type(ETargetingType::TargetLocation),
		Location(InTargetLocation),
		Actors(TArray<AGameCharacter*>()),
		bPositive(false)
	{}

	FTargetingInfo(TArray<AGameCharacter*> InTargetActors) :
		Type(ETargetingType::TargetActor),
		Location(FVector::ZeroVector),
		Actors(InTargetActors),
		bPositive(false)
	{}

	FTargetingInfo(FVector InTargetLocation, TArray<AGameCharacter*> InTargetActors) :
		Type(ETargetingType::TargetCustom),
		Location(InTargetLocation),
		Actors(InTargetActors),
		bPositive(false)
	{}

	FTargetingInfo(ETargetingType InType, FVector InTargetLocation, TArray<AGameCharacter*> InTargetActors) :
		Type(InType),
		Location(InTargetLocation),
		Actors(InTargetActors),
		bPositive(false)
	{}
};

UENUM(BlueprintType)
enum class ESkillStage: uint8 {
	NotInited UMETA(Tooltip="Skill is not inited"),
	First UMETA(Tooltip="First stage"),
	Power UMETA(Tooltip="Power is leveled"),
	Efficiency UMETA(Tooltip="Efficiency is leveled"),
	ThirdPower UMETA(Tooltip="Power + Efficiency"),
	ThirdEfficiency UMETA(Tooltip="Efficiency + Power")
};

UENUM(BlueprintType)
enum class EProgressType: uint8 {
	Init UMETA(Tooltip="InitSkill"),
	Power UMETA(Tooltip="Power"),
	Efficiency UMETA(Tooltip="Efficiency"),
	Power2 UMETA(Tooltip="Power-2"),
	Efficiency2 UMETA(Tooltip="Efficiency-2"),
	Power3 UMETA(Tooltip="Power-3"),
	Efficiency3 UMETA(Tooltip="Efficiency-3"),
};

USTRUCT(BlueprintType)
struct FSkillProgress {
	GENERATED_BODY()

	UPROPERTY(BlueprintReadWrite, Category = "!KL: Skill")
	int32 Limit;

	UPROPERTY(BlueprintReadWrite, Category = "!KL: Skill")
	int32 Power;

	UPROPERTY(BlueprintReadWrite, Category = "!KL: Skill")
	int32 Efficiency;

	UPROPERTY(BlueprintReadWrite, Category = "!KL: Skill")
	ESkillStage Stage;

	FSkillProgress() :
		Limit(5),
		Power(0),
		Efficiency(0),
		Stage(ESkillStage::NotInited)
	{}

	void InitSkill() {
		if (Stage == ESkillStage::NotInited) {
			Update(EProgressType::Init);
		}
	}

	void IncreasePower() {
		if (Power < Limit) {
			Update(EProgressType::Power);
		}

	}

	void IncreaseEfficiency() {
		if (Efficiency < Limit) {
			Update(EProgressType::Efficiency);
		}
	}

	ESkillStage CalculateNewStage(EProgressType ProgressType, bool bNeedUpdate = false) {
		int32 NewPower = Power;
		int32 NewEfficiency = Efficiency;
		ESkillStage NewStage = Stage;

		// Increase param only if skill is already inited;
		if (Stage != ESkillStage::NotInited) {
			switch (ProgressType) {
			case EProgressType::Power:
			case EProgressType::Power2:
			case EProgressType::Efficiency3:
				NewPower = Power + 1;
				break;
			case EProgressType::Efficiency:
			case EProgressType::Efficiency2:
			case EProgressType::Power3:
				NewEfficiency = Efficiency + 1;
				break;
			default:
				break;
			}
		}

		switch (Stage) {
		case ESkillStage::NotInited:
			NewStage = ESkillStage::First;
			break;
		case ESkillStage::First:
			if (NewPower == Limit) {
				NewStage = ESkillStage::Power;
			} else if (NewEfficiency == Limit) {
				NewStage = ESkillStage::Efficiency;
			}
			break;
		case ESkillStage::Power:
			if (NewEfficiency == Limit) {
				NewStage = ESkillStage::ThirdPower;
			}
			break;
		case ESkillStage::Efficiency:
			if (NewPower == Limit) {
				NewStage = ESkillStage::ThirdEfficiency;
			}
			break;
		default:
			break;
		}

		if (bNeedUpdate) {
			Power = NewPower;
			Efficiency = NewEfficiency;
			Stage = NewStage;
		}

		return NewStage;
	}

private:
	void Update(EProgressType ProgressType) {
		CalculateNewStage(ProgressType, true);
	}
};

USTRUCT(BlueprintType)
struct FSkillUpgrade {
	GENERATED_BODY()

	UPROPERTY(BlueprintReadWrite, Category = "!KL: Skill")
	APlayerCharacter* HeroRef;

	UPROPERTY(BlueprintReadWrite, Category = "!KL: Skill")
	USkillLauncher* SkillRef;

	UPROPERTY(BlueprintReadWrite, Category = "!KL: Skill")
	EProgressType ProgressType;

	FSkillUpgrade() :
		HeroRef(nullptr),
		SkillRef(nullptr),
		ProgressType(EProgressType::Init)
	{}

	FSkillUpgrade(USkillLauncher* SkillLauncher) :
		HeroRef(nullptr),
		SkillRef(SkillLauncher),
		ProgressType(EProgressType::Init)
	{}

	FSkillUpgrade(APlayerCharacter* Hero) :
		HeroRef(Hero),
		SkillRef(nullptr),
		ProgressType(EProgressType::Power)
	{}

	FSkillUpgrade(USkillLauncher* SkillLauncher, EProgressType Type) :
		HeroRef(nullptr),
		SkillRef(SkillLauncher),
		ProgressType(Type)
	{}

	FSkillUpgrade(APlayerCharacter* Hero, EProgressType Type) :
		HeroRef(Hero),
		SkillRef(nullptr),
		ProgressType(Type)
	{}
};

UENUM(BlueprintType)
enum class EKLGameState: uint8 {
	MainMenu UMETA(Tooltip="Main menu mode, game is not started"),
	InGame UMETA(Tooltip="Gameplay mode"),
	GameOver UMETA(Tooltip="Game over mode"),
	Demo UMETA(Tooltip="Demo mode")
};
