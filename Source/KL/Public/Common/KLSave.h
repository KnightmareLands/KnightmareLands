// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/SaveGame.h"
#include "KLSave.generated.h"

/**
 *
 */
UCLASS()
class KL_API UKLSave : public USaveGame {
	GENERATED_BODY()

public:
	UPROPERTY(VisibleAnywhere, Category = "!KL: Save")
	FString SaveName;
};
