// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TimeEnums.generated.h"

UENUM(BlueprintType)
enum class EKLTimeMode: uint8 {
	Normal UMETA(Tooltip="Time scale == 1"),
	Slow UMETA(Tooltip="Time is slowed down")
};

UENUM(BlueprintType)
enum class EKLDilationState: uint8 {
	Game UMETA(Tooltip="Dilation is for the gameplay"),
	Pause UMETA(Tooltip="Dilation is close to zero, game is considered as paused")
};
