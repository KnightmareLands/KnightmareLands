// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "HealthComponent.generated.h"

class UWidgetComponent;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FKLHealthEvent, float, HealthChange, AActor*, Causer, bool, bLethalDamage);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class KL_API UHealthComponent : public UActorComponent {
	GENERATED_BODY()

public:
	// Sets default values for this component's properties
	UHealthComponent();

	void SetInitialHealth(float Health);
	void UpdateInitialHealth(float Health);

	UFUNCTION(BlueprintCallable, Category = "!KL: Health")
	float GetMaxHealth() const;

	UFUNCTION(BlueprintCallable, Category = "!KL: Health")
	float GetCurrentHealth() const;

	void SetCurrentHealth(float InHealth);

	void DeathCheck();

	UFUNCTION(BlueprintCallable, Category = "!KL: Health")
	float GetHealthPercent() const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "!KL: Health")
	bool IsAlive() const;

	UFUNCTION(BlueprintCallable, Category = "!KL: Health")
	float TakeDamage(float Amount, AActor* Causer);

	UFUNCTION(BlueprintCallable, Category = "!KL: Health")
	float TakeHeal(float Amount, AActor* Causer);

	UFUNCTION(BlueprintCallable, Category = "!KL: Health")
	void Revive(float Health, AActor* Causer);

	UPROPERTY(BlueprintAssignable, Category = "!KL: Health")
	FKLHealthEvent OnUpdate;

	UPROPERTY(BlueprintAssignable, Category = "!KL: Health")
	FKLHealthEvent OnHealed;

	UPROPERTY(BlueprintAssignable, Category = "!KL: Health")
	FKLHealthEvent OnDamaged;

	UPROPERTY(BlueprintAssignable, Category = "!KL: Health")
	FKLHealthEvent OnDead;

	UPROPERTY(BlueprintAssignable, Category = "!KL: Health")
	FKLHealthEvent OnRevive;

protected:
	// Called when the game starts
	void BeginPlay() override;

	UPROPERTY(BlueprintReadOnly, Category = "!KL: Health")
	bool bIsAlive = false;

	bool Inited = false;
	float InitialHealth = 0;
	float CurrentHealth = 0;
};
