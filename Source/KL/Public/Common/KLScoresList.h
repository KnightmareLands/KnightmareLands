// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/SaveGame.h"
#include "KLScoresList.generated.h"

USTRUCT(BlueprintType)
struct FKLScoresItem {
	GENERATED_BODY()

	UPROPERTY(BlueprintReadWrite, Category = "!KL: Scores")
	FName PlayerName;

	UPROPERTY(BlueprintReadWrite, Category = "!KL: Scores")
	int32 Scores;

	UPROPERTY(BlueprintReadWrite, Category = "!KL: Scores")
	bool bWinner;

	UPROPERTY(BlueprintReadWrite, Category = "!KL: Scores")
	int32 PointsSpent;

	UPROPERTY(BlueprintReadWrite, Category = "!KL: Scores")
	int32 Level;

	UPROPERTY(BlueprintReadWrite, Category = "!KL: Scores")
	float Duration;

	UPROPERTY(BlueprintReadWrite, Category = "!KL: Scores")
	bool bRecent;

	UPROPERTY(BlueprintReadWrite, Category = "!KL: Scores")
	bool bSaved;

	FKLScoresItem() :
		PlayerName(FName("")),
		Scores(0),
		bWinner(false),
		PointsSpent(0),
		Level(0),
		Duration(0.f),
		bRecent(false),
		bSaved(false)
	{}
};

/**
 *
 */
UCLASS()
class KL_API UKLScoresList : public USaveGame {
	GENERATED_BODY()

public:
	UKLScoresList();

	UPROPERTY(VisibleAnywhere, Category = "!KL: Scores")
	FString SaveSlotName;

	UPROPERTY(VisibleAnywhere, Category = "!KL: Scores")
	TArray<FKLScoresItem> Items;
};
