// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "Perks/PerkTypes.h"
#include "Perks/Perk.h"
#include "PerkSkill.generated.h"

class UPerkData;
class AGameCharacter;
class USkillData;

/**
 * Perk Object, handles perk effect
 */
UCLASS(Blueprintable, BlueprintType)
class KL_API UPerkSkill : public UPerk {
	GENERATED_BODY()

public:
	void Init(const UPerkData* InPerkData, int32 Grade = 1, AGameCharacter* InHost = nullptr) override;

	UFUNCTION(BlueprintCallable, Category = "!KL: Perk")
	bool SetSkill();

	UFUNCTION(BlueprintCallable, Category = "!KL: Perk")
	bool RemoveSkill();

	void Apply_Implementation(AGameCharacter* InTarget) override;
	void OnRemove_Implementation() override;

protected:
	FORCEINLINE USkillData* GetSkillData() const;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "!KL: Perk")
	int32 Index = INDEX_NONE;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "!KL: Perk")
	TArray<USkillData*> SkillDataGrades;

	bool bIsSet = false;
};
