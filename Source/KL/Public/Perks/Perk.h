// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "Perks/PerkTypes.h"
#include "Perk.generated.h"

class UPerkData;
class AGameCharacter;
class APlayerCharacter;
class UKLStateSubsystem;

/**
 * Perk Object, handles perk effect
 */
UCLASS(Blueprintable, BlueprintType)
class KL_API UPerk : public UObject {
	GENERATED_BODY()

public:
	UWorld* GetWorld() const override {
		if (HasAllFlags(RF_ClassDefaultObject)) {
			// If we are a CDO, we must return nullptr instead of calling Outer->GetWorld() to fool UObject::ImplementsGetWorld.
			return nullptr;
		}

		return GetOuter()->GetWorld();
	}

	// Initial setup for the perk, should not be called more than once
	virtual void Init(const UPerkData* InPerkData, int32 Grade = 1, AGameCharacter* InHost = nullptr);
	void UpdateHost(AGameCharacter* InHost);

	// Trigger the perk's effect
	// Target can be non-host character or nullptr: either a one who attacked the host, or host's target
	// Depending on the perk's trigger type
	UFUNCTION(BlueprintNativeEvent, Category = "!KL: Perk")
	void Apply(AGameCharacter* InTarget = nullptr);

	// Event triggered when perk is removed
	// Should be used in case if needed to reset some stats or whatever
	UFUNCTION(BlueprintNativeEvent, Category = "!KL: Perk")
	void OnRemove();

	// Increase perk's grade
	void Upgrade();

	UFUNCTION(BlueprintNativeEvent, Category = "!KL: Perk")
	void OnPreUpgrade();

	UFUNCTION(BlueprintNativeEvent, Category = "!KL: Perk")
	void OnPostUpgrade();

	// Method to use on CDO to check if the perk can be applied before making the object
	UFUNCTION(BlueprintNativeEvent, Category = "!KL: Perk")
	bool CanBeApplied(const UKLStateSubsystem* State, const UPerkData* InPerkData, const AGameCharacter* InHost) const;

	FORCEINLINE EPerkTrigger GetTriggerType() const { return TriggerType; };
	FORCEINLINE int32 GetGrade() const { return Grade; };
	FORCEINLINE const UPerkData* GetData() const { return PerkData; };

	FName GetTitle() const;

protected:

	// Can be null for Instant or Group perks
	UPROPERTY(BlueprintReadOnly, Category = "!KL: Perk")
	AGameCharacter* Host = nullptr;

	// Pointer to the perk's data
	UPROPERTY(BlueprintReadOnly, Category = "!KL: Perk")
	const UPerkData* PerkData;

	UPROPERTY(BlueprintReadOnly, Category = "!KL: Perk")
	EPerkTrigger TriggerType;

	// Current perk's grade
	UPROPERTY(BlueprintReadOnly, Category = "!KL: Perk")
	int32 Grade = 0;
};
