// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "Perks/PerkTypes.h"
#include "PerkData.generated.h"

class AGameCharacter;
class UPerk;

UCLASS(BlueprintType)
class KL_API UPerkData : public UDataAsset {
	GENERATED_BODY()

public:
	// Perk can be used for the game
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="!KL: Perk")
	bool bActive = false;

	/** Perk title */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="!KL: Perk")
	TArray<FName> Title;

	/** Perk description */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="!KL: Perk")
	TArray<FName> Description;

	/** Icon of the perk */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="!KL: Perk")
	TArray<UTexture2D*> Icon;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="!KL: Perk")
	int32 MaxGrade = 3;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "!KL: Perk")
	EPerkType Type = EPerkType::None;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "!KL: Perk")
	EPerkTrigger Trigger = EPerkTrigger::Instant;

	// Perk should be saved in the perk manager
	// Applies only to Trigger == Instant
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="!KL: Perk")
	bool bSaved = true;

	/** Perk effect reference */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, AssetRegistrySearchable, Category="!KL: Perk")
	TSoftClassPtr<UPerk> Perk;

	/** List of character classes who are allowed to have this perk */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, AssetRegistrySearchable, Category="!KL: Perk")
	TArray<TSoftClassPtr<AGameCharacter>> AllowedHosts;

	UClass* GetPerk() const;
};
