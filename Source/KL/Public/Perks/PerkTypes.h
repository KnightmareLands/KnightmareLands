// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "PerkTypes.generated.h"

class UPerkData;

UENUM(BlueprintType)
enum class EPerkType: uint8 {
	None UMETA(Tooltip="Do nothing"),
	Instant UMETA(Tooltip="Instant action, no saving"),
	Group UMETA(Tooltip="Saved to the group"),
	Character UMETA(Tooltip="Saved to the character")
};

UENUM(BlueprintType)
enum class EPerkTrigger: uint8 {
	Instant UMETA(Tooltip="Applied immediately, one-time action"),
	Tick UMETA(Tooltip="Per-tick action"),
	Attack UMETA(Tooltip="Applied when the host characters is attacking"),
	Damage UMETA(Tooltip="Applied when the host character is damaged"),
	Death UMETA(Tooltip="Applied when the host character dies"),
	Flag UMETA(Tooltip="Just a flag for check from other places; don't do anything by itself"),
};

USTRUCT(BlueprintType)
struct FPerkInfo {
	GENERATED_BODY()

	UPROPERTY(BlueprintReadOnly, Category = "!KL: Perk")
	FName Title;

	UPROPERTY(BlueprintReadOnly, Category = "!KL: Perk")
	int32 Grade;

	UPROPERTY(BlueprintReadOnly, Category = "!KL: Perk")
	const UPerkData* Data;
};
