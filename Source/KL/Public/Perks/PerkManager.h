// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "Perks/PerkTypes.h"
#include "PerkManager.generated.h"

class UPerkData;
class UPerk;
class AGameCharacter;
class UKLTimeFactorSubsystem;

UCLASS(Blueprintable, ClassGroup=(Custom) )
class KL_API UPerkManager : public UObject, public FTickableGameObject {
	GENERATED_BODY()

public:
	// Sets default values for this component's properties
	UPerkManager();

	//~ Begin FTickableObject Interface
	void Tick(float DeltaTime) override;
	bool IsTickable() const override { return bShouldTick; }
	TStatId GetStatId() const override { RETURN_QUICK_DECLARE_CYCLE_STAT(UPerkManager, STATGROUP_Tickables); }
	//~ End FTickableObject Interface

	void Init(AGameCharacter* InHost);

	UFUNCTION(BlueprintCallable, Category = "!KL: Perk", meta=(WorldContext="WorldContextObject"))
	static bool RegisterInstant(UObject* WorldContextObject, const UPerkData* PerkData);

	UFUNCTION(BlueprintCallable, Category = "!KL: Perk")
	static bool IsAllowedForHost(const UPerkData* PerkData, const AGameCharacter* Host);

	// Add a new perk to the Host character
	UFUNCTION(BlueprintCallable, Category = "!KL: Perk")
	bool Register(const UPerkData* PerkData, int32 Grade = 1);

	// Add a new perk to the Host character
	UFUNCTION(BlueprintCallable, Category = "!KL: Perk")
	bool Remove(const UPerkData* PerkData);

	// Remove all perks from the character
	void Clear();

	UFUNCTION(BlueprintCallable, Category = "!KL: Perk")
	int32 GetPerkGrade(const UPerkData* PerkData) const;

	UFUNCTION(BlueprintCallable, Category = "!KL: Perk")
	TArray<FPerkInfo> GetPerksList() const;

	// Turn PerkManager off (when the Hero is replaced)
	UFUNCTION()
	void Deactivate();

protected:
	FName GetPerkID(const UPerkData* PerkData) const;

	void ApplyPerks(EPerkTrigger TriggerType, AGameCharacter* TriggerCharacter = nullptr);

	// Turn PerkManager on (on Init)
	void Activate();

	UFUNCTION()
	void OnDamage(AGameCharacter* InAttacker);

	UFUNCTION()
	void OnAttack(AGameCharacter* InTarget);

	UFUNCTION()
	void OnDeath();

	void RemoveGroupSkill();

	UPROPERTY()
	TMap<FName, UPerk*> Perks;

	// If Host == nullptr it means the manager is for the whole group
	UPROPERTY(BlueprintReadOnly, Category = "!KL: Perk")
	AGameCharacter* Host = nullptr;

	// True when have tickable perks
	bool bShouldTick = false;

	bool bActive = false;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "!KL: Perk")
	float TickInterval = 0.1f;

	float Timeout = 0.f;

	UPROPERTY()
	UKLTimeFactorSubsystem* TimeFactor;

};
