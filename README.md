# Knightmare Lands

A shoot-em-up single-player game in a high-fantasy theme with RPG elements and (probably) with group mechanics.

## Contribution

Anyone interested – [welcome to our Discord channel](https://discord.gg/JBejs2E)!

### Requirements

- [Unreal Engine 4](https://www.unrealengine.com) (currently used: custom fork from 4.25.1)
  - Sources: https://github.com/norlin/UnrealEngine/tree/4.25.1-release-toon
    - Add plugins to the engine:
      - Substance (no direct link for 4.25): https://www.unrealengine.com/marketplace/en-US/product/substance-plugin
      - FMOD: https://www.fmod.com/download#integrations // for now only 4.24 version available, require small patching to make it work with 4.25
  - Binary build: [not ready]
- Any С++ editor/IDE
  - Windows: e.g., [VS Code](https://code.visualstudio.com/) (it's free)
  - macOS: [XCode](https://developer.apple.com/support/xcode/)
  - Linux: not sure which one is better, up to you! (and please share with us about your decision)
- Any [git](https://git-scm.com/downloads) client
- [git-lfs](https://git-lfs.github.com/)
- A [GitLab](https://gitlab.com) account

### Getting started

- In Git Shell:

```
git clone git@gitlab.com:KnightmareLands/KnightmareLands.git
cd KnightmareLands
git submodule init && git submodule update
```
- Open the KL.uproject
- Try to compile/run it to check if everything is working
- Choose a task to work on (from the [issues](https://gitlab.com/KnightmareLands/KnightmareLands/issues) or from the [RoadMap](https://www.mindmeister.com/908612764/shoot-em-up))
- Make a new branch for the task
- Do stuff! :-)
- Commit your changes and push the branch
- Add a new [Merge request](https://gitlab.com/KnightmareLands/KnightmareLands/merge_requests/new) with brief description about your changes
- Basically, that's it!
