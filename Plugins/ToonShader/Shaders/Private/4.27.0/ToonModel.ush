// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "/Engine/Private/DeferredShadingCommon.ush"
#include "/Engine/Private/BRDF.ush"
#include "/Engine/Private/FastMath.ush"
#include "/Engine/Private/CapsuleLight.ush"
#include "/Engine/Private/RectLight.ush"

FDirectLighting ToonShadowBxDF(FGBufferData GBuffer, half3 N, half3 V, half3 L, float Falloff, float NoL, FAreaLight AreaLight, FShadowTerms Shadow) {
	BxDFContext Context;
	Init(Context, N, V, L);
	SphereMaxNoH(Context, AreaLight.SphereSinAlpha, true);
	Context.NoV = saturate(abs(Context.NoV) + 1e-5);

	const float Intensity = Shadow.SurfaceShadow * NoL;
	const float Threshold = GBuffer.CustomData.x;
	const float ShadowIntensity = GBuffer.CustomData.y;

	const float ToonStep = max(1.2f*step(1.f, Intensity), max(ShadowIntensity, step(Threshold, Intensity)));

	FDirectLighting Lighting;
	Lighting.Diffuse = AreaLight.FalloffColor * ToonStep * Diffuse_Lambert(GBuffer.DiffuseColor);

	if (AreaLight.bIsRect) {
		Lighting.Specular = RectGGXApproxLTC(GBuffer.Roughness, GBuffer.SpecularColor, N, V, AreaLight.Rect, AreaLight.Texture);
	} else {
		Lighting.Specular = AreaLight.FalloffColor * ToonStep * SpecularGGX(GBuffer.Roughness, GBuffer.SpecularColor, Context, NoL, AreaLight);
	}

	Lighting.Specular = 0;
	Lighting.Transmission = 0;

	return Lighting;
}
