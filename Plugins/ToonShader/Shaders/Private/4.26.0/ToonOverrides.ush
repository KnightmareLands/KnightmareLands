
#pragma once

#ifndef SUPPORT_CONTACT_SHADOWS
#define SUPPORT_CONTACT_SHADOWS 1
#endif

#include "/Engine/Private/DeferredShadingCommon.ush"
#include "/Engine/Private/DeferredLightingCommon.ush"
#include "/Engine/Private/BRDF.ush"
#include "/Engine/Private/FastMath.ush"
#include "/Engine/Private/CapsuleLight.ush"
#include "/Engine/Private/RectLight.ush"
#include "ToonModel.ush"

// #if TOON_SHADER_ACTIVE
#include "CapsuleLightIntegrateToon.ush"
#include "RectLightIntegrateToon.ush"
// #endif

/** Calculates lighting for a given position, normal, etc with a fully featured lighting model designed for quality. */
FDeferredLightingSplit GetDynamicLightingSplitToon(
	float3 WorldPosition, float3 CameraVector, FGBufferData GBuffer, float AmbientOcclusion, uint ShadingModelID, 
	FDeferredLightData LightData, float4 LightAttenuation, float Dither, uint2 SVPos, FRectTexture SourceTexture,
	inout float SurfaceShadow)
{
	FLightAccumulator LightAccumulator = (FLightAccumulator)0;

	const bool bUseToonShader = (GBuffer.ShadingModelID == SHADINGMODELID_DEFAULT_LIT) && GBuffer.CustomData.x > 0;

	float3 V = -CameraVector;
	float3 N = GBuffer.WorldNormal;
	BRANCH if( GBuffer.ShadingModelID == SHADINGMODELID_CLEAR_COAT && CLEAR_COAT_BOTTOM_NORMAL)
	{
		const float2 oct1 = ((float2(GBuffer.CustomData.a, GBuffer.CustomData.z) * 2) - (256.0/255.0)) + UnitVectorToOctahedron(GBuffer.WorldNormal);
		N = OctahedronToUnitVector(oct1);			
	}
	
	float3 L = LightData.Direction;	// Already normalized
	float3 ToLight = L;
	
	float LightMask = 1;
	if (LightData.bRadialLight)
	{
		LightMask = GetLocalLightAttenuation( WorldPosition, LightData, ToLight, L );
	}

	LightAccumulator.EstimatedCost += 0.3f;		// running the PixelShader at all has a cost

	BRANCH
	if( LightMask > 0 )
	{
		FShadowTerms Shadow;
		Shadow.SurfaceShadow = AmbientOcclusion;
		Shadow.TransmissionShadow = 1;
		Shadow.TransmissionThickness = 1;
		Shadow.HairTransmittance.OpaqueVisibility = 1;
		GetShadowTerms(GBuffer, LightData, WorldPosition, L, LightAttenuation, Dither, Shadow);
		SurfaceShadow = Shadow.SurfaceShadow;

		LightAccumulator.EstimatedCost += 0.3f;		// add the cost of getting the shadow terms

		BRANCH
		if( Shadow.SurfaceShadow + Shadow.TransmissionShadow > 0 )
		{
			const bool bNeedsSeparateSubsurfaceLightAccumulation = UseSubsurfaceProfile(GBuffer.ShadingModelID);
			float3 LightColor = LightData.Color;

			const float Threshold = GBuffer.CustomData.x;
			const float ShadowIntensity = GBuffer.CustomData.y;

		#if NON_DIRECTIONAL_DIRECT_LIGHTING
			float Lighting;

			if( LightData.bRectLight )
			{
				FRect Rect = GetRect( ToLight, LightData );

				Lighting = IntegrateLight( Rect, SourceTexture);
			}
			else
			{
				FCapsuleLight Capsule = GetCapsule( ToLight, LightData );

				Lighting = IntegrateLight( Capsule, LightData.bInverseSquared );
			}

			float Attenuation;

			BRANCH
			if (bUseToonShader) {
				const float NoL = saturate( dot(N, L) );
				const float Intensity = Shadow.SurfaceShadow * NoL;
				Attenuation = max(1.2f * step(1.f, Intensity), max(ShadowIntensity, step(Threshold, Intensity)));
			} else {
				Attenuation = LightMask * Shadow.SurfaceShadow;
			}

			float3 LightingDiffuse = Diffuse_Lambert( GBuffer.DiffuseColor ) * Lighting;
			LightAccumulator_AddSplit(LightAccumulator, LightingDiffuse, 0.0f, 0, LightColor * Attenuation, bNeedsSeparateSubsurfaceLightAccumulation);
		#else
			FDirectLighting Lighting;

			if (LightData.bRectLight)
			{
				FRect Rect = GetRect( ToLight, LightData );

				#if REFERENCE_QUALITY
					Lighting = IntegrateBxDF( GBuffer, N, V, Rect, Shadow, SourceTexture, SVPos );
				#else
					BRANCH
					if (bUseToonShader) {
						Lighting = IntegrateBxDFToon( GBuffer, N, V, Rect, Shadow, SourceTexture);
					} else {
						Lighting = IntegrateBxDF( GBuffer, N, V, Rect, Shadow, SourceTexture);
					}
				#endif
			}
			else
			{
				FCapsuleLight Capsule = GetCapsule( ToLight, LightData );

				#if REFERENCE_QUALITY
					Lighting = IntegrateBxDF( GBuffer, N, V, Capsule, Shadow, SVPos );
				#else
					BRANCH
					if (bUseToonShader) {
						Lighting = IntegrateBxDFToon( GBuffer, N, V, Capsule, Shadow, LightData.bInverseSquared );
					} else {
						Lighting = IntegrateBxDF( GBuffer, N, V, Capsule, Shadow, LightData.bInverseSquared );
					}
				#endif
			}

			Lighting.Specular *= LightData.SpecularScale;

			float Attenuation;
			float TransmissionAttenuation;

			BRANCH
			if (bUseToonShader) {
				const float NoL = saturate( dot(N, L) );
				const float Intensity = Shadow.SurfaceShadow * NoL;
				const float TransmissionIntensity = Shadow.TransmissionShadow * NoL;
				Attenuation = max(1.2f * step(1.f, Intensity), max(ShadowIntensity, step(Threshold, Intensity)));
				TransmissionAttenuation = max(1.2f * step(1.f, TransmissionIntensity), max(ShadowIntensity, step(Threshold, TransmissionIntensity)));
			} else {
				Attenuation = LightMask * Shadow.SurfaceShadow;
				TransmissionAttenuation = LightMask * Shadow.TransmissionShadow;
			}

			LightAccumulator_AddSplit( LightAccumulator, Lighting.Diffuse, Lighting.Specular, Lighting.Diffuse, LightColor * Attenuation, bNeedsSeparateSubsurfaceLightAccumulation );
			LightAccumulator_AddSplit( LightAccumulator, Lighting.Transmission, 0.0f, Lighting.Transmission, LightColor * TransmissionAttenuation, bNeedsSeparateSubsurfaceLightAccumulation );

			LightAccumulator.EstimatedCost += 0.4f;		// add the cost of the lighting computations (should sum up to 1 form one light)
		#endif
		}
	}

	return LightAccumulator_GetResultSplit(LightAccumulator);
}

float4 GetDynamicLightingToon(
	float3 WorldPosition, float3 CameraVector, FGBufferData GBuffer, float AmbientOcclusion, uint ShadingModelID, 
	FDeferredLightData LightData, float4 LightAttenuation, float Dither, uint2 SVPos, FRectTexture SourceTexture,
	inout float SurfaceShadow)
{
	FDeferredLightingSplit SplitLighting = GetDynamicLightingSplitToon(
		WorldPosition, CameraVector, GBuffer, AmbientOcclusion, ShadingModelID, 
		LightData, LightAttenuation, Dither, SVPos, SourceTexture,
		SurfaceShadow);

	return SplitLighting.SpecularLighting + SplitLighting.DiffuseLighting;
}
