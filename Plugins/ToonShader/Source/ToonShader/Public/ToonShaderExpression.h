// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "MaterialExpressionIO.h"
#include "Materials/MaterialExpression.h"
#include "UObject/ObjectMacros.h"

#include "ToonShaderExpression.generated.h"

class FMaterialCompiler;

UCLASS()
class TOONSHADER_API UToonShaderExpression : public UMaterialExpression {
	GENERATED_UCLASS_BODY()

public:

	UPROPERTY()
	FExpressionInput Exec;

	UPROPERTY(meta = (RequiredInput = "false", ToolTip = "Defaults to 'DefaultThreshold' if not specified"))
	FExpressionInput Threshold;

	UPROPERTY(meta = (RequiredInput = "false", ToolTip = "Defaults to 'DefaultShadowIntensity' if not specified"))
	FExpressionInput ShadowIntensity;

	UPROPERTY(EditAnywhere, Category = MaterialExpressionAddLayer)
	float DefaultThreshold;

	UPROPERTY(EditAnywhere, Category = MaterialExpressionAddLayer)
	float DefaultShadowIntensity;

#if WITH_EDITOR
	// void PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent) override;
	// virtual bool CanEditChange(const FProperty* InProperty) const override;

	// virtual FText GetKeywords() const override;
	// virtual const TArray<FExpressionInput*> GetInputs() override;
	// virtual FExpressionInput* GetInput(int32 InputIndex) override;
	// virtual FName GetInputName(int32 InputIndex) const override;
	// virtual bool IsInputConnectionRequired(int32 InputIndex) const override;
	// virtual uint32 GetInputType(int32 InputIndex) override;
	// virtual uint32 GetOutputType(int32 OutputIndex) override;
	// virtual FText GetCreationDescription() const override;
	// virtual FText GetCreationName() const override;
	virtual void GetCaption(TArray<FString>& OutCaptions) const override;
	// virtual void GetConnectorToolTip(int32 InputIndex, int32 OutputIndex, TArray<FString>& OutToolTip) override;
	virtual int32 Compile(FMaterialCompiler* Compiler, int32 OutputIndex) override;
#endif // WITH_EDITOR

private:
	static int32 ExpressionIndex;
};
