// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Modules/ModuleManager.h"

DEFINE_LOG_CATEGORY_STATIC(LogToonShader, Log, All);

class FToonShaderModule : public IModuleInterface {
public: // IModuleInterface

	virtual void StartupModule() override;
	virtual void ShutdownModule() override;

public:

	static FString ShaderVersion;

};
