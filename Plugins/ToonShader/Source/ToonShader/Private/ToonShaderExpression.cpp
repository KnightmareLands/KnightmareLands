// Fill out your copyright notice in the Description page of Project Settings.

#include "ToonShaderExpression.h"

#include "EdGraph/EdGraphNode.h"
#include "MaterialCompiler.h"
#include "Materials/MaterialExpressionCustom.h"
#include "Runtime/Launch/Resources/Version.h"

#define LOCTEXT_NAMESPACE "MaterialExpression"

namespace {
void PrepareCode(FString& Code) {
	if (Code.Len() > 0) {
		Code.ReplaceInline(TEXT("\n"), TEXT("\r\n"), ESearchCase::CaseSensitive);
	}
}
} // namespace

UToonShaderExpression::UToonShaderExpression(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
	, DefaultThreshold(0.6f)
	, DefaultShadowIntensity(0.4f)
{

#if WITH_EDITORONLY_DATA
	static const FText Category_ToonShading(LOCTEXT("ToonShadingCategory", "ToonShading"));
	MenuCategories.Add(Category_ToonShading);

	bRealtimePreview = false;
	bNeedToUpdatePreview = false;
#endif
}

int32 UToonShaderExpression::ExpressionIndex = 0;

#if WITH_EDITOR

void UToonShaderExpression::GetCaption(TArray<FString>& OutCaptions) const {
	OutCaptions.Add(TEXT("Toon Shader"));
}

int32 UToonShaderExpression::Compile(FMaterialCompiler* Compiler, int32 OutputIndex) {
	ExpressionIndex += 1;

	const int32 ExecIndex = Exec.Compile(Compiler);

	TArray<int32> CompiledInputs;

	UMaterialExpressionCustom* CustomExpression = NewObject<UMaterialExpressionCustom>();
	CustomExpression->Inputs.Reset();
	CustomExpression->Code.Reset();

	const auto ThresholdIndex = Threshold.GetTracedInput().Expression ? Threshold.Compile(Compiler) : Compiler->Constant(0.6f);
	CustomExpression->Inputs.Add({TEXT("Threshold"), {}});
	CompiledInputs.Add(ThresholdIndex);
	
	const auto ShadowIntensityIndex = ShadowIntensity.GetTracedInput().Expression ? ShadowIntensity.Compile(Compiler) : Compiler->Constant(0.4f);
	CustomExpression->Inputs.Add({TEXT("ShadowIntensity"), {}});
	CompiledInputs.Add(ShadowIntensityIndex);

	CustomExpression->Code += FString::Printf(TEXT("\tToonShaderValues = float2(Threshold, ShadowIntensity);"));
	CustomExpression->Code += FString::Printf(TEXT("\treturn 1.0;"));
	CustomExpression->AdditionalDefines.Add({TEXT("TOON_SHADER"), TEXT("1\nstatic float2 ToonShaderValues;\n")});

#if ENGINE_MINOR_VERSION >= 26
	Compiler->CustomExpression(CustomExpression, OutputIndex, CompiledInputs);
#else
	Compiler->CustomExpression(CustomExpression, CompiledInputs);
#endif

	return ExecIndex;
}
#endif // WITH_EDITOR

#undef LOCTEXT_NAMESPACE
