// Fill out your copyright notice in the Description page of Project Settings.

using UnrealBuildTool;

public class ToonShader : ModuleRules {
	public ToonShader(ReadOnlyTargetRules Target) : base(Target) {
		PCHUsage = ModuleRules.PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicIncludePaths.AddRange(new string[] {
			// ... add public include paths required here ...
		});


		PrivateIncludePaths.AddRange(new string[] {
			// ... add other private include paths required here ...
		});


		PublicDependencyModuleNames.AddRange(new string[] {
			"Core",
			// ... add other public dependencies that you statically link with here ...
		});


		PrivateDependencyModuleNames.AddRange(new string[] {
			"CoreUObject",
			"Engine",
			"Projects",
			"RenderCore",
			"RHI",
			"Slate",
			"SlateCore",
		});


		DynamicallyLoadedModuleNames.AddRange(new string[] {
			// ... add any modules that your module loads dynamically here ...
		});
	}
}
